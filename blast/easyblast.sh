#!/usr/bin/env bash
#
# USAGE : easyblast.sh QUERY(FILE/STRING) DB(FILE) <blastoptions> <blasttype>
#

RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m' # No Color

version="0.1.23"; # last updated on 2021-10-01 14:11:18

export LC_ALL=C

if [ "$#" -lt 2 ]; then
    echo -e "${ORANGE}USAGE$NC : easyblast.sh (OPTIONS) QUERY(FILE/STRING) DB(FILE)"
    echo " QUERY(FILE/STRING)    :  The input query, e.g. /home/paul/pRNA.fa or just a string with a sequence like 'AATAATAATA'."
    echo " DB(FILE)              :  The database (path to file)."
    echo ""
    echo "(Note: 4 cores are used by default)"
    echo ""
    echo -e "${ORANGE}OPTIONS$NC:"
    echo " -b <STRING>           :  The blast type [blastn, blastp, tblastn, blastx]. If not set, then it will try to auto detect the right one."
    echo " -t <INTEGER/PERCENT%> :  This option sets the threshold for the number of returned hits per query (sorted by bitscore). If 1 then only the best match is returned (for each query). Given a percentile (with a % suffix), all hits within this percentile times the maximum are returned (bitscore). So -t 90% -> from maximal bitscore to 10% below. If set, the blastoption have to contain -outfmt 6 because of the bitscores (by default this is the case)."
    echo " -e <INTEGER>          :  The -evalue threshold (default:10) "
    echo " -j <INTEGER>          :  The number of threads used (default:4)"
    echo " -d                    :  If set, then for each query no duplicated hits are returned like A->B and A->B again with a different bitscore (hit with best bitscore is printed)."
    echo " -h                    :  If set, then the header for outfmt 6 is printed as well."
    echo " -s                    :  If set, then the following options are set -soft_masking false -dust no."
    echo " -g                    :  If set, the (sstart,send) positions are converted to strand +/- and +based-positions."
    echo " -x                    :  If set, a summarized output is produced." # -qcov_hsp_perc
    #echo " -p <INTEGER>          :  Corresponds to the qcov_hsp_perc, the percent query coverage per hsp (percent 0..100)." # this will only change the HSP coverage .. not the query coverage ...
    echo " -o <STRING>           :  Explicit blast+ options, by default '-outfmt 6 -num_threads 4' is used. NOTE: this overwrites all other options (like -j threads and -e evalue)."
    echo ""
    echo -e "${ORANGE}EXAMPLES$NC:"
    echo " bash easyblast.sh 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa"
    echo " bash easyblast.sh /some/file.fasta /home/Ecoli.faa -> "
    echo " bash easyblast.sh -t 1 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa -> This gives only the best match"
    echo " bash easyblast.sh -t 90% 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa -> Up to 10% from maximum is returned"
    echo " bash easyblast.sh -t 90% -d 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa -> same as ^ and duplicated matches are excluded."
    echo "  bash easyblast.sh -j 16 'AAGTATAGATAGATCCACACACAGGCACGAGT' /home/Ecoli.faa -> automatic detecting blastx and using 16 cores"
    echo "  bash easyblast.sh -o '-outfmt 6 -num_threads 16' -b blastx 'AAGTATAGATAGATCCACACACAGGCACGAGT' /home/Ecoli.faa -> forcing blastx algorithm"
    echo "(The result is written to stdout)"
    echo ""
    echo "Output is sorted by qseqid (column 1) first and then by evalue (column 12)"
    echo ""
    echo "Here the some useful BLAST commands: "
    echo "makeblastdb -dbtype nucl -in DBFILE"
    echo "makeblastdb -dbtype prot -in DBFILE -out OUTFILE"
    echo "blastp -query QUERYFILE -db DBFILE -outfmt 6 -num_threads 4"
    echo "blastp -query QUERYFILE -db DBFILE -outfmt '6 qseqid sseqid bitscore' -num_threads 4 | sort -k1,1 -k3,3nr | awk '{if(last != \$1){last=\$1;print \$0;}}'"
    echo ""
    echo "Here the some useful options: "
    echo "-evalue 1e-4 : sets the evalue cut-off"
    echo "-soft_masking false -dust no : disables soft masking (low complexity filtering)"
    echo "-word_size 11 : should be 3 for FAA vs FAA and 11-28 for FNA vs FNA"
    echo "-task blastn : for blastn, this sets -word_size 11"
    echo "-strand {both,plus,minus} : by default blast searches both strands (so also the rc)"
    echo "extract matches : easyblast.sh query db.fna | awk '{print \$2}' | grepfasta.pl - db.fna"
    echo ""
    echo -e "${ORANGE}VERSION$NC $version"
    echo -e "${ORANGE}AUTHOR$NC Paul Klemm"
    echo -e "${ORANGE}DEPENDANCIES$NC ncbi blast+, awk, grep, xargs, ls, perl"
    exit
fi

# remove all temporary files that are older than 10 days
# ls .tmp_eb* 2>/dev/null | grep '.tmp_eb.*' | xargs -I{} stat {} --printf="%n\t%Y\n" | awk 'BEGIN{t=systime()-864000}$2<t{print $1}' 2>/dev/null | grep '.tmp_eb.*' | xargs -I{} rm -r {} 2>/dev/null

ls .tmp_eb* 2>/dev/null | xargs -I{} >&2 echo "WARNING temporary file '"{}"' still present"

top=-1
blasttype="unknown_do_auto_detection"
blastoption="" #-outfmt 6 -num_threads 4
noduplicatedhits=-1
printheader=-1
evalue=10
threads=4
softmasking=-1
summarized=-1
genomepos=-1
qcov_hsp_perc=""
while getopts ":dhvxsgt:b:j:e:o:" o; do
    case "${o}" in
        t)
            top=$(echo $OPTARG | sed "s/^[-=]//")
            ;;
        e)
            evalue=$(echo $OPTARG | sed "s/^[-=]//")
            ;;
        b)
            blasttype=$(echo $OPTARG | sed "s/^[-=]//" | sed "s/^['\"]//" | sed "s/['\"]$//")
            ;;
        o)
            blastoption=$(echo $OPTARG | sed "s/^[=]//")
            ;;
        #p)
        #    qcov_hsp_perc="-qcov_hsp_perc "$(echo $OPTARG | sed "s/^[=]//")
        #    ;;
        j)
            threads=$(echo $OPTARG | sed "s/^[=]//")
            ;;
        d)
            noduplicatedhits=1
            ;;
        x)
            summarized=1
            ;;
        h)
            printheader=1
            ;;
        v)
            echo $version;
            exit 0;
            ;;
        s)
            softmasking=1
            ;;
        g)
            genomepos=1
            ;;
	    \? )
			echo "Invalid Option: -$OPTARG" 1>&2
			exit 1
			;;
		: )
			echo "Invalid Option: -$OPTARG requires an argument" 1>&2
			exit 1
			;;
    esac
done
shift $((OPTIND-1))

if [[ "$blastoption" == "" ]];then
	blastoption="-outfmt 6 -num_threads $threads -evalue $evalue"
	if [[ "$softmasking" == "1" ]];then
		blastoption="$blastoption -soft_masking false -dust no"
	fi
fi

if [ "$printheader" != "-1" ] && [[ "$blastoption" != *"-outfmt 6"* ]];then
	>&2 echo "ERROR : if -h (print header) is defined, i need -outfmt 6..."
    exit
fi
if [ "$top" != "-1" ] && [[ "$blastoption" != *"-outfmt 6"* ]];then
	>&2 echo "ERROR : if -t is defined, i need outfmt 6 for bitscores ..."
    exit
fi
if [ "$noduplicatedhits" != "-1" ] && [[ "$blastoption" != *"-outfmt 6"* ]];then
	>&2 echo "ERROR : if -d is defined, i need outfmt 6 for sseqid ..."
    exit
fi
if [ "$genomepos" != "-1" ] && [[ "$blastoption" != *"-outfmt 6"* ]];then
	>&2 echo "ERROR : if -g is defined, i need outfmt 6..."
    exit
fi

if [[ "$qcov_hsp_perc" != "" ]];then
	blastoption="$blastoption $qcov_hsp_perc";
fi

dbfile=""
t=$(date +%s)

if [ ! -e $2 ]; then echo >&2 ""; echo >&2 "I cannot find $2. Aborting.";echo >&2 "";  exit 1; fi

if [ "$blasttype" == "unknown_do_auto_detection" ]; then
	>&2 echo -n -e "[Step 0]$ORANGE Detecting blasttype$NC : "
	if [ ! -e $1 ] || [ ! -s $1 ]; then
		echo $1 >.tmp_eb$t.tmp;
		isQueryFNA=$(head -n100 .tmp_eb$t.tmp | perl -lne '$_=~s/[^a-z]//gi;$sum+=length($_);$countATGCU += () = $_ =~ /a|g|t|u|c|n/gi;if($sum>0){($countATGCU/$sum)>0.8 ? print 1 : print 0}' | tail -n1);
	else
		isQueryFNA=$(head -n100 $1 | perl -lne 'if(substr($_,0,1)ne">"){$_=~s/[^a-z]//gi;$sum+=length($_);$countATGCU += () = $_ =~ /a|g|t|u|c|n/gi;}if($sum>0){($countATGCU/$sum)>0.8 ? print 1 : print 0}' | tail -n1);
	fi
	isDbFNA=$(head -n100 $2 | perl -lne 'if(substr($_,0,1)ne">"){$_=~s/[^a-z]//gi;$sum+=length($_);$countATGCU += () = $_ =~ /a|g|t|u|c|n/gi;}if($sum>0){($countATGCU/$sum)>0.8 ? print 1 : print 0}' | tail -n1);
	if [ "$isQueryFNA" == "1" ] && [ "$isDbFNA" == "1" ]; then
		blasttype="blastn"
		>&2 echo -e "pretty sure it is$GREEN blastn$NC"
	elif [ "$isQueryFNA" == "0" ] && [ "$isDbFNA" == "0" ]; then
		blasttype="blastp"
		>&2 echo -e "pretty sure it is$GREEN blastp$NC"
	elif [ "$isQueryFNA" == "1" ] && [ "$isDbFNA" == "0" ]; then
		blasttype="blastx"
		>&2 echo -e "pretty sure it is$GREEN blastx$NC"
	elif [ "$isQueryFNA" == "0" ] && [ "$isDbFNA" == "1" ]; then
		blasttype="tblastn"
		>&2 echo -e "pretty sure it is$GREEN tblastn$NC"
	fi
fi
# fallback :
if [ "$blasttype" == "unknown_do_auto_detection" ]; then
	case "$2" in
	    *.faa) blasttype="blastp"; >&2 echo "[WARNING] I have no clue what blasttype this is, guessing blastp" ;;
	    *.fna) blasttype="blastn"; >&2 echo "[WARNING] I have no clue what blasttype this is, guessing blastn";;
		*) blasttype="blastp"; >&2 echo "[WARNING] I have no clue what blasttype this is, just guessing blastp" ;;
	esac
fi

>&2 echo -n -e "[Step 1/2]$ORANGE DATABASE$NC : "

forceNewDB=0

if [ -e "$2.*in" ]; then
	lastModificationSeconds_index=$(date +%s -r $2.*in);
	lastModificationSeconds_db=$(date +%s -r $2);
	if [ "$lastModificationSeconds_index" -lt "$lastModificationSeconds_db" ]; then 
		>&2 echo -n -e "!! ${ORANGE}WARNING$NC !! The blast index file is older than the database file, this seems odd -> therfore re";
		forceNewDB=1
	fi
fi

if ( [ "$forceNewDB" == "1" ] ||  [ ! -e $2.nsq ] ) && ( [ "$blasttype" == "blastn" ] || [ "$blasttype" == "tblastn" ] ); then
	>&2 echo -e "building with\n$ makeblastdb -dbtype nucl -in $2"
	makeblastdb -dbtype nucl -in $2 >/dev/null 2>&1;
	dbfile=$2
	if [ ! -e $2.nsq ]; then
		# no writing permission -> tmp
		makeblastdb -dbtype nucl -in $2 -out .tmp_eb$t.db >/dev/null 2>&1;
		dbfile=.tmp_eb$t.db
	fi
elif ( [ "$forceNewDB" == "1" ] ||  [ ! -s $2.psq ] ) && ( [ "$blasttype" == "blastp" ] || [ "$blasttype" == "blastx" ] ); then
	>&2 echo -e "building with\n$ makeblastdb -dbtype prot -in $2"
	makeblastdb -dbtype prot -in $2 >/dev/null 2>&1;
	dbfile=$2
	if [ ! -e $2.psq ]; then
		# no writing permission -> tmp
		makeblastdb -dbtype prot -in $2 -out .tmp_eb$t.db >/dev/null 2>&1;
		dbfile=.tmp_eb$t.db
	fi
elif [ -e $2.nsq ] || [ -e $2.psq ]; then
	>&2 echo "detected"

	dbfile=$2

	if [ "$blasttype" == "unknown_do_auto_detection" ]; then
		if [ -e $2.nsq ]; then 
			blasttype="blastn"; >&2 echo "detected nucleotide blasttype (blastn)"
		else
		    blasttype="blastp"; >&2 echo "detected protein blasttype (blastp)"
		fi
	fi
elif [ "$blasttype" == "unknown_do_auto_detection" ]; then
	echo -e "$RED[ERROR]$NC Could not determine the blasttype or the given ($blasttype) is invalid, please specify the blasttype [blastn, blastp, tblastn, blastx]"
	exit
fi

if [ "$blasttype" == "unknown_do_auto_detection" ]; then
	>&2 echo "[ERROR] Invalid blasttype $blasttype, please provide the blasttpye in the 3. argument [blastn, blastp, tblastn, blastx]"
	exit
fi


>&2 echo -n -e "[Step 2/2]$ORANGE SEARCHING$NC : "

if [ "$blasttype" == "zip" ]; then

	export fileA=$1
	export fileB=$2

	perl -e '$gn="-";%gziplens_a; %seqs_a;
	open($fA,$ENV{"fileA"});

	while(<$fA>){
		chomp;
		if(substr($_,0,1)eq ">"){
			if($gn ne "-"){
			$gziplens_a{$gn}=`echo $seqs_a{$gn} | gzip | wc -m`;};
			$gn=substr($_,1);$seqs_a{$gn}="";
		}else{$seqs_a{$gn}.=$_;}
		$gziplens_a{$gn}=`echo $seqs_a{$gn} | gzip | wc -m`;
	}close($fA);
	open($fB,$ENV{"fileB"});
	$seq_b;$gziplen_b;
	while(<$fB>){
		chomp;
		if(substr($_,0,1)eq ">"){
			if($gn ne "-"){
				$gziplen_b=`echo $seq_b | gzip | wc -m`;
				foreach $gn_a (sort keys %seqs_a){
					$seq_a=$seqs_a{$gn_a};
					$gziplen_a=$gziplens_a{$gn_a};
					$gziplen_ab=`echo ${seq_a}${seq_b} | gzip | wc -m`;
					$score=($gziplen_ab/($gziplen_a*$gziplen_b));
					print $gn_a."\t".$gn."\t".($score)."\t\t\t\t\t\t\t\t".(length($seq_a)*(scalar keys %seqs_a) * (2**(-$score*10000)))."\t".($score*10000)."\n";
				}
			};
			$gn=substr($_,1);$seq_b="";
		}else{$seq_b.=$_;}
		$gziplen_b=`echo $seq_b | gzip | wc -m`;
		foreach $gn_a (sort keys %seqs_a){
			$seq_a=$seqs_a{$gn_a};
			$gziplen_a=$gziplens_a{$gn_a};
			$gziplen_ab=`echo ${seq_a}${seq_b} | gzip | wc -m`;
			$score=($gziplen_ab/($gziplen_a*$gziplen_b));
			print $gn_a."\t".$gn."\t".($score)."\t\t\t\t\t\t\t\t".(length($seq_a)*(scalar keys %seqs_a) * (2**(-$score*10000)))."\t".($score*10000)."\n";
		}
	}close($fB);
	' #>.tmp_eb$t.stdout 2>.tmp_eb$t.stderr

else # blast+
	if [ -e $1 ] && [ -s $1 ]; then
		>&2 echo -e "searching with\n$ $blasttype -query $1 -db $dbfile $blastoption 2>.tmp_eb$t.stderr"
		$blasttype -query $1 -db $dbfile $blastoption >.tmp_eb$t.stdout 2>.tmp_eb$t.stderr
	else
		>&2 echo ">query">.tmp_eb$t
		>&2 echo $1>>.tmp_eb$t
		>&2 echo -e "searching with temporary file .tmp_eb$t : \n$ $blasttype -query .tmp_eb$t -db $dbfile $blastoption 2>.tmp_eb$t.stderr"
		$blasttype -query .tmp_eb$t -db $dbfile $blastoption >.tmp_eb$t.stdout 2>.tmp_eb$t.stderr;
	fi
fi

if [ "$genomepos" == "1" ]; then
	>&2 echo -e "work $genompos in progress"
	exit;
fi

if [ -s .tmp_eb$t.stderr ]; then
	>&2 echo -e "----------[$RED ERRORS $NC]----------"
	>&2 cat .tmp_eb$t.stderr 
	>&2 echo "-------------------------------"
fi

>&2 echo -e "----------[$GREEN RESULT$NC (STDOUT)]----------"

if [ "$summarized" != "-1" ]; then

	awk '{print $1}' .tmp_eb$t.stdout | sort | uniq -c;

else

	if [ "$printheader" != "-1" ]; then
		echo -e "# qseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore"
	fi

	if [ "$noduplicatedhits" != "-1" ]; then
		>&2 echo -e "[STDERR] ONLY non-duplicated hits are shown (for each query)."
	fi
	if [ "$top" != "-1" ]; then
		>&2 echo -e "[STDERR] ONLY the top $top results are returned."
		
		if [[ "$top" == *"%" ]]; then
			top=${top%\%}
			sort -k1,1 -k12,12nr .tmp_eb$t.stdout | awk '{if('$noduplicatedhits'==1){if(sseqid[$2]==$1){next;}sseqid[$2]=$1;}if(last != $1){last=$1;lastbit=$12;print $0;}else if($12>lastbit * (1 - '$top'/100) ){print $0}}'
		else
			sort -k1,1 -k12,12nr .tmp_eb$t.stdout | awk '{if('$noduplicatedhits'==1){if(sseqid[$2]==$1){next;}sseqid[$2]=$1;}if(last != $1){i=1;last=$1;print $0;}else if(i<'$top'){i++;print $0}}'
		fi
	else
		if [ "$noduplicatedhits" != "-1" ]; then
			sort -k1,1 -k12,12nr .tmp_eb$t.stdout | awk '{if(sseqid[$2]==$1){next;}sseqid[$2]=$1;print $0;}'
		else
			cat .tmp_eb$t.stdout 
		fi
	fi
fi
>&2 echo "--------------------------------------"

>&2 echo "[CLEANUP] Deleting temporary files"
rm .tmp_eb$t* 2>/dev/null;

>&2 echo "[extract sequences] easyblast.sh query db.fna | awk '{print \$2}' | grepfasta.pl - db.fna"
