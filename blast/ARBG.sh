#!/usr/bin/env bash
#
# USAGE : ARBG.sh FASTA_A FASTA_B 
#

RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m' # No Color

export LC_ALL=C

if [ "$#" -lt 2 ]; then
    echo -e "${ORANGE}USAGE$NC : ARBG.sh (OPTIONS) FASTA_A FASTA_B"
    echo " FASTA_A, FASTA_B   :  Two fasta files"
    echo ""
    echo -e "This interface calls easyblast.sh (using 4 cores) 2 times and compares the blast outputs to create the ${GREEN}a${NC}daptive ${GREEN}r${NC}eciprocal ${GREEN}b${NC}last ${GREEN}g${NC}raph."
    echo ""
    echo -e "${ORANGE}OPTIONS$NC:"
    echo " -s DOUBLE (0 to 1)  :  The minimal similarity for adaptive best matches [default:0.95]. -s 1 results in the reciprocal best(!) blast hit graph. [-s 0 disables this feature]"
    echo " -c DOUBLE (0 to 1)  :  The minimal alignment coverage [default:0.5]. NOTE: this option does not make sense if you compare fna with faa files (fnas are way longer anyways). [-c 0 disables this feature]"
    echo " -e DOUBLE (0 to 1)  :  evalue cutoff [default:1e-5], [-e 0 disables this feature]"
    echo " -t INT  :  number of threads [default:4]."
    echo ""
    echo " (all temporary files are written to /tmp)"
    echo ""
    echo -e "${ORANGE}AUTHOR$NC Paul Klemm"
    echo -e "${ORANGE}DEPENDANCIES$NC easyblast.sh (https://gitlab.com/paulklemm_PHD/scripts), awk, grep, xargs, ls, perl"
    exit
fi

# remove all temporary files that are older than 10 days
ls -d /tmp/* | grep '/ARBG_tmpfile_.*tmp' | xargs -I{} stat {} --printf="%n\t%Y\n" | awk 'BEGIN{t=systime()-864000}$2<t{print $1}' 2>/dev/null | grep '/ARBG.*tmp' | xargs -I{} rm -r {} 2>/dev/null

sim=0.95
cov=0.5
threads=4
evalue=1e-5
while getopts "s:c:t:e:" o; do
    case "${o}" in
        s)
            sim=$(echo $OPTARG | sed "s/^[-=]//")
            ;;
        c)
            cov=$(echo $OPTARG | sed "s/^[-=]//")
            ;;
 #       b)
 #           blastopt=$(echo $OPTARG | sed "s/^[-=]//")
 #           ;;
        t)
            threads=$(echo $OPTARG | sed "s/^[-=]//")
            ;;
        e)
            evalue=$(echo $OPTARG | sed "s/^[-=]//")
            ;;
    esac
done
shift $((OPTIND-1))

t=$(date +%s)

if [ ! -e $1 ]; then echo >&2 ""; echo >&2 "I cannot find $1. Aborting.";echo >&2 "";  exit 1; fi
if [ ! -e $2 ]; then echo >&2 ""; echo >&2 "I cannot find $2. Aborting.";echo >&2 "";  exit 1; fi

which easyblast.sh >/dev/null 2>&1 || { echo >&2 "I require easyblast.sh but it's not installed (or not present in the PATH variable). Aborting."; exit 1; }

export ARBG_fileA=/tmp/ARBG_tmpfile_$(basename $1).vs.$(basename $2).bla.tmp
export ARBG_fileB=/tmp/ARBG_tmpfile_$(basename $2).vs.$(basename $1).bla.tmp

>&2 echo -e "[STDERR] ${ORANGE}easyblasting$NC -d -o '-outfmt 6 -evalue $evalue -num_threads $threads' $1 $2 ..."

easyblast.sh -d -o '-outfmt 6 -evalue '$evalue' -num_threads '$threads $1 $2 >$ARBG_fileA

>&2 echo -e "[STDERR] ${ORANGE}easyblasting$NC -d -o '-outfmt 6 -evalue $evalue -num_threads $threads' $2 $1 ..."

easyblast.sh -d -o '-outfmt 6 -evalue '$evalue' -num_threads '$threads $2 $1 >$ARBG_fileB

export ARBG_fileA_name=$1
export ARBG_fileB_name=$2
export ARBG_sim=$sim
export ARBG_cov=$cov

export ARBG_fileA_len=/tmp/ARBG_tmpfile_$(basename $1).length.tmp
export ARBG_fileB_len=/tmp/ARBG_tmpfile_$(basename $2).length.tmp

getGeneLength.pl $1 >$ARBG_fileA_len
getGeneLength.pl $2 >$ARBG_fileB_len

perl -e '

my %lengths_A;
my %lengths_B;
open(my $fA,$ENV{"ARBG_fileA_len"});
while(<$fA>){
	chomp;$_=~s/^>//g;
	my @arr=split("\t",$_);
	if(scalar @arr != 2){next;}
	$lengths_A{$arr[0]}=$arr[1];
}
close($fA);
open(my $fA,$ENV{"ARBG_fileB_len"});
while(<$fA>){
	chomp;$_=~s/^>//g;
	my @arr=split("\t",$_);
	if(scalar @arr != 2){next;}
	$lengths_B{$arr[0]}=$arr[1];
}
close($fA);

sub identitybylength {
    # Accoding to the formula of Rost, 1999 (Twilight-zone)
	if($_[0] <= 11)		{return 100;}
	if($_[0] <= 450)	{return 480*$_[0]**(-0.32*(1+exp(-$_[0]/1000)));}
	return 19.5;
}

print "# file_a\tfile_b\n";
print "# a\tb\tevalue_ab\tbitscore_ab\tevalue_ba\tbitscore_ba\n";
print "# ".$ENV{"ARBG_fileA_name"}."\t".$ENV{"ARBG_fileB_name"}."\n";

my %match_ab;
my %match_ba;
my %max_firstarg_ab;
my %max_firstarg_ba;
my $coverage=$ENV{"ARBG_cov"};

open(my $fA,$ENV{"ARBG_fileA"});
while(<$fA>){
	chomp;if(length($_)<2 && substr($_,0,1) eq "#"){next;}
	my @arr=split("\t",$_);$a=$arr[0];$b=$arr[1];

	$local_identity=$arr[2];
	$alignment_length=$arr[3];
	$missmatches=$arr[4];
	if ( $local_identity < &identitybylength($alignment_length) )	{next;} 
	if ( $alignment_length < ($lengths_A{$arr[0]})*($coverage)+0.5 ) 		{next;}
	if ( $alignment_length < ($lengths_B{$arr[1]})*($coverage)+0.5 ) 		{next;}

	$match_ab{$a}{$b}=[$arr[10],$arr[11]];
	
}close($fA);


open(my $fB,$ENV{"ARBG_fileB"});
while(<$fB>){
	chomp;if(length($_)<2 && substr($_,0,1) eq "#"){next;}
	my @arr=split("\t",$_);$a=$arr[0];$b=$arr[1];

	if(!exists($match_ab{$b}{$a})){next;} # only continue if a-b AND b-a exists
	
	$local_identity=$arr[2];
	$alignment_length=$arr[3];
	if ( $local_identity < &identitybylength($alignment_length) )	{next;} 
	if ( $alignment_length < ($lengths_B{$arr[0]})*($coverage)+0.5 ) 		{next;}
	if ( $alignment_length < ($lengths_A{$arr[1]})*($coverage)+0.5 ) 		{next;}

	$match_ba{$a}{$b}=[$arr[10],$arr[11]];

	if($max_firstarg_ba{$a}<$arr[11]){$max_a{$a}=$arr[11];}
}close($fB);

foreach $a (keys %match_ab){
	foreach $b (keys %{$match_ab{$a}}){
		if(!exists($match_ba{$b}{$a}) ){	
			delete $match_ab{$a}{$b};
		}
	}
}

# now ab and ba are only reciprocal hits


foreach $a (keys %match_ab){
	foreach $b (keys %{$match_ab{$a}}){
		if($max_firstarg_ab{$a}<$match_ab{$a}{$b}->[1]){$max_firstarg_ab{$a}=$match_ab{$a}{$b}->[1];}
		if($max_firstarg_ba{$b}<$match_ba{$b}{$a}->[1]){$max_firstarg_ba{$b}=$match_ba{$b}{$a}->[1];}
	}
}

foreach $a (sort keys %match_ab){
	foreach $b (sort keys %{$match_ab{$a}}){
		if( $match_ab{$a}{$b}->[1] >= $ENV{"ARBG_sim"}*$max_firstarg_ab{$a} && 
			$match_ba{$b}{$a}->[1] >= $ENV{"ARBG_sim"}*$max_firstarg_ba{$b} ){
			print $a."\t".$b."\t".$match_ab{$a}{$b}->[0]."\t".$match_ab{$a}{$b}->[1]."\t".$match_ba{$b}{$a}->[0]."\t".$match_ba{$b}{$a}->[1]."\n";
		}
	}
}'

rm $ARBG_fileA
rm $ARBG_fileB
rm $ARBG_fileA_len
rm $ARBG_fileB_len
