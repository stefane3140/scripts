#!/usr/bin/env bash
#pk

version="0.8.10"; # last updated on 2021-10-01 14:11:18

export LC_ALL=C

RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
STRIKE='\e[9m'
UNDER='\e[4m'
NC='\033[0m' # No Color

arg=$(echo "$1") # chomps newline
if [[ "$#" < 1 ]] || [[ "$arg" = "-h" ]] || [[ "$arg" = "-help" ]] || [[ "$arg" = "--help" ]]; then
    echo -e "${ORANGE}USAGE$NC: alignNrdbReadsPlot.sh (options) <SAMFILE>"
    echo "Simple pipeline that converts a sam file to an aligned html."
    echo "redundant reads are pooled together, the name encodes the number of reads (x_NUMBEROFREADS)."
    echo "Output files are: SAMFILE.fna, SAMFILE.fna.nrdb, SAMFILE.fna.nrdb.aln, SAMFILE.fna.nrdb.aln.html"
    echo ""
    echo -e "${ORANGE}OPTIONS$NC:"
    echo " -r <STRING> : Reference sequence that will be displayed at the top of the output"
    echo " -g start|end|length|rname : group reads by the start or end position or the RNAME column"
    echo " -a cigar|qual|muscle|clustalo : align by either one of these option (cigar: insertions are omitted, qual: Phred+33), default: muscle"
    echo " -n : exclude groups of size 1"
    echo " -x : only print largest representant for each group"
    echo " -k : if set intermediate files are kept as well"
    echo ""
    echo " common options are -a cigar -g rname"
    echo ""
    echo "VERSION v"$version
    echo "AUTHOR Paul Klemm"
    echo "DEPENDENCIES muscle, nrdb.pl, MSAplot, aha"
    exit 1
fi

group=""
alignmentmodus="muscle"
nosingles=0
keep=0
onlyLargestRepresentant=0
ref=""
while getopts ":r:g:a:nxk" o; do
    case "${o}" in
        r)
            ref=$(echo $OPTARG | sed "s/^[=]//")
            ;;
        g)
            group=$(echo $OPTARG | sed "s/^[=]//")
            ;;
        c)
            alignmentmodus="cigar"
            ;;   
        a)
            alignmentmodus=$(echo $OPTARG | sed "s/^[=]//")
            ;;
        n)
            nosingles=1
            ;;
        x)
            onlyLargestRepresentant=1
            ;;
        k)
            keep=1
            ;;
        \? )
            echo "Invalid Option: -$OPTARG" 1>&2
            exit 1
            ;;
        : )
            echo "Invalid Option: -$OPTARG requires an argument" 1>&2
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

group_label="";
if [ "$group" != "" ]; then
    group_label="grouped by $group ";
fi;
if [ "$alignmentmodus" != "" ]; then
    group_label="$group_label colored by $alignmentmodus ";
fi;

group_label_filename="";
if [ "$group" != "" ]; then
    group_label_filename=".$group";
fi;
if [ "$alignmentmodus" != "" ]; then
    group_label_filename="$group_label_filename.$alignmentmodus";
fi;

echo -e "$ORANGE[alignNrdbReadsPlot]$NC 1: converting sam to fna";

# sam to fna
perl -lne 'my @a=split("\t",$_);
my $opt_group="'$group'";
my $opt_alignmentmodus="'$alignmentmodus'";
my $seq = "";

if(/^@/ || scalar @a < 9){next}

my $return_id=$i++;

my $end = $a[3];
my $start = $a[3];
my $CIGAR = $a[5]; 
my $QUAL = $a[10]; 
my $len_read = length $a[9]; 

#if($excludePerfectScore && $CIGAR=~/^[0-9]+[M=]$/){next}

if($a[1] & hex("0x10")){ ## read is in reverse

    # rc sequence

    $a[9]=~s/[Uu]/T/g;
    $a[9]=~tr/ACGTacgt/TGCAtgca/;
    $a[9]=reverse $a[9];
    $a[5]="";
    
    # correct the start position

    my $numOfInsertions = 0; 
    my $numOfDeletions = 0; 
    while ($a[5] =~ /([0-9]+)D/ig) { $numOfDeletions+=$1; } 
    while ($a[5] =~ /([0-9]+)[ISH]/ig) { $numOfInsertions+=$1; }
    $start = $start -1 + $lengthOfRead - $numOfInsertions + $numOfDeletions;
    
    # reorder the cigar string -> for cigar option

    $a[5]="";
    while ($CIGAR =~ /([0-9]+)([^0-9])/ig) { $a[5] = "$1$2".$a[5] }
    $CIGAR = $a[5]; 
    reverse $QUAL;

}else{
    ## read is normal

    $lengthOfRead = length($a[9]); 

    # correct the end position

    my $numOfInsertions = 0; 
    my $numOfDeletions = 0; 
    while ($a[5] =~ /([0-9]+)D/ig) { $numOfDeletions+=$1; } 
    while ($a[5] =~ /([0-9]+)[ISH]/ig) { $numOfInsertions+=$1; }
    $end = $end -1 + $lengthOfRead - $numOfInsertions + $numOfDeletions;
}

## if cigar option is set, output the cigar string enriched sequence 

if($opt_alignmentmodus eq "cigar"){
    $seq = ""; my $pos = 0;
    while ($CIGAR =~ /([0-9]+)([^0-9])/ig) {
        $what=$2;$l=$1;
        if($what =~ /[M=]/){$seq.="'$GREEN'".substr($a[9],$pos,$l)."'$NC'";$pos+=$l;} 
        elsif($what =~ /[D]/){$seq.="".("-" x $l)} 
        elsif($what =~ /[ISH]/){$seq.="'$UNDER'".substr($a[9],$pos,$l)."'$NC'";$pos+=$l;} 
        else{$seq.="'$RED'".substr($a[9],$pos,$l)."'$NC'";$pos+=$l;} 
    }
}elsif($opt_alignmentmodus eq "qual"){
    $seq = ""; my $pos = 0;
    while ($QUAL =~ /(.)/ig) {
        $what=$1;
        if(ord($what)-33 > 28){$seq.="'$GREEN'".substr($a[9],$pos,1)."'$NC'";$pos++;} 
        elsif(ord($what)-33 > 20){$seq.="'$ORANGE'".substr($a[9],$pos,1)."'$NC'";$pos++;}
        else{$seq.="'$RED'".substr($a[9],$pos,1)."'$NC'";$pos++;}
    }
}else{
    $seq=$a[9];
}

if($opt_group eq "rname"){ $return_id = $a[2]; $return_id=~s/[ #]/_/g; }
elsif($opt_group eq "start"){ $return_id = $start }
elsif($opt_group eq "end"){ $return_id = $end }
elsif($opt_group eq "length"){ $return_id = $len_read }

print ">$return_id\n$seq";' $1 > "$1${group_label_filename}".fna

# !!! if minus strand (0x10 is set) then the sequence need to be rc !!!

# build non redundant version
# and convert the #equals#.. format of nrdb to just a count version -> NAME_x_NUMBEROFHITS
# and add an id to the lines

echo -e "$ORANGE[alignNrdbReadsPlot]$NC 2: build a non-redundant database of the fna";

if [ "$group" == "" ]; then
    nrdb.pl "$1${group_label_filename}".fna |
        perl -lne 'if(/^>([^#]+)_#equals/){$a=$1;$cnt=()=$_=~/equals/g;$cnt++;print ">x $cnt"}elsif(/^>/){print ">x 1"}else{print $_}' | 
        perl -lne 'if(/^>/){$_=~s/>//;$id++;print ">id${id}_$_"}else{print $_}' >"$1${group_label_filename}".fna.nrdb
else
    echo -e "$ORANGE[alignNrdbReadsPlot]$NC  : integrate groups";

    nrdb.pl "$1${group_label_filename}".fna | fasta2vector.pl - |
        perl -lne 'my @b=split("\t",$_); if(scalar @b != 2){next} $b[0]=~s/^>//g; my %cnt; $cnt{$_}++ for split("_#equals#_",$b[0]); print ">${_} x ".$cnt{$_}."\n".$b[1] for keys %cnt' | 
        perl -lne 'if(/^>/){$_=~s/>//;$id++;print ">id${id} $_"}else{print $_}' >"$1${group_label_filename}".fna.nrdb;

fi

if [ "$nosingles" == "1" ]; then
    fasta2vector.pl "$1${group_label_filename}".fna.nrdb | grep  -P -v " x 1\t" | awk '{print ">"$0}' | tr '\t' '\n' > "$1${group_label_filename}".fna.nrdb.tmp; mv "$1${group_label_filename}".fna.nrdb.tmp "$1${group_label_filename}".fna.nrdb; 
fi

if [ "$ref" != "" ]; then
    echo -e "$ORANGE[alignNrdbReadsPlot]$NC  : append the reference sequence";
    printf ">-1 x 0 REF\n$ref\n" >> "$1${group_label_filename}".fna.nrdb;
fi

# align 
if [ "$alignmentmodus" == "muscle" ];then
    echo -e "$ORANGE[alignNrdbReadsPlot]$NC 3: build alignment of sequences";
    muscle -in "$1${group_label_filename}".fna.nrdb -out "$1${group_label_filename}".fna.nrdb.aln
elif [ "$alignmentmodus" == "clustalo" ];then
    echo -e "$ORANGE[alignNrdbReadsPlot]$NC 3: build alignment of sequences";
    clustalo -in "$1${group_label_filename}".fna.nrdb -out "$1${group_label_filename}".fna.nrdb.aln
else
    echo -e "$ORANGE[alignNrdbReadsPlot]$NC 3: alignment is skipped and nucleotides are colored by $alignmentmodus";
    cp "$1${group_label_filename}".fna.nrdb "$1${group_label_filename}".fna.nrdb.aln;
fi

# sort by occurences (Schwartzian transformation)
argMSAplot=""
if [ "$alignmentmodus" != "" ];then
    argMSAplot="1"
fi
alignNrdbReadsPlotName="$1"
alignNrdbReadsPlotGroup="$group"

echo -e "$ORANGE[alignNrdbReadsPlot]$NC 4: sort by group,id,occurences + send to MSAplot + convert to HTML (aha)";

fasta2vector.pl "$1${group_label_filename}".fna.nrdb.aln | 
    perl -lne 'if(/ ([^ ]+) x *([0-9]+)/){print "$1 $2 $_"}elsif(/ x *([0-9]+)/){print "1 $1 $_"}else{print "1 0 $_"}' | 
    sort -k1,1 -k2,2nr | 
    perl -lne 'my $olr='$onlyLargestRepresentant'; @a=split("\t",$_);@b=split(" ",$a[0]);shift @b;shift @b; if($olr && $last eq $b[1]){next} $last=$b[1]; print ">".(join("#",@b))."\n".$a[1]' | 
    tee "$1${group_label_filename}".fna.nrdb.aln.md | 
    alignNrdbReadsPlot="$argMSAplot" alignNrdbReadsPlotName="$alignNrdbReadsPlotName" alignNrdbReadsPlotGroup="$alignNrdbReadsPlotGroup" MSAplot.pl - | 
    tee "$1${group_label_filename}".fna.nrdb.aln.msaplot | 
    aha | 
    sed 's/&lt;/</g' | sed 's/&gt;/>/g' |
    sed 's/#/ /g' |  
    perl -lne '$_=~s/^>id\d+_/>/g;print $_' >"$1${group_label_filename}".fna.nrdb.aln.html;

perl -lne 'if(/<title>stdin<\/title>/){$_="<title>'"$1 $group_label"'</title>"}
elsif(/<pre>/ && $i ne "1"){$_.="'"$group_label"'"; $i="1"};print $_' "$1${group_label_filename}".fna.nrdb.aln.html > "$1${group_label_filename}".fna.nrdb.aln.html.tmp; mv "$1${group_label_filename}".fna.nrdb.aln.html.tmp "$1${group_label_filename}".fna.nrdb.aln.html;

if [ "$keep" != "1" ]; then
    rm "$1${group_label_filename}".fna;
    rm "$1${group_label_filename}".fna.nrdb;
    rm "$1${group_label_filename}".fna.nrdb.aln;
    rm "$1${group_label_filename}".fna.nrdb.aln.md;
    rm "$1${group_label_filename}".fna.nrdb.aln.msaplot;
    mv "$1${group_label_filename}".fna.nrdb.aln.html "$1${group_label_filename}".html;
fi

echo -e "$ORANGE[alignNrdbReadsPlot]$NC Done";
