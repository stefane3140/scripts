#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
$|=1; #autoflush

my $version="0.4.36"; # last updated on 2021-10-02 23:08:44

my $maxFast=10000;
my $gff_filter="";
my $gff_overlap=0.5;
my $gff_id="ID|product";

my $usage = "RNAseq_counting_gene.pl        converts given sam/bam files to a count table. The output format is sgr.

SYNOPSIS

RNAseq_counting_gene.pl (options) FILE1.sam/bam FILE2.sam/bam FILE3.sam/bam ...

	-outputReads	
		If set, then reads are outputted for each target (for each gff entry, or mapped target). 
		WARNING: this can produce a lot of files and it can take a long time...

	--ambiguousUniform
		If set, all mapped reads are counted but ambiguously mapped reads are equally distributed among all hits. 
		So if a read maps to 3 targets, then each target gets +1/3
	
	-ambiguousDistribution FILE
		Same as --ambiguousUniform but the input file specifies the relative quantities of the targets.
		FILE : a mapping (tab separated) of the name of the target and the amount of that entry (other targets are dismissed)

	-gff, -g 	
		A gff file for counting: if at least 50% of the read mapped inside the start-end range of a gff entry, if genes overlap all entries are counted
		The sam CIGAR string is used for the overlap calculations

		counted Reads :        |=>  |=>  |=>         |=>  |=>           
		omitted Reads : |=>|=>                                |=>                   
		GFF           : ____|=======Gene1======>____________________
		GFF           : ____________________|=======Gene2======>____

	-gff_filter : filter the gff file by feature (the 3. column). Usually this is set to 'gene' or 'CDS'.
				  You can use 'CDS|rRNA|exon' or '.*RNA|gene' to define multiple filters at once [default:'']
	-gff_id 	: The id that is used for counting (do not use Name/product since it is usually not unique!)
				  You can use 'ID|product' to combine the 2 attributes ID and product [default:$gff_id]
	-gff_start	: If set, only reads are counted that overlap the +-3nt area around the start, you can also encode this in the gff (you can also add 'gff_start=3;' to the gff attribute)
	-gff_end	: Same for the end.
	-gff_overlap: minimal percentual overlap of a read with a gff entry (relative to read length) [default:$gff_overlap].
                  A value of 0 corresponds to the default featureCount (at least 1 nt).

	-examples   : displays various examples

	mandatory for -gff : 
		-uc  strand unspecific counting (see -examples for more details) 
		-sc  strand specific counting (see -examples for more details) 

	(If no -gff is used, strand information is omitted)

	-s	: a tab separated grouping file that maps from a groupid to target names (<-as provided in the sam file). 
		  This can be used for counting isoforms, splicing variations, ... 
		  If a read maps uniquely to only target names of a group, the groupid counts +1. 
		  All other target names are treated normally.
		  This option can be combined with -gff option (the grouping should then reference the names/ids of -gff_id)
	-m	: the maximum number of elements that the gff can contain to use the fast and memory intensive method (set this to 0 to save memory on the cost of time) [default:$maxFast]. 

	Hint for large gff files: You can use the 'split -l 1000 --numeric-suffixes some.gff some.gff' to split your gff into smaller chunks, then run this program multiple times (using the fast approach with -m 1000) and add the results together.

DESCRIPTION
 
	- input can be sam or bam (samtools is needed for bam)
	- output is written to STDOUT (sgr format)
	- unmapped reads are always omitted (bit flag 0x4)
	- ambiguously mapped reads are omitted by default (NH:i:X, X>1)
	- if NH:i:X read attribute is missing all reads are treated as uniquely mapped
		For Bowtie2 use : 'samtools view -q 1 input' (Bowtie2 assigns a MAPQ of 1 to unique reads)
		Other : grep -v 'XT:A:R' input.sam or grep -v 'XA:Z:' input.sam 
	- CIGAR string is used for end positions/coverage !
	- RNASeq sam/bam input files are assumend to be 1based

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES Perl Getopt::Long, File::Basename
";

my $usage_examples="EXAMPLES 

	# example grouping file for -s option:
	grp1    target1,target2,target3
	grp2    target4,target5
	...

	# ignore any strand information
	-gff x.gff -uc 
		counted Reads :    |=> |=>  <=|   |=> |=>       
		omitted Reads : |=>                       |=>
		GFF           : ____|=======Gene1======>____
		=> OUTPUT     : Gene1	5

	# use strand information
	-gff x.gff -sc 
		counted Reads :    |=> |=>        |=> |=>       
		omitted Reads : |=>          <=|          |=>
		GFF           : ____|=======Gene1======>____
		=> OUTPUT     : Gene1	4

		counted Reads :              <=|            
		omitted Reads : |=>|=> |=>        |=> |=> |=>
		GFF           : ____<=======Gene1======|____
		=> OUTPUT     : Gene1	1

	# gff_start option:
	-gff x.gff -sc -gff_start

		# if gff entry is on + : only take reads mapping to PLUS strand and 
		# only those who start within 3nt from the start position of the gff entry
			counted Reads :     |=>        
			omitted Reads : |=>     |=>  <=|  |=> |=>
			GFF           : ____|=======Gene1======>____
		=> OUTPUT         : Gene1	1

		# if gff entry is on - : only take reads mapping to MINUS strand and 
		# only those who end within 3nt from the end position of the gff entry
			counted Reads :                      <=|        
			omitted Reads : |=>     |=>  <=|  |=> |=>
			GFF           : ____<=======Gene1======|____
			                                       \\- gff end
		=> OUTPUT         : Gene1	1

	# gff_end option:
	-gff x.gff -sc -gff_end

		# if gff entry is on + : only take reads mapping to PLUS strand and 
		# only those who end within 3nt from the end position of the gff entry
			counted Reads :                      |=> 
			omitted Reads : |=>     |=>  <=|  |=> |=>
			GFF           : ____|=======Gene1======>____
			                                        \\- gff end
		=> OUTPUT         : Gene1	1
";

#	--ambiguous (deprecated)		If set, all mapped reads are counted

my $gfffile = "";
my $groupfile = "";

# only for gff option:
my $unstranded; # -uc : overwrites all strand info with +
my $stranded; # -sc : only take reads that are in same orientation as gff entry

my $verbose; 
my $outputReads; 
my $ambiguous; 
my $ambiguousUniform;
my $ambiguousDistribution;
my $gff_count_only_in_start; 
my $gff_count_only_in_end; 
my $numOfProcessedReads;
my $help;
my $debug;
my $numFiles=0;
our %stats;
$stats{"ambiguous"}=0;
$stats{"counting read"}=0;
$stats{"counting gff"}=0;
$stats{"gff"}=0;
$stats{"bad read"}=0;
$stats{"counting group"}=0;
$stats{"no gff found"}=0;

GetOptions('help!'=>\$help,'h!'=>\$help,
	'gff=s'=>\$gfffile,
	'g=s'=>\$gfffile,
	's=s'=>\$groupfile,
	'ambiguousDistribution=s' => \$ambiguousDistribution,
	'ambiguousUniform' => \$ambiguousUniform ,
	'gff_filter=s'=>\$gff_filter,
	'gff_id=s'=>\$gff_id,
	'gff_overlap=f'=>\$gff_overlap,
	'gff_start'=>\$gff_count_only_in_start,
	'gff_end'=>\$gff_count_only_in_end,
	'm=i'=>\$maxFast,
	'uc' => \$unstranded,
	'sc' => \$stranded,
	'version!' => sub { print "$0\tv$version\n";exit 0; },
	'v!' => sub { print "$0\tv$version\n";exit 0; },
	'examples!' => sub { print "$usage_examples\n";exit 0; },
	'verbose' => \$verbose,
	'outputReads' => \$outputReads,
	'ambiguous' => \$ambiguous,
	'x' => \$debug
);

if ($help || scalar(@ARGV) == 0 ){
    print $usage;
    exit(1);
}
if($ambiguous){print STDERR "[WARNING] --ambiguous is deprecated\n"}

if($groupfile ne "" && $outputReads){
	print STDERR "[ERROR] the combination of -s and -outputReads is not supported";
	exit 1;
}

if(($gff_count_only_in_start || $gff_count_only_in_end) && !$gfffile){
	print STDERR "[ERROR] the combination of -gff_start or -gff_end without -gff is not supported";
	exit 1;
}

if($ambiguousDistribution){$ambiguous=1}
if($ambiguousUniform){$ambiguous=1}

if($ambiguous){$ambiguous=1}else{$ambiguous=0;}

if( $gfffile ne "" && ( !defined $stranded && !defined $unstranded ) ){print STDERR $usage."\n[ERROR] Please specify either -uc for strand unspecific counting OR -sc for strand specific counting (and not both).\n";exit 1;}
if( $gfffile eq ""){$unstranded=1}

my %gff_plus;
my %gff_minus;
my $do_gff;
my @gff_full_data;
my $db;
my %gff_lokus2idx;

if($gfffile ne ""){
	print STDERR "[RNAseqCounting] Using strand ".( $unstranded ? "unspecific":"specific")." counting\n";
}

my $gff_len = `wc -l $gfffile`;
$gff_len =~ s/^([0-9]+ ).*/$1/g;

if($verbose){print STDERR " -verbose is set \n"}

our %multimapping;
if($ambiguousDistribution && -e $ambiguousDistribution){
	open(my $FH,"<",$ambiguousDistribution) or die("ERROR input ambiguousDistribution file: $!");
	while(<$FH>){ 
		chomp;
		if(length($_)<2 || substr($_,0,1) eq "#" || $_ eq ""){next;}
		my @arr=split("\t",$_); 
		## 1. = target name, 2. = frequency/amount (number)
		if( scalar @arr != 2 ){print STDERR "[RNAseqCounting] multimapping file ERROR : $ambiguousDistribution wrong format (10 columns are needed)...\n";exit 1;}
		if( exists($multimapping{$arr[0]}) ){print STDERR "[RNAseqCounting] multimapping file ERROR : ".$arr[0]." is ambiguous...\n";exit 1;}
		$multimapping{$arr[0]} = $arr[1];
	}close($FH);
	print STDERR "[RNAseqCounting]  loaded a multimapping file with ".(scalar keys %multimapping)." entries...\n";
	if(scalar keys %multimapping == 0){print STDERR "[RNAseqCounting] ERROR: the multimapping file seems to be empty..."; exit 1;}
}

if($gfffile ne ""){
	print STDERR "[RNAseqCounting]  processing ".$gfffile."...\n";

	my $num_of_entries = 0;
	my $num_of_filtered_entries = 0;
	my $num_of_id_entries = 0;

	my %check_id_clash;

	$numOfProcessedReads=0;

	open(FH,"<",$gfffile) or die("ERROR input gff file: $!");while(<FH>){
		chomp; if(length($_)<2||substr($_,0,1) eq "#"){next;} # empty line / no information
		
		my $line = $_;
		my @line_split = split "\t",$line;
		# with $line_split[2] => feature 
		# and $line_split[8] => attributes

		if($debug){print STDERR "$line\n"}

		# if multiple gff_id are defined -> combine the values of those attributes
		if($gff_id=~/[#+|]/){
			my $cumm_value="";
			foreach my $id (split /[#+|]/,$gff_id) {
			 	if($line_split[8] =~ m/${id}="?([^;]+)"?/i){$cumm_value.="$1 "}
			}
			$line_split[8].="gff_id=\"$cumm_value\";"; # set gff_id 
		}

		$stats{"gff total"}++;

		# if multiple gff_filter are defined -> search all
		my $has_one_gff_filter = 0;
		if($gff_filter=~/[#+|]/){
			foreach my $id (split /[#+|]/,$gff_filter) {
				if($line_split[2] =~ m/${id}/i){
					$has_one_gff_filter = 1; last;
				}
			}
		}else{
			if($line_split[2] =~ m/${gff_filter}/i){ $has_one_gff_filter=1 }
		}

		if( $gff_filter eq "" || $has_one_gff_filter ){

			$num_of_filtered_entries++;
			if( $line_split[8] =~ m/gff_id="?([^;]+)"?/i || $line_split[8] =~ m/${gff_id}="?([^;]+)"?/i ){	

				$num_of_id_entries++;

				my $name=$1; # this defines the NAME column ($gff_lokus2idx...[9]) 

				if(exists $check_id_clash{$name}){
					print STDERR "[WARNING]  the id '$name' of the given gff did clash, skipping this entry (try to adjust the -gff_id)...\n";
					next;
				} 
				$check_id_clash{$name}=1;

				$name=~s/["' ;]+$//g;

				if($name eq ""){next;}
				
				my @a=split("\t",$line);
				my $chr=$a[0];
				if($a[4]<$a[3]){my $tmp=$a[3];$a[3]=$a[4];$a[4]=$tmp} # reorder if start and end are mixed
				my $start=$a[3];
				my $end=$a[4];
				my $strand=$a[6];
				my $is_plus_strand;

				if( $unstranded ){ $strand="+"; }
				if( $strand eq "+" ){ $is_plus_strand=1 }elsif($strand eq "-"){ $is_plus_strand=0;}else{ $is_plus_strand=-1;}
				push(@a,$name); # the 10th entry ([9]) is the extracted name/id (gff_id) ! 

				$stats{"gff"}++;

				if($gff_len <= $maxFast){

					for(my $i = $start ; $i < $end ; $i++){ # store for all positions inside the gene the current gff entry -> arrays of arrays since these can overlap
						
						$gff_lokus2idx{$is_plus_strand}{$chr}{$i}[ scalar @{$gff_lokus2idx{$is_plus_strand}{$chr}{int($i)}} ] = scalar @gff_full_data;

						if($verbose){print STDERR "gff $name -> gff_lokus2idx{$is_plus_strand}{$chr}{$i} = ".(scalar @gff_full_data)." \n"}
					}
					$numOfProcessedReads++;
					if($numOfProcessedReads % 100 == 0){print STDERR "\033[J"." processed ".($numOfProcessedReads/100)."e+2/".int($num_of_entries/100)."e+2 gff entries ..."."\033[G"} 

				}
				push(@gff_full_data,\@a);
			}
		}
	}close(FH);

	print STDERR "[RNAseqCounting]  loaded ".$stats{"gff"}."/".$stats{"gff total"}." entries (adjust this with the -gff_filter option) from the gff file\n";
	
	if($stats{"gff"}<1){
		print STDERR "[ERROR]  did load nothing out of the given gff (adjust this with the -gff_filter option)...";
		if($stats{"gff total"}==0){
			print STDERR " the gff seems to be empty.\n";
		}elsif($num_of_id_entries == 0 && $num_of_filtered_entries > 0 ){
			print STDERR " please adjust the -gff_id option, currently '${gff_id}' is used but no gff entry has this attribute.\n";
		}elsif($num_of_filtered_entries == 0){
			print STDERR " please adjust the -gff_filter option, currently '${gff_filter}' is used but no gff entry has this type (third column of the gff). Set '-gff_filter \"\"' to disable the filtering completly.\n";
		}
		exit 1;
	}

	$do_gff=1;

	if($stats{"gff total"} <= $maxFast){print STDERR "[RNAseqCounting]  since there are less than $maxFast (-m option) entries in the gff, the fast and memory heavy method is used. If you experience memory overflow use the -m option (-m=0 disables the fast approach).\n";}else{print STDERR "[RNAseqCounting]  since there are more than $maxFast (-m option) entries in the gff, the slow memory and conservative method is used.\n";}
}

our %grouping;
our $docheckgrouping=0; # grouping is the grouping of the genes to the group id (replace all genes with that group id)
if($groupfile ne ""){
	open(FH,"<",$groupfile) or die("ERROR input group file: $!");while(<FH>){ # build grouping
		chomp;
		if(length($_)<2 || substr($_,0,1) eq "#" || $_ eq ""){next;}
		my @arr=split("\t",$_); 
		if(scalar @arr !=2){next;}
		my $groupid=$arr[0];
		foreach my $trans (split(",",$arr[1])){ 
			if( exists($grouping{$trans}) ){print STDERR "[RNAseqCounting] grouping file ERROR : ".$trans." is ambiguous...\n";exit 1;}
			$grouping{$trans}=$groupid;
			$docheckgrouping=1;
		}
	}close(FH);
	print STDERR "[RNAseqCounting]  loaded a grouping file with ".(scalar keys %grouping)." entries...\n";
	if(scalar keys %grouping == 0){print STDERR "[RNAseqCounting] ERROR: the grouping file seems to be empty..."; exit 1;}
}

our %count;
our @curSamArray;
our $fp;
our $cur_read;
our %ambiguous_reads;

sub approxProgress{
	my $in=shift;
	if($in > 0.999){$in=0.999}
	return $in;
}
sub doCounting{

	my $readname = $curSamArray[0];
	my $bitflag = $curSamArray[1];
	my $name = $curSamArray[2]; # this is either the sam target (could be chromosome or transcript name of mapping) OR the name of the gff target (changed if -gff is set)

	# replace the name with the grouping name if present
	my $thisIsAGroupingEntry=0;
	if(exists($grouping{$name})){$name=$grouping{$name};$thisIsAGroupingEntry=1}; # replace the gene id with group id

	if( !$ambiguous && $_ !~ m/\bNH:i:/ ){ print STDERR "[WARNING]  Warning the NH:i:X flag is missing... All reads are now treated as uniquely mapped !!! \n"; $ambiguous = 1 }

	if( $debug ){print STDERR "$name : "}

	
	if( $docheckgrouping==1 &&  # count in groups -> only take reads that are multimapped -> not NH:i:1
		$name ne "*" && 
		$thisIsAGroupingEntry &&
		!( $bitflag & hex("0x4") ) && 
		( $_ !~ m/\bNH:i:1\b/ || $ambiguous ) && # is a multi-mapped read
		!exists($ambiguous_reads{$readname}{"bad read"}) ){ 

		if($debug){print STDERR "counted group\n"}
		$stats{"counting read"}++;

		$ambiguous_reads{$readname}{$name}=1; # here save the read name -> mapping target (= replaced by group name, so they have all the same $name)
		# this ^ mapping should only be 1-1 if only target of the group are hit (all elements of the group have same $name)

		# otherwise do the following:
		if(keys(%{$ambiguous_reads{$readname}})>1){ # correct for these later
			$ambiguous_reads{$readname}{"bad read"}=1; 
			$stats{"bad read"}++;
		}

	}elsif( ($ambiguousUniform || $ambiguousDistribution) &&  # count ambiguous -> only take reads that are multimapped -> not NH:i:1
		$name ne "*" && 
		!( $bitflag & hex("0x4") ) && 
		( $_ !~ m/\bNH:i:1\b/ ) # is a multi-mapped read
		){

		if($debug){print STDERR "counted group\n"}
		$stats{"counting read"}++;
		
		# ambiguously mapped read add to stack to analyse later
		# multimapped reads have all the same readname 
		$ambiguous_reads{$readname}{$name}=1; 

	}elsif( # count normal:
		!exists($grouping{$name}) && 
		$name ne "*" && 
		!( $bitflag & hex("0x4") ) && 
		( $ambiguous || $_=~m/\bNH:i:1\b/ ) ){ 
		# mapped reads that are unqiue OR all multi if -ambiguous is set 

		if($debug){print STDERR "counted\n"}
		$stats{"counting read"}++;

		if(!exists $count{$fp}{$name}){
			
			if($outputReads){
				my @arr = ($cur_read);
				$count{$fp}{$name} = \@arr;
			}else{
				$count{$fp}{$name} = 1;
			}

		}else{

			if($outputReads){
				push(@{$count{$fp}{$name}},$cur_read);
			}else{
				$count{$fp}{$name}++;
			}

		}

	}else{

		$stats{"ambiguous"}++;

		if($debug){print STDERR "not counted\n"}
		# unmapped read / ambiguously mapped read 

	}
}

my %seen_gff_entries = (); 

foreach $fp (@ARGV) {

	$numFiles++;

	if($outputReads){
		my $basefp = $fp; 
		$basefp=~s/^.*\/([^\/]+)$/$1/g;
		mkdir($basefp."_countedreads");
	}
			
	undef %count;				# count is a mapping from the gene (or grouping group) to all read ids that map to that gene (for aeach file) # gene just contains all genes that are mapped
	undef %ambiguous_reads; %ambiguous_reads=(); 	# this contains for a given read all genes / groups that are mapped to by this read (should be only one group/gene)

	$numOfProcessedReads=0;
	
	my $wcl = 1;

	my $FH;
	if($fp=~m/\.bam$/){
		open($FH,"samtools view $fp |") or die("ERROR input bam file: $!");
		my $filesize = (-s $fp);
		$wcl= $filesize * 0.0042 * 10; # aproximate line count (bam are aprox 10 times smaller than sam files)
	}else{
		open($FH,"<",$fp) or die("ERROR input sam file: $!");
		my $filesize = (-s $fp);
		$wcl= $filesize * 0.0042; # aproximate line count
	}
	if($wcl==0){$wcl=1}

	$wcl *= 0.5; # further correction

	if(int($wcl/1000000) == 0){
		print STDERR "[RNAseqCounting]  counting $fp (approximately ~".(int($wcl))." reads)...\n";
	}else{
		print STDERR "[RNAseqCounting]  counting $fp (approximately ~".(int($wcl/1000000))."e+6 reads)...\n";
	}

	while(<$FH>){

		my @cur_gff=();
		chomp;
		my $curLine = $_;
		@curSamArray = split("\t",$_); 	# 0 = read ID, 1 = bitflag (0x4=is unmapped), 2 = target gene 

		if(length($_)<1 || substr($_,0,1)eq "#" || scalar @curSamArray < 10){next;}

		if($outputReads){$cur_read = $curLine};

		my $readname = $curSamArray[0];
		my $bitflag = $curSamArray[1];
		my $RNAME_chromosome = $curSamArray[2]; # chrosome / plasmid name 
		my $leftmostpos = $curSamArray[3];
		my $seq = $curSamArray[9];
		my $is_plus_strand;

		my $name="";

		if($debug){print STDERR "-> $curLine\n"}

		#
		# correct the length of the read with the CIGAR string 
		#
		my $lengthOfRead = length($curSamArray[9]);
		my $CIGAR = $curSamArray[5];
		my $numOfInsertions = 0;
		my $numOfDeletions = 0;
		while ($CIGAR =~ /([0-9]+)D/ig) { $numOfDeletions+=$1; } # D : deletion
		while ($CIGAR =~ /([0-9]+)[ISH]/ig) { $numOfInsertions+=$1; } # I : insertion , S+H : clipping (Segment of the query sequence that does not appear in the alignment)

		# missmatches (X) and matches (=) can be ignored
		my $len = $lengthOfRead - $numOfInsertions + $numOfDeletions; # correct for CIGAR : - insertion and + deletions

		if($bitflag & hex("0x10")){$is_plus_strand=0;}else{$is_plus_strand=1;} # 0x10 = bit 16 = is rc = minus strand 
		# ^ equivalent formulation : if( $bitflag & 16 )

		if( $unstranded ){ $is_plus_strand=1; }

		if($debug){print STDERR "len=$len\n"}
		if($debug){print STDERR "unstranded=$unstranded\n"}

		my %seen = (); # exclude duplicated names (NAME id of attribute in [9])

		if( $do_gff ){ # if gff is enabled then replace the target name with the corresponding gff entry

			if($debug){print STDERR "do_gff:\n"}

			# collect gff entries that have some overlap with the current read
			# IF -sc : take only gff entries that have same orientation as the read ($is_plus_strand from the read)
			if(scalar keys %gff_lokus2idx == 0){@cur_gff=@gff_full_data;}else{
				
				if(exists $gff_lokus2idx{$is_plus_strand}{$RNAME_chromosome}{int($leftmostpos)}){ 
					# collect all gff entries that intersect with the start of the read
					foreach my $i (@{$gff_lokus2idx{$is_plus_strand}{$RNAME_chromosome}{int($leftmostpos)}}){
						if(!exists $seen{$gff_full_data[$i][9]} ){push(@cur_gff,$gff_full_data[$i]); $seen{$gff_full_data[$i][9]} = 1;$seen_gff_entries{$gff_full_data[$i][9]} = 1;}
					}
				}
				if(exists $gff_lokus2idx{$is_plus_strand}{$RNAME_chromosome}{int($leftmostpos+int($len/2))}){ 
					# or the middle
					foreach my $i (@{$gff_lokus2idx{$is_plus_strand}{$RNAME_chromosome}{int($leftmostpos+int($len/2))}}){
						if(!exists $seen{$gff_full_data[$i][9]} ){push(@cur_gff,$gff_full_data[$i]); $seen{$gff_full_data[$i][9]} = 1;$seen_gff_entries{$gff_full_data[$i][9]} = 1;}
					}
				}
				if(exists $gff_lokus2idx{$is_plus_strand}{$RNAME_chromosome}{int($leftmostpos+$len)}){ 
					# or the end of the read of the read
					foreach my $i (@{$gff_lokus2idx{$is_plus_strand}{$RNAME_chromosome}{int($leftmostpos+$len)}}){
						if(!exists $seen{$gff_full_data[$i][9]} ){push(@cur_gff,$gff_full_data[$i]); $seen{$gff_full_data[$i][9]} = 1;$seen_gff_entries{$gff_full_data[$i][9]} = 1;}
					}
				}

				# => now @cur_gff only contains all unique entries (therefore the %seen) that overlap the read anywhere (3 points make sure that all entries are looked at for a >50% coverage) 
			}

			for(my $i = 0 ; $i < scalar @cur_gff ; $i++){ # go over all gff entries that overlap the given read (if -sc then only same orientated gff entries are looked at)

				if($debug){print STDERR " $i gff:$cur_gff[$i][9]\n"}

				my $gff_start=$cur_gff[$i][3];
				my $gff_end=$cur_gff[$i][4];
				my $gff_atttr=$cur_gff[$i][8];
				my $gff_strand=$cur_gff[$i][6];

				my $contains_gff_start = $gff_count_only_in_start ? 3 : 0;
				my $contains_gff_end = $gff_count_only_in_end ? 3 : 0;

				if($gff_atttr =~ /gff_start=([0-9]+)/){
					$contains_gff_start = $1;
				}elsif($gff_atttr =~ /gff_start/){
					$contains_gff_start = 3;
				}
				if($gff_atttr =~ /gff_end=([0-9]+)/){
					$contains_gff_end = $1;
				}elsif($gff_atttr =~ /gff_end/){
					$contains_gff_end = 3;
				}

				# if -gff_start
				if( $gff_count_only_in_start && $contains_gff_start>0){ # if gff_start is set -> only search reads that overlap the start area of the gff entry

					if($gff_strand eq "+"){
						if( $leftmostpos >= $gff_start-$contains_gff_start && $leftmostpos <= $gff_start+$contains_gff_start){ # gff is + strand -> take start of read as start
							$name = $cur_gff[$i][9]; 
							$seen_gff_entries{$name}=1;
						}else{ $stats{"no gff found"}++ }
					}else{

						if($debug){print STDERR "contains_gff_start=$contains_gff_start \n".($leftmostpos+$len-1)." ".($gff_end-$contains_gff_start)."\n"}

						if( $leftmostpos+$len-1 >= $gff_end-$contains_gff_start && $leftmostpos+$len-1 <= $gff_end+$contains_gff_start){ # gff is - strand -> take end of read as start
							$name = $cur_gff[$i][9];
							$seen_gff_entries{$name}=1;
						}else{ $stats{"no gff found"}++ }
					}
					
				# if -gff_end
				}elsif( $gff_count_only_in_end && $contains_gff_end>0){ # same for gff_end (end position = leftmost+len)

					if($gff_strand eq "+"){
						if( $leftmostpos+$len-1 >= $gff_end-$contains_gff_end && $leftmostpos+$len-1 <= $gff_end+$contains_gff_end){ # gff is + strand -> take end of read as end
							$name = $cur_gff[$i][9]; 
							$seen_gff_entries{$name}=1;
						}else{ $stats{"no gff found"}++ }
					}else{
						if( $leftmostpos >= $gff_start-$contains_gff_end && $leftmostpos <= $gff_start+$contains_gff_end){ # gff is - strand -> take start of read as end
							$name = $cur_gff[$i][9]; 
							$seen_gff_entries{$name}=1;
						}else{ $stats{"no gff found"}++ }
					}

				# if normal counting
				}else{
					if( $leftmostpos+$len-1 < $gff_end && $leftmostpos+$len-1 > $gff_start && $leftmostpos<$gff_start && ($leftmostpos+$len - $gff_start)/($len) > $gff_overlap){
						$name = $cur_gff[$i][9]; # read overlaps with start gffarea
						$seen_gff_entries{$name}=1;
					}elsif( $leftmostpos+$len-1 < $gff_end && $leftmostpos>=$gff_start ){ # if the whole read is inside = 100% is covered
						$name = $cur_gff[$i][9]; # read totally inside the gffgene
						$seen_gff_entries{$name}=1;
					}elsif( $leftmostpos+$len-1 >= $gff_end && $leftmostpos < $gff_end && ($gff_end - $leftmostpos)/($len) > $gff_overlap){
						$name = $cur_gff[$i][9]; # read overlaps the end gffarea
						$seen_gff_entries{$name}=1;
					}else{ $stats{"no gff found"}++ }
				}

				if($name ne ""){
					$curSamArray[2]=$name;
					doCounting();
					$name="";
				}
			}
		}elsif($RNAME_chromosome ne ""){
			# DO COUNTING ::::::::::::
			doCounting();
			# DONE COUNTING ::::::::::
		}

		$numOfProcessedReads++;
		if(!$debug && $numOfProcessedReads % 10000 == 0){print STDERR "\033[J"." processed ~".(int(approxProgress(($numOfProcessedReads)/int($wcl))*10000)/100)."% of the reads ..."."\033[G"} # log
 
	}close(FH);

	print STDERR "                                           \n";# clean up progress output

	if(($ambiguousUniform || $ambiguousDistribution)){ # correct for ambiguousUniform
		print STDERR "[RNAseqCounting] $fp now doing the multimapping correction\n";
		foreach my $read (keys %ambiguous_reads) {
			# collect all the amounts of the given multimapping file for normalization
			my $sum=0; # sum all amounts of targets (if no ambiguousUniformGile this is equal to the number of targets for that multi read)
			if($ambiguousDistribution){
				foreach my $target_name (keys %{$ambiguous_reads{$read}}) {
					if(exists $multimapping{$target_name}){ $sum += $multimapping{$target_name}[1] } 
				}
			}else{
				$sum = scalar keys %{$ambiguous_reads{$read}};
			}
			if($sum == 0){next;}
			foreach my $target_name (keys %{$ambiguous_reads{$read}}) {
				if(exists $multimapping{$target_name}){ $count{$fp}{ $target_name } += $multimapping{$target_name}[1]/$sum }
				else{ $count{$fp}{ $target_name } += 1 / $sum }
			}
		}
	}elsif($docheckgrouping){ # correct for grouping --> AND COUNT THE GROUPINGS
		foreach my $read (keys %ambiguous_reads){
			if( !exists($ambiguous_reads{$read}{"bad read"}) && 
				scalar(keys(%{$ambiguous_reads{$read}}))==1 ){ 

				my @a=keys(%{$ambiguous_reads{$read}}); 
				my $name_group = $a[0]; # the name of the group that should be counted

				$stats{"counting group"}++;
				if( !exists $count{$fp}{ $name_group } ){ $count{$fp}{ $name_group }=1 }else{ $count{$fp}{ $name_group }++ }; 
			}
		}
	}
	%ambiguous_reads=();

	if(scalar keys %{$count{$fp}} == 0){
		print STDERR "[ERROR] I did count nothing :( please check input files...\n"; exit 1;
	}

	# print results :
	print("# gene\tfile\tcount\n");
	foreach my $gene (keys %{$count{$fp}} ){
		print $gene."\t".$fp; 
		if($outputReads){

			my $basefp = $fp; 
			$basefp=~s/^.*\/([^\/]+)$/$1/g;

			print "\t".scalar(@{$count{$fp}{$gene}});

			open(FH,">".$basefp."_countedreads/readsOf_".$gene.".sam") or die("ERROR output read file: $!");
			foreach my $read (@{$count{$fp}{$gene}}){
				print FH $read."\n";
			}
			close(FH);
		}else{
			print "\t".($count{$fp}{$gene});
		}
		print "\n";
	}

	# perl unset stuff:
	undef %count; undef %ambiguous_reads; undef %ambiguous_reads;
}

sub makePercent{
	my $a = shift;
	return( (int($a*10000)/100)."%" );
}

print STDERR "[RNAseqCounting] I processed $numFiles file(s) and counted ".$stats{"counting read"}."/".($stats{"no gff found"}+$stats{"counting read"}+$stats{"ambiguous"}+$stats{"bad read"})." (".makePercent( $stats{"counting read"}/($stats{"no gff found"}+$stats{"counting read"}+$stats{"ambiguous"}+$stats{"bad read"}) ).")".
								" reads while omitting ".($stats{"no gff found"}+$stats{"ambiguous"}+$stats{"bad read"})." (".makePercent( ($stats{"no gff found"}+$stats{"ambiguous"}+$stats{"bad read"})/($stats{"no gff found"}+$stats{"counting read"}+$stats{"ambiguous"}+$stats{"bad read"}) ).")"." reads because they are ".
								($ambiguous ? "unmapped" : "either unmapped or ambiguously mapped")."".
								($do_gff ? " or not matching any gff" : "")."".
								($groupfile eq "" ? "" : " (groups are treated as one mapping target)") .
								($do_gff ? ". Of those omitted reads ".$stats{"no gff found"}." (".makePercent( $stats{"no gff found"}/($stats{"no gff found"}+$stats{"counting read"}+$stats{"ambiguous"}+$stats{"bad read"}) ).") reads did not find any gff target and ".($stats{"gff"}-(scalar keys %seen_gff_entries))."/".$stats{"gff"}." (".makePercent( ($stats{"gff"}-(scalar keys %seen_gff_entries))/$stats{"gff"} ).") gff entries where not covered by any reads." : ""). "\n";

if($groupfile ne ""){ print STDERR "[RNAseqCounting] I counted in particular ".$stats{"counting group"}." (".makePercent( $stats{"counting group"}/($stats{"counting read"}+$stats{"ambiguous"}+$stats{"bad read"}) ).") reads mapping on the given group file.\n" }
