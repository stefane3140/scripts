#!/usr/bin/env perl
#pk
# simple tool to determine the phred format for a given fastq file 
# reads only the first 10000 lines reads at most 

use strict;
use warnings "all";

my $version="0.0.4"; # last updated on 2021-10-02 23:08:44

if (scalar @ARGV == 0 || $ARGV[0] =~ m/^--?(version|v)$/) { print "$0\tv$version\n"; exit 0;   }

if(scalar @ARGV == 0 || $ARGV[0] eq "-h" || $ARGV[0] eq "-help" || $ARGV[0] eq "--help"){
	
	print 'identifyPhred.pl        Determines the phred format (Sanger,Illumina,...) for a given fastq file
 
SYNOPSIS
 
identifyPhred.pl FASTQ

	FASTQ : the fastq formatted file.

DESCRIPTION
 
	The program looks at up to 10k reads and excludes all formats that do not agree with the given phreds of these reads. Then the result is printed.

 	Possible formats : 

		S = Sanger Phred+33 (!"#$%&\'()*+,\-.\/0123456789:;<=>?@ABCDEFGHI)
		X = Solexa Solexa+64 (;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\[\\\]^_`abcdefgh)
		I = Illumina 1.3+ Phred+64 (@ABCDEFGHIJKLMNOPQRSTUVWXYZ\[\\\]^_`abcdefgh)
		J = Illumina 1.5+ Phred+64 (BCDEFGHIJKLMNOPQRSTUVWXYZ\[\\\]^_`abcdefghi)
		L = Illumina 1.8+ (very close to S!) Phred+33 (!"#$%&\'()*+,\-.\/0123456789:;<=>?@ABCDEFGHIJ)
	 
 	Source : https://en.wikipedia.org/wiki/FASTQ_format

SEE ALSO https://en.wikipedia.org/wiki/FASTQ_format
VERSION v'.$version.'
AUTHOR Paul Klemm
';
	exit 0;
}

my $tic=0;

my $linenum=0;

# assume it is everything
our $isSanger=1;
our $isSolexa=1;
our $isIllumina13=1;
our $isIllumina15=1;
our $isIllumina18=1;

while(<>){
	chomp;

	if(length $_ < 2 || substr($_,0,1) eq "@" || substr($_,0,1) eq "+" || $tic==0 || $_ =~ m/^[A-Za-z]+$/ ){
		$tic=1-$tic;
		next;
	}
	$tic=1-$tic;
	$linenum++;
	
	# if the current phred is is disagreeing with any format -> set to false (0)
	if($_ =~ m/[^!-I]/){$isSanger=0;}
	if($_ =~ m/[^;-h]/){$isSolexa=0;}
	if($_ =~ m/[^@-h]/){$isIllumina13=0;}
	if($_ =~ m/[^B-i]/){$isIllumina15=0;}
	if($_ =~ m/[^!-J]/){$isIllumina18=0;}

	if($isSanger + $isSolexa + $isIllumina13 + $isIllumina15 + $isIllumina18 <= 1){ printres(); }

	if($linenum > 10000){
		printres();
	}
}
printres();


sub printres{ 
	# print result and exit
	if($isSanger + $isSolexa + $isIllumina13 + $isIllumina15 +$isIllumina18 >0){
		if($isSanger ==1 && $isSolexa + $isIllumina13 + $isIllumina15 ==0  && $isIllumina18 ==1 ){ # SPECIAL CASE: S or L : the only difference here is that I never saw 'J' -> so could be L but kinda unlikely ...
			print "very very likely S but could also be L (but I never saw that J):\n";
			if($isSanger){print 'S = Sanger Phred+33 (!"#$%&\'()*+,\-.\/0123456789:;<=>?@ABCDEFGHI)';print "\n"}
			if($isIllumina18){print '( L = Illumina 1.8+ (very close to S!) Phred+33 (!"#$%&\'()*+,\-.\/0123456789:;<=>?@ABCDEFGHIJ) )';print "\n"}
			exit 0;
		}
		if($isSanger + $isSolexa + $isIllumina13 + $isIllumina15 +$isIllumina18 >=2){
			print "cannot decide... could one of these:\n";
		}
		if($isSanger){print 'S = Sanger Phred+33 (!"#$%&\'()*+,\-.\/0123456789:;<=>?@ABCDEFGHI)';print "\n"}
		if($isSolexa){print 'X = Solexa Solexa+64 (;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\[\\\]^_`abcdefgh)';print "\n"}
		if($isIllumina13){print 'I = Illumina 1.3+ Phred+64 (@ABCDEFGHIJKLMNOPQRSTUVWXYZ\[\\\]^_`abcdefgh)';print "\n"}
		if($isIllumina15){print 'J = Illumina 1.5+ Phred+64 (BCDEFGHIJKLMNOPQRSTUVWXYZ\[\\\]^_`abcdefghi)';print "\n"}
		if($isIllumina18){print 'L = Illumina 1.8+ (very close to S!) Phred+33 (!"#$%&\'()*+,\-.\/0123456789:;<=>?@ABCDEFGHIJ)';print "\n"}
		exit 0;
	}else{ 
		# all are 0 ...
		print "invalid input...\n";
		exit 1;
	}
}
