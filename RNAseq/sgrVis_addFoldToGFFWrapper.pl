#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()

our $PURPLE="\033[0;45m";
our $COLOR="\033[30;42m";
our $SHELL2="\033[30;47m";
our $NC="\033[0m"; # No Color

my $version="0.0.5"; # last updated on 2021-10-02 23:08:45

my $usage = <<ENDUSAGE;
sgrVis_addFoldToGFFWrapper.pl       Auxillary program to run sgrVis.R --varna 

SYNOPSIS
 
sgrVis_addFoldToGFFWrapper.pl --gff GFFFILE --fasta GENOMEFILE

		adds secondary struture and sequence to the gff entries for the use in sgrVis.R

		# first call this program
		sgrVis_addFoldToGFFWrapper.pl --gff GFFFILE --fasta GENOMEFILE >GFFFILE.sgrvis.gff

		# then feed the new gff to sgrVis.R
		sgrVis.R -g GFFFILE.sgrvis.gff -f GENOMEFILE ...

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES Getopt::Long, File::Basename, RNAfold (vienna package), getgff.pl 
ENDUSAGE

use Cwd;
my $wdir=getcwd;
my $help;
my $gff;
my $fasta;

GetOptions('help!'=>\$help, 'h!'=>\$help, 'version!'=>sub { print "$0\tv$version\n";exit 0; },
	'gff=s'=>\$gff,
	'fasta=s'=>\$fasta,
);
if ($help || ! -e $gff || ! -e $fasta){
    print $usage;
    exit(1);
}


open(my $FH,"<",$gff);
while(<$FH>){
	chomp;
	my @a = split "\t",$_;

	my $fold_result = `getgff.pl $fasta $a[0] $a[3] $a[4] $a[6] | RNAfold --noPS `;

	my @fold_split = split(/\n/,$fold_result);

	print "$_; Str=$fold_split[0]; Seq=$fold_split[1];\n"
}
close($FH);
