#!/usr/bin/env perl
# pk
use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
$|=1; #autoflush

my $version="0.9.9"; # last updated on 2021-10-02 23:08:44

our $PURPLE="\033[0;45m";
our $COLOR="\033[30;42m";
our $SHELL2="\033[30;47m";
our $NC="\033[0m"; # No Color

my $usage = <<ENDUSAGE;
RNAseq_counting_position.pl        calculates read coverage/start/end distribution for each position with respect to the CIGAR string.

SYNOPSIS
 
RNAseq_counting_position.pl (options) FILE1.sam/bam FILE2.sam/bam FILE3.sam/bam ...

	--strand [+-b] 
		strand: +:plus or -:minus or b:both added together	[default:b]

	--modus [start,end,coverage]
		start: start positions, 
		end: stop positions (CIGAR corrected),
		coverage: all positions (=coverage) [default:coverage]

	--CPM  
		cpm values are printed instead of the raw readcounts

	--additonal [length,other,unique]
		output additonally the information in an additional column (;-separated)
		length : raw read length without CIGAR (requires --modus start or end),
		other : if --modus start, then this is the end position and vice versa (requires --modus start or end)
		unique : outputs the unique countings too (requires --ambiguous*)

	--ambiguousUniform
		If set, all mapped reads are counted but are equally distributed among all hits. 
		So if a read maps to 3 target positions, then each position gets +1/3

	-ambiguousDistribution FILE
		Same as --ambiguousUniform but the input file specifies the relative quantities of the targets.
		FILE : A gff file with an additional column (10th) with the amount of that entry (other ambiguous positions are dismissed)
		FILE : A tab file with first target name (sam column 3) and second the amount of that entry (all positions are used for that target)

DESCRIPTION
	
	- input can be sam or bam (samtools is needed for bam)
	- output is written to STDOUT (sgr format)
	- unmapped reads are always omitted (bit flag 0x4)
	- ambiguously mapped reads are omitted by default (NH:i:X, X>1)
	- if NH:i:X read attribute is missing all reads are treated as uniquely mapped
		For Bowtie2 use : samtools view -q 1 input > unique.sam (Bowtie2 assigns a MAPQ of 1 to unique reads)
		Other : grep -v 'XT:A:R' input.sam or grep -v 'XA:Z:' input.sam 

	- CIGAR string is used for end positions/coverage !
	- The -strand and -modus option: 
		-strand + -modus start => only take reads from PLUS strand, take the left most mapping position of the read
		-strand - -modus start => only take reads from MINUS strand, take the right most mapping position of the read
		-strand b -modus end => take the right most mapping position of the read of PLUS strand, and left most if MINUS

	- RNASeq sam/bam input files are assumend to be 1based

EXAMPLES
	# total coverage ignoring the strands:
	-modus coverage -strand b
		counts        :  112121211 1221  222 111
		Reads         :    |=> |=>  <=|  |=>        
		Reads         :  |=> <=|   |=>   |=> |=>       

	# ignore strand information of the reads:
	-modus start -strand b
		counts        :  1 1   2   1  1  2   1
		Reads         :    |=> |=>  <=|  |=>        
		Reads         :  |=> <=|   |=>   |=> |=>       

	# count start positions of reads of PLUS strand:
	-modus start -strand +
		counts        :  1 1   1   1     2   1
		Reads         :    |=> |=>  <=|  |=>        
		Reads         :  |=> <=|   |=>   |=> |=>    

	# same for MINUS strand:
	-modus start -strand -
		counts        :        1      1
		Reads         :    |=> |=>  <=|  |=>        
		Reads         :  |=> <=|   |=>   |=> |=>  

	# now the end positions:
	-modus end -strand -
		counts        :      1      1
		Reads         :    |=> |=>  <=|  |=>        
		Reads         :  |=> <=|   |=>   |=> |=>    

	# total coverage only for the MINUS strand:
	-modus coverage -strand -
		counts        :      111    111      
		Reads         :    |=> |=>  <=|  |=>        
		Reads         :  |=> <=|   |=>   |=> |=>       

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES Getopt::Long, File::Basename
ENDUSAGE
#		(deprecated)	--RNAseqB	Group B RNASeq files. If set, then the absolute difference is calculated.
#	--ambiguous (deprecated)		If set, all mapped reads are counted

use Cwd;
my $wdir=getcwd;

my $fasta=''; #file name of fasta
my $fastaData; # file content of fasta^
my %RNAseqs; # group -> array of rnaseqs (here just filenames)
my %hist; # each group(rnaseq) has a vector(lenght of fasta) that contains the mean read count (only starts)
my %hist_additional; # --additonal
my %librarySize; # for CPM normalization
my $help; 
my $strand="b";
my $is_B_active=0; # old
my $position_modus="coverage";
my $CPM;
my $sort;
my $ambiguous=0;
my $ambiguousUniform;
my $ambiguousDistribution;
my $numFiles=0;
our $debug;
my $output_additional = "";
our %stats; # for outputting log information (how many reads are unmapped etc ...)

$stats{"ambiguous"}=0;
$stats{"counting read"}=0;
$stats{"counting position"}=0;
$stats{"counting ambiguously"}=0;
$stats{"counting unique"}=0;

GetOptions('help!'=>\$help, 'h!'=>\$help, 
	'ambiguous' => \$ambiguous,
	'ambiguousDistribution=s' => \$ambiguousDistribution,
	'ambiguousUniform' => \$ambiguousUniform ,
	'strand=s'=>\$strand,
	'modus=s'=>\$position_modus,
	'CPM'=>\$CPM,
	'additonal=s'=>\$output_additional,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	'RNAseqA=s'=>sub { my ($opt_name, $opt_value) = @_; push(@{$RNAseqs{"A"}},split(",",$opt_value)); },
	'input=s'=>sub { my ($opt_name, $opt_value) = @_; push(@{$RNAseqs{"A"}},split(",",$opt_value)); },
	'i=s'=>sub { my ($opt_name, $opt_value) = @_; push(@{$RNAseqs{"A"}},split(",",$opt_value)); },
	'x'=>\$debug
);

foreach (@ARGV){ push( @{$RNAseqs{"A"}} , $_ ) }

if ($help || !defined $RNAseqs{"A"} || scalar(@{$RNAseqs{"A"}})==0 || ( $strand ne "+" && $strand ne "-" && $strand ne "b" )){
    print $usage;
    exit(1);
}
if($ambiguous){print STDERR "[WARNING] --ambiguous is deprecated\n"}

if($ambiguousDistribution){$ambiguous=1}
if($ambiguousUniform){$ambiguous=1}

my $CPM_prefix="CPM.";

my $numOfGroups=0;

if(defined $RNAseqs{"A"}){
	$numOfGroups+=scalar @{$RNAseqs{"A"}};
}
if(defined $RNAseqs{"B"}){
	$numOfGroups+=scalar @{$RNAseqs{"B"}};
}

sub approxProgress{
	my $in=shift;
	if($in > 0.999){$in=0.999}
	return $in;
}

my $prefix="";
if($position_modus eq "end"){
	$prefix="end.";
}elsif($position_modus eq "start"){
	$prefix="start.";
}elsif($position_modus eq "coverage"){
	$prefix="coverage.";
	if($output_additional =~ /length|other/){ die "-modus coverage and -additonal length|other are incompatible." }
}else{
	die "invalid --modus option: use start, end or coverage.";
}

if($ambiguous eq "" && $output_additional eq "unique"){ die "option -additonal unique requires -ambiguous*." }

our %multimapping;
if($ambiguousDistribution && -e $ambiguousDistribution){
	open(my $FH,"<",$ambiguousDistribution) or die("ERROR input ambiguousDistribution file: $!");
	while(<$FH>){ 
		chomp;
		if(length($_)<2 || substr($_,0,1) eq "#" || $_ eq ""){next;}
		my @arr=split("\t",$_); 
		## 0-8 entries = gff format, 9 = frequency/amount (number)
		## or 0 entry = chr, 1 = frequency/amount (number)
		if( scalar @arr != 10 && scalar @arr != 2 ){print STDERR "[RNAseqCounting] ambiguousDistribution file ERROR : $ambiguousDistribution wrong format (10 columns are needed)...\n";exit 1;}
		if(scalar @arr == 2){
			if( exists $multimapping{$arr[0]}{"*"} ){print STDERR "[RNAseqCounting] ambiguousDistribution file ERROR : ".$arr[0]." is ambiguous...\n";exit 1;}
			$multimapping{$arr[0]}{"*"} = \@arr;
			if($debug){print STDERR "multimapping : $arr[0] -> $arr[1]\n";}
		}else{
			for (my $i = $arr[3]; $i <= $arr[4]; $i++) {
				if( exists $multimapping{$arr[0]}{$i} ){print STDERR "[RNAseqCounting] ambiguousDistribution file ERROR : ".$arr[0]." at position $i is ambiguous...\n";exit 1;}
				$multimapping{$arr[0]}{$i} = \@arr;
			}
		}
		
	}close($FH);
	print STDERR "[RNAseqCounting]  loaded a ambiguousDistribution file with ".(scalar keys %multimapping)." entries...\n";
	if(scalar keys %multimapping == 0){print STDERR "[RNAseqCounting] ERROR: the ambiguousDistribution file seems to be empty..."; exit 1;}
}

if($CPM){$prefix = "CPM.".$prefix}

#read RNAseqs
foreach my $group (keys %RNAseqs){
	for(my $i = 0 ; $i < scalar(@{$RNAseqs{$group}});$i++){

		$numFiles++;

		my $filesize= (-s @{$RNAseqs{$group}}[$i]);
		my $wcl=0;
		if(@{$RNAseqs{$group}}[$i]=~m/\.bam$/){
			$wcl = $filesize * 0.0042 * 10; # aproximate line count (bam are aprox 10 times smaller than sam files)
		}else{
			$wcl = $filesize * 0.0042; # aproximate line count
		}
		if($wcl==0){$wcl=1}

		if(int($wcl/1000000) == 0){
			print STDERR "[RNAseqCounting]  counting ".@{$RNAseqs{$group}}[$i]." (~".(int($wcl))." reads)...\n";
		}else{
			print STDERR "[RNAseqCounting]  counting ".@{$RNAseqs{$group}}[$i]." (~".(int($wcl/1000000))."e+6 reads)...\n";
		}

		if($CPM){ $librarySize{@{$RNAseqs{$group}}[$i]}=0 }

		if(@{$RNAseqs{$group}}[$i]=~m/\.bam$/){
			open(FILE,"samtools view ".@{$RNAseqs{$group}}[$i]." |") or die("ERROR input bam file: $!");
		}else{
			open(FILE,"<",@{$RNAseqs{$group}}[$i]) or die("ERROR input sam file: $!");
		}

		my $count=0;
		my %ambiguous_reads;

		while(<FILE>){
			if($count++ % 10000 == 0){
				print STDERR "\033[J"." processed ~".(int(approxProgress(($count)/int($wcl))*10000)/100)."% of the reads ..."."\033[G"
			} # log
			chomp;

			if($_ eq "" || substr($_,0,1) eq "#"){next;}
			my @arr=split("\t",$_);
			if(scalar(@arr)<5){next;}

			#
			# strand specific counting ? ->
			#
			my $cur_strand = ( $arr[1] & hex("0x10") ) ? "-" : "+" ;
			# ^ equivalent formulation : if( $arr[1] & 16 )
			
			if( ( $cur_strand eq "+" ) && $strand eq "-"){next;} # skip wrong strand
			if( ( $cur_strand eq "-" ) && $strand eq "+"){next;}
			if( ( $arr[0] eq "*" || $arr[1]  eq "" ) || ( $arr[3]  eq "*" || $arr[3]  eq "" ) || ( $arr[9]  eq "*" || $arr[9]  eq "" )){next;}

			my $readname = $arr[0];
			my $chr = $arr[2];

			if(!$ambiguous && $_ !~ m/\bNH:i:/){ print STDERR "[WARNING]  Warning the NH:i:X flag is missing... All reads are now treated as uniquely mapped !!! \n"; $ambiguous = 1 }

			my $is_unique_mapped = ($_ =~ m/\bNH:i:1\b/);
			if( ($arr[1] & hex("0x4")) || # is unmapped 
				( !$ambiguous && !$is_unique_mapped ) # or read is multimapped (only if -ambiguous is not set)
				){ $stats{"ambiguous"}++; next; } # if read is mapped and unique /or ambiguous (if -ambiguous is set) then multimaps are counted too
			$stats{"counting read"}++;

			#
			# correct the length of the read with the CIGAR string 
			#
			my $lengthOfRead = length($arr[9]);
			my $CIGAR = $arr[5];
			my $numOfInsertions = 0;
			my $numOfDeletions = 0;
			while ($CIGAR =~ /([0-9]+)D/ig) { $numOfDeletions+=$1; } # D : deletion
			while ($CIGAR =~ /([0-9]+)[ISH]/ig) { $numOfInsertions+=$1; } # I : insertion , S+H : clipping (Segment of the query sequence that does not appear in the alignment)

			# missmatches (X) and matches (=) can be ignored
			my $alignmentLength = $lengthOfRead - $numOfInsertions + $numOfDeletions; # correct for CIGAR : - insertion and + deletions

			#
			# set the cur_pos according to the modus (if end + is set then cur_pos = leftmost ($arr[3]) + length of alignment )
			#
			my $cur_pos;
			my $other_cur_pos;
			if($position_modus eq "start"){
				$stats{"counting position"}++;
				if($cur_strand eq "-"){
					$cur_pos=$arr[3] + $alignmentLength -1; # right most position = start of read if - strand # minus 1 because the length includes the first position !
					$other_cur_pos=$arr[3]; 
				}else{
					$cur_pos=$arr[3]; 
					$other_cur_pos=$arr[3] + $alignmentLength -1;
				}
			}elsif($position_modus eq "end"){
				$stats{"counting position"}++;
				if($cur_strand eq "-"){
					$cur_pos=$arr[3]; 
					$other_cur_pos=$arr[3] + $alignmentLength -1;
				}else{
					$cur_pos=$arr[3] + $alignmentLength -1; # right most position = end of read if + strand
					$other_cur_pos=$arr[3]; 
				}
			}elsif($position_modus eq "coverage"){

				#
				# for coverage loop over all positions and increase the counter
				#
				$stats{"counting position"}+=$alignmentLength;
				for( $cur_pos=$arr[3] ; $cur_pos < $arr[3]+$alignmentLength ; $cur_pos++ ){

					if(!exists($hist{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]}) ){
						$hist{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]}=0;
					}
					if($output_additional =~ /unique/ && !exists($hist_additional{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]}) ){
						$hist_additional{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]} = 0;
					}

					# if($is_B_active==1){ # fill in 0 in the other group (for all files) at the current POS ($arr[3])
					# 	foreach my $group2 (keys %RNAseqs){
					# 		for(my $ii = 0 ; $ii < scalar(@{$RNAseqs{$group2}});$ii++){
					# 			# set to 0
					# 			if(!exists($hist{$group2}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group2}}[$ii]})){$hist{$group2}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group2}}[$ii]}=0;}
					# 			# lengths : 
					# 			if($output_additional =~ /unique/ && !exists($hist_additional{$group2}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group2}}[$ii]})){$hist_additional{$group2}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group2}}[$ii]}=0;}
					# 		}
					# 	}
					# }

					# increment the given position
					$hist{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]} += 1;
					if($output_additional =~ /unique/ && $is_unique_mapped ){$hist_additional{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]} += 1;}
				}

			}else{
				print $usage."\n\n ### ERROR ### wrong -position value\n";
    			exit(1);
			}

			if($position_modus eq "start" || $position_modus eq "end"){

				if($output_additional ne "" && !exists($hist_additional{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]}) ){
					if($output_additional =~ /unique/){
						$hist_additional{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]} = 0;
					}else{
						$hist_additional{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]} = "";
					}
				}

				if(($ambiguousUniform || $ambiguousDistribution) && !$is_unique_mapped){
					# ambiguously mapped read add to stack to analyse later
					# ( multimapped reads have all the same readname )
					$ambiguous_reads{$readname}{"$chr$;$cur_pos"}=1; 

					$stats{"counting ambiguously"}++;

					if($debug){print STDERR "found multimapped read $readname : chr=$chr at position=$cur_pos\n"}

				}else{
					
					$stats{"counting unique"}++;

					if( !exists($hist{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]}) ){
						$hist{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]}=0;
					}
					
					# if($is_B_active==1){ # fill in 0 in the other group (for all files) at the current POS ($arr[3])
					# 	foreach my $group2 (keys %RNAseqs){
					# 		for(my $ii = 0 ; $ii < scalar(@{$RNAseqs{$group2}});$ii++){
					# 			# set to 0
					# 			if( !exists($hist{$group2}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group2}}[$ii]})){
					# 				$hist{$group2}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group2}}[$ii]}=0;
					# 			}
					# 			# lengths :
					# 			if($output_additional ne "" && !exists($hist_additional{$group2}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group2}}[$ii]})){
					# 				if($output_additional =~ /unique/){
					# 					$hist_additional{$group2}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group2}}[$ii]}=0;
					# 				}else{
					# 					$hist_additional{$group2}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group2}}[$ii]}="";
					# 				}
					# 			}
					# 		}
					# 	}
					# }

					# increment the position by 1 (or less if -ambiguousUniform is set)
					$hist{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]} += 1;
					if($output_additional ne ""){
						if($output_additional =~ /unique/){ 
							$hist_additional{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]} += 1;
						}else{
							my $what = "";
							if($output_additional eq "length"){
								$what=$lengthOfRead;
							}elsif($output_additional eq "other"){
								$what=$other_cur_pos;
							}
							$hist_additional{$group}{$chr."\t".$cur_pos}{ $prefix.@{$RNAseqs{$group}}[$i]} .= ";$what";
						}
					}
				}
			}
			
			if($CPM && !($arr[1] & hex("0x100") ) ){ $librarySize{@{$RNAseqs{$group}}[$i]}++ } # only count main reads
		}

		print STDERR "                                           \n";# clean up progress output
		print STDERR "[RNAseqCounting] ".@{$RNAseqs{$group}}[$i]." reading done\n";

		# correct the ambiguous mapped reads (-ambiguousUniform, -ambiguousDistribution)
		if(($ambiguousUniform || $ambiguousDistribution)){
			print STDERR "[RNAseqCounting] ".@{$RNAseqs{$group}}[$i]." now doing the multimapping correction\n";
			foreach my $read (keys %ambiguous_reads) {
				# collect all the amounts of the given multimapping file for normalization
				if($debug){print STDERR "[DEBUG] $read is multimapping hitting ".(scalar keys %{$ambiguous_reads{$read}})." targets\n"}

				my $sum=0;
				if($ambiguousDistribution){
					foreach my $chrpos (keys %{$ambiguous_reads{$read}}) {
						my ($chr, $pos) = split $;, $chrpos;
						if(exists $multimapping{$chr}{"*"}){ $sum += $multimapping{$chr}{"*"}[1] } 
						elsif(exists $multimapping{$chr}{$pos}){ $sum += $multimapping{$chr}{$pos}[9] } 
					}
				}else{
					$sum = scalar keys %{$ambiguous_reads{$read}};
				}

				if($debug){print STDERR "[DEBUG] total sum=$sum"}

				if($sum == 0){next;}
				foreach my $chrpos (keys %{$ambiguous_reads{$read}}) {
					my ($chr, $pos) = split $;, $chrpos;
					if(exists $multimapping{$chr}{"*"}){ $hist{$group}{$chr."\t".$pos}{ $prefix.@{$RNAseqs{$group}}[$i] } += $multimapping{$chr}{"*"}[1]/$sum }
					elsif(exists $multimapping{$chr}{$pos}){ $hist{$group}{$chr."\t".$pos}{ $prefix.@{$RNAseqs{$group}}[$i] } += $multimapping{$chr}{$pos}[9]/$sum }
					else{ $hist{$group}{$chr."\t".$pos}{ $prefix.@{$RNAseqs{$group}}[$i] } += 1 / $sum }
				}
			}
		}
	}

	# mean hist
	foreach my $keyPOS (keys %{$hist{$group}}){

		$hist{$group}{$keyPOS}{"mean.".($CPM ? "" : "readcount")}=0;

		for(my $j = 0 ; $j < scalar(@{$RNAseqs{$group}});$j++){

			if(!exists($hist{$group}{$keyPOS}{ $prefix.@{$RNAseqs{$group}}[$j]})){
				$hist{$group}{$keyPOS}{ $prefix.@{$RNAseqs{$group}}[$j]}=0;
			}

			if($CPM){ # normalize CPM values 
				if($debug){
					print STDERR "$group $keyPOS : 1000000 * $hist{$group}{$keyPOS}{ $prefix.@{$RNAseqs{$group}}[$j]} / ($librarySize{@{$RNAseqs{$group}}[$j]})".(1000000 * $hist{$group}{$keyPOS}{ $prefix.@{$RNAseqs{$group}}[$j]} / ($librarySize{@{$RNAseqs{$group}}[$j]}))."\n";
				}
				$hist{$group}{$keyPOS}{ $prefix.@{$RNAseqs{$group}}[$j]}=1000000*$hist{$group}{$keyPOS}{ $prefix.@{$RNAseqs{$group}}[$j]}/($librarySize{@{$RNAseqs{$group}}[$j]}); 
			}
			
			if($numOfGroups==1){next;}
			$hist{$group}{$keyPOS}{"mean.".($CPM ? "" : "readcount")}+=$hist{$group}{$keyPOS}{ $prefix.@{$RNAseqs{$group}}[$j]};
		}

		if($numOfGroups==1){next;}
		$hist{$group}{$keyPOS}{"mean.".($CPM ? "" : "readcount")}/=scalar(@{$RNAseqs{$group}});
	}
}

my @keys = keys(%{$hist{"A"}});

print STDERR "[RNAseqCounting] RNAseqs reading done\n";

############
#if($sort){
#	if($is_B_active==1){ # sort if there are 2 groups 
#		@keys = sort { abs($hist{"A"}{$b}{"mean.".($CPM ? "CPM" : "readcount")}-$hist{"B"}{$b}{"mean.".($CPM ? "CPM" : "readcount")}) <=> abs($hist{"A"}{$a}{"mean.".($CPM ? "CPM" : "readcount")}-$hist{"B"}{$a}{"mean.".($CPM ? "CPM" : "readcount")}) } @keys;
#		print STDERR "[RNAseqCounting] sorting done\n"
#	}else{
#		@keys = sort { abs($hist{"A"}{$b}{"mean.".($CPM ? "CPM" : "readcount")}) <=> abs($hist{"A"}{$a}{"mean.".($CPM ? "CPM" : "readcount")}) } @keys;
#		print STDERR "[RNAseqCounting] sorting done\n"
#	}
#}
############

for(my $i = 0 ; $i < scalar(@keys) ; $i++){
	if($i==0){ # print HEADER
		print "# Seqname	Position";

		if($numOfGroups!=1){
			print "\tmean." . $prefix . ($CPM ? "" : "readcount");
		}
		foreach my $j (sort keys(%{$hist{"A"}{$keys[$i]}})){
			if($j=~m/^(?!mean)/){print "\t".$j;}
		}

		if($output_additional ne ""){
			foreach my $j (sort keys(%{$hist_additional{"A"}{$keys[$i]}})){
				print "\t$output_additional.".$j;
			}
		}

		print "\n";
	}

	#print DATA

	print $keys[$i]; # POS

	if($numOfGroups!=1){
		print "\t".$hist{"A"}{$keys[$i]}{"mean.".($CPM ? "" : "readcount")}; 
	}

	foreach my $j (sort keys(%{$hist{"A"}{$keys[$i]}})){ # raw readcount+CPM A
		if($j=~m/^(?!mean)/){print "\t".$hist{"A"}{$keys[$i]}{$j}};
	}

	if($output_additional ne ""){
		foreach my $j (sort keys(%{$hist_additional{"A"}{$keys[$i]}})){
			$hist_additional{"A"}{$keys[$i]}{$j}=~s/^;//;
			print "\t".$hist_additional{"A"}{$keys[$i]}{$j};
		}
	}

	print "\n";
}

sub makePercent{
	my $a = shift;
	return( (int($a*10000)/100)."%" );
}

print STDERR "[RNAseqCounting] I processed $numFiles file(s) and counted ".$stats{"counting read"}." (".makePercent( $stats{"counting read"}/($stats{"counting read"}+$stats{"counting ambiguously"}+$stats{"counting unique"}) ).") reads".
								($ambiguous ? 
									" (".$stats{'counting ambiguously'}." (".makePercent( $stats{"counting ambiguously"}/($stats{"counting read"}+$stats{"counting ambiguously"}+$stats{"counting unique"}) ).")"." ambiguously and $stats{'counting unique'}"." (".makePercent( $stats{"counting unique"}/($stats{"counting read"}+$stats{"counting ambiguously"}+$stats{"counting unique"}) ).")"." uniquely)" : 
							   		" while omitting ".$stats{"counting ambiguously"}." (".makePercent( $stats{"counting ambiguously"}/($stats{"counting read"}+$stats{"counting ambiguously"}+$stats{"counting unique"}) ).")"." reads because they are ").
								($ambiguous ? "unmapped" : "either unmapped or ambiguously mapped").
		  ".\n[RNAseqCounting] I counted a total of ".$stats{"counting position"}." positions.\n";

if($CPM){
	foreach my $group (keys %RNAseqs){
		for(my $j = 0 ; $j < scalar(@{$RNAseqs{$group}});$j++){	
			print STDERR "[RNAseqCounting] librarySize of ".@{$RNAseqs{$group}}[$j]." = ".($librarySize{@{$RNAseqs{$group}}[$j]})." (multimapped reads are counted once)\n";
		}
	} 
}


