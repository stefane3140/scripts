#!/usr/bin/env perl
# pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
my $version="0.0.4"; # last updated on 2021-10-02 23:08:44

my $usage = <<ENDUSAGE;
RNAseq_polyXtrimmer.pl        trimms polyX ends/starts (polyA, polyC, ...)
 
SYNOPSIS
 
RNAseq_polyXtrimmer.pl (options) INPUT

	INPUT	file/string/STDIN containing the sequences (for files : fasta,fq). - : STDIN. String input, e.g. "ACACA"

	optional:
		-polyX	defines the nucleotide that needs to be trimmed (A : polyA, C: polyC,...) [default:A]
		-min	miminal length of the polyX sequences [default:5]
		-greedy	if set, then the first sequence is cut of length > min
		-onlyt	if set, then only trimmed sequences are outputted
		-d		direction of cutting (3: at 3' end, 5: at 5'end) [default:3]
DESCRIPTION
 
	This script finds the longest continues sequences of the specified -poly NT and trimmes the sequences
	and everything following the match.
       
EXAMPLES
 
 	# 1. most simple call:

	RNAseq_polyXtrimmer.pl -polyX C - <<< "ACCCCGCCCCCCCGTGT"
	RNAseq_polyXtrimmer.pl -polyX C "ACCCCGCCCCCCCGTGT"
	RNAseq_polyXtrimmer.pl -polyX C myfile.fasta
	
	-> ACCCCG

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long, File::Basename
ENDUSAGE

my $query;
my $help;
my $min=5;
my $greedy=0;
my $onlyt=0;
my $direction="";
my $polyX="A";

GetOptions('help!'=>\$help,'h!'=>\$help,
	'min=s'=>\$min,
	'd=s'=>\$direction,
	'polyX=s'=>\$polyX,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	'greedy'=>\$greedy,
	'onlyt'=>\$onlyt);

my $INPUT = $ARGV[0];
shift(@ARGV);

if ($help || (scalar(@ARGV) == 0 && !$INPUT) ){ print $usage; exit(1); } # USAGE

if(! open(FH,'<',$INPUT) ) { # input string / STDIN
	if($INPUT eq "-"){ # string is - = STDIN
		foreach my $line (<>) { chomp($line); trimLine($line); }
	}else{  # input is just a string
		trimLine($INPUT);
	}
}else{ # input file (open was successful)
	while(<FH>){ chomp($_); trimLine($_); }
	close(FH);
}

our $lastIdx = -1;

sub trimLine{
	my $line = shift;
	if($direction eq "5"){$line = reverse $line}

	if($line =~ /^[#>+]/){print "$line\n"; return; }

	my $test = substr($line,0,5).substr($line,length $line - 5,5);
	if( $test !~ /^[ACGTUNX]+$/i ) { 
		if( $onlyt ) { 
			if($direction eq "5"){  print "".(reverse substr($line,0,$lastIdx))."\n"}
			else{ 					print substr($line,0,$lastIdx)."\n" 		 };
		} 
		return
	} # fastq things

	my $max_len = -1;
	my $max_idx = -1;

	for (my $i = 1 ; $i < length($line); $i ++ ){ 
		my $cur_idx = $i;
		my $len = 0;
		while(substr($line,$i,1) eq $polyX){
			$len++;
			$i++;
		}
		if($len > $min && $len > $max_len){
			$max_len = $len;
			$max_idx = $cur_idx;

			if($greedy){last;}
		}
	}

 	if($max_idx == -1 && $onlyt){return}
	if($max_idx == -1){print $line."\n"}
	else{
		$lastIdx = $max_idx;
		if($direction eq "5"){	print "".(reverse substr($line,0,$lastIdx))."\n"}
		else{ 					print substr($line,0,$max_idx)."\n"; 		 } # substr result, done
	}
}
