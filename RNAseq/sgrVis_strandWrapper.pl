#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()

our $PURPLE="\033[0;45m";
our $COLOR="\033[30;42m";
our $SHELL2="\033[30;47m";
our $NC="\033[0m"; # No Color

my $version="0.0.5"; # last updated on 2021-10-02 23:08:45

my $usage = <<ENDUSAGE;
sgrVis_strandWrapper.pl       This program counts (raw) the number of reads (start,end,coverage) on the + and - strands (generates 6 files for each sam file). The raw readcounts of the minus strand gets multiplied by -1.

SYNOPSIS
 
sgrVis_strandWrapper.pl --input SAMFILE

		--strand [+-b] "+" : plus , "-": minus , "+,-" : both strands independently, "b" : both added together	default:+,-
		--modus [start,end,coverage]	start:only start positions, end: only stop positions, coverage: all positions (=coverage) default:start,end,coverage
		--input -i	FILE NEEDS TO BE IN SAM FORMAT !!

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES Getopt::Long, File::Basename, sgrVis.R, RNAseq_counting_position.pl (https://gitlab.com/paulklemm_PHD/scripts)
ENDUSAGE

use Cwd;
my $wdir=getcwd;
my $help;
my $strand="+,-";
my $onlyCPM;
my @RNAseqs;
my $modus="start,end,coverage";

GetOptions('help!'=>\$help, 'h!'=>\$help, 
	'strand=s'=>\$strand,
	'modus=s'=>\$modus,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
#	'onlyCPM'=>\$onlyCPM,
	'input=s'=>sub { my ($opt_name, $opt_value) = @_; push(@RNAseqs,split(",",$opt_value)); },
	'i=s'=>sub { my ($opt_name, $opt_value) = @_; push(@RNAseqs,split(",",$opt_value)); },
);
if ($help || scalar(@RNAseqs)==0 || ( $strand !~ m/^[\+,-]+$/ ) || ( $modus !~ m/start|end|coverage/ )){
    print $usage;
    exit(1);
}

my $outputsgrs="";
foreach my $cur_strand (split(",",$strand)) {
	foreach my $cur_modus (split(",",$modus)){
		for(my $i = 0 ; $i < scalar(@RNAseqs);$i++){

			my $file=$RNAseqs[$i];
			my $com = "RNAseq_counting_position.pl --input ".($file)." ".($onlyCPM ? "--onlyCPM" : "")." --modus $cur_modus --strand $cur_strand | awk ".'\'{print $1"\t"$2"\t"'.($strand eq "+,-" && $cur_strand eq "-" ? '(-$3)' : '$3' ).'}\''." > ".($file).".$cur_modus.$cur_strand.sgr";

			print STDERR $com."\n";

			system($com);

			if($outputsgrs ne ""){$outputsgrs.="|";}
			$outputsgrs.="$file.$cur_modus.$cur_strand.sgr";
		}
	}
}

print "# please run the following command (replace FASTAFILE with the corresponding fasta file name):\n";
print "sgrVis.R -s '$outputsgrs' --plot line --fasta FASTAFILE\n";
