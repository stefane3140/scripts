#!/usr/bin/env sh
dd if=/dev/zero of=test_benchmark_write.iso bs=1M count=100 2>io.out && rm test_benchmark_write.iso;
perl -lne 'if($_=~m/ ([0-9.,]+ .?.B\/s)/){print $1;}' io.out;
rm io.out;
