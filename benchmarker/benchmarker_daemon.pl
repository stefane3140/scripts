#!/usr/bin/perl

#
# simple daemon that takes a command as input. 
#  - A child is forked that checks every 2 seconds the current rss,%cpu of the parent
#  - The parent (this script) will just execute the given command
# If parent does exit/terminate -> the child is killed (daemon vanishes)

my $update=2;
my $output="";
my $command="";
my $help=0;
my $version="0.0.3"; # last updated on 2021-10-02 23:08:45

my $usage = "
Usage: benchmarker_daemon.pl    simple perl daemon that watches the input command over time (memory usage, %cpu).

SYNOPSIS
     
    benchmarker_daemon.pl (options) cmd

        cmd    the input command given as a string. Note that the command cannot be a asynchronous (e.g. ending with &)...

    options
        
        -update=N   The refresh interval in seconds (default:$update)
        -output=F   The output file path, please put \" around the path (default:'' -> the standard error output)

DESCRIPTION

    A child is forked that checks every 2 seconds the current 'rss,%cpu' of the parent (output is printed to STDERR).
    The parent (this script) will just execute the given command.
    If parent does exit/terminate -> the child is killed (daemon vanishes).

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES pstree, tail, grep, xargs, awk
";

if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }

foreach my $a (@ARGV){
    if($a=~m/^-?-update=([0-9]+)/){$update=$1}
    elsif($a=~m/^-?-output="?(.+)"?$/){$output=$1}
    elsif($a=~m/^-?-(help|h)/){$help=1}
    else{$command.=" $a"}
}

if($help){
    print STDERR $usage; exit 0;
}
if($command eq "" || $update < 1 || scalar @ARGV < 1){
    print STDERR $usage."\nERROR : input file missing / invalid update option\n"; exit 1;
}

if ((my $pid = fork) == 0) { # make a fork of the current process 
    # this is now executed in parallel !
    my $FH;
    if($output ne ""){open($FH,">>",$output) || die $usage."\nERROR : $?\n";}

    if($output ne ""){print $FH "# time\tRSS\t%CPU\tCOMMAND\n"}else{print STDERR "# time\tRSS\t%CPU\tCOMMAND\n"}
    my $ppid = getppid; # parent pid 
    while (1) {
        sleep $update;
        unless (kill 0, $ppid) { # check if parent is still alive (kill 0 = no kill is send but checked for errors) 
            exit;
        } else {
            my $ret = `pstree -p $ppid | tail -n +2 | grep -o '[0-9]\+' | xargs ps -wo rss,pcpu,command:99999 | grep -v 'command:99999' | grep -v 'xargs ps -wo' | grep -v 'tail -n +2' | grep -v 'grep.*grep' | grep -v 'awk.*systime' | grep -v 'benchmarker_daemon.pl' | tail -n +2 | awk '{print systime()"\t"\$0}'`; 
            if($output ne ""){print $FH $ret;}else{print STDERR $ret}
        }
    }
    if($output ne ""){close($FH)}
}
my $exitcode = system($command); # call the command
if($exitcode == 0){
    exit(0);
}else{
    exit($exitcode >> 8); # correct the perl system call return code
}
