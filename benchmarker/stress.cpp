// pk
#include <stdio.h> 
#include <string.h>
#include <ctime>
#include <cstdlib>
#include <iostream>
// compile:
//
// g++ -fopenmp stress.cpp -o stress
//
// Usage: stress M T O
// K : total memory that should be allocated in KB (not per thread!)
// T : number of threads that are spawned
// O : time till termination

int main(int argc, char **argv)
{

    if(argc != 3){
        std::cerr << "Usage: stress M T O" <<std::endl <<
"K : total memory that should be allocated in KB (not per thread!) " <<std::endl <<
"T : number of threads that are spawned " <<std::endl <<
"O : time till termination (seconds) 

AUTHOR
    Paul Klemm

DEPENDENCIES
    std : <stdio.h>,<string.h>,<ctime>,<cstdlib>,<iostream>

" << std::endl;
    }

    long bytes;
    void *buf;
    bytes = atol(argv[1]) * 1024 ; // input -> KB
    //if(bytes > 1604*1024){ bytes-= 1604*1024; } // correct for own size + libraries
    buf = malloc(bytes);
    memset(buf, 0, bytes);
    printf(" "); // force update

    std::time_t timeA = std::time(0);
    int threads = atoi(argv[2]);
    std::time_t maxRuntime = atoi(argv[3]);
 
    float x = 0.9999f;
    #pragma omp parallel num_threads(threads) shared(timeA,maxRuntime)
   	while (1){if(std::time(0) - timeA > maxRuntime){break;}}
   	// just keep checking if the program should terminate or not -> 100% cpu usage

	return 0;
}

