# Table of Contents
<br><b>plots</b>
0. [plots/drawgffd3.pl](#plotsdrawgffd3pl) (v0.9.31)
1. [plots/easyUpsetr.R](#plotseasyupsetrr) 
2. [plots/csvDatatables.R](#plotscsvdatatablesr) 
3. [plots/draw_gff.R](#plotsdraw_gffr) 
<br><b>wrapper</b>
4. [wrapper/startRstudioDocker.sh](#wrapperstartrstudiodockersh) 
5. [wrapper/findLineWiseDiff.sh](#wrapperfindlinewisediffsh) 
<br><b>printCSV.pl</b>
6. [printCSV.pl](#printcsvpl) (v0.0.31)
<br><b>RNAfold</b>
7. [RNAfold/RNAup_slidingWindow.pl](#rnafoldrnaup_slidingwindowpl) (v0.0.8)
8. [RNAfold/getQiagen.pl](#rnafoldgetqiagenpl) (v0.0.3)
9. [RNAfold/varnacmd.pl](#rnafoldvarnacmdpl) (v0.1.6)
10. [RNAfold/greedyQiagenLnaOptimizer.pl](#rnafoldgreedyqiagenlnaoptimizerpl) (v0.0.5)
11. [RNAfold/ssps2svg.R](#rnafoldssps2svgr) 
12. [RNAfold/gridRNAcofold.pl](#rnafoldgridrnacofoldpl) (v0.0.5)
13. [RNAfold/plot_gridRNAcofold.R](#rnafoldplot_gridrnacofoldr) 
14. [RNAfold/predictTmValue.pl](#rnafoldpredicttmvaluepl) (v0.0.4)
<br><b>RNAseq</b>
15. [RNAseq/easyTopGO.R](#rnaseqeasytopgor) 
16. [RNAseq/sgrVis_addFoldToGFFWrapper.pl](#rnaseqsgrvis_addfoldtogffwrapperpl) (v0.0.5)
17. [RNAseq/DNAseqMappedAssembler.pl](#rnaseqdnaseqmappedassemblerpl) (v0.1.1)
18. [RNAseq/RNAseq_counting_position.pl](#rnaseqrnaseq_counting_positionpl) (v0.9.9)
19. [RNAseq/identifyPhred.pl](#rnaseqidentifyphredpl) (v0.0.4)
20. [RNAseq/RNAseq_counting_gene.pl](#rnaseqrnaseq_counting_genepl) (v0.4.36)
21. [RNAseq/sgrVis.R](#rnaseqsgrvisr) (v1.7.10)
22. [RNAseq/RNAseq_removePolyEnds.pl](#rnaseqrnaseq_removepolyendspl) (v0.0.4)
23. [RNAseq/alignNrdbReadsPlot.sh](#rnaseqalignnrdbreadsplotsh) 
24. [RNAseq/sgrVis_strandWrapper.pl](#rnaseqsgrvis_strandwrapperpl) (v0.0.5)
25. [RNAseq/easyDeseq2.R](#rnaseqeasydeseq2r) 
<br><b>alignment</b>
26. [alignment/MSAconsensus.pl](#alignmentmsaconsensuspl) (v0.0.4)
27. [alignment/MSAplot.pl](#alignmentmsaplotpl) (v0.1.21)
28. [alignment/pairwiseAverageNW.pl](#alignmentpairwiseaveragenwpl) 
29. [alignment/MSAscorer.pl](#alignmentmsascorerpl) (v0.0.13)
30. [alignment/easyphylogeny.py](#alignmenteasyphylogenypy) (v0.8.28)
31. [alignment/MSAget_indelMutationDiffof2seqs.pl](#alignmentmsaget_indelmutationdiffof2seqspl) (v0.0.3)
32. [alignment/MSAtrimmer.pl](#alignmentmsatrimmerpl) (v0.0.19)
33. [alignment/easyalignment.py](#alignmenteasyalignmentpy) 
<br><b>blast</b>
34. [blast/ARBG.sh](#blastarbgsh) 
35. [blast/easyblast.sh](#blasteasyblastsh) 
<br><b>benchmarker</b>
36. [benchmarker/benchmarker_daemon.pl](#benchmarkerbenchmarker_daemonpl) (v0.0.3)
37. [benchmarker/benchmark_write.sh](#benchmarkerbenchmark_writesh) 
<br><b>conversion</b>
38. [conversion/excel2csv.R](#conversionexcel2csvr) 
39. [conversion/embl2fasta.pl](#conversionembl2fastapl) (v0.0.25)
40. [conversion/tRNAscanse2gff.sh](#conversiontrnascanse2gffsh) 
41. [conversion/fasta2vector.pl](#conversionfasta2vectorpl) (v0.0.9)
42. [conversion/infernal2gff.sh](#conversioninfernal2gffsh) 
43. [conversion/faa2fna.pl](#conversionfaa2fnapl) (v0.0.4)
44. [conversion/fna2faaPlus.pl](#conversionfna2faapluspl) 
45. [conversion/clustal2fasta.pl](#conversionclustal2fastapl) (v0.0.9)
<br><b>igit.sh</b>
46. [igit.sh](#igitsh) (v0.5.10)
<br><b>test</b>
47. [test/myproject.result.opt.html](#testmyprojectresultopthtml) 
48. [test/test.sam.fna.nrdb.aln.html](#testtestsamfnanrdbalnhtml) 
49. [test/test.sam.gff.html](#testtestsamgffhtml) 
50. [test/test.sam.fna.nrdb.aln.sort.html](#testtestsamfnanrdbalnsorthtml) 
51. [test/test.sam.html](#testtestsamhtml) 
<br><b>fastaAndGff</b>
52. [fastaAndGff/easyNCBI.pl](#fastaandgffeasyncbipl) (v0.1.19)
53. [fastaAndGff/highlightFasta.pl](#fastaandgffhighlightfastapl) (v0.3.2)
54. [fastaAndGff/splitGFF.pl](#fastaandgffsplitgffpl) (v0.0.4)
55. [fastaAndGff/splitfasta.pl](#fastaandgffsplitfastapl) (v0.0.3)
56. [fastaAndGff/getgff.pl](#fastaandgffgetgffpl) (v0.2.0)
57. [fastaAndGff/rc.pl](#fastaandgffrcpl) (v0.0.2)
58. [fastaAndGff/compareMummerFasta.pl](#fastaandgffcomparemummerfastapl) (v0.7.6)
59. [fastaAndGff/compareGffFasta.pl](#fastaandgffcomparegfffastapl) (v0.3.16)
60. [fastaAndGff/getGeneLength.pl](#fastaandgffgetgenelengthpl) (v0.0.11)
61. [fastaAndGff/easybcfconsensus.sh](#fastaandgffeasybcfconsensussh) 
62. [fastaAndGff/grepfasta.pl](#fastaandgffgrepfastapl) (v0.0.39)
63. [fastaAndGff/clusterStrings.pl](#fastaandgffclusterstringspl) (v0.0.19)
64. [fastaAndGff/grep_k_neighbourhood.pl](#fastaandgffgrep_k_neighbourhoodpl) (v0.0.6)
65. [fastaAndGff/newickToAverageDistance.pl](#fastaandgffnewicktoaveragedistancepl) 

# Recent changes
 CHANGELOG                  |    6 +<br>
 alignment/MSAplot.pl       |   35 +-<br>
 alignment/easyalignment.py |   18 +-<br>
 alignment/easyphylogeny.py |   15 +-<br>
 6 files changed, 120 insertions(+), 2442 deletions(-)<br>

# Scripts
## plots/drawgffd3.pl
    drawgffd3.pl        produces a colored representation of a given MSA
     
    SYNOPSIS
     
    drawgffd3.pl (options) GFFFILE 

    	GFFFILE	gff file

    	-group= sets the grouping/column for the different lanes/colors (default:source)

    OUTPUT
    	Static HTML page (STDOUT) using d3 to plot the entries of the gff entry.
    	Different sources are plotted as different lanes with different colors.

    VERSION v0.9.31
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## plots/easyUpsetr.R
    Usage: easyUpsetr.R    simple interface to UpSetR

    SYNOPSIS

    easyUpsetr.R -i FILEA,FILEB,...

      FILE(...) : 2 or more input files, entries are compared linewise between these files
      FILE : if only one input file is given, the first column represents the entities and the second the comparators (= file names for multiple FILEs)
      output plot and intersection csv file is written to files with prefix of --project

    VERSION v0.0.12
    AUTHOR Paul Klemm
    DEPENDENCIES ggplot2,UpSetR,readr,reshape2,optparse,svglite,grid


    Options:
    	-p CHARACTER, --project=CHARACTER
    		output prefix

    	-t CHARACTER, --title=CHARACTER
    		output plot title

    	-x CHARACTER, --width=CHARACTER
    		output witdth

    	-y CHARACTER, --height=CHARACTER
    		output height

    	-i CHARACTER, --input=CHARACTER
    		comma separated list of files

    	-h, --help
    		Show this help message and exit



---
## plots/csvDatatables.R
    also installing the dependencies openssl, httr

    trying URL 'https://cloud.r-project.org/src/contrib/openssl_1.4.5.tar.gz'
    Content type 'application/x-gzip' length 1311351 bytes (1.3 MB)
    ==================================================
    downloaded 1.3 MB

    trying URL 'https://cloud.r-project.org/src/contrib/httr_1.4.2.tar.gz'
    Content type 'application/x-gzip' length 159950 bytes (156 KB)
    ==================================================
    downloaded 156 KB

    trying URL 'https://cloud.r-project.org/src/contrib/plotly_4.10.0.tar.gz'
    Content type 'application/x-gzip' length 3842199 bytes (3.7 MB)
    ==================================================
    downloaded 3.7 MB

    * installing *source* package openssl ...
    ** package openssl successfully unpacked and MD5 sums checked
    ** using staged installation
    Using PKG_CFLAGS=
    --------------------------- [ANTICONF] --------------------------------
    Configuration failed because openssl was not found. Try installing:
     * deb: libssl-dev (Debian, Ubuntu, etc)
     * rpm: openssl-devel (Fedora, CentOS, RHEL)
     * csw: libssl_dev (Solaris)
     * brew: openssl@1.1 (Mac OSX)
    If openssl is already installed, check that 'pkg-config' is in your
    PATH and PKG_CONFIG_PATH contains a openssl.pc file. If pkg-config
    is unavailable you can set INCLUDE_DIR and LIB_DIR manually via:
    R CMD INSTALL --configure-vars='INCLUDE_DIR=... LIB_DIR=...'
    -------------------------- [ERROR MESSAGE] ---------------------------
    tools/version.c:1:10: fatal error: openssl/opensslv.h: No such file or directory
        1 | #include \<openssl/opensslv.h\>
          |          ^~~~~~~~~~~~~~~~~~~~
    compilation terminated.
    --------------------------------------------------------------------
    ERROR: configuration failed for package openssl
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/openssl
    ERROR: dependency openssl is not available for package httr
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/httr
    ERROR: dependency httr is not available for package plotly
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/plotly

    The downloaded source packages are in
    	/tmp/RtmpCi6qDy/downloaded_packages
    Warning messages:
    1: In install.packages(f, lib = Sys.getenv("R_LIBS_USER"), repos = "https://cloud.r-project.org/") :
      installation of package openssl had non-zero exit status
    2: In install.packages(f, lib = Sys.getenv("R_LIBS_USER"), repos = "https://cloud.r-project.org/") :
      installation of package httr had non-zero exit status
    3: In install.packages(f, lib = Sys.getenv("R_LIBS_USER"), repos = "https://cloud.r-project.org/") :
      installation of package plotly had non-zero exit status
    4: In library(package, lib.loc = lib.loc, character.only = TRUE, logical.return = TRUE,  :
      there is no package called plotly
    Usage: this program converts a given csv/tsv table to DT:tatables (html). 

    AUTHOR Paul Klemm
    VERSION v0.0.20
    DEPENDENCIES optparse,crosstalk,DT,plotly,htmlwidgets,readr,optparse


    Options:
    	-i CHARACTER, --input=CHARACTER
    		input table (csv or tsv). E.g. '--input test.csv'. Use - for STDIN

    	-f, --firstRowIsNotHeader
    		sets the header to X1, X2, ... and enables automatic column namings for files ending in .gff, .sam and .bla format

    	-t CHARACTER, --type=CHARACTER
    		sets the header to 'gff', 'sam' and 'blast'. If the header contains the first row, the first row is appended to the table.

    	-s CHARACTER, --sep=CHARACTER
    		sets delimiter

    	-o CHARACTER, --open=CHARACTER
    		open directly with [firefox,safari,chrome,...]

    	-h, --help
    		Show this help message and exit



---
## plots/draw_gff.R
    draw_gff.R    simple script to plot gff formats (gggenes)

    SYNOPSIS

    boxplot.R GFFFILE

    	GFFFILE : input gff file

    	genes are named according to the product=... attribute
    	(if product= is not set, then name= or ID= is used)

    EXAMPLES

    draw_gff.R somegenes.gff

    VERSION v0.0.9
    AUTHOR Paul Klemm
    DEPENDENCIES ggplot2, gggenes

    Error: 
    Execution halted

---
## wrapper/startRstudioDocker.sh
     please install docker and bioconductor/bioconducter_docker container first (https://docs.docker.com/engine/install/ubuntu/)
     sudo docker pull bioconductor/bioconductor_docker:devel

     use run -d for background
     use -it for interactive session
     add bash at the end to start in a shell instead of starting /init script

    echo "# visit >> localhost:8787 << with user=rstudio pw=bioconductor123#"
    sudo docker run -p 127.0.0.1:8787:8787 --rm --mount "type=bind,src=/home/$(id -un),dst=/home/$(id -un)" --mount "type=bind,src=/home/$(id -un)/R.dockerlibs,dst=/home/rstudio/R.dockerlibs" -e PASSWORD=bioconductor123# bioconductor/bioconductor_docker:devel bash -c "sudo echo \".libPaths(c(\\\"/home/rstudio/R.dockerlibs\\\",.libPaths()))\" >/home/rstudio/.Rprofile && usermod -u $(id -u) rstudio && groupmod -g $(id -g) rstudio  && ln -s /home/$(id -un) /home/rstudio/$(id -un) && su rstudio && /init"

     --mount "type=bind,src=/home/$(id -un),dst=/home/$(id -un)" 
     -> mounts the home directory to /home. Later a symbolic link is created in the home of rstudio pointing to this folder.

     --mount "type=bind,src=/home/$(id -un)/R.dockerlibs,dst=/home/rstudio/R.dockerlibs"
     -> mounts the R.docklibs, all libraries should be installed here automatically (install.packages...)

     usermod -u $(id -u) rstudio && groupmod -g $(id -g) rstudio
     -> this will fix permission issues with the share folders. The rstudio user has now the same UID as you

     su rstudio && /init
     -> starts the rstdio server, ...

     sudo docker ps
     sudo docker attach ID # if you run with -d and -it
     sudo docker stop ID

     start a bash of an existing/running docker container 
     sudo docker exec -it IDHERE bash

     Communicate the proxy configuration to docker
     cat /etc/systemd/system/docker.service.d/http-proxy.conf
     [Service]
     Environment="HTTP_PROXY=http://proxy.example.com:80/"
     ...
     sudo systemctl daemon-reload && sudo systemctl restart docker
#


---
## wrapper/findLineWiseDiff.sh
    line	-h	
    paste: invalid option -- 'h'
    Try 'paste --help' for more information.

---
## printCSV.pl
    printCSV.pl	prints a given tab/comma/semicolon separated file to the terminal (maximal width is automatically adjusted).

    SYNOPSIS

    	printCSV.pl (options) FILENAME
    	printCSV.pl (options) -

    OPTIONS

    	FILENAME 			can also be - to idicate STDIN.

    	-width NUMBER, -w NUMBER	the maximal width of the table, can be absolute values or relative values (0.5 = half of the maximal window width) [default:auto detect maximal window width]
    	-noheader, -h			if set, every row is treated independently (otherwise the first row determines the with of each column)
    	-k a-b				only show columns from a to b, e.g. 0-3 shows the first 4 columns (0,1,2,3). a,b are 0-index numbers.
    	-k a,b,c...			only show specific columns from a,b,c,.. a,b are 0-index numbers OR column names (specified in first line)
    	-skip				skip first x lines
    	-d delim			specify the delimiter of the input table [default:auto detect]
    	-plain,-p			output plain csv table (only useful if you select some columns with -k as well)
    	-html,-h			generate a html table (-plain suppresses the head-boilercode)
    	-header				generate headers from typical formats (blast6,gff,vcf,sam)

    	NOTE: reoccuring header lines are ignored.

    EXAMPLE

    	$ cat input.csv

     Species       Genes   Alg.-Conn.      C.faa   C2.faa  E.faa   L.faa   M.faa
    2       5       0.16    *       *       *       L_641,L_643     M_640,M_642,M_649
    4       6       0.115   C_12,C_21       *       E_313,E_315     L_313   M_313
    3       6       0.301   C_164,C_166,C_167,C_2   *       *       L_2     M_2
    2       4       0.489   *       *       *       L_645,L_647     M_644,M_646
    3       3       0.312   *       *       E_367   L_319   M_319
    4       5       0.165   C_63,C_22       *       E_19    L_19    M_19


    	$ printCSV.pl input.csv
    	$ cat input.csv | printCSV.pl -

    -------------+------------+------------+------------+------------+------------+------------+------------
       Species   |   Genes    | Alg.-Conn. |   C.faa    |   C2.faa   |   E.faa    |   L.faa    |   M.faa    
    -------------+------------+------------+------------+------------+------------+------------+------------
          2      |     5      |    0.16    |     *      |     *      |     *      |L_641,L_643 |M_640,M_642#
          4      |     6      |   0.115    | C_12,C_21  |     *      |E_313,E_315 |   L_313    |   M_313    
          3      |     6      |   0.301    |C_164,C_166#|     *      |     *      |    L_2     |    M_2     
          2      |     4      |   0.489    |     *      |     *      |     *      |L_645,L_647 |M_644,M_646 
          3      |     3      |   0.312    |     *      |     *      |   E_367    |   L_319    |   M_319    
          4      |     5      |   0.165    | C_63,C_22  |     *      |    E_19    |    L_19    |    M_19    

          The '#' indicates that the cell is cut off due to size limitations

    VERSION v0.0.31
    AUTHOR Paul Klemm
    DEPENDENCIES perl : POSIX, Getopt::Long

---
## RNAfold/RNAup_slidingWindow.pl

    RNAup_slidingWindow.pl     performes RNAup in k sized windows with step size s.

    SYNOPSIS

    RNAup_slidingWindow.pl (options) FASTA/-

    	FASTA or - (= STDIN) 
    		The input should have at least 2 sequences.

    	-w : window size (default 1500)
    	-s : step size (default 750)
    	--cpus, -t : number of threads (default: 1)

    DESCRIPTION

    	A k sized window slides in s steps across the input sequence and RNAup is perfomed and
    	the optimal/minimal binding energy isprinted.
    	The result should correspond to a full RNAup call if there are no long-range 
    	interaction (longer than window size 1500).

    	!! Either the first input or the other input sequences needs to be longer than the window size w !!
    	
    	The RNAup modus is --interaction_first --no_output_file.

    VERSION v0.0.8
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Cwd qw(abs_path cwd), List::Util qw(max), File::Basename, Thread::Queue, RNAup (viennarna package)

---
## RNAfold/getQiagen.pl
    [getQiagen.pl] loaded input sequence : '-H'
     sequence	rnaTm	dnaTm	hybrydizationScore	hybridizationLines	secondaryStructureScore	secondaryStructureLines
    -H	-	-	0	-:-	0	-H:  

---
## RNAfold/varnacmd.pl
    varnacmd.pl  a simple varna interface

    SYNOPSIS
        varnacmd.pl (options)

        -seq SEQ             : the sequence (varnas -sequenceDBN)
        -fold FOLD           : the fold information (varnas -structureDBN)
        -highlight HIGHLIGHT : varnas -highlightRegion. 
                               e.g. 1-15:fill=#cfcfcf;5-20:outline=#cfcfcf
                               -> highlights pos 1-16 (1-based index!)
                               or you can supply one subsequence that you want to highlight (or multiple delimitted by ;)
        -output OUTPUTNAME   : the output name (default:varna.svg). Needs a suffix like png,jpg,svg,... !
        -resolution R        : resolution of the output image (default:resolution)
        -varnadir DIR        : varna directory (default:cwd and /usr/local/bin/)
        --ignore NUM         : ignore the first NUM positions (secondary structure)
                               if NUM is negative: ignore the left most NUM positions
        --anno STR           : annotation vector (;-separated)
                               e.g. :type=B,anchor=2;texthere:type=B,anchor=3;...
                               (1-based index)

        instead of seq and fold you can also provide a fold file as given by RNAfold through the STDIN (RNAfold myseq.fasta | varnacmd.pl)

        NOTE : svg images can be easily converted to pdf using 'inkscape --export-pdf=output.pdf input.svg'

    VERSION v0.1.6
    AUTHOR Paul Klemm
    DEPENDENCIES java : VARNA*.jar (http://varna.lri.fr/index.php?lang=en&css=varna&page=downloads), perl : File::Basename

---
## RNAfold/greedyQiagenLnaOptimizer.pl

    greedyQiagenLnaOptimizer.pl        simple gradient descent algorithm for finding the optimal LNA modifications using the qiagen web API

    SYNOPSIS

    greedyQiagenLnaOptimizer.pl (options) RNA

    	RNA sequence given as string (U are converted to T automatically)

    	-k : set the number of restarts (default:5)

    DESCRIPTION

    	Simple greedy optimization:

    	for each -k restart do:
    		load the input sequence with all position being unmarked

    		while there is a unmarked position:
    	
    			add a modification to a random position that is unmodified and unmarked and mark this position
    			send this to qiagen 
    			if hybrydizationScore < 20 AND secondaryStructureScore < 20 AND rnaTm improves:
    				save this modification
    				unmark all unmodified positions

    		report this optimum

    EXAMPLES

        $ perl greedyQiagenLnaOptimizer.pl 'GCACAAGAGUAGACU'
    	
    	[greedyQiagenLnaOptimizer.pl] loaded input sequence : GCACAAGAGTAGACT
    	[greedyQiagenLnaOptimizer.pl] started with max_hybrydizationScore=20 and max_secondaryStructureScore=20
    	# sequence	rnaTm	hybrydizationScore	hybridizationLines	secondaryStructureScore	secondaryStructureLines
    	GCACAAGAGTAGACT	43	17	AGTAGACT:TCAGATGA	8	GCACAAGAGTAGACT:  ((    ))     
    	+GC+AC+AA+GAGTAGACT	71	20	AGAGTAGACT:TCAGATGAGA	12	GCACAAGAGTAGACT:  (   (      ))
    	+GC+AC+A+AG+AGT+AGACT	79	19	AGTAGACT:TCAGATGA	10	GCACAAGAGTAGACT:  ((    ))     
    	G+C+A+C+A+AG+AGT+A+G+ACT	88	20	AGACT:TCAGA	14	GCACAAGAGTAGACT: (         )   
    	GC+A+CA+AG+AGT+AGACT	81	19	AGTAGACT:TCAGATGA	14	GCACAAGAGTAGACT:  ((    ))    

    	# First line (GCACAAGAGTAGACT) corresponds to the input sequence with rnaTm,... values
    	# next lines show up to -k optimums for each restart 

        The secondary structure of an oligonucleotide with a score below 20 is unlikely to be stable at room temperature. 
        Scores above 30 are likely to produce secondary structures that are stable at room temperature.

    SEE ALSO
    	the geneglobe.qiagen API for optimizedoligo: 
    		https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?secondaryStructure=true&sequence=SEQUENCE

    	the geneglobe.qiagen API for tmprediction: 
    		https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=SEQUENCE

    VERSION v0.0.5
    AUTHOR Paul Klemm
    DEPENDENCIES perl : List::Util qw/shuffle/, perl : wget

---
## RNAfold/ssps2svg.R
    Loading required package: ggplot2
    Loading required package: svglite
    Loading required package: ggrepel
    ssps2svg.R    convert postscript files generated by viennas RNAfold, RNAcofold... to SVG.

    SYNOPSIS

    ssps2svg.R *.ps (options)

      needs to be *ss.ps or *dp.ps files (secondary structure or dotplot) of RNAfold/RNAup/RNAcofold... 
      You can also provide a folder and ssps2svg searches for all ss.ps files inside

      options (needs to be at the end of the line and in that order)
        NUMBER    size factor, scales the output svg image
        -title    if set, then the file name is printed with +ggtitle()
        SEQUENCE  highlighted sequence

    EXAMPLE

        # simple call for all subdirectories 
        ssps2svg.R */*ss.ps
        # include filename as title
        ssps2svg.R */*ss.ps -title
        # scale by factor 10
        ssps2svg.R */*ss.ps 10
        # include title and highlight 'GAGAGAGAGA'
        ssps2svg.R */*ss.ps -title GAGAGAGAGA
        # all together
        ssps2svg.R */*ss.ps 10 -title GAGAGAGAGA

    VERSION v0.0.4
    AUTHOR Paul Klemm
    DEPENDENCIES ggplot2,svglite,ggrepel


---
## RNAfold/gridRNAcofold.pl

    gridRNAcofold.pl        This program executes RNAcofold on a grid of parameters (different monomer concentrations and temperatures). 

    SYNOPSIS

    gridRNAcofold.pl (options) FASTA

    	FASTA : input with 2 sequences as one entry in the RNAcofold format (ACACA...&...ACACA)

    DESCRIPTION

    	temperature grid: 
    		from 0C to 100C in 5C steps

    	concentration grid:
    		input sequence from 1e-06 to 0.2 increased by the factor 1.5 in each step
    		rev.comp.(input) sequence from 1e-06 to 0.2 increased by the factor 1.5 in each step

    EXAMPLES

    	input.fna file with:
    	>test
    	ACAGCGTTGTGTAAGA&AAAAGTGTGTGTGAAA

    	produces a output file input.fna.out : 

    	# T	concA	concB	AB, AA, BB, A, B
    	0 1e-06 1e-06 0.00000 0.24983 0.00001 0.00033 0.49998
    	0 1e-06 1.5e-06 0.00000 0.19987 0.00002 0.00027 0.59996
    	0 1e-06 2.25e-06 0.00000 0.15374 0.00004 0.00021 0.69223
    	0 1e-06 3.375e-06 0.00000 0.11421 0.00006 0.00015 0.77131
    	(...)

    VERSION v0.0.5
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Cwd qw(abs_path cwd), perl : File::Basename, RNAcofold (viennarna package)

---
## RNAfold/plot_gridRNAcofold.R
    Loading required package: readr
    Loading required package: ggplot2
    Loading required package: reshape2
    Error in is.connection(x) : object 'ARGV' not found
    Calls: data.frame ... read_delim -> read_delimited -> source_name -> is.connection
    Execution halted

---
## RNAfold/predictTmValue.pl

    predictTmValue.pl     predict melting temperature Tm.

    SYNOPSIS

    predictTmValue.pl (options) FASTA/STDIN

    	FASTA/STDIN (from file or string argument)

    	--dnaTm, -d : corrects for dna Tm value : = 0.72587 * temp + 0.53782
    	--rnaTm, -r : corrects for rna Tm value : = 0.83807 * temp - 13.26109
    	--full, -f : full output for debugging

    DESCRIPTION

    	RNAcofold wrapper for searching of the self-hybridization temperature (-T), #
    	such that the dimer concentration c(A-rc(A)) equals about 
    	half of the predicted ensemble.

    EXAMPLES

    	# predict a RNA Tm value:
    	$ predictTmValue.pl -r ACACAGATAGA
    	30.21379125

    	# predict a plain Tm value:
    	$ predictTmValue.pl ACACAGATAGA
    	51.875

    VERSION v0.0.4
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Cwd qw(abs_path cwd), File::Basename, RNAcofold (viennarna package)

---
## RNAseq/easyTopGO.R
    installing topGO...
    Installing package into /home/paul/R/x86_64-pc-linux-gnu-library/3.6
    (as lib is unspecified)
    Warning messages:
    1: package topGO is not available (for R version 3.6.3) 
    2: In library(package, lib.loc = lib.loc, character.only = TRUE, logical.return = TRUE,  :
      there is no package called topGO
    Usage: topGO analysis wrapper, either set --deseqlfc OR --regulated_genes and --omega_genes


    Options:
    	-d FILE, --deseqlfc=FILE
    		the deseq_lfc.csv file from simpleDeseq.R containing the columns Gene;comparison;l2fc;baseMean;padj

    	-f NUMBER, --l2fcfilter=NUMBER
    		the cutoff for filtering the --deseqlfc table (l2fc), 1 -> only genes above 1, -1 -> only genes below -1 [default: 1]

    	-r REGULATED_GENES, --regulated_genes=REGULATED_GENES
    		list of regulated genes, ',' or ' ' or ';'-separated

    	-o OMEGA_GENES, --omega_genes=OMEGA_GENES
    		list of all possible genes, ',' or ' ' or ';'-separated

    	-g GOCLASS, --goclass=GOCLASS
    		goclass to analyse one or multiple of BP,CC,MF, ','-separated [default: BP,CC,MF]

    	-t TITLE, --title=TITLE
    		plot title [default: easy]

    	-c, --correct
    		do typical cleanup of input data (trim ",_,...)

    	--org=CHARACTER
    		the species annotation https://www.bioconductor.org/packages/2.10/data/annotation/ [default: org.Hs.eg.db]

    	-v, --version
    		

    	-h, --help
    		Show this help message and exit



---
## RNAseq/sgrVis_addFoldToGFFWrapper.pl
    sgrVis_addFoldToGFFWrapper.pl       Auxillary program to run sgrVis.R --varna 

    SYNOPSIS
     
    sgrVis_addFoldToGFFWrapper.pl --gff GFFFILE --fasta GENOMEFILE

    		adds secondary struture and sequence to the gff entries for the use in sgrVis.R

    		# first call this program
    		sgrVis_addFoldToGFFWrapper.pl --gff GFFFILE --fasta GENOMEFILE >GFFFILE.sgrvis.gff

    		# then feed the new gff to sgrVis.R
    		sgrVis.R -g GFFFILE.sgrvis.gff -f GENOMEFILE ...

    VERSION v0.0.5
    AUTHOR Paul Klemm
    DEPENDENCIES Getopt::Long, File::Basename, RNAfold (vienna package), getgff.pl 

---
## RNAseq/DNAseqMappedAssembler.pl
    DNAseqMappedAssembler.pl        updates a genome from mapped reads

    SYNOPSIS

    DNAseqMappedAssembler.pl (options) FILE.sam/bam

    DESCRIPTION
    	-gff,-g : reference GFF file
    	-mapq : minimum MAPQ score [default:30]
    	-len : minimum read length [default:30]
    	-depth : minimum depth [default:50]

    VERSION v0.1.1
    AUTHOR Paul Klemm
    DEPENDENCIES Perl Getopt::Long, File::Basename

---
## RNAseq/RNAseq_counting_position.pl [![pipeline status](https://gitlab.com/paulklemm_PHD/scripts/badges/master/pipeline.svg)](https://gitlab.com/paulklemm_PHD/scripts/pipelines)
    RNAseq_counting_position.pl        calculates read coverage/start/end distribution for each position with respect to the CIGAR string.

    SYNOPSIS
     
    RNAseq_counting_position.pl (options) FILE1.sam/bam FILE2.sam/bam FILE3.sam/bam ...

    	--strand [+-b] 
    		strand: +:plus or -:minus or b:both added together	[default:b]

    	--modus [start,end,coverage]
    		start: start positions, 
    		end: stop positions (CIGAR corrected),
    		coverage: all positions (=coverage) [default:coverage]

    	--CPM  
    		cpm values are printed instead of the raw readcounts

    	--additonal [length,other,unique]
    		output additonally the information in an additional column (;-separated)
    		length : raw read length without CIGAR (requires --modus start or end),
    		other : if --modus start, then this is the end position and vice versa (requires --modus start or end)
    		unique : outputs the unique countings too (requires --ambiguous*)

    	--ambiguousUniform
    		If set, all mapped reads are counted but are equally distributed among all hits. 
    		So if a read maps to 3 target positions, then each position gets +1/3

    	-ambiguousDistribution FILE
    		Same as --ambiguousUniform but the input file specifies the relative quantities of the targets.
    		FILE : A gff file with an additional column (10th) with the amount of that entry (other ambiguous positions are dismissed)
    		FILE : A tab file with first target name (sam column 3) and second the amount of that entry (all positions are used for that target)

    DESCRIPTION
    	
    	- input can be sam or bam (samtools is needed for bam)
    	- output is written to STDOUT (sgr format)
    	- unmapped reads are always omitted (bit flag 0x4)
    	- ambiguously mapped reads are omitted by default (NH:i:X, X>1)
    	- if NH:i:X read attribute is missing all reads are treated as uniquely mapped
    		For Bowtie2 use : samtools view -q 1 input > unique.sam (Bowtie2 assigns a MAPQ of 1 to unique reads)
    		Other : grep -v 'XT:A:R' input.sam or grep -v 'XA:Z:' input.sam 

    	- CIGAR string is used for end positions/coverage !
    	- The -strand and -modus option: 
    		-strand + -modus start => only take reads from PLUS strand, take the left most mapping position of the read
    		-strand - -modus start => only take reads from MINUS strand, take the right most mapping position of the read
    		-strand b -modus end => take the right most mapping position of the read of PLUS strand, and left most if MINUS

    	- RNASeq sam/bam input files are assumend to be 1based

    EXAMPLES
    	# total coverage ignoring the strands:
    	-modus coverage -strand b
    		counts        :  112121211 1221  222 111
    		Reads         :    |=> |=>  <=|  |=>        
    		Reads         :  |=> <=|   |=>   |=> |=>       

    	# ignore strand information of the reads:
    	-modus start -strand b
    		counts        :  1 1   2   1  1  2   1
    		Reads         :    |=> |=>  <=|  |=>        
    		Reads         :  |=> <=|   |=>   |=> |=>       

    	# count start positions of reads of PLUS strand:
    	-modus start -strand +
    		counts        :  1 1   1   1     2   1
    		Reads         :    |=> |=>  <=|  |=>        
    		Reads         :  |=> <=|   |=>   |=> |=>    

    	# same for MINUS strand:
    	-modus start -strand -
    		counts        :        1      1
    		Reads         :    |=> |=>  <=|  |=>        
    		Reads         :  |=> <=|   |=>   |=> |=>  

    	# now the end positions:
    	-modus end -strand -
    		counts        :      1      1
    		Reads         :    |=> |=>  <=|  |=>        
    		Reads         :  |=> <=|   |=>   |=> |=>    

    	# total coverage only for the MINUS strand:
    	-modus coverage -strand -
    		counts        :      111    111      
    		Reads         :    |=> |=>  <=|  |=>        
    		Reads         :  |=> <=|   |=>   |=> |=>       

    VERSION v0.9.9
    AUTHOR Paul Klemm
    DEPENDENCIES Getopt::Long, File::Basename

---
## RNAseq/identifyPhred.pl
    identifyPhred.pl        Determines the phred format (Sanger,Illumina,...) for a given fastq file
     
    SYNOPSIS
     
    identifyPhred.pl FASTQ

    	FASTQ : the fastq formatted file.

    DESCRIPTION
     
    	The program looks at up to 10k reads and excludes all formats that do not agree with the given phreds of these reads. Then the result is printed.

     	Possible formats : 

    		S = Sanger Phred+33 (!"#$%&'()*+,\-.\/0123456789:;<=>?@ABCDEFGHI)
    		X = Solexa Solexa+64 (;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\[\\]^_`abcdefgh)
    		I = Illumina 1.3+ Phred+64 (@ABCDEFGHIJKLMNOPQRSTUVWXYZ\[\\]^_`abcdefgh)
    		J = Illumina 1.5+ Phred+64 (BCDEFGHIJKLMNOPQRSTUVWXYZ\[\\]^_`abcdefghi)
    		L = Illumina 1.8+ (very close to S!) Phred+33 (!"#$%&'()*+,\-.\/0123456789:;<=>?@ABCDEFGHIJ)
    	 
     	Source : https://en.wikipedia.org/wiki/FASTQ_format

    SEE ALSO https://en.wikipedia.org/wiki/FASTQ_format
    VERSION v0.0.4
    AUTHOR Paul Klemm

---
## RNAseq/RNAseq_counting_gene.pl [![pipeline status](https://gitlab.com/paulklemm_PHD/scripts/badges/master/pipeline.svg)](https://gitlab.com/paulklemm_PHD/scripts/pipelines)
    RNAseq_counting_gene.pl        converts given sam/bam files to a count table. The output format is sgr.

    SYNOPSIS

    RNAseq_counting_gene.pl (options) FILE1.sam/bam FILE2.sam/bam FILE3.sam/bam ...

    	-outputReads	
    		If set, then reads are outputted for each target (for each gff entry, or mapped target). 
    		WARNING: this can produce a lot of files and it can take a long time...

    	--ambiguousUniform
    		If set, all mapped reads are counted but ambiguously mapped reads are equally distributed among all hits. 
    		So if a read maps to 3 targets, then each target gets +1/3
    	
    	-ambiguousDistribution FILE
    		Same as --ambiguousUniform but the input file specifies the relative quantities of the targets.
    		FILE : a mapping (tab separated) of the name of the target and the amount of that entry (other targets are dismissed)

    	-gff, -g 	
    		A gff file for counting: if at least 50% of the read mapped inside the start-end range of a gff entry, if genes overlap all entries are counted
    		The sam CIGAR string is used for the overlap calculations

    		counted Reads :        |=>  |=>  |=>         |=>  |=>           
    		omitted Reads : |=>|=>                                |=>                   
    		GFF           : ____|=======Gene1======>____________________
    		GFF           : ____________________|=======Gene2======>____

    	-gff_filter : filter the gff file by feature (the 3. column). Usually this is set to 'gene' or 'CDS'.
    				  You can use 'CDS|rRNA|exon' or '.*RNA|gene' to define multiple filters at once [default:'']
    	-gff_id 	: The id that is used for counting (do not use Name/product since it is usually not unique!)
    				  You can use 'ID|product' to combine the 2 attributes ID and product [default:ID|product]
    	-gff_start	: If set, only reads are counted that overlap the +-3nt area around the start, you can also encode this in the gff (you can also add 'gff_start=3;' to the gff attribute)
    	-gff_end	: Same for the end.
    	-gff_overlap: minimal percentual overlap of a read with a gff entry (relative to read length) [default:0.5].
                      A value of 0 corresponds to the default featureCount (at least 1 nt).

    	-examples   : displays various examples

    	mandatory for -gff : 
    		-uc  strand unspecific counting (see -examples for more details) 
    		-sc  strand specific counting (see -examples for more details) 

    	(If no -gff is used, strand information is omitted)

    	-s	: a tab separated grouping file that maps from a groupid to target names (<-as provided in the sam file). 
    		  This can be used for counting isoforms, splicing variations, ... 
    		  If a read maps uniquely to only target names of a group, the groupid counts +1. 
    		  All other target names are treated normally.
    		  This option can be combined with -gff option (the grouping should then reference the names/ids of -gff_id)
    	-m	: the maximum number of elements that the gff can contain to use the fast and memory intensive method (set this to 0 to save memory on the cost of time) [default:10000]. 

    	Hint for large gff files: You can use the 'split -l 1000 --numeric-suffixes some.gff some.gff' to split your gff into smaller chunks, then run this program multiple times (using the fast approach with -m 1000) and add the results together.

    DESCRIPTION
     
    	- input can be sam or bam (samtools is needed for bam)
    	- output is written to STDOUT (sgr format)
    	- unmapped reads are always omitted (bit flag 0x4)
    	- ambiguously mapped reads are omitted by default (NH:i:X, X>1)
    	- if NH:i:X read attribute is missing all reads are treated as uniquely mapped
    		For Bowtie2 use : 'samtools view -q 1 input' (Bowtie2 assigns a MAPQ of 1 to unique reads)
    		Other : grep -v 'XT:A:R' input.sam or grep -v 'XA:Z:' input.sam 
    	- CIGAR string is used for end positions/coverage !
    	- RNASeq sam/bam input files are assumend to be 1based

    VERSION v0.4.36
    AUTHOR Paul Klemm
    DEPENDENCIES Perl Getopt::Long, File::Basename

---
## RNAseq/sgrVis.R
    Usage: this program visualizes sgr/tsv file(s) and plots the values on the sequence (given by a gff+fasta). If multiple sgr files are given, then error bars are added to the bar plots. If groups of sgr files are given (like control+delta ,...) then mutliple bar plots are drawn side-by-side. If the gff contains the atrribute Str="((...))..." the secondary structure can be drawn using the -varna option. Output files are output.pdf=all plots, output.csv=detailed txt file of all plots, output_short.csv=only significant positions are printed. If 'Error: C stack usage  xxx is too close to the limit' then set 'ulimit -s unlimited'


    Options:
    	-f CHARACTER, --fasta=CHARACTER
    		fasta file name

    	-g CHARACTER, --gff=CHARACTER
    		gff file name OR a string 'seqname start end strand (name)' (e.g. 'NC_123 100 500 - testgene', name is optional)

    	-s CHARACTER, --sgr=CHARACTER
    		sgr file or group of files separated by ',' or ':' or list of groups separated by '|'

    	--tsv=CHARACTER
    		(optional) tsv with 2 columns: first the nucleotide sequence and second some value. This overwrites the -sgr option and sets --delta 0.

    	-d NUMBER, --delta=NUMBER
    		(optional) upstream downstream regionlength around a gene (in nt), default:10

    	-k NUMBER, --k=NUMBER
    		(optional) histogram barsize (width), defines a sliding window that sums up k values at a time (the higher k, the lower the resolution), default:1

    	-n CHARACTER, --norm=CHARACTER
    		(optional) normalize modus ax,sum,globalmax,globalsum,nothing] the global* option define the max/sum over all given values while the other 2 define the max/sum for each vector of values (then all values are devided by this normalization-value) , default:globalmax

    	-p CHARACTER, --plot=CHARACTER
    		(optional) plot type [bar,box,line,violin,linebar]-plot default:bar

    	-q REGEXSTRING, --grep=REGEXSTRING
    		(optional) grep the given gff with this query string (regex enabled)

    	-c [RESIDUE,SGRGROUPS], --color=[RESIDUE,SGRGROUPS]
    		(optional) color, default:sgrgroups if at least 2 groups are given and and residue otherwise. residue coloring requires the fasta or tsv option

    	-a NUMBER, --alpha=NUMBER
    		(optional) alpha level (type 1 error) threshold for --test, default:0.05

    	-o DIRECTORY, --outputdir=DIRECTORY
    		(optional) output directory. 2 files are generated a output.pdf containing all plots and a output.csv containing all the information as txt file. default:cwd

    	--name=CHARACTER
    		(optional) output name. default:name of the input sgr or tsv file

    	-m NUMBER, --mintest=NUMBER
    		(optional) minimum value for applying the test (after normalization, so this is in %), default:0.05

    	--ignore=INTEGER
    		(optional) ignores sgr information of Xnt of the start (left). If X is negative the same for the end position

    	--nofold=INTEGER
    		(optional) do not fold given X nt of the start if positive. if negative the same for the end

    	--varna
    		(optional) invoke varna system call. The gff needs the attribute Str="((.)(..." or the program will invoke RNAfold. You need to be able to call 'java -cp /usr/local/bin/VARNA*.jar fr.orsay.lri.varna.applications.VARNAcmd'. Requires the fasta option.

    	--firstIsCTRL
    		(optional) if number of groups >= 2 : this enables that the first group is marked as control for testing (all other groups are compared against this group only). Otherwise Kruskal-Wallis group comparison is perfomed.

    	--relativeToFirst
    		(optional) if number of groups >= 2 : All possible 2 paired differences against the control are displayed (and the first group is discarded).

    	--highlight=CHARACTER
    		(optional) sequence hightlight (e.g. GATC, then all GATC are marked yellow)

    	--maxY=DOUBLE
    		(optional) maximal y-value

    	--csv
    		(optional) output various csv tables

    	--circular
    		(optional) circularizes the plot

    	--rnafold
    		(optional) generate RNAfold secondary structure

    	--rds
    		(optional) output RDS

    	-w INTEGER, --width=INTEGER
    		(optional) set the width of the plot

    	--strand=[+,-]
    		(optional) only plot gff entries of plus or minus strand

    	--gggenes=BOOL
    		(optional) if set, then no sequence is printed but instead gggenes representation supplied gff file/string. The -gff should then correspond to a larger region is plotted (simply give a string input). Either give a gff file or a string defining one gff entry in the format : 'seqname start end strand (name)' (e.g. 'NC_123 100 500 - 6SRNA')

    	--zoom=[LEFTRIGHT,MINMAX,BOTH,LEFTMAX]
    		(optional) plot zoomed subplots of the left and right region and/or the minimum and maximum region

    	--ylab=
    		(optional) set the ylabel

    	-h, --help
    		Show this help message and exit



---
## RNAseq/RNAseq_removePolyEnds.pl
    RNAseq_polyXtrimmer.pl        trimms polyX ends/starts (polyA, polyC, ...)
     
    SYNOPSIS
     
    RNAseq_polyXtrimmer.pl (options) INPUT

    	INPUT	file/string/STDIN containing the sequences (for files : fasta,fq). - : STDIN. String input, e.g. "ACACA"

    	optional:
    		-polyX	defines the nucleotide that needs to be trimmed (A : polyA, C: polyC,...) [default:A]
    		-min	miminal length of the polyX sequences [default:5]
    		-greedy	if set, then the first sequence is cut of length > min
    		-onlyt	if set, then only trimmed sequences are outputted
    		-d		direction of cutting (3: at 3' end, 5: at 5'end) [default:3]
    DESCRIPTION
     
    	This script finds the longest continues sequences of the specified -poly NT and trimmes the sequences
    	and everything following the match.
           
    EXAMPLES
     
     	# 1. most simple call:

    	RNAseq_polyXtrimmer.pl -polyX C - <<< "ACCCCGCCCCCCCGTGT"
    	RNAseq_polyXtrimmer.pl -polyX C "ACCCCGCCCCCCCGTGT"
    	RNAseq_polyXtrimmer.pl -polyX C myfile.fasta
    	
    	-> ACCCCG

    VERSION v0.0.4
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, File::Basename

---
## RNAseq/alignNrdbReadsPlot.sh
    USAGE: alignNrdbReadsPlot.sh (options) \<SAMFILE\>
    Simple pipeline that converts a sam file to an aligned html.
    redundant reads are pooled together, the name encodes the number of reads (x_NUMBEROFREADS).
    Output files are: SAMFILE.fna, SAMFILE.fna.nrdb, SAMFILE.fna.nrdb.aln, SAMFILE.fna.nrdb.aln.html

    OPTIONS:
     -r \<STRING\> : Reference sequence that will be displayed at the top of the output
     -g start|end|length|rname : group reads by the start or end position or the RNAME column
     -a cigar|qual|muscle|clustalo : align by either one of these option (cigar: insertions are omitted, qual: Phred+33), default: muscle
     -n : exclude groups of size 1
     -x : only print largest representant for each group
     -k : if set intermediate files are kept as well

     common options are -a cigar -g rname

    VERSION v0.8.10
    AUTHOR Paul Klemm
    DEPENDENCIES muscle, nrdb.pl, MSAplot, aha

---
## RNAseq/sgrVis_strandWrapper.pl
    sgrVis_strandWrapper.pl       This program counts (raw) the number of reads (start,end,coverage) on the + and - strands (generates 6 files for each sam file). The raw readcounts of the minus strand gets multiplied by -1.

    SYNOPSIS
     
    sgrVis_strandWrapper.pl --input SAMFILE

    		--strand [+-b] "+" : plus , "-": minus , "+,-" : both strands independently, "b" : both added together	default:+,-
    		--modus [start,end,coverage]	start:only start positions, end: only stop positions, coverage: all positions (=coverage) default:start,end,coverage
    		--input -i	FILE NEEDS TO BE IN SAM FORMAT !!

    VERSION v0.0.5
    AUTHOR Paul Klemm
    DEPENDENCIES Getopt::Long, File::Basename, sgrVis.R, RNAseq_counting_position.pl (https://gitlab.com/paulklemm_PHD/scripts)

---
## RNAseq/easyDeseq2.R
    loading packages...
    installing plotly...
    Installing package into /home/paul/R/x86_64-pc-linux-gnu-library/3.6
    (as lib is unspecified)
    also installing the dependencies credentials, desc, gert, gh, openssl, usethis, webdriver, s2, units, httr, devtools, shinytest, Cairo, rgeos, sf

    trying URL 'https://cloud.r-project.org/src/contrib/credentials_1.3.1.tar.gz'
    Content type 'application/x-gzip' length 230470 bytes (225 KB)
    ==================================================
    downloaded 225 KB

    trying URL 'https://cloud.r-project.org/src/contrib/desc_1.4.0.tar.gz'
    Content type 'application/x-gzip' length 295610 bytes (288 KB)
    ==================================================
    downloaded 288 KB

    trying URL 'https://cloud.r-project.org/src/contrib/gert_1.4.1.tar.gz'
    Content type 'application/x-gzip' length 67293 bytes (65 KB)
    ==================================================
    downloaded 65 KB

    trying URL 'https://cloud.r-project.org/src/contrib/gh_1.3.0.tar.gz'
    Content type 'application/x-gzip' length 45619 bytes (44 KB)
    ==================================================
    downloaded 44 KB

    trying URL 'https://cloud.r-project.org/src/contrib/openssl_1.4.5.tar.gz'
    Content type 'application/x-gzip' length 1311351 bytes (1.3 MB)
    ==================================================
    downloaded 1.3 MB

    trying URL 'https://cloud.r-project.org/src/contrib/usethis_2.1.2.tar.gz'
    Content type 'application/x-gzip' length 333769 bytes (325 KB)
    ==================================================
    downloaded 325 KB

    trying URL 'https://cloud.r-project.org/src/contrib/webdriver_1.0.6.tar.gz'
    Content type 'application/x-gzip' length 65019 bytes (63 KB)
    ==================================================
    downloaded 63 KB

    trying URL 'https://cloud.r-project.org/src/contrib/s2_1.0.7.tar.gz'
    Content type 'application/x-gzip' length 2235408 bytes (2.1 MB)
    ==================================================
    downloaded 2.1 MB

    trying URL 'https://cloud.r-project.org/src/contrib/units_0.7-2.tar.gz'
    Content type 'application/x-gzip' length 855840 bytes (835 KB)
    ==================================================
    downloaded 835 KB

    trying URL 'https://cloud.r-project.org/src/contrib/httr_1.4.2.tar.gz'
    Content type 'application/x-gzip' length 159950 bytes (156 KB)
    ==================================================
    downloaded 156 KB

    trying URL 'https://cloud.r-project.org/src/contrib/devtools_2.4.2.tar.gz'
    Content type 'application/x-gzip' length 371298 bytes (362 KB)
    ==================================================
    downloaded 362 KB

    trying URL 'https://cloud.r-project.org/src/contrib/shinytest_1.5.1.tar.gz'
    Content type 'application/x-gzip' length 232148 bytes (226 KB)
    ==================================================
    downloaded 226 KB

    trying URL 'https://cloud.r-project.org/src/contrib/Cairo_1.5-12.2.tar.gz'
    Content type 'application/x-gzip' length 88631 bytes (86 KB)
    ==================================================
    downloaded 86 KB

    trying URL 'https://cloud.r-project.org/src/contrib/rgeos_0.5-8.tar.gz'
    Content type 'application/x-gzip' length 289853 bytes (283 KB)
    ==================================================
    downloaded 283 KB

    trying URL 'https://cloud.r-project.org/src/contrib/sf_1.0-3.tar.gz'
    Content type 'application/x-gzip' length 3524145 bytes (3.4 MB)
    ==================================================
    downloaded 3.4 MB

    trying URL 'https://cloud.r-project.org/src/contrib/plotly_4.10.0.tar.gz'
    Content type 'application/x-gzip' length 3842199 bytes (3.7 MB)
    ==================================================
    downloaded 3.7 MB

    * installing *source* package desc ...
    ** package desc successfully unpacked and MD5 sums checked
    ** using staged installation
    ** R
    ** inst
    ** byte-compile and prepare package for lazy loading
    ** help
    *** installing help indices
    ** building package indices
    ** testing if installed package can be loaded from temporary location
    ** testing if installed package can be loaded from final location
    ** testing if installed package keeps a record of temporary installation path
    * DONE (desc)
    * installing *source* package openssl ...
    ** package openssl successfully unpacked and MD5 sums checked
    ** using staged installation
    Using PKG_CFLAGS=
    --------------------------- [ANTICONF] --------------------------------
    Configuration failed because openssl was not found. Try installing:
     * deb: libssl-dev (Debian, Ubuntu, etc)
     * rpm: openssl-devel (Fedora, CentOS, RHEL)
     * csw: libssl_dev (Solaris)
     * brew: openssl@1.1 (Mac OSX)
    If openssl is already installed, check that 'pkg-config' is in your
    PATH and PKG_CONFIG_PATH contains a openssl.pc file. If pkg-config
    is unavailable you can set INCLUDE_DIR and LIB_DIR manually via:
    R CMD INSTALL --configure-vars='INCLUDE_DIR=... LIB_DIR=...'
    -------------------------- [ERROR MESSAGE] ---------------------------
    tools/version.c:1:10: fatal error: openssl/opensslv.h: No such file or directory
        1 | #include \<openssl/opensslv.h\>
          |          ^~~~~~~~~~~~~~~~~~~~
    compilation terminated.
    --------------------------------------------------------------------
    ERROR: configuration failed for package openssl
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/openssl
    * installing *source* package s2 ...
    ** package s2 successfully unpacked and MD5 sums checked
    ** using staged installation
    Testing compiler using PKG_CFLAGS=
    --------------------------- [ANTICONF] --------------------------------
    Configuration failed because openssl was not found. Try installing:
     * deb: libssl-dev (Debian, Ubuntu, etc)
     * rpm: openssl-devel (Fedora, CentOS, RHEL)
     * csw: libssl_dev (Solaris)
     * brew: openssl@1.1 (Mac OSX)
    If openssl is already installed, check that 'pkg-config' is in your
    PATH and PKG_CONFIG_PATH contains a openssl.pc file. If pkg-config
    is unavailable you can set INCLUDE_DIR and LIB_DIR manually via:
    R CMD INSTALL --configure-vars='INCLUDE_DIR=... LIB_DIR=...'
    -------------------------- [ERROR MESSAGE] ---------------------------
    tools/version.c:1:10: fatal error: openssl/opensslv.h: No such file or directory
        1 | #include \<openssl/opensslv.h\>
          |          ^~~~~~~~~~~~~~~~~~~~
    compilation terminated.
    --------------------------------------------------------------------
    ERROR: configuration failed for package s2
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/s2
    * installing *source* package units ...
    ** package units successfully unpacked and MD5 sums checked
    ** using staged installation
    configure: units: 0.7-2
    checking whether the C++ compiler works... yes
    checking for C++ compiler default output file name... a.out
    checking for suffix of executables... 
    checking whether we are cross compiling... no
    checking for suffix of object files... o
    checking whether we are using the GNU C++ compiler... yes
    checking whether g++ -std=gnu++11 accepts -g... yes
    checking how to run the C++ preprocessor... g++ -std=gnu++11 -E
    checking for grep that handles long lines and -e... /usr/bin/grep
    checking for egrep... /usr/bin/grep -E
    checking for ANSI C header files... yes
    checking for sys/types.h... yes
    checking for sys/stat.h... yes
    checking for stdlib.h... yes
    checking for string.h... yes
    checking for memory.h... yes
    checking for strings.h... yes
    checking for inttypes.h... yes
    checking for stdint.h... yes
    checking for unistd.h... yes
    checking for stdbool.h that conforms to C99... no
    checking for _Bool... no
    checking for error_at_line... yes
    checking for x86_64-conda-linux-gnu-gcc... gcc -std=gnu99
    checking whether we are using the GNU C compiler... yes
    checking whether gcc -std=gnu99 accepts -g... yes
    checking for gcc -std=gnu99 option to accept ISO C89... none needed
    checking for XML_ParserCreate in -lexpat... yes
    checking udunits2.h usability... no
    checking udunits2.h presence... no
    checking for udunits2.h... no
    checking udunits2/udunits2.h usability... no
    checking udunits2/udunits2.h presence... no
    checking for udunits2/udunits2.h... no
    checking for ut_read_xml in -ludunits2... no
    configure: error: in `/tmp/RtmpbuTqHw/R.INSTALL541e260852cb/units':
    configure: error: 
    --------------------------------------------------------------------------------
      Configuration failed because libudunits2.so was not found. Try installing:
        * deb: libudunits2-dev (Debian, Ubuntu, ...)
        * rpm: udunits2-devel (Fedora, EPEL, ...)
        * brew: udunits (OSX)
      If udunits2 is already installed in a non-standard location, use:
        --configure-args='--with-udunits2-lib=/usr/local/lib'
      if the library was not found, and/or:
        --configure-args='--with-udunits2-include=/usr/include/udunits2'
      if the header was not found, replacing paths with appropriate values.
      You can alternatively set UDUNITS2_INCLUDE and UDUNITS2_LIBS manually.
    --------------------------------------------------------------------------------

    See `config.log' for more details
    ERROR: configuration failed for package units
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/units
    * installing *source* package Cairo ...
    ** package Cairo successfully unpacked and MD5 sums checked
    ** using staged installation
    checking for x86_64-conda-linux-gnu-gcc... gcc -std=gnu99
    checking whether the C compiler works... yes
    checking for C compiler default output file name... a.out
    checking for suffix of executables... 
    checking whether we are cross compiling... no
    checking for suffix of object files... o
    checking whether we are using the GNU C compiler... yes
    checking whether gcc -std=gnu99 accepts -g... yes
    checking for gcc -std=gnu99 option to accept ISO C89... none needed
    checking how to run the C preprocessor... gcc -std=gnu99 -E
    checking for grep that handles long lines and -e... /usr/bin/grep
    checking for egrep... /usr/bin/grep -E
    checking for ANSI C header files... yes
    checking for sys/wait.h that is POSIX.1 compatible... yes
    checking for sys/types.h... yes
    checking for sys/stat.h... yes
    checking for stdlib.h... yes
    checking for string.h... yes
    checking for memory.h... yes
    checking for strings.h... yes
    checking for inttypes.h... yes
    checking for stdint.h... yes
    checking for unistd.h... yes
    checking for string.h... (cached) yes
    checking sys/time.h usability... yes
    checking sys/time.h presence... yes
    checking for sys/time.h... yes
    checking for unistd.h... (cached) yes
    checking for an ANSI C-conforming const... yes
    checking for pkg-config... /usr/bin/pkg-config
    checking whether pkg-config knows about cairo... yes
    checking for configurable backends... cairo cairo-ft cairo-pdf cairo-png cairo-ps cairo-xlib cairo-xlib-xrender
    checking whether --static is needed... no
    configure: CAIRO_CFLAGS=-I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16
    checking if R was compiled with the RConn patch... no
    checking cairo.h usability... yes
    checking cairo.h presence... yes
    checking for cairo.h... yes
    checking for PNG support in Cairo... yes
    checking for ATS font support in Cairo... no
    configure: CAIRO_LIBS=-lfreetype -lz -lpng16 -lz -lcairo -lXext -lXrender -lX11
    checking for library containing deflate... none required
    checking whether Cairo programs can be compiled... yes
    checking whether cairo_image_surface_get_format is declared... no
    checking for FreeType support in cairo... yes
    checking whether FreeType needs additional flags... no
    checking wheter libjpeg works... yes
    checking wheter libtiff works... yes
    configure: creating ./config.status
    config.status: creating src/Makevars
    config.status: creating src/cconfig.h
    ** libs
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c cairobem.c -o cairobem.o
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c cairogd.c -o cairogd.o
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c cairotalk.c -o cairotalk.o
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c img-backend.c -o img-backend.o
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c img-jpeg.c -o img-jpeg.o
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c img-tiff.c -o img-tiff.o
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c pdf-backend.c -o pdf-backend.o
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c ps-backend.c -o ps-backend.o
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c svg-backend.c -o svg-backend.o
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c w32-backend.c -o w32-backend.o
    gcc -std=gnu99 -I"/usr/share/R/include" -DNDEBUG -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /home/paul/anaconda3/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16  -I. -Iinclude   -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g -fpic  -g -O2 -fdebug-prefix-map=/build/r-base-jbaK_j/r-base-3.6.3=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -g  -c xlib-backend.c -o xlib-backend.o
    xlib-backend.c:34:10: fatal error: X11/Intrinsic.h: No such file or directory
       34 | #include \<X11/Intrinsic.h\>      /*->    Xlib.h  Xutil.h Xresource.h .. */
          |          ^~~~~~~~~~~~~~~~~
    compilation terminated.
    make: *** [/usr/lib/R/etc/Makeconf:168: xlib-backend.o] Error 1
    ERROR: compilation failed for package Cairo
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/Cairo
    * installing *source* package rgeos ...
    ** package rgeos successfully unpacked and MD5 sums checked
    ** using staged installation
    configure: CC: /home/paul/anaconda3/bin/x86_64-conda-linux-gnu-cc
    configure: CXX: /home/paul/anaconda3/bin/x86_64-conda-linux-gnu-c++
    configure: rgeos: 0.5-8
    checking for /usr/bin/svnversion... no
    configure: svn revision: 679
    checking for geos-config... no
    no
    configure: error: geos-config not found or not executable.
    ERROR: configuration failed for package rgeos
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/rgeos
    ERROR: dependency openssl is not available for package credentials
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/credentials
    ERROR: dependency openssl is not available for package httr
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/httr
    ERROR: dependencies s2, units are not available for package sf
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/sf
    ERROR: dependencies credentials, openssl are not available for package gert
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/gert
    ERROR: dependency httr is not available for package gh
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/gh
    ERROR: dependency httr is not available for package webdriver
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/webdriver
    ERROR: dependency httr is not available for package plotly
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/plotly
    ERROR: dependencies gert, gh are not available for package usethis
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/usethis
    ERROR: dependencies httr, webdriver are not available for package shinytest
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/shinytest
    ERROR: dependencies usethis, httr are not available for package devtools
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/devtools

    The downloaded source packages are in
    	/tmp/RtmpSp6Log/downloaded_packages
    installing umap...
    Installing package into /home/paul/R/x86_64-pc-linux-gnu-library/3.6
    (as lib is unspecified)
    also installing the dependency openssl

    trying URL 'https://cloud.r-project.org/src/contrib/openssl_1.4.5.tar.gz'
    Content type 'application/x-gzip' length 1311351 bytes (1.3 MB)
    ==================================================
    downloaded 1.3 MB

    trying URL 'https://cloud.r-project.org/src/contrib/umap_0.2.7.0.tar.gz'
    Content type 'application/x-gzip' length 78073 bytes (76 KB)
    ==================================================
    downloaded 76 KB

    * installing *source* package openssl ...
    ** package openssl successfully unpacked and MD5 sums checked
    ** using staged installation
    Using PKG_CFLAGS=
    --------------------------- [ANTICONF] --------------------------------
    Configuration failed because openssl was not found. Try installing:
     * deb: libssl-dev (Debian, Ubuntu, etc)
     * rpm: openssl-devel (Fedora, CentOS, RHEL)
     * csw: libssl_dev (Solaris)
     * brew: openssl@1.1 (Mac OSX)
    If openssl is already installed, check that 'pkg-config' is in your
    PATH and PKG_CONFIG_PATH contains a openssl.pc file. If pkg-config
    is unavailable you can set INCLUDE_DIR and LIB_DIR manually via:
    R CMD INSTALL --configure-vars='INCLUDE_DIR=... LIB_DIR=...'
    -------------------------- [ERROR MESSAGE] ---------------------------
    tools/version.c:1:10: fatal error: openssl/opensslv.h: No such file or directory
        1 | #include \<openssl/opensslv.h\>
          |          ^~~~~~~~~~~~~~~~~~~~
    compilation terminated.
    --------------------------------------------------------------------
    ERROR: configuration failed for package openssl
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/openssl
    ERROR: dependency openssl is not available for package umap
    * removing /home/paul/R/x86_64-pc-linux-gnu-library/3.6/umap

    The downloaded source packages are in
    	/tmp/RtmpSp6Log/downloaded_packages
    There were 19 warnings (use warnings() to see them)
    Warning message:
    In library(package, lib.loc = lib.loc, character.only = TRUE, logical.return = TRUE,  :
      there is no package called DESeq2
    Warning message:
    In library(package, lib.loc = lib.loc, character.only = TRUE, logical.return = TRUE,  :
      there is no package called DEGreport
    Warning message:
    In library(package, lib.loc = lib.loc, character.only = TRUE, logical.return = TRUE,  :
      there is no package called ComplexHeatmap
    Usage: simple deseq2 script comparing control and treatment count files


    Options:
    	-c CHARACTER, --counts=CHARACTER
    		count file with the format 'gene	filename	count'

    	--control=CHARACTER
    		control replicates/filenames separated by ','

    	--treatment=CHARACTER
    		treament replicates/filenames separated by ','. Multiple treatment group can be separated by ':'

    	--treatmentNames=CHARACTER
    		(optional) treament group names separated by ':' too, needs to match the groups of --treatment

    	--controlNames=CHARACTER
    		(optional) control group names separated by ':' too, needs to match the groups of --control. If omitted, then all are control entries are called 'ctrl' and compared against all treatments

    	--project=CHARACTER
    		(optional) prefix for all outputfiles

    	--readcount_cutoff=INTEGER
    		(optional) genes with a sum count (across all samples) with less than readcount_cutoff are excluded

    	--html
    		(optional) enables html output of the MAplot

    	--correctReplicateOutlier
    		(optional) removes DEG between replicates based on correlation

    	--lrt
    		(optional) enables LRT test

    	--sd_cutoff=INTEGER
    		(optional) minimum number of sd that a outlier needs to deviate from the mean residual error for --correctReplicateOutlier (default:4)

    	--r2_cutoff=DOUBLE
    		(optional) maximum R2 to correct for outlier for --correctReplicateOutlier (default:0.9)

    	-h, --help
    		Show this help message and exit



---
## alignment/MSAconsensus.pl [![pipeline status](https://gitlab.com/paulklemm_PHD/scripts/badges/master/pipeline.svg)](https://gitlab.com/paulklemm_PHD/scripts/pipelines)
    MSAconsensus.pl        calculates a consensus of a given alignment (columnwise).
     
    SYNOPSIS
     
    MSAconsensus.pl (options) ALIGNMENTFILE 

    	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

    		e.g. CLUSTAL W formated input:

    		...
    		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
    		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
    		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
    		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
    		...

    	options

    		-includeGaps, -g : include gaps in the consensus
    		-ambiguous, -a   : if there are MULTIPLE dominant characters, then print ONE at random.
    		-multiple, -m    : if there are MULTIPLE dominant characters, then print ALL if the format '{TA}' if T and A are the dominat characters.
    		-plain, -p       : remove no-domiant/ambiguous characters '#?' entirely.
    		-c       : outputs tsv positionwise conservation to STDERR (0-1).
    		-v       : outputs varna style conservation scores to STDERR (usable with varnas -colorMap option).

    EXAMPLES

    	MSAconsensus.pl test.aln 

    		ATG?TTATGGTATA#AT?ATTAT??AA##########

    		# = no domiant character (e.g. if all chars are gaps and -g is NOT set)
    		? = there are multiple dominant characters found (set -a to display any)

    	MSAconsensus.pl test.aln -p

    		ATGATTTATGGTATAATATATTAT

    	MSAconsensus.pl test.aln -a
    	
    		ATGTTTATGGTATA#ATAATTATTCAA##########
    		---^-------------^-----^^------------ the '?' position can vary each call

    	MSAconsensus.pl test.aln -a
    	
    		ATGATTATGGTATA#ATTATTATGGAA##########
    		---^-------------^-----^^------------ the '?' position can vary each call

    	MSAconsensus.pl test.aln -a -g

    		ATGTTTATGGTATA-ATAATTAT--------------

    	MSAconsensus.pl test.aln -m

    		ATG{AT}TTATGGTATA#AT{AT}ATTAT{GT}{CG}AA##########

    VERSION v0.0.4
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## alignment/MSAplot.pl
    MSAplot.pl        produces a colored representation of a given MSA
     
    SYNOPSIS
     
    MSAplot.pl (options) ALIGNMENTFILE 

    	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

    		e.g. CLUSTAL W formated input:
    		...
    		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
    		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
    		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
    		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
    		...

    OUTPUT
    	Sequence identity color code: 100%, >90%, >80%, >50%.
    	You can use 'aha' to convert the output to html (conda install aha)

    VERSION v0.1.21
    AUTHOR Paul Klemm

---
## alignment/pairwiseAverageNW.pl
    pairwiseAverageNW.pl        performs an all versus all pairwise averaged length-normalized needlemanwunsch
     
    SYNOPSIS
     
    pairwiseAverageNW.pl (-help|-h) (-noselfhit) (-notlengthnormed) (-verbose) FASTA1 (FASTA2 ...)

    	FASTA	Fasta file containing genes/proteins
    		if one file is given, all sequences are compared
    		if multiple files are given, the average score is calculated between the files

    	optional:
    		-noself	do not compare the same file/sequence
    		-nonorm	do not normalizing the scores to the maximal length
    		-verbose	displaying the progress (STDERR)
    		-all	print the list of all scores instead of the mean (STDERR)
    		-wide   outputs a matrix format instead of a long list of pairs
    		-help	displays this.

    DESCRIPTION 

    	This script computes all pairwise averaged (normalized) needlemanwunsch 
    	global alignment score of all genes/proteins versus all genes/proteins of
    	a set of fasta files. 
        e.g. 2 files (A,B) with 2 genes each (A1,A2,B1,B2) 
    	-> score for A vs B = mean(needlemanwunsch(A1,B1),needlemanwunsch(A1,B2),needlemanwunsch(A1,B2),needlemanwunsch(A2,B1),needlemanwunsch(A2,B2)) 
     	scores are length normed (/max length of the two sequences)

     	Careful: This will fail if there are many input sequences (curse of dimensionality), many sequences are on average always similar

    EXAMPLES
     
    	perl pairwiseAverageNW.pl Test1.faa Test2.faa
    	
    	with
    		------Test1.faa:------------
    		>G1_1
    		ABC
    		>G1_2
    		AB

    		------Test2.faa:------------
    		>G2_1
    		AB
    		>G2_2
    		AA

    	STDOUT:
    		Test1.faa       Test1.faa       0.7778
    		Test1.faa       Test2.faa       0.25
    		Test2.faa       Test2.faa       0.6667

    	perl pairwiseAverageNW.pl Test1.faa --> comparing the sequences of Test1.faa against each other

    VERSION v0.0.18
    AUTHOR Paul Klemm
    DEPENDENCIES Getopt::Long, File::Basename, easyalignment.py (https://gitlab.com/paulklemm_PHD/scripts)

---
## alignment/MSAscorer.pl
    Died at ./alignment/MSAscorer.pl line 52.
    MSAscorer.pl        scores a given alignment (average linewise identity, number of gaps, ...)
     
    SYNOPSIS
     
    MSAscorer.pl ALIGNMENTFILE 

    	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

    		e.g. CLUSTAL W formated input:

    		...
    		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
    		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
    		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
    		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
    		...

    	OUTPUT : clustalw formatted alignment

    VERSION v0.0.13
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## alignment/easyphylogeny.py
    Usage: easyphylogeny.py [options] 'INPUTFILE'
    you can use 'cat << EOF | phylogeny.py' to directly read from terminal

    Options:
      --version             show program's version number and exit
      -h, --help            show this help message and exit
      -t TYPE, --type=TYPE  (input file type) dist : leaf distance matrix. adj :
                            adjacency list of a tree (each row represents an edge
                            in the form a->b if unweighted or a->b|10 if
                            weighted). aln: fasta-alignment (used for --work
                            largeParsimony) [default: dist]
      -w WORK, --work=WORK  work of type=dist: 'NJ','UPGMA': clustering
                            algorithms. 'AdditivePhylogeny': input matrix needs to
                            be additive. 'generateRandomBinaryTree','generateRando
                            mRootedBinaryTree': tree generation (needs --leaves
                            and maybe --labels). 'smallParsimony': generate edge
                            weights with minimal parsimony score.
                            'greedyNearestNeighborInterchange': change nearest
                            neighbors and apply smallParsimony. 'largeParsimony':
                            build a random binary tree and perform
                            greedyNearestNeighborInterchange. [default: NJ]
      --leaves=LEAVES       mandatory for --work largeParsimony or
                            generateRandomBinaryTree. The leave node ids (species
                            names)
      --labels=LABELS       optional for --work largeParsimony or
                            generateRandomBinaryTree. The leave node labels
                            (sequences)
      -d, --draw            generate a nx plot of the output tree
      --largeParsimonyRestarts=LARGEPARSIMONYRESTARTS
                            the number of initially randomly sampled random binary
                            trees
      --drawIDs             draw node ids instead of labels (usually species names
                            instead of sequences)
      --verbose             
      -v                    
      -x, --test            
      --bigpicture          outputs a step-by-step picture for the algorithm
      --innerNodesJoin      if set inner nodes are displayed as the joined names
                            from the leaves (for NJ and UPGMA)
      --example=EXAMPLE     perform various examples (combine this with --draw
                            and/or --bigpicture): 'corona': do a NJ of a distance
                            matrix based on the corona virus genome found in 9
                            animals. 'HIV': calculate the largeParsimony of
                            another HIV protein of the 20 patients of 'State of
                            Louisiana vs. Richard Schmidt' trial of 1998 (~10min).
                            'adh': generate largeParsimony of the alcohol
                            dehydrogenase (adh) protein of 12 yeast species
                            (~3min).
      --discrepancy         calculate discrepancy between the generated tree and
                            the input distance matrix for work
                            'NJ','UPGMA','AdditivePhylogeny'
      --parsimony           calculate parsimony score for the generated tree (sum
                            of all edge weights)

---
## alignment/MSAget_indelMutationDiffof2seqs.pl
    Useless use of private variable in void context at ./alignment/MSAget_indelMutationDiffof2seqs.pl line 110.
    MSAget_indelMutationDiffof2seqs.pl	calculates the indel/mutation difference between 2 given fasta files

    SYNOPSIS	

    	MSAget_indelMutationDiffof2seqs.pl ALNFILE

    	ALNFILE : alignment file of 2 sequences in FASTA format. 
    	STDOUT : a list of areas with indes/mutations (and the genomic position of the first sequences)

    VERSION v0.0.3
    AUTHOR Paul Klemm

---
## alignment/MSAtrimmer.pl [![pipeline status](https://gitlab.com/paulklemm_PHD/scripts/badges/master/pipeline.svg)](https://gitlab.com/paulklemm_PHD/scripts/pipelines)
    MSAtrimmer.pl        trimmes a given alignment (column- and row-wise) and realigns it. By default only gaprich (>0.5) columns are trimmed.
     
    SYNOPSIS
     
    MSAtrimmer.pl (options) ALIGNMENTFILE 

    	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

    		e.g. CLUSTAL W formated input:
    		...
    		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
    		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
    		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
    		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
    		...

    	options 

    		-p=(clustalw|musle) : the algorithm that is called after trimming to realign [default:clustalw]. This also defines the output format!
    		-norealn    :	this option disables the re-alignment call on the results
    		-fasta      :	outputs fasta format

    	options VERTICAL (columns)

    		-VmaxGap=number, -g number :	the maximum number/percentage (with a % suffix) of gap characters in each column [default:50%]
    		-VminId=number,  -t number :	the minimum number/percentage (with a % suffix) of the dominant nongap-characters in each column [default:0 <=> disabled]
    		
    		percentage numbers [0% - 100%]
    		integer numbers [1 - 999999]
    		NOTE: If you want to specify a percentage number the % symbol is neccesary 

    	options HORIZONTAL (rows)

    		-HminPairId=percentage, -h percentage:	the minimum average pairwise identity to all other rows. Scoring gap-gap=0 [default:0% <=> disabled]

    		percentage numbers [0% - 100%] (this option needs the % symbol !)

    	options display

    		-color      :	showing original aln with all removed rows/cols colored (+ this option disables the realn call on the results)

    EXAMPLE

    	MSAtrimmer.pl -VmaxGap=25% -VminId=3 test.aln

        Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
        Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
        Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
        Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
                                              ^^ ^        ^  ^ ^ ^ ^^^^^^^^^^^^^^

        --> VmaxGap=25% -> a maximum of 1 gap per column, VminId=3 -> each column should now have at least 3 common non-gap characters
        --> Trimmed coloumns are marked red

    OUTPUT
    	alingment file

    VERSION v0.0.19
    AUTHOR Paul Klemm
    DEPENDENCIES clustal2fasta.pl, clustalw (optional for re-alignment)

---
## alignment/easyalignment.py [![pipeline status](https://gitlab.com/paulklemm_PHD/scripts/badges/master/pipeline.svg)](https://gitlab.com/paulklemm_PHD/scripts/pipelines)
    WARNING: Did not found 'progressbar2', this package is not essential but enhances the program. I will skip it but consider installing it...
    Usage: easyalignment.py [options] 'SEQUENCE_A' 'SEQUENCE_B'
    or: easyalignment.py [options] 'FASTAFILE'
    or: easyalignment.py [options] 'SEQUENCE_A' 'FASTAFILE'

    Options:
      --version             show program's version number and exit
      -h, --help            show this help message and exit
      -t TYPE, --type=TYPE  comparison type: auto, nucl or prot [default: auto]
      -m MODUS, --modus=MODUS
                            comparison modus: needlemanwunsch/nw (global),
                            smithwaterman/sw (local), fitted (first longer
                            sequence locally and second shorter sequence
                            globally), overlapped (local suffix with local
                            prefix), levenshtein (edit distance) [default:
                            needlemanwunsch]
      -o GAPOPEN, --gapopen=GAPOPEN
                            gap open penalty [default 10 for prot and 15 for nucl]
      -e GAPEXTEND, --gapextend=GAPEXTEND
                            gap extend penalty, if 0 then affine gap penalty are
                            disabled [default 0.1 for prot and 6.66 for nucl]
      -d MAXDIAG, --maxdiag=MAXDIAG
                            restricts the similarity matrix by this value up and
                            down from the diagonal, corresponds to the maximal
                            possible gap length [default: 200]
      -r, --getAllEquivalentResult
                            if set, then all possible alignments with same score
                            are printed
      -g, --progress        print further informations progressbar, ...
      -l, --plot            print the similarity matrix build by the algorithm
                            (only works nicely for smaller examples). You can send
                            the output to aha to produce a html version.
      -p, --path            print matrix path (combine this with -v).
      -x, --test            
      --maxmemp=MAXMEMP     maximal virtual_memory percent (if exceeded the job is
                            killed).
      -s FILE, --scoring=FILE
                            comparison scoring file or one of the predefined
                            matrices: BLOSUM62, PAM250, IUB (match=1.9,
                            missmatch=0), ONE (match=1, missmatch=-1). By default
                            IUB is used for nucl and PAM250 for prot comparisons

---
## blast/ARBG.sh [![pipeline status](https://gitlab.com/paulklemm_PHD/scripts/badges/master/pipeline.svg)](https://gitlab.com/paulklemm_PHD/scripts/pipelines)
    USAGE : ARBG.sh (OPTIONS) FASTA_A FASTA_B
     FASTA_A, FASTA_B   :  Two fasta files

    This interface calls easyblast.sh (using 4 cores) 2 times and compares the blast outputs to create the adaptive reciprocal blast graph.

    OPTIONS:
     -s DOUBLE (0 to 1)  :  The minimal similarity for adaptive best matches [default:0.95]. -s 1 results in the reciprocal best(!) blast hit graph. [-s 0 disables this feature]
     -c DOUBLE (0 to 1)  :  The minimal alignment coverage [default:0.5]. NOTE: this option does not make sense if you compare fna with faa files (fnas are way longer anyways). [-c 0 disables this feature]
     -e DOUBLE (0 to 1)  :  evalue cutoff [default:1e-5], [-e 0 disables this feature]
     -t INT  :  number of threads [default:4].

     (all temporary files are written to /tmp)

    AUTHOR Paul Klemm
    DEPENDANCIES easyblast.sh (https://gitlab.com/paulklemm_PHD/scripts), awk, grep, xargs, ls, perl

---
## blast/easyblast.sh [![pipeline status](https://gitlab.com/paulklemm_PHD/scripts/badges/master/pipeline.svg)](https://gitlab.com/paulklemm_PHD/scripts/pipelines)
    USAGE : easyblast.sh (OPTIONS) QUERY(FILE/STRING) DB(FILE)
     QUERY(FILE/STRING)    :  The input query, e.g. /home/paul/pRNA.fa or just a string with a sequence like 'AATAATAATA'.
     DB(FILE)              :  The database (path to file).

    (Note: 4 cores are used by default)

    OPTIONS:
     -b \<STRING\>           :  The blast type [blastn, blastp, tblastn, blastx]. If not set, then it will try to auto detect the right one.
     -t \<INTEGER/PERCENT%\> :  This option sets the threshold for the number of returned hits per query (sorted by bitscore). If 1 then only the best match is returned (for each query). Given a percentile (with a % suffix), all hits within this percentile times the maximum are returned (bitscore). So -t 90% -> from maximal bitscore to 10% below. If set, the blastoption have to contain -outfmt 6 because of the bitscores (by default this is the case).
     -e \<INTEGER\>          :  The -evalue threshold (default:10) 
     -j \<INTEGER\>          :  The number of threads used (default:4)
     -d                    :  If set, then for each query no duplicated hits are returned like A->B and A->B again with a different bitscore (hit with best bitscore is printed).
     -h                    :  If set, then the header for outfmt 6 is printed as well.
     -s                    :  If set, then the following options are set -soft_masking false -dust no.
     -g                    :  If set, the (sstart,send) positions are converted to strand +/- and +based-positions.
     -x                    :  If set, a summarized output is produced.
     -o \<STRING\>           :  Explicit blast+ options, by default '-outfmt 6 -num_threads 4' is used. NOTE: this overwrites all other options (like -j threads and -e evalue).

    EXAMPLES:
     bash easyblast.sh 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa
     bash easyblast.sh /some/file.fasta /home/Ecoli.faa -> 
     bash easyblast.sh -t 1 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa -> This gives only the best match
     bash easyblast.sh -t 90% 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa -> Up to 10% from maximum is returned
     bash easyblast.sh -t 90% -d 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa -> same as ^ and duplicated matches are excluded.
      bash easyblast.sh -j 16 'AAGTATAGATAGATCCACACACAGGCACGAGT' /home/Ecoli.faa -> automatic detecting blastx and using 16 cores
      bash easyblast.sh -o '-outfmt 6 -num_threads 16' -b blastx 'AAGTATAGATAGATCCACACACAGGCACGAGT' /home/Ecoli.faa -> forcing blastx algorithm
    (The result is written to stdout)

    Output is sorted by qseqid (column 1) first and then by evalue (column 12)

    Here the some useful BLAST commands: 
    makeblastdb -dbtype nucl -in DBFILE
    makeblastdb -dbtype prot -in DBFILE -out OUTFILE
    blastp -query QUERYFILE -db DBFILE -outfmt 6 -num_threads 4
    blastp -query QUERYFILE -db DBFILE -outfmt '6 qseqid sseqid bitscore' -num_threads 4 | sort -k1,1 -k3,3nr | awk '{if(last != $1){last=$1;print $0;}}'

    Here the some useful options: 
    -evalue 1e-4 : sets the evalue cut-off
    -soft_masking false -dust no : disables soft masking (low complexity filtering)
    -word_size 11 : should be 3 for FAA vs FAA and 11-28 for FNA vs FNA
    -task blastn : for blastn, this sets -word_size 11
    -strand {both,plus,minus} : by default blast searches both strands (so also the rc)
    extract matches : easyblast.sh query db.fna | awk '{print $2}' | grepfasta.pl - db.fna

    VERSION 0.1.23
    AUTHOR Paul Klemm
    DEPENDANCIES ncbi blast+, awk, grep, xargs, ls, perl

---
## benchmarker/benchmarker_daemon.pl

    Usage: benchmarker_daemon.pl    simple perl daemon that watches the input command over time (memory usage, %cpu).

    SYNOPSIS
         
        benchmarker_daemon.pl (options) cmd

            cmd    the input command given as a string. Note that the command cannot be a asynchronous (e.g. ending with &)...

        options
            
            -update=N   The refresh interval in seconds (default:2)
            -output=F   The output file path, please put " around the path (default:'' -> the standard error output)

    DESCRIPTION

        A child is forked that checks every 2 seconds the current 'rss,%cpu' of the parent (output is printed to STDERR).
        The parent (this script) will just execute the given command.
        If parent does exit/terminate -> the child is killed (daemon vanishes).

    VERSION v0.0.3
    AUTHOR Paul Klemm
    DEPENDENCIES pstree, tail, grep, xargs, awk

---
## benchmarker/benchmark_write.sh
    2,4 GB/s

---
## conversion/excel2csv.R
    Loading required package: optparse
    Loading required package: readr
    Loading required package: writexl
    Loading required package: readxl
    Usage: this program converts a given excel XLS/XLSX table to csv (tab-separated) or vice versa. 

    AUTHOR
    	Paul Klemm

    VERSION v0.9.4

    DEPENDENCIES -


    Options:
    	-i CHARACTER, --input=CHARACTER
    		input table (excel .xls or .xlsx format or tab-csv format). E.g. '--input test.xls'

    	-d CHARACTER, --sep=CHARACTER
    		field separator

    	-h, --help
    		Show this help message and exit



---
## conversion/embl2fasta.pl
    embl2fasta.pl        coverts EMBL to fasta format
     
    SYNOPSIS
     
    embl2fasta.pl (options) EMBLFILE 

    VERSION v0.0.25
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## conversion/tRNAscanse2gff.sh
    Usage: tRNAscanse2gff.sh \<tRNAscansePREDICTEDSTRUCTE.csv\>
    Returns gff format. tRNAscanse reports - strand hits with reversed start stop indices.
      Input looks like this:
    ------
    NZ_CP037857_1_Escherichia_coli_BW25113__complete_genome.trna1 (221868-221944)	Length: 77 bp
    Type: Ile	Anticodon: GAT at 35-37 (221902-221904)	Score: 75.8
             *    |    *    |    *    |    *    |    *    |    *    |    *    |    * 
    Seq: AGGCTTGTAGCTCAGGTGGTtAGAGCGCACCCCTGATAAGGGTGAGGtCGGTGGTTCAAGTCCACTCAGGCCTACCA
    Str: >>>>>>>..>>>>.........<<<<.>>>>>.......<<<<<.....>>>>>.......<<<<<<<<<<<<....

    (...)
    ------

      Output looks like this:
    ------
    NC_013967.1	tRNAscan-SE	tRNA	11661	11733	.	+	.	Length=73; Type=Gln; Anticodon=CTGat35-37(11695-11697); Score=57.4; Seq=AGTCCCATGGGGTAGTGGCcaATCCTGTTGCCTTCTGGGGGCAACGACCCAGGTTCGAATCCTGGTGGGACTA; Str=(((((((..(((...........))).(((((.......)))))....(((((.......)))))))))))).
    NC_013967.1	tRNAscan-SE	tRNA	121223	121296	.	+	.	Length=74; Type=Arg; Anticodon=TCTat35-37(121257-121259); Score=78.9; Seq=GGGCGCGTAGCTCAGTCGGAcAGAGCGTCGGACTTCTAATCCGATGGtCGCGGGTTCGAATCCCGTCGCGCTCG; Str=(((((((..((((.........)))).(((((.......))))).....(((((.......)))))))))))).
    (...)
    ------

    AUTHOR Paul Klemm
    DEPENDENCIES sed, perl


---
## conversion/fasta2vector.pl
    fasta2vector.pl        reads a fasta (file or STDIN) and returns a linear version: 
    	name\tsequence
    	name\tsequence
    	name\tsequence
    	(...)
     
    SYNOPSIS
     
    fasta2vector.pl INFILE

    	INFILE can also be - for STDIN

    VERSION v0.0.9
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long

---
## conversion/infernal2gff.sh
    Usage: infernal2gff.sh \<infernal.tblout\>
    Returns gff format.
      Input looks like this:
    ------
    target name         accession query name           accession mdl mdl from   mdl to seq from   seq to strand trunc pass   gc  bias  score   E-value inc description of target
    ------------------- --------- -------------------- --------- --- -------- -------- -------- -------- ------ ----- ---- ---- ----- ------ --------- --- ---------------------
    NC_013967.1          -         5S_rRNA              RF00001    cm        1      119  1603076  1603197      +    no    1 0.59   0.0   86.8   1.7e-18 !   Haloferax volcanii DS2 complete genome
    NC_013967.1          -         5_8S_rRNA            RF00002    cm        1      154  1600063  1600219      +    no    1 0.58   0.0   41.3   7.8e-09 !   Haloferax volcanii DS2 complete genome
    (...)
    ------

      Output looks like this:
    ------
    NC_013967.1     infernal        5S_rRNA 1603076 1603197 .	+      .       accession="RF00001"; mdl="cm"; mdlfrom="1"; mdlfrom="119"; trunc="no"; pass="1"; gc="0.59"; bias="0.0"; score="86.8"; E-value="1.7e-18"; inc="!"; descriptionOfTarget="Haloferax volcanii DS2 complete genome "
    NC_013967.1     infernal        5S_rRNA 2766751 2766630 .	-      .       accession="RF00001"; mdl="cm"; mdlfrom="1"; mdlfrom="119"; trunc="no"; pass="1"; gc="0.59"; bias="0.0"; score="86.8"; E-value="1.7e-18"; inc="!"; descriptionOfTarget="Haloferax volcanii DS2 complete genome "
    (...)
    ------

    AUTHOR Paul Klemm
    DEPENDENCIES sed, perl


---
## conversion/faa2fna.pl
    USAGE: faa2fna.pl  Converts fasta files containing  amino acids to nucleotides.

    SYNOPSIS 

        faa2fna.pl (OPTIONS) FAAFILE 

        -transl_table=i : the translation table from NCBI (see link below) (default:1) 
        1 The Standard Code (-transl_table=1)
        2 The Vertebrate Mitochondrial Code (-transl_table=2)
        3 The Yeast Mitochondrial Code (-transl_table=3)
        4 The Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code (-transl_table=4)
        5 The Invertebrate Mitochondrial Code (-transl_table=5)
        6 The Ciliate, Dasycladacean and Hexamita Nuclear Code (-transl_table=6)
        9 The Echinoderm and Flatworm Mitochondrial Code (-transl_table=9)
        10 The Euplotid Nuclear Code (-transl_table=10)
        11 The Bacterial, Archaeal and Plant Plastid Code (-transl_table=11)
        12 The Alternative Yeast Nuclear Code (-transl_table=12)
        13 The Ascidian Mitochondrial Code (-transl_table=13)
        14 The Alternative Flatworm Mitochondrial Code (-transl_table=14)
        16 Chlorophycean Mitochondrial Code (-transl_table=16)
        21 Trematode Mitochondrial Code (-transl_table=21)
        22 Scenedesmus obliquus Mitochondrial Code (-transl_table=22)
        23 Thraustochytrium Mitochondrial Code (-transl_table=23)
        24 Pterobranchia Mitochondrial Code (-transl_table=24)
        25 Candidate Division SR1 and Gracilibacteria Code (-transl_table=25)
        (updated 3.2.2020)

    DESCRIPTION

        Unknown aa are converted to '???'

        NCBI translation table : https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes

    SEE ALSO https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes
    VERSION v0.0.4
    AUTHOR Paul Klemm

---
## conversion/fna2faaPlus.pl
    USAGE: fna2faaPlus.pl  Converts fasta files containing nucleotides to amino acids (automatically detecting reading frames)

    SYNOPSIS

        fna2faaPlus.pl (OPTIONS) FNAFILE

        -transl_table=i : the translation table from NCBI (see link below) (default:1) 
    		1 The Standard Code (-transl_table=1)
                    AAs  = FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
                  Starts = ---M------**--*----M---------------M----------------------------
                  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
                  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
                  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

    		2 The Vertebrate Mitochondrial Code (-transl_table=2)
    		3 The Yeast Mitochondrial Code (-transl_table=3)
    		4 The Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code (-transl_table=4)
    		5 The Invertebrate Mitochondrial Code (-transl_table=5)
    		6 The Ciliate, Dasycladacean and Hexamita Nuclear Code (-transl_table=6)
    		9 The Echinoderm and Flatworm Mitochondrial Code (-transl_table=9)
    		10 The Euplotid Nuclear Code (-transl_table=10)
    		11 The Bacterial, Archaeal and Plant Plastid Code (-transl_table=11)
    		12 The Alternative Yeast Nuclear Code (-transl_table=12)
    		13 The Ascidian Mitochondrial Code (-transl_table=13)
    		14 The Alternative Flatworm Mitochondrial Code (-transl_table=14)
    		16 Chlorophycean Mitochondrial Code (-transl_table=16)
    		21 Trematode Mitochondrial Code (-transl_table=21)
    		22 Scenedesmus obliquus Mitochondrial Code (-transl_table=22)
    		23 Thraustochytrium Mitochondrial Code (-transl_table=23)
    		24 Pterobranchia Mitochondrial Code (-transl_table=24)
    		25 Candidate Division SR1 and Gracilibacteria Code (-transl_table=25)
    		(updated 3.2.2020)

        -shift=i -frame=i [0,1,2] : the frame shift. If nothing given, then all possible shifts are analysed.
        -all : if set, then all possible translated sequences starting in a START codon are printed
        -full : if set, then a start codon is not needed anymore
        -M : if set, then only the aminoacid Methionin is used as a start codon (for -transl_table=1 Leucine=L could also represent a start codon).
        -ATG : if set, then only the sequence ATG is used as a start codon (for -transl_table=1 this is equivalent to -M).
        -fna : include the fna sequence in the header of the output 

    DESCRIPTION

        Converts fasta files containing nucleotides to amino acids:
        	* If there are ORFs : Find the longes ORF (start-stop sequence)
    	* Unknown codons are encoded with 'X'
        	* If there are multiple STARTS take the ORF that maximizes

        		numOfCodons(START->STOP/END) - numOfUnknownCodons + 5*hasSTOPcodon

        NCBI translation table : https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes

    SEE ALSO https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes
    VERSION v0.0.15
    AUTHOR Paul Klemm (modified version of the fna2faa.pl)

---
## conversion/clustal2fasta.pl
    clustal2fasta.pl        coverts alignments in clustalw to fasta format
     
    SYNOPSIS
     
    clustal2fasta.pl (options) ALIGNMENTFILE 

    	ALIGNMENTFILE	clustaw format or - for STDIN

    		e.g.:
    		...
    		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
    		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
    		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
    		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
    		...

    	OUTPUT

    		>Lactobacillus_salivarius__GCF_
    		ATGATTATGCTATGATTGATTT---
    		>Lactobacillus_hayakitensis_DSM
    		CTAAT-ATGGTATTATAATTAT---
    		>Dolosicoccus_paucivorans__GCF_
    		ATGT-TATGGTATACTTAT-GATGC
    		>Leuconostoc_garlicum__GCF_0019
    		ATTTTCGCGGTATAATAATTGATG-

    VERSION v0.0.9
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## igit.sh
    USAGE: igit.sh : a simple script to update version numbers and generate git commits. 

    	(i) if .gitignore is missing, I create a general purpose one.
    	(ii) looks for all files in current directory and searches for git updates.
    	(iii) I find 'version=' strings and will calculate a new version number according to the changes (see below).
    		supported languages: pl|R|sh|py|js|php|html.
    	(iv) A file called 'version' will recive a combined version update and 
    		'CHANGELOG' will update with a more detailed summary 
    		(only if those files are generated by this script).
    	
    	Format of updated versions : 'Major:Median:Minor'
    		Every 10 changes the Minor gets incremented by one. 
    		40 Minor changes = one Median update. 
    		10 Median changes = one Major update

    	Only works if there is no pending commit (git log -1)!

    SYNOPSIS igit.sh (options)
    	no option : No changes are made (safe to execute).
    	-c : Changes are made to the version strings and a commit is created from all changes. 'git push' can be called afterwards
    	-c -r : A README.md is automatically created from all help pages of the scripts within the directory. 
    		In further commits -r is not needed anymore and the README will be updated automatically.
    AUTHER Paul Klemm
    VERSION v0.5.10

---
## fastaAndGff/easyNCBI.pl
     usually a good option is: -exclude virus
    easyNCBI.pl        simple interface to NCBI refseq/genbank ftp server
     
    SYNOPSIS

    easyNCBI.pl options (genbank|refseq|ensembl) MODUS species

    	Allready downloaded files are not overwritten (save to rerun) only if there are new files on the NCBI server.

    	(genbank|refseq|ensembl)
    		either the genbank, refseq or ensembl database (ftp)

    	MODUS  the main modus
    		get -> download all available data for the given species
    		get genome        : *genomic.fna
    		get gff           : *genomic.gff
    		get genomic       : *genomic.fna and *genomic.gff (both)
    		get proteome      : *protein.faa
    		get transcriptome : *rna.fna
    		get cds           : *cds.fna
    		get cdna          : *cdna.fna
    		get from          : *_from_*
    		get ... reference : only download entries marked as "reference genome" 
    		
    	species
    		A species name / NCBI id (GCF* or ASM* or PRJNA*) or a file with multiple species names / ids (use - for STDIN)

    	prefilter options: (set to "" to disable)
    		-assembly_level (default:"Complete Genome")
    			if you dont find anything usually set this to an empty string.
    		-release_type (default:"Major")
    		-genome_rep (default:"Full")
    		-version_status (default:"latest")

    	with -relaxed all prefilters are disabled (except -version_status "latest")

    	other options:
    		-exclude term : exclude any entry with the given search term (e.g. you could use 'virus')
    		-dry : do not download anything just search the database (FTP connection is required to check files)
    		-drier : only search the summary files (very fast)
    		-timeout : timeout between ftp->get queries in seconds (default:1)
    		-fallback : if nothing found try again without the defined prefilters. If reference is set and nothing was found
    					then first a search is conducted without the reference option and then without the filters.
    		-first : download only the first entry if there are multiple hits

    	Enviroment variable:
    		EASYNCBISUMMARY="$HOME/"
    			the path to the summary file. If set then the summary file is not dowloaded in every directory easyNCBI.pl is executed.
    			In case nothing is defined or the path is invalid, the home directory is used ($HOME).
    	! usually use -exclude virus to exclude any virus !

    EXAMPLES
    	$ easyNCBI.pl refseq get genomic reference Bacillus subtilis -exclude virus
    	would download the 2 files:
    		Bacillus_subtilis_subsp._subtilis_str._168_2009_03_17_PRJNA57675.GCF_000009045.1_ASM904v1_genomic.fna.gz
    		Bacillus_subtilis_subsp._subtilis_str._168_2009_03_17_PRJNA57675.GCF_000009045.1_ASM904v1_genomic.gff.gz 

    	$ easyNCBI.pl refseq get genome Bacillus subtilis 3610
    	would download:
    		Bacillus_subtilis_subsp._subtilis_NCIB_3610_=_ATCC_6051_=_DSM_10_2019_06_06_PRJNA224116.GCF_006088795.1_ASM608879v1_genomic.fna.gz

    	$ easyNCBI.pl refseq get gff ^Bacillus
    	would download the genomic.gff files of all ~780 species of the bacillus genus

    	$ easyNCBI.pl ensembl get cdna Bacillus subtilis 168 -first
    	would download the latest cdna.fa from ensembl 

    	$ easyNCBI.pl -drier refseq get gff 000027085
    	search for GCF identifier

    VERSION v0.1.19
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, Net::FTP

---
## fastaAndGff/highlightFasta.pl
    USAGE : highlightFasta.pl   marks genomic positions in a fasta file

    SYNOPSIS 
      highlightFasta.pl \<FASTAFILE\> chromosom start end strand
      chromosom can be - for the first entry of the fasta file

      or

      highlightFasta.pl \<FASTAFILE\> \<GFFFILE\>
      start and end are 1-based indices/coordinates

      You can use 'aha' to convert the output to html (conda install aha)

    VERSION v0.3.2
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Cwd 'abs_path', perl : File::Basename

---
## fastaAndGff/splitGFF.pl
    splitgff.pl (options) \<input.gff\>        splits the input.gff into chromosom and strand specific gff files (the files are created in the cwd)

    OPTIONS

    	-nostrand : disables the splitting by strand
    	-nochromosome : disables the splitting by chromosome

    VERSION v0.0.4
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, perl : File::Basename

---
## fastaAndGff/splitfasta.pl [![pipeline status](https://gitlab.com/paulklemm_PHD/scripts/badges/master/pipeline.svg)](https://gitlab.com/paulklemm_PHD/scripts/pipelines)
    USAGE: splitfasta.pl \<FASTAFILE\>
    Returns for each '>' entry of the fasta a single file inside a created *_fastaFiles/ directory (* = input fasta name)

    VERSION v0.0.3
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## fastaAndGff/getgff.pl [![pipeline status](https://gitlab.com/paulklemm_PHD/scripts/badges/master/pipeline.svg)](https://gitlab.com/paulklemm_PHD/scripts/pipelines)
    USAGE : getgff.pl   extracts genomic positions of a given gff file OR from command line

    SYNOPSIS 
      getgff.pl (options) FASTA(S) chromosom start end strand
        chromosom: can be - for the first entry of the fasta file
        start, end: are 1-based indices/coordinates
        FASTA(S) : one or multiple fasta file

      or

      getgff.pl (options) FASTA(S) GFF
        FASTA(S) : one or multiple fasta file (FASTA1 FASTA2 ...)
        GFF : one gff file

    OPTIONAL options

      -id=STRING 
        set the name of the output fasta entries to the gff attribute with this name 
        STRING: product, id, name, ...
          multiple fields can be specified by | (e.g. -id='ID|product|Name')
          the gff column names can be used here too:
        seqname source feature start end score strand frame attribute 

      -format
        standard 80 characters per line output

    VERSION v0.2.0
    AUTHOR Paul Klemm
    DEPENDENCIES perl: Cwd, File::Basename

---
## fastaAndGff/rc.pl [![pipeline status](https://gitlab.com/paulklemm_PHD/scripts/badges/master/pipeline.svg)](https://gitlab.com/paulklemm_PHD/scripts/pipelines)
    USAGE : ./fastaAndGff/rc.pl  generates the reverse and or complement of a given input sequence

    SYNOPSIS
      ./fastaAndGff/rc.pl (options) FASTAFILE/-

      FASTAFILE = nucleotide (fna) file OR '-' then it takes the input from STDIN (no header is needed).
      
      --complement, --comp, -c : only do the complement
      --reverse, --rev, -r 	: only reverse
      --rna                 : force to use Uracil instead of Thymin (otherwise it is set automatically)
      --dna                 : force to use Thymin instead of Uracil 
      --all, -a             : print all all variations (reversed, complemented, rc and the original sequence). Variations are separated by |
                              you should use the --dna or --rna option along with this or this can produce unwanted results! 
    EXAMPLES

      ./fastaAndGff/rc.pl - <<< 'ACGCAGCGAC'

      # search all variations of a string in a fasta file:

      grep -E "$(./fastaAndGff/rc.pl -a - <<< 'ACGCAGCGAC')" fasta.fna

    VERSION v0.0.2
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Cwd 'abs_path', perl : File::Basename

---
## fastaAndGff/compareMummerFasta.pl
    compareMummerFasta.pl        compares a gff+fasta with a assembled genome + mummer comparison of the 2 fasta files

    SYNOPSIS

    compareMummerFasta.pl (options) -fastaRef b.fna -gffRef b.gff -fastaNew a.fna -mums avsb.mums

    MANDATORY

    	-fastaRef and -gffRef : the reference gff and genomic fasta files
    	-fastaNew : new assembled genomic fasta file (needs to have matching chromosome names)
    	-mums : the output of 'mummer -mums -F FASTANEW FASTAREF'

    OPTIONAL

    	-gff_filter : filters the input gff (feature) [default:gene]
    	-gff_id : the attribute name used as the id (part of attribute) [default:ID]
    	-out : the output prefix name (default:compareMummerFasta.pl.project)

    DESCRIPTION

    	The input to mummer needs to be 1. new assmbled fasta and 2. the reference fasta, s.t. the mums contains:
    		chr,pos_new,pos_ref,len_match
    	The output is split in 3 files:
    		out.pos : VCF like output of single nt changes (includes codon changes according to the gff)
    		out.stat : gene-level changes (how many genes contain a indel,...)
    		out.aln : each non-identical gene is aligned (between the REF and NEW). 

    	Use case:
    		1. Map DNAseq reads against a reference genomic fasta file
    		2. Create a consensus fasta file (-fastaNew) e.g. with easybcfconsensus.sh
    		3. apply mummer to compare the fasta files
    		4. summarize differences with this script

    LIMITATIONS

    	1. for genes on - strand: the codons are printed from the - strand while the Ref/New nt are always from + strand
    	2. gene start of NEW is assumed to be identical to the one of the REF (gff start of ref with alignment position of new)
    	3. frameshift is not detected (e.g. could be #indels mod 3 != 0) and mutations are analyzed independently

    VERSION v0.7.6
    AUTHOR Paul Klemm
    DEPENDENCIES Perl Getopt::Long, File::Basename, easyalignment.py (https://gitlab.com/paulklemm_PHD/scripts)

---
## fastaAndGff/compareGffFasta.pl
    compareGffFasta.pl        compares gffA with gffB using sequences of the corresponding fasta files

    SYNOPSIS

    compareGffFasta.pl (options) -gffA a.gff -gffB b.gff -fastaA a.fna -fastaB b.fna -out output

    	-gff_filter : filters the input gff (feature) [default:gene]
    	-gff_id : the attribute name used as the id (part of attribute) [default:ID]

    DESCRIPTION

    VERSION v0.3.16
    AUTHOR Paul Klemm
    DEPENDENCIES Perl Getopt::Long, File::Basename, easyalignment.py (https://gitlab.com/paulklemm_PHD/scripts)

---
## fastaAndGff/getGeneLength.pl
    USAGE: getGeneLength.pl \<FASTAFILE\> Returns for each '>' entry of the fasta file: 'name\tlength(gene)'

    VERSION v0.0.11
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## fastaAndGff/easybcfconsensus.sh
    USAGE : easybcfconsensus.sh REFFASTA READS
     This scripts generate a consensus.fa from READS compared to the reference REFFASTA (missing regions are filled in with with reference sequences)
     Reads are filtered with default bcftools filter [UNMAP,SECONDARY,QCFAIL,DUP]
     Input READS can be in sam or bam format, REFFASTA is the mapping target of READS (matching QNAME of the READS)
     Input is expected to be haploidic
     adapted from https://samtools.github.io/bcftools/howtos/consensus-sequence.html
     You can use murmer and compareMummerFasta.pl for further analysis


    VERSION 0.0.10
    AUTHOR Paul Klemm
    DEPENDANCIES bcftools, samtools

---
## fastaAndGff/grepfasta.pl
    grepfasta.pl        greps all genes/proteins of a given fasta file
     
    SYNOPSIS
     
    grepfasta.pl (options) QUERY INFILE1 (INFILE2 ...)

    	QUERY	identifier FILE or search query STRING:
    		a)	string of one identifier e.g. 'tr|asd3|asd' OR multiple identifier separated by ',' OR '-' for STDIN
    		b)	(-file QUERY) file with the ids. The file contains either just 
    			identifier or tabseparated a mapping:
    			e.g. POP1 to the 3 identifier 
    			tr|asd|asd, tr|asd2|asd and tr|asd3|asd.
    	INFILE	file containing the query ids (database)

    	optional:
    		-tofiles, -t 	print everything to files instead of files 
    		-prefix=s 	prefix of each identifier (is regex interpreted)
    				[default: -prefix=">"]
    		-E    	enables regex matching otherwise the string is escaped (e.g. | -> |)
    		-source, -s    	adds the filename to the found gene-name
    		-F=s 	char delimiter for multiple identifier (default: ',')
    		-tag=s 	search by fasta tag, e.g. -tag='GN' will search for ... GN=PRPSAP2 ...
    		-seq    search in the sequence space instead of in the header

    DESCRIPTION
     
    	This script finds all ids of a list of fasta files with identifier
    	provided in a different file and saves the output to seperate files named
    	ID_FASTA where ID is the found identifier and FASTA the 
    	name of the fasta file that contains this identifier.
    	STDERR contains ids and the number of times they are found.
           
    EXAMPLES
     
     	# 1. most simple call:

    	perl grepfasta.pl 'tr|asd|asd' *.faa

    		STDOUT:
    			>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
    			MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG

     	# 2. regex search:

    	perl grepfasta.pl -E '.*a?sd[0-3].*' *.faa

     	# 3. multiple ids:

    	perl grepfasta.pl 'tr|asd|asd,tr|asd2|asd' *.faa

     	# 4. id file and write output to files:

    	cat test.idlist | grepfasta.pl - test.faa

    		-----test.idlist:---------- (does not need the first column)
    		POP1	tr|asd|asd
    		POP1	tr|asd1|asd
    		POP1	tr|asd2|asd
    		POP2	tr|asd3|asd
    		
     	# 5. id file and write output to files:

    	grepfasta.pl -tofiles test.idlist test.faa

    		-----test.idlist:---------- (does not need the first column)
    		POP1	tr|asd|asd
    		POP1	tr|asd1|asd
    		POP1	tr|asd2|asd
    		POP2	tr|asd3|asd
    		
    		------test.faa:------------
    		>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
    		MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG
    		>tr|asd3|asd Histone H4 OS=Acremonium chrysogenum (strain ATCC 11550 (...)
    		MTGRGKGGKGLGKGGAKRHRKILKDNIQGITKPAIRRLARRGGVKRISAMIYEETRGVLK
    		>tr|A0A086SUI7|A0A086SUI7_ACRC1 Uncharacterized protein OS=Acremonium(...)
    		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN
    		>tr|asd2|asd 1-phosphatidylinositol 3-phosphate 5-(...)
    		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN

    		OUTPUT (2 files)

    		------POP1_test:------------
    		>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
    		MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG
    		>tr|asd2|asd 1-phosphatidylinositol 3-phosphate 5-(...)
    		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN
    		
    		------POP2_test:------------
    		>tr|asd3|asd Histone H4 OS=Acremonium chrysogenum (strain ATCC 11550 (...)
    		MTGRGKGGKGLGKGGAKRHRKILKDNIQGITKPAIRRLARRGGVKRISAMIYEETRGVLK

    		STDERR: 	
    		tr|asd|asd	1
    		tr|asd1|asd	1
    		tr|asd2|asd	0
    		tr|asd3|asd	1

    VERSION v0.0.39
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, perl : File::Basename

---
## fastaAndGff/clusterStrings.pl
    clusterStrings.pl        cluster input strings of same lengths (sequences)
     
    SYNOPSIS
     
    clusterStrings.pl (options) INFILE

    	INFILE  input file containing the strings (can be - for STDIN)

    	optional:
    		-fasta      parses fasta formats
    		-consensus  prints out the consensus string (with maximal character probability)
    					otherwise one member of the group is printed
    		-all        print all entries with a group number
    		-tofiles    split output in different files 
    		-stat       emits for each symbol the number input strings that agree with that symbol. The numbers are separated by |
    		-t FLOAT    cluster threshold: the maximal percentual similarity for merging 2 strings/groups (default:0.45) 
    		-maxit INT  maximal number of iterations (default:10000) 

    DESCRIPTION
     
    	TODO

    EXAMPLES

    	# use protein sequences

    	clusterStrings.pl -fasta -consensus proteins.faa

    	# use STDIN

    	awk '{print $1}' somefile | clusterStrings.pl -

    VERSION v0.0.19
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, File::Basename, List::Util qw( max )

---
## fastaAndGff/grep_k_neighbourhood.pl
    grep_k_neighbourhood.pl        finds k upper and lower proteins
     
    SYNOPSIS
     
    grep_k_neighbourhood.pl (OPTIONS) INFILE

    OPTIONS

    	INFILE : a gff file or - for STDIN

    	mandatory:
    		-k=	 the number of matches that are reported (up and down)
    		-q=	 the query string

    	optional:
    		-prefix= 	
    			the regularexpression matching the query string 
    			(e.g. for gff-files: gene=([^:]+))
    		-preselection=
    			regular expression for for identifing rows
    			(e.g. for genes in a gff-files: 	gene	)
    		-full         
    			if set then the full lines are printed instead of the raw IDs

    DESCRIPTION
     
    	The gff is assumed to be sorted.
           
    	Example:
     
    	perl grep_k_neighbourhood.pl -k=3 -q='POP1' gffs/saccharomyces_cerevisiae.gff3

    	STDOUT:
    		SQS1
    		ATG4
    		SSU72
    		POP1 <--------
    		ADE12
    		ALG9
    		MGS1
     
    	If the gff is not sorted, use sort first:

    	LC_ALL=C sort -k1,1 -k4,4n -t$'\t' Lactobacillus_sen.gff | grep_k_neighbourhood.pl -preselection=".*" -prefix="ID=([^;]+)" -q=6SRNA -k=1 -

    	STDOUT:
    		cds549
    		6SRNA <--------
    		cds444

    VERSION v0.0.6
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long

---
## fastaAndGff/newickToAverageDistance.pl
    Can't locate Bio/TreeIO.pm in @INC (you may need to install the Bio::TreeIO module) (@INC contains: /home/paul/perl5/lib/perl5/x86_64-linux-thread-multi /home/paul/perl5/lib/perl5 /home/paul/perl5/lib/perl5/x86_64-linux-thread-multi /home/paul/perl5/lib/perl5 /home/paul/anaconda3/lib/site_perl/5.26.2/x86_64-linux-thread-multi /home/paul/anaconda3/lib/site_perl/5.26.2 /home/paul/anaconda3/lib/5.26.2/x86_64-linux-thread-multi /home/paul/anaconda3/lib/5.26.2 .) at ./fastaAndGff/newickToAverageDistance.pl line 4.
    BEGIN failed--compilation aborted at ./fastaAndGff/newickToAverageDistance.pl line 4.

---
generated_using_igit