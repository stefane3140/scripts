#!/usr/bin/env perl
#pk

use threads;
use threads::shared;
use Thread::Queue;
my $q = Thread::Queue->new();
my @ret :shared;

use strict;
use warnings "all";

my $version="0.0.8"; # last updated on 2021-10-02 23:08:44

our $COLOR="\033[33m"; #yellowish
our $COLOR1="\033[32m"; #green
our $COLOR2="\033[31m"; #red
our $NC ="\033[0m"; # No Color

my $tput=`tput color 2>/dev/null`; # test if the shell supports colors
$tput=~s/[\r\n]+$//;
if ($tput=~m/[^0-9]|^&/ && $tput <16) {
  $COLOR="";$NC="";$COLOR1="";$COLOR2="";
}

use Cwd qw(abs_path cwd);
use List::Util qw(max);
use File::Basename; # basename()

my $help=0;
our $INPUT="";
our $w=1500;
our $s=$w/2;
our $cpus=1;
local $ENV{PATH} = "$ENV{PATH}:".abs_path(dirname(__FILE__));

my $rnd = rand();

my $usage = "
RNAup_slidingWindow.pl     performes RNAup in k sized windows with step size s.

SYNOPSIS

RNAup_slidingWindow.pl (options) FASTA/-

	FASTA or - (= STDIN) 
		The input should have at least 2 sequences.

	-w : window size (default $w)
	-s : step size (default $s)
	--cpus, -t : number of threads (default: $cpus)

DESCRIPTION

	A k sized window slides in s steps across the input sequence and RNAup is perfomed and
	the optimal/minimal binding energy is printed.
	The result should correspond to a full RNAup call if there are no long-range 
	interaction (longer than window size $w).

	!! Either the first input or the other input sequences needs to be longer than the window size w !!
	
	The RNAup modus is --interaction_first --no_output_file.

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Cwd qw(abs_path cwd), List::Util qw(max), File::Basename, Thread::Queue, RNAup (viennarna package)
";

foreach my $a (@ARGV) {
	if ($a =~ m/^--?(help?|h)/) {$help = 1;}
	elsif ($a =~ m/^--?(s)[= ]([0-9]+)/) {$s = $2;}
	elsif ($a =~ m/^--?(w)[= ]([0-9]+)/) {$w = $2;}
	elsif ($a =~ m/^--?(version|v)$/) {print "$0\tv$version\n"; exit 0;}
	elsif ($a =~ m/^--?(cpus?|t)[= ]([0-9]+)/) {$cpus = $2;}
	elsif($a =~ m/^--?./){
		print $usage."\n[ERROR] unknown option '$_'!?\n";exit 1;
	}else{
		$INPUT=$a;
	}
}

if($help){print $usage; exit 0}

my %data;
our $name="";
our $name_first="";
our $seq_first="";
our $total_qs=1;
sub processLine{
	$_=~s/[\n\r]+$//g;
	if($_=~m/^>/){
		if($name_first eq ""){ $name_first = $_ }
		else{ $name=$_ }
	}else{
		if($name eq ""){
			$seq_first.=$_
		}else{
			if(!exists $data{$name}){ $data{$name}="" }
			$data{$name}.=$_;
		}
	}
}

# read input file or from STDIN or as string from first argument
if(-e $INPUT){ # if file
	print STDERR "reading $INPUT\n";
	open(my $FH_in,"<$INPUT") || die($!);
	while(<$FH_in>){ processLine($_) }
	close($FH_in);
}elsif($INPUT eq "-"){ # STDIN
	print STDERR "reading STDIN\n";
	while(<STDIN>){ processLine($_) }
}

if( length $seq_first > $w && max map { length $_ } values %data > $w ){
	print STDERR "[ERROR] !! Either the first input is longer than $w or the other input sequences !!\n"; die;
}

print $name_first."\n";

sub worker{
	local $SIG{KILL} = sub { threads->exit };

	my $tid = threads->tid;

	while (defined(my $job = $q->dequeue())) { # they terminate for a undef job 
		
		print STDERR "\033[J[tid=$tid] ".(int(10000*(1-$q->pending()/$total_qs))/100)."%\033[G";

		my $res = `$job`;

		$res =~ s/[\n\r]/#/g;
		
		if(!defined $ret[$tid]){
			$ret[$tid] = "$res";
		}else{
			$ret[$tid].="$;$res";
		}
	}

	return; # return the results + join does not work ...
}

# handle a SIG/INT terminating gracefully
$SIG{TERM} = sub {
 	foreach (threads->list()) {$_->kill('KILL')->detach}
 	exit 1;
};
$SIG{INT} = $SIG{TERM};

foreach my $key (sort keys %data) {

	print $key."\n";

	# enqueue all RNAup calls
	for (my $j = 0; $j < length $seq_first; $j+=$s) {
		my $cur_seq_first = substr($seq_first,$j,$w);
		for (my $i = 0; $i < length $data{$key}; $i+=$s) {
			my $cur_seq = substr($data{$key},$i,$w);
			$q->enqueue("echo 'OFFSETi=$i;OFFSETj=$j;LENi=".(length $data{$key}).";LENj=".(length $seq_first)."'; RNAup −−no_output_file --interaction_first 2>/dev/null << EOF\n$name_first\n$cur_seq_first\n$key\n$cur_seq\nEOF");
		}
	}

	$total_qs = $q->pending();

	# stop signal for each worker
	$q->enqueue(undef) for 1..$cpus; 

	# spawn $cpus worker
	for (my $i = 0; $i < $cpus; $i++) { threads->create("worker") }

	# wait for + collect results from worker 
	my $mindg=0;
	my $minLine="";
	foreach my $thr ( threads->list() ) {

		$thr->join; # wait on finish

		my $joined = $ret[$thr->tid];
		if(!defined $joined || $joined eq "" ){next}

		foreach my $result (split $;,$joined) {	
			if(!defined $result || $result eq "" ){next}

			my $offseti=0; if($result =~ /OFFSETi=([^ ().;#]+)/){ $offseti=$1 }
			my $offsetj=0; if($result =~ /OFFSETj=([^ ().;#]+)/){ $offsetj=$1 }
			my $leni=0; if($result =~ /LENi=([^ ().;#]+)/){ $leni=$1 }
			my $lenj=0; if($result =~ /LENj=([^ ().;#]+)/){ $lenj=$1 }
			
			my @result_arr = split("#",$result);
			my @result_arr2 = split(/[ :,\t]+/,$result_arr[3],6);

			if( ( $offseti > 0 && $offseti + $w + $s < $leni && ( $result_arr2[1] < $s/5 || $result_arr2[2] > $w-$s/5 ) ) ||
				( $offsetj > 0 && $offsetj + $w + $s < $lenj && ( $result_arr2[3] < $s/5 || $result_arr2[4] > $w-$s/5 ) )){
				# the current bind is at the very start or end of a window -> dissmiss
				next;
			}

			$result_arr2[1]+=$offseti;
			$result_arr2[2]+=$offseti;
			$result_arr2[3]+=$offsetj;
			$result_arr2[4]+=$offsetj;
			
			$result = $result_arr2[0]." ".$result_arr2[1].",".$result_arr2[2]." : ".$result_arr2[3].",".$result_arr2[4]." ".$result_arr2[5]."\n".$result_arr[4];

			my $dg = 0;
			if($result =~ /\( ?([^ =]+) ?=/){ $dg=$1 }
			if($dg < $mindg){$mindg = $dg; $minLine = $result}
		}
	}
	print "$minLine\n";
}

