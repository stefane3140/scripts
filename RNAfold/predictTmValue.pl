#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

our $COLOR="\033[33m"; #yellowish
our $COLOR1="\033[32m"; #green
our $COLOR2="\033[31m"; #red
our $NC ="\033[0m"; # No Color
my $version="0.0.4"; # last updated on 2021-10-02 23:08:44

my $tput=`tput color 2>/dev/null`; # test if the shell supports colors
$tput=~s/[\r\n]+$//;
if ($tput=~m/[^0-9]|^&/ && $tput <16) {
  $COLOR="";$NC="";$COLOR1="";$COLOR2="";
}

use Cwd qw(abs_path cwd);
use File::Basename; # basename()

my $help=0;
our $INPUT="";
my $threads=4;
my $fullResult=0;
my $correct=0;
local $ENV{PATH} = "$ENV{PATH}:".abs_path(dirname(__FILE__)); # add the current directory of the script astarget.pl to $PATH enviroment variable

our $rnd=rand();

my $usage = "
predictTmValue.pl     predict melting temperature Tm.

SYNOPSIS

predictTmValue.pl (options) FASTA/STDIN

	FASTA/STDIN (from file or string argument)

	--dnaTm, -d : corrects for dna Tm value : = 0.72587 * temp + 0.53782
	--rnaTm, -r : corrects for rna Tm value : = 0.83807 * temp - 13.26109
	--full, -f : full output for debugging

DESCRIPTION

	RNAcofold wrapper for searching of the self-hybridization temperature (-T), #
	such that the dimer concentration c(A-rc(A)) equals about 
	half of the predicted ensemble.

EXAMPLES

	# predict a RNA Tm value:
	\$ predictTmValue.pl -r ACACAGATAGA
	30.21379125

	# predict a plain Tm value:
	\$ predictTmValue.pl ACACAGATAGA
	51.875

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Cwd qw(abs_path cwd), File::Basename, RNAcofold (viennarna package)
";

foreach my $a (@ARGV) {
	if ($a =~ m/^--?(help?|h)/) {$help = 1;}
	elsif ($a =~ m/^--?(full?|f)/) {$fullResult = 1;}
	elsif ($a =~ m/^--?(dnaTm?|d)/) {$correct = 1;$fullResult = 0;}
	elsif ($a =~ m/^--?(version?|v)$/) {print "$0\tv$version\n"; exit 0; }
	elsif ($a =~ m/^--?(rnaTm?|r)/) {$correct = 2;$fullResult = 0;}
	elsif($a =~ m/^-/){
		print $usage."\n[ERROR] unknown option '$_'!?\n";exit 1;
	}else{
		$INPUT=$a;
	}
}

if($help){print $usage; exit 0}

mkdir(".tmvalue_tmp$rnd");

system("RNAcofold -h >.tmvalue_tmp$rnd/.RNAcofold.err 2>&1");
if($? != 0){print STDERR `cat .tmvalue_tmp$rnd/.RNAcofold.err`."\n[ERROR] RNAcofold (from viennarna) is missing or is not executable (please make sure that RNAcofold is installed correctly).\n";unlink(".tmvalue_tmp$rnd/.RNAcofold.err");exit 1;}
unlink(".tmvalue_tmp$rnd/.RNAcofold.err");
print STDERR "RNAcofold ${COLOR1}ok${NC}\n";


open(FHOUT,">.tmvalue_tmp$rnd/.concfile");
print FHOUT "0.0000001\t0.0000001\n";
close(FHOUT);


sub optimizeTm{
	my $name="";
	open(my $FH_in,"<$INPUT") || die($!);
	while(<$FH_in>){
		chomp;

		if($_=~m/^>/){ $name=$_ }else{

			$_=~tr/uU/tT/;
			if($_!~m/&/){
				my $rc = reverse($_);
				$rc =~ tr/ACGTacgt/TGCAtgca/;
				if($rc eq $_){$_.="A";}
				$_.="&".$rc;
			}
			
			#print STDERR ">$name\t$_\n";

			my $temp = -1;     # current value (always in the middle between up and down -> move up and down for gradient descent)
			my $temp_up = 100; # upper bound
			my $temp_down = -10; # lower bound
			my $temp_last=-2;  # save last value for end condition

			while(1){
				$temp=($temp_up-$temp_down)/2+$temp_down;

				#print STDERR ">$temp\n";

				my $cofold=`cd .tmvalue_tmp$rnd/; echo "$_" | RNAcofold --noPS -f .concfile -T $temp | tail -n1`;
				chomp($cofold);

				my @spl = split("\t",$cofold);

				if($fullResult){print $cofold."\t$name\t$temp\n";} # print current ensemble + temperature

				if(abs($temp-$temp_last)<0.0001){ # if temp does not change that much anymore -> done
					if($correct==1){$temp=0.72587*$temp+0.53782} # dna
					elsif($correct==2){$temp=0.83807*$temp-13.26109} # rna
					print "$name\n$temp\n";
					last;
				}elsif($spl[2] < 0.48 * ($spl[2]+$spl[3]+$spl[4]+$spl[5]+$spl[6]) ){ # AB is underrepresented -> max temp is now the given temp (= decreasing the temperature to increase the AB ratio)
					$temp_up=$temp;
				}elsif($spl[2] > 0.52 * ($spl[2]+$spl[3]+$spl[4]+$spl[5]+$spl[6]) ){ # ... increase temp ...
					$temp_down=$temp;
				}else{
					if($correct==1){$temp=0.72587*$temp+0.53782} # dna
					elsif($correct==2){$temp=0.83807*$temp-13.26109} # rna
					print "$name\n$temp\n";
					last; # if AB equals about half of the predicted ensemble -> done
				}
				$temp_last=$temp;
			}
		}
	}
}

# read input file or from STDIN or as string from first argument
if(-e $INPUT){ # if file -> OK
	optimizeTm();
}elsif($INPUT ne ""){ # if given as string in command line -> write to temp file .infile

	open(FH,">.tmvalue_tmp$rnd/.infile");
	print FH ">infile"."\n";
	my $seq=$INPUT;
	$seq=~tr/uU/tT/;
	my $rc = reverse($seq);
	$rc =~ tr/ACGTacgt/TGCAtgca/;
	if($rc eq $seq){$seq.="A";}
	print FH $seq."&".$rc;
	close(FH);
	$INPUT=".tmvalue_tmp$rnd/.infile";
	optimizeTm();
	unlink(".tmvalue_tmp$rnd/.infile");

}

unlink(".tmvalue_tmp$rnd/.concfile");
system("rm -r .tmvalue_tmp$rnd");
