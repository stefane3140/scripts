#!/usr/bin/env perl
#pk

use List::Util qw/shuffle/;
my $version="0.0.3"; # last updated on 2021-10-01 14:11:18

my $usage = "Usage: getQiagen.pl    simple interface to the qiagen web API

SYNOPSIS
 
getQiagen.pl SEQUENCE

    SEQUENCE    the input sequence.

    Use Tm DNA for applications such as Chromatin ISH-PCR - Southern
    Use Tm RNA for applications such as RNA ISH - microarray - Northern

EXAMPLES

	\$ getQiagen.pl ACTACACACACAGGG
	
	[getQiagen.pl] loaded input sequence : ACTACACACACAGGG
	# sequence	rnaTm	hybrydizationScore	hybridizationLines	secondaryStructureScore	secondaryStructureLines
	ACTACACACACAGGG	36	11	CACAGGG:GGGACAC	11	ACTACACACACAGGG: (  (   (   )))

    The secondary structure of an oligonucleotide with a score below 20 is unlikely to be stable at room temperature. 
    Scores above 30 are likely to produce secondary structures that are stable at room temperature.

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : List::Util qw/shuffle/
    wget

SEE ALSO

    the geneglobe.qiagen API for optimizedoligo: 
        https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?secondaryStructure=true&sequence=SEQUENCE

    the geneglobe.qiagen API for tmprediction: 
        https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=SEQUENCE
";

# + = %2B
# only ACGT
# https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=AC%2BGTA%2BC%2BG

###########################
###########################
###########################

if(scalar @ARGV != 0 && $ARGV[0] =~ m/-version|-v$/){print "$0\tv$version\n";exit 0;}
if(scalar @ARGV == 0 || $ARGV =~ m/help|-h/){print $usage; exit 0}

my $input=$ARGV[0];
$input=~s/u/t/ig;
$input=uc $input;
my $org_input=$input;
$input =~ s/\+(.)/%2B$1/g;

print STDERR "[getQiagen.pl] loaded input sequence : '$org_input'\n";


my $send=join("",$input);
my $lnaOpt_RAW = `wget -q -O - "https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?secondaryStructure=true&sequence=$send" 2>/dev/null`;

my $hybridizationLines="";
if($lnaOpt_RAW=~m/hybridizationLines":"([^\\]+)\\n[^\\]+\\n([^\\]+)/){$hybridizationLines=$1." :".$2;my $i = 0; $hybridizationLines=join "", grep { $i++ % 2 } split //, $hybridizationLines }
my $secondaryStructureLines="";
if($lnaOpt_RAW=~m/secondaryStructureLines":"[^\\]+\\n([^\\]+)\\n([^\\]+)/){$secondaryStructureLines=$1." :".$2; my $i = 0; $secondaryStructureLines=join "", grep { $i++ % 2 } split //, $secondaryStructureLines }

my $hybrydizationScore=9999;
my $max_hybrydizationScore = 20;
if($lnaOpt_RAW=~m/"hybrydizationScore":"([^"]+)"/){$hybrydizationScore=$1;}
if($max_hybrydizationScore < $hybrydizationScore){$max_hybrydizationScore=$hybrydizationScore;}
my $max_secondaryStructureScore = 20;
my $secondaryStructureScore=9999;
if($lnaOpt_RAW=~m/"secondaryStructureScore":"([^"]+)"/){$secondaryStructureScore=$1;}
if($max_secondaryStructureScore < $secondaryStructureScore){$max_secondaryStructureScore=$secondaryStructureScore;}

my $tm_RAW = `wget -q -O - "https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=$send" 2>/dev/null`;

my $rnaTm_opt = "-";
my $dnaTm_opt = "-";
if($tm_RAW=~m/"rnaTm":"([^ "]+) /){$rnaTm_opt=$1;}
if($tm_RAW=~m/"dnaTm":"([^ "]+) /){$dnaTm_opt=$1;}
my $hybrydizationScore_opt=0;
my $secondaryStructureScore_opt=0;

my $did_found_one_opt=0;

print "# sequence\trnaTm\tdnaTm\thybrydizationScore\thybridizationLines\tsecondaryStructureScore\tsecondaryStructureLines\n";
print "$org_input\t$rnaTm_opt\t$dnaTm_opt\t$hybrydizationScore\t$hybridizationLines\t$secondaryStructureScore\t$secondaryStructureLines\n";
