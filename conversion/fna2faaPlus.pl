#!/usr/bin/env perl
#+pk

use warnings "all";
use strict;

my $help = 0;
my $ignoreStop = 0;
my $ignoreStart = 0;
my $shift = -1; #shift = -1 -> then search for the first ATG and use this as the frame!
my $all = 0; 
my $full = 0; 
my $transl_table_modus = 1; 
my $m=0; 
my $atg=0; 
my $includefna=0; 
my $file="";
my $version="0.0.15"; # last updated on 2021-10-05 11:53:44

foreach my $option (@ARGV) {
  if ($option =~ m/^--?he?l?p?$/)            { $help = 1;   }
  elsif ($option =~ m/^--?transl_table=([0-9]*)$/)      { $transl_table_modus = $1;  }
  elsif ($option =~ m/^--?shift=(.*)$/)      { $shift = $1;  }
  elsif ($option =~ m/^--?frame=(.*)$/)      { $shift = $1;  }
  elsif ($option =~ m/^--?ignoreStop$/i)     { $ignoreStop = 1;}
  elsif ($option =~ m/^--?ignoreStart$/i)    { $ignoreStart = 1;}
  elsif ($option =~ m/^--?all$/i)            { $all = 1;}
  elsif ($option =~ m/^--?fna$/i)            { $includefna = 1;}
  elsif ($option =~ m/^--?full$/i)            { $full = 1;}
  elsif ($option =~ m/^--?m$/i)              { $m = 1;}
  elsif ($option =~ m/^--?atg$/i)              { $atg = 1;}
  elsif ($option =~ m/^(.+)$/)               { $file=$1; }
  elsif ($option =~ m/^--?version|v$/)      { print "$0\tv$version\n";exit 0;  }
}

if($help || $file eq ""){  
    print "USAGE: fna2faaPlus.pl  Converts fasta files containing nucleotides to amino acids (automatically detecting reading frames)

SYNOPSIS

    fna2faaPlus.pl (OPTIONS) FNAFILE

    -transl_table=i : the translation table from NCBI (see link below) (default:1) 
		1 The Standard Code (-transl_table=1)
                AAs  = FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
              Starts = ---M------**--*----M---------------M----------------------------
              Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
              Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
              Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

		2 The Vertebrate Mitochondrial Code (-transl_table=2)
		3 The Yeast Mitochondrial Code (-transl_table=3)
		4 The Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code (-transl_table=4)
		5 The Invertebrate Mitochondrial Code (-transl_table=5)
		6 The Ciliate, Dasycladacean and Hexamita Nuclear Code (-transl_table=6)
		9 The Echinoderm and Flatworm Mitochondrial Code (-transl_table=9)
		10 The Euplotid Nuclear Code (-transl_table=10)
		11 The Bacterial, Archaeal and Plant Plastid Code (-transl_table=11)
		12 The Alternative Yeast Nuclear Code (-transl_table=12)
		13 The Ascidian Mitochondrial Code (-transl_table=13)
		14 The Alternative Flatworm Mitochondrial Code (-transl_table=14)
		16 Chlorophycean Mitochondrial Code (-transl_table=16)
		21 Trematode Mitochondrial Code (-transl_table=21)
		22 Scenedesmus obliquus Mitochondrial Code (-transl_table=22)
		23 Thraustochytrium Mitochondrial Code (-transl_table=23)
		24 Pterobranchia Mitochondrial Code (-transl_table=24)
		25 Candidate Division SR1 and Gracilibacteria Code (-transl_table=25)
		(updated 3.2.2020)

    -shift=i -frame=i [0,1,2] : the frame shift. If nothing given, then all possible shifts are analysed.
    -all : if set, then all possible translated sequences starting in a START codon are printed
    -full : if set, then a start codon is not needed anymore
    -M : if set, then only the aminoacid Methionin is used as a start codon (for -transl_table=1 Leucine=L could also represent a start codon).
    -ATG : if set, then only the sequence ATG is used as a start codon (for -transl_table=1 this is equivalent to -M).
    -fna : include the fna sequence in the header of the output 

DESCRIPTION

    Converts fasta files containing nucleotides to amino acids:
    	* If there are ORFs : Find the longes ORF (start-stop sequence)
	* Unknown codons are encoded with 'X'
    	* If there are multiple STARTS take the ORF that maximizes

    		numOfCodons(START->STOP/END) - numOfUnknownCodons + 5*hasSTOPcodon

    NCBI translation table : https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes

SEE ALSO https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes
VERSION v$version
AUTHOR Paul Klemm (modified version of the fna2faa.pl)
";
    exit 0;
}

# parse https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes
my %transl_table;
my $cur_i="";
my $cur_aa="";
my $cur_feature="";
my $cur_Base1="";
my $cur_Base2="";
my $cur_Base3="";
while(<DATA>){
	my $line=$_;
	chomp($line);
	if($line=~m/transl_table=([0-9]+)/){
		$cur_i="";
		$cur_aa="";
		$cur_feature="";
		$cur_Base1="";
		$cur_Base2="";
		$cur_Base3="";

		$cur_i=$1;
	}elsif($line=~m/AAs *= *([^ ]+)/){
		$cur_aa = $1;
	}elsif($line=~m/Starts *= *([^ ]+)/){
		$cur_feature = $1;
	}elsif($line=~m/Base1 *= *([^ ]+)/){
		$cur_Base1 = $1;
	}elsif($line=~m/Base2 *= *([^ ]+)/){
		$cur_Base2 = $1;
	}elsif($line=~m/Base3 *= *([^ ]+)/){
		$cur_Base3 = $1;
		# last base -> assemble informations : 

		for(my $j = 0 ; $j < length($cur_Base1) ; $j++){
			my $cur_codon = substr($cur_Base1,$j,1).substr($cur_Base2,$j,1).substr($cur_Base3,$j,1);

			$transl_table{$cur_i}{$cur_codon}{"aa"}=substr($cur_aa,$j,1);
			$transl_table{$cur_i}{$cur_codon}{"feature"}=substr($cur_feature,$j,1);
		}
	}
}

# load fasta file

my ($ph, $ps) = fasta2array($file);
our @seqs = @$ps;
our @header = @$ph;

my $noStop_counter=0;
my $ambigousStart_counter=0;
my $noStart_counter=0;
my $hasUnknownCodon_counter=0;
my $good_gene_counter=0; # only one start codon with one matching stop codon, no ambigous codon 
# MAIN :

for (my $i = 0; $i < scalar(@seqs); $i++) {

	# print HEADER and no newline -> print whats happening first, then newline, then the aa sequence !
    
    my @STARTS = match_all_start_positions($seqs[$i]);

    if($full){ if ( !grep( /^0$/, @STARTS ) ){ push(@STARTS,0) } }

    if($shift != -1){ # if shift is defined -> take only starts in the shift frame 
    	@STARTS = map { if ( ( $_ - $shift ) % 3 == 0 ) { $_ } } @STARTS;
    }

    if(scalar(@STARTS) == 0){ # fallback : if no start is found, just start at first nt in 0-frame (or at shift if defined)
    	if($shift == -1 ){
			@STARTS=(0); 
		}else{ 
			@STARTS=($shift); 
		}
    }

    my @translatedaa;

    for(my $j = 0 ; $j < scalar(@STARTS) ; $j++){

        my $curfna=substr($seqs[$i],$STARTS[$j]);
        if(length $curfna <1){next;}
        $curfna =~ s/U/T/g;

        @{$translatedaa[$j]}=("",$STARTS[$j],"");

        for(my $k=0;$k<length($curfna);$k+=3){

        	if($k+2>length($curfna)-1){last;}

            my $aa;

            if (!defined($transl_table{$transl_table_modus}{substr($curfna,$k,3)}{"aa"})) {
                $aa = "X";
            }else{
                $aa = $transl_table{$transl_table_modus}{substr($curfna,$k,3)}{"aa"};
            }

            $translatedaa[$j][0].=$aa;
            $translatedaa[$j][2].=substr($curfna,$k,3);

            if($aa eq "*"){ last }
        }
    }

    @translatedaa = grep { length($_->[0]) > 0 } @translatedaa; 

    @translatedaa = sort { length($b->[0]) - occurrences($b->[0],"X") + ( occurrences($b->[0],"\\*") > 0 ? 5 : 0 ) <=> length($a->[0]) - occurrences($a->[0],"X") + ( occurrences($a->[0],"\\*") > 0 ? 5 : 0 ) } (@translatedaa); # ascending sort 

    if(!$all && scalar(@translatedaa)>0){
    	@translatedaa=($translatedaa[0]); # [0] is the largest after sort (swapped a and b in the sort {...<=>...})
    }

    for(my $j = 0 ; $j < scalar(@translatedaa) ; $j++){

    	if((length($translatedaa[$j][0])) < 1){next;}

        my $is_good_gene=1;

    	print ">$header[$i]\tfna2faaPlus;";

    	print " alength=".(length($translatedaa[$j][0])-1)."; nlength=".(length($translatedaa[$j][2]))."; 1pos=".($translatedaa[$j][1]+1).";";

		if(scalar(@STARTS) > 1){ $is_good_gene=0; $ambigousStart_counter++; print " ambigousSTART=".scalar(@translatedaa).";";}
		elsif(scalar(@STARTS) == 0){ # fallback : if no start is found, just start at first nt in 0-frame (or at shift if defined)
	    	$is_good_gene=0;$noStart_counter++;
            print " noSTART;";
		}

	    if(occurrences($translatedaa[$j][0],"X")>0){$is_good_gene=0;$hasUnknownCodon_counter++;print " numOfUnknownCODON=".occurrences($translatedaa[$j][0],"X").";";}
	    if(occurrences($translatedaa[$j][0],"\\*")==0){$is_good_gene=0;$noStop_counter++; print " noSTOP;";}

        if($includefna){
            print " fna=".$translatedaa[$j][2].";";
        }

	    $translatedaa[$j][0] =~s/\*//g;

        if($is_good_gene){$good_gene_counter++;}

	    print "\n"; # end of header line
	    print $translatedaa[$j][0]."\n";
	}
}

print STDERR "[fna2faaPlus] number of loaded sequences = ".scalar(@seqs)."\n";
print STDERR "[fna2faaPlus]  - perfectly translated genes = one start codon and exactly one matching stop codon = ".($good_gene_counter)."\n";
print STDERR "[fna2faaPlus]  - with no start codon = ".$noStart_counter."\n";
print STDERR "[fna2faaPlus]  - with start codon / translated genes = ".(scalar(@seqs)-$noStart_counter)."\n";
print STDERR "[fna2faaPlus]  - with unknown codons = ".$hasUnknownCodon_counter."\n";
print STDERR "[fna2faaPlus]  - with ambigous number of start codons = ".$ambigousStart_counter."\n";
print STDERR "[fna2faaPlus]  - with no matching stop codons = ".$noStop_counter."\n";

exit 0;

sub occurrences {
	my $heystack = shift;
	my $needle = shift;
	my $count = () = $heystack =~ /$needle/g;  

    return $count;
}

sub match_all_start_positions {
    my $str = shift;
    my @ret;
    for(my $k=0;$k<length($str);$k++){
    	if($k+2>length($str)-1){last;}
    	if (
            defined($transl_table{$transl_table_modus}{substr($str,$k,3)}{"feature"}) && 
            (
                ($transl_table{$transl_table_modus}{substr($str,$k,3)}{"feature"} eq "M") &&
                ( ! $m || $transl_table{$transl_table_modus}{substr($str,$k,3)}{"aa"} eq "M") &&
                ( ! $atg ||  substr($str,$k,3) eq "ATG" )
            )) {
    		push @ret, $k ;
    	}
    }
    return @ret
}

# Reads a fasta file, returns two array pointers for header an sequence
# Access e.g. via $results[x]->[y]
#my ($ph, $ps) = fasta2array($file);
#our @seqs = @$ps;
#our @header = @$ph;
sub fasta2array {
    my $file = shift;
    my $i = -1;
    my @header;
    my @sequences;
    if($file eq "-"){
        $header[0]="STDIN";
        $i=0;
        while(<STDIN>) {
            chomp;
            $_ =~ s/\r$//g;
            $sequences[$i] .= $_;
        }
    }else{
        open(FILE,"<$file") || die("fasta2array(): Could not open file: $file\n");
        while(<FILE>) {
            chomp;
            $_ =~ s/\r$//g;
            if ($_ =~ /^>/) {
                $_ =~ s/^>//;
                $i++;
                $header[$i] = $_;
                $sequences[$i] = "";
            }
            else {
                $sequences[$i] .= $_;
            }   
        }
        close(FILE);
    }
   

    return(\@header,\@sequences);
}


# just copy paste the whole https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes below __TRANSTABLESTRING__
# last time modified 3.2.2020

__DATA__

NCBI Front Page 	Taxonomy Logo
PubMed 	Entrez 	BLAST 	Genome 	Taxonomy 	Structure
	Search for As
lock    
	
Just empty space

Taxonomy browser

Taxonomy common tree

Taxonomy information

Taxonomic advisors

Genetic codes
Translation tables
1; 2; 3; 4; 5; 6; 9; 10; 11; 12; 13; 14; 16; 21; 22; 23; 24; 25; 26; 27; 28; 29; 30; 31; 33;


Taxonomy Statistics

Taxonomy Name/Id Status Report

Taxonomy FTP site

FAQs

How to reference the NCBI taxonomy database

How to create links to the NCBI taxonomy

How to create LinkOut links from the NCBI taxonomy

Extinct organisms

Recent changes to classification

 
	Just empty space 	

 
The Genetic Codes

Compiled by Andrzej (Anjay) Elzanowski and Jim Ostell at National Center for Biotechnology Information (NCBI), Bethesda, Maryland, U.S.A.
Last update of the Genetic Codes: Jan. 7, 2019

NCBI takes great care to ensure that the translation for each coding sequence (CDS) present in GenBank records is correct. Central to this effort is careful checking on the taxonomy of each record and assignment of the correct genetic code (shown as a /transl_table qualifier on the CDS in the flat files) for each organism and record. This page summarizes and references this work.

The synopsis presented below is based primarily on the reviews by Osawa et al. (1992) and Jukes and Osawa (1993). Listed in square brackets [] (under Systematic Range) are tentative assignments of a particular code based on sequence homology and/or phylogenetic relationships.

The print-form ASN.1 version of this document, which includes all the genetic codes outlined below, is also available here. Detailed information on codon usage can be found at the Codon Usage Database.

GenBank format by historical convention displays mRNA sequences using the DNA alphabet. Thus, for the convenience of people reading GenBank records, the genetic code tables shown here use T instead of U.

The following genetic codes are described here:

    1. The Standard Code
    2. The Vertebrate Mitochondrial Code
    3. The Yeast Mitochondrial Code
    4. The Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code
    5. The Invertebrate Mitochondrial Code
    6. The Ciliate, Dasycladacean and Hexamita Nuclear Code
    9. The Echinoderm and Flatworm Mitochondrial Code
    10. The Euplotid Nuclear Code
    11. The Bacterial, Archaeal and Plant Plastid Code
    12. The Alternative Yeast Nuclear Code
    13. The Ascidian Mitochondrial Code
    14. The Alternative Flatworm Mitochondrial Code
    16. Chlorophycean Mitochondrial Code
    21. Trematode Mitochondrial Code
    22. Scenedesmus obliquus Mitochondrial Code
    23. Thraustochytrium Mitochondrial Code
    24. Pterobranchia Mitochondrial Code
    25. Candidate Division SR1 and Gracilibacteria Code
    26. Pachysolen tannophilus Nuclear Code
    27. Karyorelict Nuclear Code
    28. Condylostoma Nuclear Code
    29. Mesodinium Nuclear Code
    30. Peritrich Nuclear Code
    31. Blastocrithidia Nuclear Code
    33. Cephalodiscidae Mitochondrial UAA-Tyr Code

1. The Standard Code (transl_table=1)

By default all transl_table in GenBank flatfiles are equal to id 1, and this is not shown. When transl_table is not equal to id 1, it is shown as a qualifier on the CDS feature.

    AAs  = FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ---M------**--*----M---------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Initiation Codon:

AUG

Alternative Initiation Codons:

In rare cases, translation in eukaryotes can be initiated from codons other than AUG. A well documented case (including direct protein sequencing) is the GUG start of a ribosomal P protein of the fungus

Candida albicans (Abramczyk et al.) and the GUG initiation in mammalian NAT1 (Takahashi et al. 2005).
Other examples can be found in the following references: Peabody 1989; Prats et al. 1989; Hann et al. 1992; Sugihara et al. 1990. The standard code currently allows initiation from UUG and CUG in addition to AUG.
Back to top
2. The Vertebrate Mitochondrial Code (transl_table=2)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIMMTTTTNNKKSS**VVVVAAAADDEEGGGG
  Starts = ----------**--------------------MMMM----------**---M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

        Code 2          Standard

 AGA    Ter  *          Arg  R
 AGG    Ter  *          Arg  R
 AUA    Met  M          Ile  I
 UGA    Trp  W          Ter  *

Alternative Initiation Codons:

Bos: AUA

Homo: AUA, AUU

Mus: AUA, AUU, AUC

Coturnix, Gallus: also GUG (Desjardins and Morais, 1991)

Systematic Range:

Vertebrata

Comments:

AGA and AGG were thought to have become mitochondrial stop codons early in vertebrate evolution (Osawa, Ohama, Jukes & Watanabe 1989). However, at least in humans it has now been shown that AGA and AGG sequences are not recognized as termination codons. A -1 mitoribosome frameshift occurs at the AGA and AGG codons predicted to terminate the CO1 and ND6 ORFs, and consequently both ORFs terminate in the standard UAG codon (Temperley et al. 2010).
Mitochondrial genes in some vertebrate (including humans) have incomplete stop codons ending in U or UA, which become complete termination codons (UAA) upon subsequent polyadenylation (Hou et al. 2006; Oh et al. 2007; Ki et al. 2010; Temperley R J et al 2010).
Back to top
3. The Yeast Mitochondrial Code (transl_table=3)

    AAs  = FFLLSSSSYY**CCWWTTTTPPPPHHQQRRRRIIMMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**----------------------MM---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

        Code 3          Standard

 AUA    Met  M          Ile  I
 CUU    Thr  T          Leu  L
 CUC    Thr  T          Leu  L
 CUA    Thr  T          Leu  L
 CUG    Thr  T          Leu  L
 UGA    Trp  W          Ter  *

Systematic Range:

Saccharomyces cerevisiae, Candida glabrata, Hansenula saturnus, and Kluyveromyces thermotolerans (Clark-Walker and Weiller, 1994)

Comments:

GUG (GTG) is used as a start codon for a few proteins in some Saccharomyces species (Sulo et al. 2017). The remaining CGN codons are rare in Saccharomyces cerevisiae and absent in Candida glabrata (= Torulopsis glabrata).

The AUA codon is common in the gene var1 coding for the single mitochondrial ribosomal protein, but rare in genes encoding the enzymes.

The coding assignments of the AUA (Met or Ile) and CUU (possibly Leu, not Thr) are uncertain in Hansenula saturnus.

The coding assignment of Thr to CUN is uncertain in Kluyveromyces thermotolerans (Clark-Walker and Weiller, 1994).
Back to top
4. The Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code (transl_table=4)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --MM------**-------M------------MMMM---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

        Code 4         Standard

 UGA    Trp  W          Ter  *

Alternative Initiation Codons:

Trypanosoma: UUA, UUG, CUG

Leishmania: AUU, AUA

Tertrahymena: AUU, AUA, AUG

Paramecium: AUU, AUA, AUG, AUC, GUG, GUA(?)

(Pritchard et al., 1990)

Systematic Range:

Bacteria: The code is used in Entomoplasmatales and Mycoplasmatales (Bove et al. 1989). The situation in the Acholeplasmatales is unclear. Based on a study of ribosomal protein genes, it had been concluded that UGA does not code for tryptophan in plant-pathogenic mycoplasma-like organisms (MLO) and the Acholeplasmataceae (Lim and Sears, 1992) and there seems to be only a single tRNA-CCA for tryptophan in Acholeplasma laidlawii (Tanaka et al. 1989). In contrast, in a study of codon usage in Phytoplasmas, it was found that 30 out of 78 ORFs analyzed translated better with code 4 (UGA for tryptophan) than with code 11 while the remainder showed no differences between the two codes (Melamed et al. 2003). In addition, the coding reassignment of UGA Stop --> Trp can be found in an alpha-proteobacterial symbiont of cicadas: Candidatus Hodgkinia cicadicola (McCutcheon et al. 2009).

Fungi: Emericella nidulans, Neurospora crassa, Podospora anserina, Acremonium (Fox, 1987), Candida parapsilosis (Guelin et al., 1991), Trichophyton rubrum (de Bievre and Dujon, 1992), Dekkera/Brettanomyces, Eeniella (Hoeben et al., 1993), and probably Ascobolus immersus, Aspergillus amstelodami, Claviceps purpurea, and Cochliobolus heterostrophus.

Other Eukaryotes: Gigartinales among the red algae (Boyen et al. 1994), and the protozoa Trypanosoma brucei, Leishmania tarentolae, Paramecium tetraurelia, Tetrahymena pyriformis and probably Plasmodium gallinaceum (Aldritt et al., 1989).

Metazoa: Coelenterata (Ctenophora and Cnidaria)

Comments:

This code is also used for the kinetoplast DNA (maxicircles, minicircles). Kinetoplasts are modified mitochondria (or their parts).
Back to top
5. The Invertebrate Mitochondrial Code (transl_table=5)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIMMTTTTNNKKSSSSVVVVAAAADDEEGGGG
  Starts = ---M------**--------------------MMMM---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Comment:

The codon AGG is absent in Drosophila.

Differences from the Standard Code:


        Code 5          Standard

 AGA    Ser  S          Arg  R
 AGG    Ser  S          Arg  R
 AUA    Met  M          Ile  I
 UGA    Trp  W          Ter  *

Alternative Initiation Codons:

AUA, AUU

AUC: Apis (Crozier and Crozier, 1993)

GUG: Polyplacophora (Boore and Brown, 1994 GenBank Accession Number:

U09810)

UUG: Ascaris, Caenorhabditis

Systematic Range:

Nematoda: Ascaris, Caenorhabditis;

Mollusca: Bivalvia (Hoffmann et al., 1992); Polyplacophora (Boore and Brown, 1994)

Arthropoda/Crustacea: Artemia (Batuecas et al., 1988);

Arthropoda/Insecta: Drosophila [Locusta migratoria (migratory locust), Apis mellifera (honeybee)]

Comments:

Several arthropods translate the codon AGG as lysine instead of serine (as in the invertebrate mitochondrial genetic code) or arginine (as in the standard genetic code) (Abascal et al., 2006).

GUG may possibly function as an initiator in Drosophila (Clary and Wolstenholme, 1985; Gadaleta et al., 1988). AUU is not used as an initiator in Mytilus (Hoffmann et al., 1992).

"An exceptional mechanism must operate for initiation of translation of the cytochrome oxidase subunit I mRNA in both D. melanogaster (de Bruijn, 1983) and D. yakuba (Clary and Wolstenholme 1983), since its only plausible initiation codon, AUA, is out of frame with the rest of the gene. Initiation appears to require the "reading" of of an AUAA quadruplet, which would be equivalent to initiation at AUA followed immediately by a specific ribosomal frameshift. Another possible mechanism ... is that the mRNA is "edited" to bring the AUA initiation into frame." (Fox, 1987)
Back to top
6. The Ciliate, Dasycladacean and Hexamita Nuclear Code (transl_table=6)

    AAs  = FFLLSSSSYYQQCC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --------------*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 6       Standard

 UAA      Gln  Q        Ter  *
 UAG      Gln  Q        Ter  *

Systematic Range:

Ciliata: Oxytricha and Stylonychia (Hoffman et al. 1995), Paramecium, Tetrahymena, Oxytrichidae and probably Glaucoma chattoni.

Dasycladaceae: Acetabularia (Schneider et al., 1989) and

Batophora (Schneider and de Groot, 1991).

Diplomonadida:

Scope: Hexamita inflata, Diplomonadida ATCC50330, and ATCC50380.

Ref.: Keeling, P.J. and Doolittle, W.F. 1996.. A non-canonical genetic code in an early diverging eukaryotic lineage. The EMBO Journal 15, 2285-2290.

Comment:

The ciliate macronuclear code has not been determined completely. The codon UAA is known to code for Gln only in the Oxytrichidae.
Back to top
9. The Echinoderm and Flatworm Mitochondrial Code (transl_table=9)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIIMTTTTNNNKSSSSVVVVAAAADDEEGGGG
  Starts = ----------**-----------------------M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:


          Code 9        Standard

 AAA      Asn  N        Lys K
 AGA      Ser  S        Arg R
 AGG      Ser  S        Arg R
 UGA      Trp  W        Ter *

Systematic Range:

Asterozoa (starfishes) (Himeno et al., 1987)

Echinozoa (sea urchins) (Jacobs et al., 1988; Cantatore et al., 1989)
Rhabditophora among the Platyhelminthes (Telford et al. 2000)
Back to top
10. The Euplotid Nuclear Code (transl_table=10)

    AAs  = FFLLSSSSYY**CCCWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**-----------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 10     Standard

 UGA      Cys  C        Ter  *

Systematic Range:

Ciliata: Euplotidae (Hoffman et al. 1995).
Back to top
11. The Bacterial, Archaeal and Plant Plastid Code (transl_table=11)

    AAs  = FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ---M------**--*----M------------MMMM---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Systematic Range and Comments:

Table 11 is used for Bacteria, Archaea, prokaryotic viruses and chloroplast proteins. As in the standard code, initiation is most efficient at AUG. In addition, GUG and UUG starts are documented in Archaea and Bacteria (Kozak 1983, Fotheringham et al. 1986, Golderer et al. 1995, Nolling et al. 1995, Sazuka & Ohara 1996, Genser et al. 1998, Wang et al. 2003). In E. coli, UUG is estimated to serve as initiator for about 3% of the bacterium's proteins (Blattner et al. 1997). CUG is known to function as an initiator for one plasmid-encoded protein (RepA) in Escherichia coli (Spiers and Bergquist, 1992). In addition to the NUG initiations, in rare cases Bacteria can initiate translation from an AUU codon as e.g. in the case of poly(A) polymerase PcnB and the InfC gene that codes for translation initiation factor IF3 (Polard et al. 1991, Liveris et al. 1993, Sazuka & Ohara 1996, Binns & Masters 2002). The internal assignments are the same as in the standard code though UGA codes at low efficiency for Trp in Bacillus subtilis and, presumably, in Escherichia coli (Hatfiled and Diamond, 1993).
Back to top
12. The Alternative Yeast Nuclear Code (transl_table=12)

    AAs  = FFLLSSSSYY**CC*WLLLSPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**--*----M---------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

           Code 12      Standard

 CUG       Ser          Leu
       

Alternative Initiation Codons:

CAG may be used in Candida albicans (Santos et al., 1993).

Systematic Range:

Endomycetales (yeasts): Candida albicans, Candida cylindracea, Candida melibiosica, Candida parapsilosis, and Candida rugosa (Ohama et al., 1993).

Comment:

However, other yeast, including Saccharomyces cerevisiae, Candida azyma, Candida diversa, Candida magnoliae, Candida rugopelliculosa, Yarrowia lipolytica, and Zygoascus hellenicus, definitely use the standard (nuclear) code (Ohama et al., 1993).
Back to top
13. The Ascidian Mitochondrial Code (transl_table=13)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIMMTTTTNNKKSSGGVVVVAAAADDEEGGGG
  Starts = ---M------**----------------------MM---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 13     Standard

 AGA      Gly  G        Arg  R
 AGG      Gly  G        Arg  R
 AUA      Met  M        Ile  I
 UGA      Trp  W        Ter  *

Systematic range and Comments:

There is evidence from a phylogenetically diverse sample of tunicates (Urochordata) that AGA and AGG code for glycine. In other organisms, AGA/AGG code for either arginine or serine and in vertebrate mitochondria they code a STOP. Evidence for glycine translation of AGA/AGG has been found in Pyura stolonifera (Durrheim et al. 1993), Halocynthia roretzi (Kondow et al. 1999,Yokobori et al., 1993, Yokobori et al. 1999) and Ciona savignyi (Yokobori et al. 2003).

In addition, the Halocynthia roretzi mitochondrial genome encodes an additional tRNA gene with the anticodon U*CU that is thought to enable the use of AGA or AGG codons for glycine and the gene has been shown to be transcribed in vivo (Kondow et al. 1999, Yokobori et al. 1999).

Alternative initiation codons:

ATA, GTG and TTG (Yokobori et al. 1999). ATT is the start codon for the CytB gene in Halocynthia roretzi (Gissi and Pesole, 2003).
Back to top
14. The Alternative Flatworm Mitochondrial Code (transl_table=14)

    AAs  = FFLLSSSSYYY*CCWWLLLLPPPPHHQQRRRRIIIMTTTTNNNKSSSSVVVVAAAADDEEGGGG
  Starts = -----------*-----------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 14      Standard

 AAA      Asn  N       Lys  K
 AGA      Ser  S       Arg  R
 AGG      Ser  S       Arg  R
 UAA      Tyr  Y       Ter  *
 UGA      Trp  W       Ter  *

Systematic Range:

Platyhelminthes (flatworms) and Nematoda (roundworms)

Comments:

Code 14 differs from code 9 only by translating UAA to Tyr rather than STOP. A recent study (Telford et al. 2000) has found no evidence that the codon UAA codes for Tyr in the flatworms but other opinions exist. There are very few GenBank records that are translated with code 14 but a test translation shows that retranslating these records with code 9 can cause premature terminations. More recently, UAA has been found to code for tyrosine in the nematodes Radopholus similis and Radopholus arabocoffeae (Jacob et al. 2009).
Back to top
16. Chlorophycean Mitochondrial Code (transl_table=16)

    AAs  = FFLLSSSSYY*LCC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------*---*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 16       Standard

TAG       Leu  L        STOP

Systematic Range:

Chlorophyceae (Hayashi-Ishimaru et al. 1996. UAG is a sense codon in several chlorophycean mitochondria) and the chytridiomycete fungus Spizellomyces punctatus (Laforest et al. 1997. Mitochondrial tRNAs in the lower fungus Spizellomyces punctatus: tRNA editing and UAG 'stop' codons recognized as leucine).
Back to top
21. Trematode Mitochondrial Code (transl_table=21)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIMMTTTTNNNKSSSSVVVVAAAADDEEGGGG
  Starts = ----------**-----------------------M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 21       Standard

TGA       Trp  W        STOP
ATA       Met  M        Ile
AGA       Ser  S        Arg
AGG       Ser  S        Arg
AAA       Asn  N        Lys 

Systematic Range:

Trematoda: Ohama, T, S. Osawa, K. Watanabe, T.H. Jukes, 1990. J. Molec Evol. 30
Garey, J.R. and D.R. Wolstenholme, 1989. J. Molec. Evol. 28: 374-387 329-332.
Back to top
22. Scenedesmus obliquus Mitochondrial Code (transl_table=22)

    AAs  = FFLLSS*SYY*LCC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ------*---*---*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 22       Standard

TCA       STOP *        Ser
TAG       Leu  L        STOP

Systematic Range:

Scenedesmus obliquus: Nedelcu A, Lee RW, Lemieux C, Gray MW and Burger G. "The complete mitochondrial DNA sequence of Scenedesmus obliquus reflects an intermediate stage in the evolution of the green algal mitochondrial genome." Genome Res. 2000 Jun;10(6):819-31.
Back to top
23. Thraustochytrium Mitochondrial Code (transl_table=23)

    AAs  = FF*LSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --*-------**--*-----------------M--M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

This code has been created for the mitochondrial genome of the labyrinthulid Thraustochytrium aureum sequenced by The Organelle Genome Megasequencing Program (OGMP).

It is the similar to the bacterial code (transl_table 11) but it contains an additional stop codon (TTA) and also has a different set of start codons.
Back to top
24. Pterobranchia Mitochondrial Code (transl_table=24)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSSKVVVVAAAADDEEGGGG
  Starts = ---M------**-------M---------------M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 24        Standard

AGA       Ser S          Arg  R
AGG       Lys K          Arg  R
UGA       Trp W          STOP *

Code 24 has been created for the mitochondrial genome of Rhabdopleura compacta (Pterobranchia). The Pterobranchia are one of the two groups in the Hemichordata which together with the Echinodermata and Chordata form the three major lineages of deuterostomes. AUA translates to isoleucine in Rhabdopleura as it does in the Echinodermata and Enteropneusta while AUA encodes methionine in the Chordata. The assignment of AGG to Lys is not found elsewhere in deuterostome mitochondria but it occurs in some taxa of Arthropoda (Perseke et al. 2011). Code 24 shares with many other mitochondrial codes the reassignment of the UGA STOP to Trp, and AGG and AGA to an amino acid other than Arg. The initiation codons in Rhabdopleura compacta are ATG and GTG (Perseke et al. 2011).
Back to top
25. Candidate Division SR1 and Gracilibacteria Code (transl_table=25)

    AAs  = FFLLSSSSYY**CCGWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ---M------**-----------------------M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 25        Standard

UGA       Gly            STOP

Initiation Codons:

AUG, GUG, UUG

Systematic Range:

Candidate Division SR1, Gracilibacteria

Comments:

Code 25 is used in two groups of (so far) uncultivated Bacteria found in marine and fresh-water environment and in the intestines and oral cavities of mammals among others. The difference to the standard and the bacterial code is that UGA represents an additional glycine codon and does not code for termination (Campbell et al. 2013).
Back to top
26. Pachysolen tannophilus Nuclear Code (transl_table=26)

    AAs  = FFLLSSSSYY**CC*WLLLAPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**--*----M---------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 26        Standard

CUG       Ala            Leu

Initiation Codons:

AUG, GUG, UUG

Systematic Range:

Pachysolen tannophilus

Comments:
Code 26 is used so far only for the ascomycete fungus Pachysolen tannophilus. The only difference to the standard code is the translation of CUG as alanine (as opposed to leucine). (Muhlhausen et al. 2016).
Back to top
27. Karyorelict Nuclear Code (transl_table=27)

    AAs  = FFLLSSSSYYQQCCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --------------*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 27        Standard

UAG       Gln            STOP
UAA       Gln            STOP
UGA       STOP or Trp    STOP

Initiation Codons:

AUG

Systematic Range:

the karyorelictid ciliate Parduczia

Comments:
Code 27 reassigns the UAG and UAA stops to glutamine while UGA can function as either STOP or tryptophan. Code 27 is used for the karyorelictid ciliate Parduczia sp. (Swart et al. 2016).
Back to top
28. Condylostoma Nuclear Code (transl_table=28)

    AAs  = FFLLSSSSYYQQCCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**--*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 28          Standard

UAA       Gln or STOP      STOP
UAG       Gln or STOP      STOP
UGA       Trp or STOP      STOP

Initiation Codons: AUG

Systematic Range:

Condylostoma magnum

Comments:
Code 28 is used in Condylostoma magnum. The difference to the standard code is that the three stop codons can also be translated as glutamine (UAA, UAG) or tryptophan (UGA), respectively(Swart et al. 2016, Heaphy et al. 2016).
Back to top
29. Mesodinium Nuclear Code (transl_table=29)

    AAs  = FFLLSSSSYYYYCC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --------------*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 29        Standard

UAA       Tyr            STOP
UAG       Tyr            STOP

Initiation Codons:

AUG

Systematic Range:

the mesodiniid ciliates Mesodinium and Myrionecta

Comments:
Code 29 is used for the haptorid ciliates Mesodinium and Myrionecta. It differs from the standard code in reassigning the stop codons UAA and UAG to Tyrosine. (Heaphy et al. 2016).
Back to top
30. Peritrich Nuclear Code (transl_table=30)

    AAs  = FFLLSSSSYYEECC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --------------*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 30        Standard

UAA       Glu            STOP
UAG       Glu            STOP

Initiation Codons:

AUG

Systematic Range:

the peritrich ciliate Carchesium

Comments:
Code 30 is used in the peritrich ciliate Carchesium. The stop codons UAA and UAG are reassigned to Glutamine. (Sanchez-Silva et al. 2003).
Back to top
31. Blastocrithidia Nuclear Code (transl_table=31)

    AAs  = FFLLSSSSYYEECCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**-----------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 31          Standard

UGA       Trp              STOP
UAG       Glu or STOP      STOP
UAA       Glu or STOP      STOP

Initiation Codons:

AUG

Systematic Range:

Blastocrithidia sp.

Comments:
Code 31 is used for the trypanosome Blastocrithidia sp. UGA encodes trytophan and UAG and UAA encode glutamate and also serve as termination codons. (Zahonova et al. 2016).
Back to top
33. Cephalodiscidae Mitochondrial UAA-Tyr Code (transl_table=33)

    AAs  = FFLLSSSSYYY*CCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSSKVVVVAAAADDEEGGGG
  Starts = ---M-------*-------M---------------M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

     Code 33     Standard
UAA  Tyr         STOP
UGA  Trp         STOP
AGA  Ser         Arg
AGG  Lys         Arg

Systematic Range:

Cephalodiscidae (Hemichordata)

Comments:

Code 33 is very similar to the mitochondrial code 24 for the Pterobranchia, which also belong to the Hemichordata, except that it uses UAA for tyrosine rather than as a stop codon (Li Y, Kocot KM, Tassia MG, Cannon JT, Bernt M, Halanych KM. Mitogenomics Reveals a Novel Genetic Code in Hemichordata. Genome Biol Evol. 2019 Jan 1;11(1):29-40.)

Back to top

 
Comments and questions to info@ncbi.nlm.nih.gov
Credits: Andrzej (Anjay) Elzanowski, Jim Ostell, Detlef Leipe, Vladimir Soussov.
	[Search] 	  	  	[NLM NIH] 	[Disclaimer]
