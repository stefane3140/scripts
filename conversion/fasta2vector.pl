#!/usr/bin/env perl
#pk

use Getopt::Long; # for parameter specification on the command line

use warnings "all";
use strict;

my $help = 0;
my $version="0.0.9"; # last updated on 2021-10-01 14:11:18

GetOptions('help!'=>\$help,'h!'=>\$help,'version!'=>sub { print "$0\tv$version\n";exit 0; });

if(scalar @ARGV != 1 || $help){
	print "fasta2vector.pl        reads a fasta (file or STDIN) and returns a linear version: 
	name\\tsequence
	name\\tsequence
	name\\tsequence
	(...)
 
SYNOPSIS
 
fasta2vector.pl INFILE

	INFILE can also be - for STDIN

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long
";
	exit 0;
}
my $last_header = "";
my $last_sequence = "";

sub parseLine{
	$_=~s/[\n\r]+$//g; # windows prove chomp

	if( $_ !~ /^>/ && $_ =~ /(.*)\t(.*)/ ){

		# convert back vector 2 fasta

		print ">$1\n$2\n";

	}else{

		# convert fasta 2 vector

		if(length $_ < 1 || substr($_,0,1) eq "#"){return;}
		if (substr($_,0,1) eq ">") {

			if($last_sequence ne "" && $last_header ne ""){
				print $last_header."\t".$last_sequence."\n";
				$last_sequence="";
			}

			$last_header = $_;
			$last_header =~ s/\t/ /g;
			$last_header =~ s/^>//g;
		}
		else {
			$last_sequence .= $_;
		}
	}
}

if($ARGV[0] eq "-") {
	foreach (<>) { parseLine($_) }
}else{
	open(my $FASTA_FH,"<".$ARGV[0]) || die($!);
	while (<$FASTA_FH>) { parseLine($_) }
	close($FASTA_FH);
}

if($last_sequence ne "" && $last_header ne ""){
	print $last_header."\t".$last_sequence."\n";
	$last_sequence="";
}
