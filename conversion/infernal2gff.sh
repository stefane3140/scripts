#!/usr/bin/env bash
#pk

arg=$(echo "$1") # chomps newline
if [[ "$#" != 1 ]] || [[ "$arg" = "-h" ]] || [[ "$arg" = "-help" ]] || [[ "$arg" = "--help" ]]; then
    echo "Usage: infernal2gff.sh <infernal.tblout>"
    echo "Returns gff format."
    echo "  Input looks like this:
------
#target name         accession query name           accession mdl mdl from   mdl to seq from   seq to strand trunc pass   gc  bias  score   E-value inc description of target
#------------------- --------- -------------------- --------- --- -------- -------- -------- -------- ------ ----- ---- ---- ----- ------ --------- --- ---------------------
NC_013967.1          -         5S_rRNA              RF00001    cm        1      119  1603076  1603197      +    no    1 0.59   0.0   86.8   1.7e-18 !   Haloferax volcanii DS2 complete genome
NC_013967.1          -         5_8S_rRNA            RF00002    cm        1      154  1600063  1600219      +    no    1 0.58   0.0   41.3   7.8e-09 !   Haloferax volcanii DS2 complete genome
(...)
------

  Output looks like this:
------
NC_013967.1     infernal        5S_rRNA 1603076 1603197 .	+      .       accession=\"RF00001\"; mdl=\"cm\"; mdlfrom=\"1\"; mdlfrom=\"119\"; trunc=\"no\"; pass=\"1\"; gc=\"0.59\"; bias=\"0.0\"; score=\"86.8\"; E-value=\"1.7e-18\"; inc=\"!\"; descriptionOfTarget=\"Haloferax volcanii DS2 complete genome \"
NC_013967.1     infernal        5S_rRNA 2766751 2766630 .	-      .       accession=\"RF00001\"; mdl=\"cm\"; mdlfrom=\"1\"; mdlfrom=\"119\"; trunc=\"no\"; pass=\"1\"; gc=\"0.59\"; bias=\"0.0\"; score=\"86.8\"; E-value=\"1.7e-18\"; inc=\"!\"; descriptionOfTarget=\"Haloferax volcanii DS2 complete genome \"
(...)
------

AUTHOR Paul Klemm
DEPENDENCIES sed, perl
"
    exit 1
fi

sed 's/^#.*$//g' "$1" | perl -lne 'chomp;@a=split(/[ \t]+/,$_);if($a[7]>$a[8]){$t=$a[7];$a[7]=$a[8];$a[8]=$t;};$s=scalar(@a);if($s>17){print "$a[0]\tinfernal\t$a[2]\t$a[7]\t$a[8]\t.\t$a[9]\t.\taccession=\"$a[3]\"; mdl=\"$a[4]\"; mdlfrom=\"$a[5]\"; mdlfrom=\"$a[6]\"; trunc=\"$a[10]\"; pass=\"$a[11]\"; gc=\"$a[12]\"; bias=\"$a[13]\"; score=\"$a[14]\"; E-value=\"$a[15]\"; inc=\"$a[16]\"; descriptionOfTarget=\"".join(" ",@a[17..$s])."\""}'