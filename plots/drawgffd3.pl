#!/usr/bin/perl 
#pk

use strict;
use warnings "all";

my $version="0.9.31"; # last updated on 2021-10-01 14:11:18

my $group="source";
my $usage = "drawgffd3.pl        produces a colored representation of a given MSA
 
SYNOPSIS
 
drawgffd3.pl (options) GFFFILE 

	GFFFILE	gff file

	-group= sets the grouping/column for the different lanes/colors (default:$group)

OUTPUT
	Static HTML page (STDOUT) using d3 to plot the entries of the gff entry.
	Different sources are plotted as different lanes with different colors.

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES -
";

if(scalar @ARGV == 0){print $usage; exit 0;}
if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }
if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-groups?=(.*)$/ ){ $group=$1; shift @ARGV }

if($ARGV[0] eq "-help" || $ARGV[0] eq "-h" || $ARGV[0] eq "--help" ){print $usage; exit(0);}


my $gff_json="var gff_text = `";
while(<>){
	if(scalar split("\t",$_) != 9){next}
	$gff_json.="$_";
}
$gff_json.="`;";

print << 'EOF';
<!DOCTYPE html>
<meta charset="utf-8">
<style type="text/css">
div.tooltip {
	position: absolute;
	text-align: center;
	padding: 2px;
	font: 12px monospace;
	background: white;
	border: 0px;
	pointer-events: none;
	display: inline-block
}

    .ticks {
      font-size: 10px;
    }

    .track,
    .track-inset,
    .track-overlay {
      stroke-linecap: round;
    }

    .track {
      stroke: #000;
      stroke-opacity: 0.3;
      stroke-width: 10px;
    }

    .track-inset {
      stroke: #dcdcdc;
      stroke-width: 8px;
    }

    .track-overlay {
      pointer-events: stroke;
      stroke-width: 50px;
      stroke: transparent;
      cursor: crosshair;
    }

    .handle {
      fill: #fff;
      stroke: #000;
      stroke-opacity: 0.5;
      stroke-width: 1.25px;
    }
</style>
<head>
<script src="https://d3js.org/d3.v4.js"></script>
<script src="https://d3js.org/d3-array.v2.min.js"></script>
<script src="https://d3js.org/d3.v4.min.js"></script>
<script src="https://unpkg.com/d3-simple-slider"></script>
</head>
<body>
<!-- Create a div where the graph will take place -->
<div id="slider"></div>
<div id="boxplotdiv"></div>
<script type="text/javascript">

	// set the dimensions and margins of the graph
	var margin = {top: 0, right: 2, bottom: 20, left: 2},
		width = 800 - margin.left - margin.right, // 800 = 100%, 800 is also the zoom level (set this to 100 to zoom in)
		height = 100 - margin.top - margin.bottom;

	function isFloat(n){
	    return Number(n) === n && n % 1 !== 0;
	}
	function isInt(n){
	    return Number(n) === n && n % 1 === 0;
	}

	function shuffle(a) {
		var j, x, i;
		for (i = a.length - 1; i > 0; i--) {
			j = Math.floor(Math.random() * (i + 1));
			x = a[i];
			a[i] = a[j];
			a[j] = x;
		}
		return a;
	}
	var colors = shuffle([
	 '#ff4422','#ee1166' ,'#9911bb' ,'#6633bb' ,'#3344bb' ,'#1199ff','#00aaff',
	 '#00bbdd','#009988','#44bb44','#88cc44','#ccdd22','#ffee11','#ffcc00','#ff9900','#ff5500',
	 '#775544','#999999','#828080','#444']);

	getColor = function(i){
		if(i>=colors.length){i = i % colors.length}
		return colors[i];
	}

	// Define the div for the tooltip
	var tooltipdiv = d3.select("body").append("div")
	.attr("class", "tooltip")
	.style("opacity", 0);

EOF
print $gff_json; print "
	var groups={};

	var globalMin,globalMax;
	var master_data = d3.tsvParseRows(gff_text).filter((row)=>{return row.length === 9}).map(function(row) {
		let e={ 'seqname' : row[0],
				'source'  : row[1],
				'feature'  : row[2],
				'start' : parseInt(row[3]),
				'end' : parseInt(row[4]),
				'score' : row[5],
				'strand' : row[6],
				'frame' : row[7],
				'attribute' : row[8],
				'dG' : row[8].match(/ \\(([-0-9+.]+)/) ? parseFloat(row[8].match(/ \\(([-0-9+.]+)/)[1]) : '',
				'evalue' : row[8].match(/evalue=([-0-9+.eE]+)/) ? parseFloat(row[8].match(/evalue=([-0-9+.eE]+)/)[1]) : '',
				'aln' : row[8].match(/aln=([ |:]+)/) ? (row[8].match(/aln=([ |:]+)/)[1].match(/\\|/g) || []).length : '',
				'oligo' : row[8].match(/oligo=(.+)/) ? row[8].match(/oligo=(.+)/)[1] : '' };
		if(!groups.hasOwnProperty(e.$group)){groups[e.$group]=Object.keys(groups).length}
		if(typeof globalMin === 'undefined' || globalMin>e.start ){ globalMin=e.start }
		if(typeof globalMax === 'undefined' || globalMax<e.end ){ globalMax=e.end }
		return e;
	  });
	
	drawPlot(master_data);

	var minVdg = d3.min(master_data.filter(function(d){return d.dG !== ''}), function(d) { return d.dG; } );
	var maxVdg = d3.max(master_data.filter(function(d){return d.dG !== ''}), function(d) { return d.dG; } );
	if((isFloat(maxVdg) || isInt(maxVdg)) && (!isFloat(minVdg) && !isInt(minVdg))){minVdg=0}

	var minVe = d3.min(master_data.filter(function(d){return d.evalue !== ''}), function(d) { return d.evalue; } );
	var maxVe = d3.max(master_data.filter(function(d){return d.evalue !== ''}), function(d) { return d.evalue; } );
	if((isFloat(maxVe) || isInt(maxVe)) && (!isFloat(minVe) && !isInt(minVe))){minVe=0}

	var minVa = d3.min(master_data.filter(function(d){return d.aln !== ''}), function(d) { return d.aln; } );
	var maxVa = d3.max(master_data.filter(function(d){return d.aln !== ''}), function(d) { return d.aln; } );
	if((isFloat(maxVa) || isInt(maxVa)) && (!isFloat(minVa) && !isInt(minVa))){minVa=0}

	var numberOfSlider=0;
	if( (isFloat(maxVdg) || isInt(maxVdg)) && minVdg<maxVdg ){numberOfSlider++}
	if( (isFloat(maxVe) || isInt(maxVe)) && minVe<maxVe ){numberOfSlider++}
	if( (isFloat(maxVa) || isInt(maxVa)) && minVa<maxVa ){numberOfSlider++}

	if( (isFloat(maxVdg) || isInt(maxVdg)) && minVdg<maxVdg ){ buildSlider('dG','RNAup dG',minVdg,maxVdg,numberOfSlider) }
	if( (isFloat(maxVe) || isInt(maxVe)) && minVe<maxVe ){ buildSlider('evalue','Blast evalue',minVe,maxVe,numberOfSlider) }
	if( (isFloat(maxVa) || isInt(maxVa)) && minVa<maxVa ){ buildSlider('aln','coiffR matches',minVa,maxVa,numberOfSlider) }

	var filters={};

	function buildSlider(column,name,minV,maxV,numberOfSlider){

		var sliderRange = d3
			.sliderBottom()
			.min(minV)
			.max(maxV)
			.width(800-100)
			.height(100)
			.tickFormat(d3.format('.4n'))
			.ticks(5)
			.default([minV, maxV])
			.fill('#2196f3')
			.on('onchange', valrange => { filters[column]=valrange; update() });
		    
		var gRange = d3
			.select('#slider')
			.append('svg')
			.attr('viewBox','0 0 800 100')
			.attr('preserveAspectRatio','none')
			.attr('width', (100/numberOfSlider)+'%')
			.attr('height', '100%')
			.append('g')
				.attr('transform','translate(' + 50 + ',' + 50 + ')');

		gRange.call(sliderRange);

		gRange.append('g')
				.attr('transform','translate(' + -50 + ',' + -20 + ')')
				.append('text').text(name);
	}

	function update() {

		// filter data set and redraw plot
		var newData = master_data.filter(function(d) {
			var ret = 1;
			Object.keys(filters).forEach(col=>{
				ret = ret && ( !isFloat(d[col]) && !isInt(d[col]) ) || ( (isFloat(d[col]) || isInt(d[col])) && d[col] >= filters[col][0] && d[col] <= filters[col][1] )
			})
			return ret;
		})
		drawPlot(newData);
	}

	function drawPlot(data) {

		d3.select('#boxplotdiv').html('');

		d3.group(data, d => d.oligo.toUpperCase() ).forEach(data_sub => {

			if(data_sub[0].oligo !== ''){
				d3.select('#boxplotdiv')
				    .append('h3')
				    .html( data_sub[0].oligo );
			}

			d3.group(data_sub, d => d.seqname.toUpperCase() ).forEach(data_sub_sub => {

				d3.select('#boxplotdiv')
				    .append('b')
				    .html(data_sub_sub[0].seqname );

				// append the svg object to the body of the page
				var svg = d3.select('#boxplotdiv')
				  .append('svg')
					.attr('viewBox','0 0 800 100')
					.attr('preserveAspectRatio','none')
					.style('display','block')
					.style('width','100%')
					.style('height','100%')
					.attr('class', 'box')   
				  .append('g')
					.attr('transform',
						  'translate(' + margin.left + ',' + margin.top + ')');

				// Show the X scale
				var y = d3.scaleBand()
					.range([ 0, height ])
					.domain(Object.keys(groups)) // x ticks can be retreved with y(NAME)
					.paddingInner(1)
					.paddingOuter(.5)

				// Show the Y scale
				var x = d3.scaleLinear()
					.domain([globalMin,globalMax]) 
					.range([0,width])
					svg.append('g')
					.attr('transform', 'translate('+ 0 +',' + height + ')')
						.call(d3.axisBottom(x))

				var box_height = height/Object.keys(groups).length;

				svg
					.selectAll('gffBox')
					.data(data_sub_sub)
					.enter()
					.append('rect')
					.attr('y', function(d){return(y(d.$group) - box_height/2 )})
					.attr('x', function(d){return(x(d.start))})
					.attr('width', function(d){return(d.end-d.start)})
					.attr('height', box_height )
					.attr('stroke', function(d){return(getColor(groups[d.$group]))} )
					.style('fill', function(d){return(d3.hsl(getColor(groups[d.$group])).brighter(1))} ) 

				.on('mouseover', function(d) {
					//console.log(d);
					tooltipdiv.transition()
						.duration(200)
						.style('opacity', .9);
					tooltipdiv.html( d.seqname + '<br>' + d.start + '-' + d.end + ':' + d.strand + '<br>' + d.attribute )
						.style('left', (d3.event.pageX) + 'px')
						.style('top', (d3.event.pageY - 28) + 'px');
					})
				.on('mouseout', function(d) {
					tooltipdiv.transition()
						.duration(500)
						.style('opacity', 0);
				});

				svg.append('g')
					.call(d3.axisRight(y))		
			})
		})
	}
</script>
</body>
";
