#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
$|=1; #autoflush

my $version="0.7.6"; # last updated on 2021-10-08 12:38:24

my $me = basename($0);

my $gff_filter="gene";
my $gff_id="ID";
my $gffRef;
my $fastaNew;
my $fastaRef;
my $mums;
my $help;
my $out="$me.project";
my $debug;
my $verbose;

my $usage = "$me        compares a gff+fasta with a assembled genome + mummer comparison of the 2 fasta files

SYNOPSIS

$me (options) -fastaRef b.fna -gffRef b.gff -fastaNew a.fna -mums avsb.mums

MANDATORY

	-fastaRef and -gffRef : the reference gff and genomic fasta files
	-fastaNew : new assembled genomic fasta file (needs to have matching chromosome names)
	-mums : the output of 'mummer -mums -F FASTANEW FASTAREF'

OPTIONAL

	-gff_filter : filters the input gff (feature) [default:$gff_filter]
	-gff_id : the attribute name used as the id (part of attribute) [default:$gff_id]
	-out : the output prefix name (default:$out)

DESCRIPTION

	The input to mummer needs to be 1. new assmbled fasta and 2. the reference fasta, s.t. the mums contains:
		chr,pos_new,pos_ref,len_match
	The output is split in 3 files:
		out.pos : VCF like output of single nt changes (includes codon changes according to the gff)
		out.stat : gene-level changes (how many genes contain a indel,...)
		out.aln : each non-identical gene is aligned (between the REF and NEW). 

	Use case:
		1. Map DNAseq reads against a reference genomic fasta file
		2. Create a consensus fasta file (-fastaNew) e.g. with easybcfconsensus.sh
		3. apply mummer to compare the fasta files
		4. summarize differences with this script

LIMITATIONS

	1. for genes on - strand: the codons are printed from the - strand while the Ref/New nt are always from + strand
	2. gene start of NEW is assumed to be identical to the one of the REF (gff start of ref with alignment position of new)
	3. frameshift is not detected (e.g. could be #indels mod 3 != 0) and mutations are analyzed independently

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES Perl Getopt::Long, File::Basename, easyalignment.py (https://gitlab.com/paulklemm_PHD/scripts)
";


GetOptions('help!' =>\$help,'h!' =>\$help,
	'gff_filter=s' =>\$gff_filter,
	'gff_id=s' =>\$gff_id,
	#'gffRef=s' =>\$gffRef,
	'gffRef=s' =>\$gffRef,
	'fastaNew=s' =>\$fastaNew,
	'fastaRef=s' =>\$fastaRef,
	'mums=s' =>\$mums,
	'out=s' =>\$out,
	'version!' => sub { print "$0\tv$version\n";exit 0; },
	'v!' => sub { print "$0\tv$version\n";exit 0; },
	'verbose' => \$verbose,
	'x' => \$debug
);

if ($help || !defined($gffRef) || !defined($fastaNew) || !defined($fastaRef) || !defined($mums) || !defined($out) ){
    print $usage;
    exit(1);
}

our %stat;
sub make_stat{
	my $chr=shift;
	my $what=shift;

	if(!exists $stat{$chr}){$stat{$chr} = ()}
	if(!exists $stat{$chr}{$what}){$stat{$chr}{$what} = 0}

	$stat{$chr}{$what}++;
}

our %BLOSUM62 = ('A'=> {'A'=> 4, 'C'=> 0, 'E'=> -1, 'D'=> -2, 'G'=> 0, 'F'=> -2, 'I'=> -1, 'H'=> -2, 'K'=> -1, 'M'=> -1, 'L'=> -1, 'N'=> -2, 'Q'=> -1, 'P'=> -1, 'S'=> 1, 'R'=> -1, 'T'=> 0, 'W'=> -3, 'V'=> 0, 'Y'=> -2}, 'C'=> {'A'=> 0, 'C'=> 9, 'E'=> -4, 'D'=> -3, 'G'=> -3, 'F'=> -2, 'I'=> -1, 'H'=> -3, 'K'=> -3, 'M'=> -1, 'L'=> -1, 'N'=> -3, 'Q'=> -3, 'P'=> -3, 'S'=> -1, 'R'=> -3, 'T'=> -1, 'W'=> -2, 'V'=> -1, 'Y'=> -2}, 'E'=> {'A'=> -1, 'C'=> -4, 'E'=> 5, 'D'=> 2, 'G'=> -2, 'F'=> -3, 'I'=> -3, 'H'=> 0, 'K'=> 1, 'M'=> -2, 'L'=> -3, 'N'=> 0, 'Q'=> 2, 'P'=> -1, 'S'=> 0, 'R'=> 0, 'T'=> -1, 'W'=> -3, 'V'=> -2, 'Y'=> -2}, 'D'=> {'A'=> -2, 'C'=> -3, 'E'=> 2, 'D'=> 6, 'G'=> -1, 'F'=> -3, 'I'=> -3, 'H'=> -1, 'K'=> -1, 'M'=> -3, 'L'=> -4, 'N'=> 1, 'Q'=> 0, 'P'=> -1, 'S'=> 0, 'R'=> -2, 'T'=> -1, 'W'=> -4, 'V'=> -3, 'Y'=> -3}, 'G'=> {'A'=> 0, 'C'=> -3, 'E'=> -2, 'D'=> -1, 'G'=> 6, 'F'=> -3, 'I'=> -4, 'H'=> -2, 'K'=> -2, 'M'=> -3, 'L'=> -4, 'N'=> 0, 'Q'=> -2, 'P'=> -2, 'S'=> 0, 'R'=> -2, 'T'=> -2, 'W'=> -2, 'V'=> -3, 'Y'=> -3}, 'F'=> {'A'=> -2, 'C'=> -2, 'E'=> -3, 'D'=> -3, 'G'=> -3, 'F'=> 6, 'I'=> 0, 'H'=> -1, 'K'=> -3, 'M'=> 0, 'L'=> 0, 'N'=> -3, 'Q'=> -3, 'P'=> -4, 'S'=> -2, 'R'=> -3, 'T'=> -2, 'W'=> 1, 'V'=> -1, 'Y'=> 3}, 'I'=> {'A'=> -1, 'C'=> -1, 'E'=> -3, 'D'=> -3, 'G'=> -4, 'F'=> 0, 'I'=> 4, 'H'=> -3, 'K'=> -3, 'M'=> 1, 'L'=> 2, 'N'=> -3, 'Q'=> -3, 'P'=> -3, 'S'=> -2, 'R'=> -3, 'T'=> -1, 'W'=> -3, 'V'=> 3, 'Y'=> -1}, 'H'=> {'A'=> -2, 'C'=> -3, 'E'=> 0, 'D'=> -1, 'G'=> -2, 'F'=> -1, 'I'=> -3, 'H'=> 8, 'K'=> -1, 'M'=> -2, 'L'=> -3, 'N'=> 1, 'Q'=> 0, 'P'=> -2, 'S'=> -1, 'R'=> 0, 'T'=> -2, 'W'=> -2, 'V'=> -3, 'Y'=> 2}, 'K'=> {'A'=> -1, 'C'=> -3, 'E'=> 1, 'D'=> -1, 'G'=> -2, 'F'=> -3, 'I'=> -3, 'H'=> -1, 'K'=> 5, 'M'=> -1, 'L'=> -2, 'N'=> 0, 'Q'=> 1, 'P'=> -1, 'S'=> 0, 'R'=> 2, 'T'=> -1, 'W'=> -3, 'V'=> -2, 'Y'=> -2}, 'M'=> {'A'=> -1, 'C'=> -1, 'E'=> -2, 'D'=> -3, 'G'=> -3, 'F'=> 0, 'I'=> 1, 'H'=> -2, 'K'=> -1, 'M'=> 5, 'L'=> 2, 'N'=> -2, 'Q'=> 0, 'P'=> -2, 'S'=> -1, 'R'=> -1, 'T'=> -1, 'W'=> -1, 'V'=> 1, 'Y'=> -1}, 'L'=> {'A'=> -1, 'C'=> -1, 'E'=> -3, 'D'=> -4, 'G'=> -4, 'F'=> 0, 'I'=> 2, 'H'=> -3, 'K'=> -2, 'M'=> 2, 'L'=> 4, 'N'=> -3, 'Q'=> -2, 'P'=> -3, 'S'=> -2, 'R'=> -2, 'T'=> -1, 'W'=> -2, 'V'=> 1, 'Y'=> -1}, 'N'=> {'A'=> -2, 'C'=> -3, 'E'=> 0, 'D'=> 1, 'G'=> 0, 'F'=> -3, 'I'=> -3, 'H'=> 1, 'K'=> 0, 'M'=> -2, 'L'=> -3, 'N'=> 6, 'Q'=> 0, 'P'=> -2, 'S'=> 1, 'R'=> 0, 'T'=> 0, 'W'=> -4, 'V'=> -3, 'Y'=> -2}, 'Q'=> {'A'=> -1, 'C'=> -3, 'E'=> 2, 'D'=> 0, 'G'=> -2, 'F'=> -3, 'I'=> -3, 'H'=> 0, 'K'=> 1, 'M'=> 0, 'L'=> -2, 'N'=> 0, 'Q'=> 5, 'P'=> -1, 'S'=> 0, 'R'=> 1, 'T'=> -1, 'W'=> -2, 'V'=> -2, 'Y'=> -1}, 'P'=> {'A'=> -1, 'C'=> -3, 'E'=> -1, 'D'=> -1, 'G'=> -2, 'F'=> -4, 'I'=> -3, 'H'=> -2, 'K'=> -1, 'M'=> -2, 'L'=> -3, 'N'=> -2, 'Q'=> -1, 'P'=> 7, 'S'=> -1, 'R'=> -2, 'T'=> -1, 'W'=> -4, 'V'=> -2, 'Y'=> -3}, 'S'=> {'A'=> 1, 'C'=> -1, 'E'=> 0, 'D'=> 0, 'G'=> 0, 'F'=> -2, 'I'=> -2, 'H'=> -1, 'K'=> 0, 'M'=> -1, 'L'=> -2, 'N'=> 1, 'Q'=> 0, 'P'=> -1, 'S'=> 4, 'R'=> -1, 'T'=> 1, 'W'=> -3, 'V'=> -2, 'Y'=> -2}, 'R'=> {'A'=> -1, 'C'=> -3, 'E'=> 0, 'D'=> -2, 'G'=> -2, 'F'=> -3, 'I'=> -3, 'H'=> 0, 'K'=> 2, 'M'=> -1, 'L'=> -2, 'N'=> 0, 'Q'=> 1, 'P'=> -2, 'S'=> -1, 'R'=> 5, 'T'=> -1, 'W'=> -3, 'V'=> -3, 'Y'=> -2}, 'T'=> {'A'=> 0, 'C'=> -1, 'E'=> -1, 'D'=> -1, 'G'=> -2, 'F'=> -2, 'I'=> -1, 'H'=> -2, 'K'=> -1, 'M'=> -1, 'L'=> -1, 'N'=> 0, 'Q'=> -1, 'P'=> -1, 'S'=> 1, 'R'=> -1, 'T'=> 5, 'W'=> -2, 'V'=> 0, 'Y'=> -2}, 'W'=> {'A'=> -3, 'C'=> -2, 'E'=> -3, 'D'=> -4, 'G'=> -2, 'F'=> 1, 'I'=> -3, 'H'=> -2, 'K'=> -3, 'M'=> -1, 'L'=> -2, 'N'=> -4, 'Q'=> -2, 'P'=> -4, 'S'=> -3, 'R'=> -3, 'T'=> -2, 'W'=> 11, 'V'=> -3, 'Y'=> 2}, 'V'=> {'A'=> 0, 'C'=> -1, 'E'=> -2, 'D'=> -3, 'G'=> -3, 'F'=> -1, 'I'=> 3, 'H'=> -3, 'K'=> -2, 'M'=> 1, 'L'=> 1, 'N'=> -3, 'Q'=> -2, 'P'=> -2, 'S'=> -2, 'R'=> -3, 'T'=> 0, 'W'=> -3, 'V'=> 4, 'Y'=> -1}, 'Y'=> {'A'=> -2, 'C'=> -2, 'E'=> -2, 'D'=> -3, 'G'=> -3, 'F'=> 3, 'I'=> -1, 'H'=> 2, 'K'=> -2, 'M'=> -1, 'L'=> -1, 'N'=> -2, 'Q'=> -1, 'P'=> -3, 'S'=> -2, 'R'=> -2, 'T'=> -2, 'W'=> 2, 'V'=> -1, 'Y'=> 7});

our %transl_table;
my $cur_i="";
my $cur_aa="";
my $cur_feature="";
my $cur_refase1="";
my $cur_refase2="";
my $cur_refase3="";
while(<DATA>){
	my $line=$_;
	chomp($line);
	if($line=~m/transl_table=([0-9]+)/){
		$cur_i="";
		$cur_aa="";
		$cur_feature="";
		$cur_refase1="";
		$cur_refase2="";
		$cur_refase3="";

		$cur_i=$1;
	}elsif($line=~m/AAs *= *([^ ]+)/){
		$cur_aa = $1;
	}elsif($line=~m/Starts *= *([^ ]+)/){
		$cur_feature = $1;
	}elsif($line=~m/Base1 *= *([^ ]+)/){
		$cur_refase1 = $1;
	}elsif($line=~m/Base2 *= *([^ ]+)/){
		$cur_refase2 = $1;
	}elsif($line=~m/Base3 *= *([^ ]+)/){
		$cur_refase3 = $1;
		# last base -> assemble informations : 

		for(my $j = 0 ; $j < length($cur_refase1) ; $j++){
			my $cur_codon = substr($cur_refase1,$j,1).substr($cur_refase2,$j,1).substr($cur_refase3,$j,1);

			$transl_table{$cur_i}{$cur_codon}{"aa"}=substr($cur_aa,$j,1);
			$transl_table{$cur_i}{$cur_codon}{"feature"}=substr($cur_feature,$j,1);
		}
	}
}

our %fastaNew_data;
my $cur_header="";

open(my $FHfa, "<$fastaNew");
while(<$FHfa>){
	chomp;
	my $entry = $_;
	if($entry eq ""){next}
	if(/^>/){
		$entry=~s/^>//g;
		$entry=~s/ .*//g;
		$cur_header=$entry;
		$fastaNew_data{$cur_header}="";
	}else{ $fastaNew_data{$cur_header}.=$entry }
}close($FHfa);

our %fastaRef_data;
$cur_header="";
open(my $FHfb, "<$fastaRef");
while(<$FHfb>){
	chomp;
	my $entry = $_;
	if($entry eq ""){next}
	if(/^>/){
		$entry=~s/^>//g;
		$entry=~s/ .*//g;
		$cur_header=$entry;
		$fastaRef_data{$cur_header}="";
	}else{ $fastaRef_data{$cur_header}.=$entry }
}close($FHfb);

print STDERR "[$me] loaded ".(scalar keys %fastaNew_data)." fastaNew entries and ".(scalar keys %fastaRef_data)." fastaRef entries\n";

our %gffRef_data;

open(my $FHb, "<$gffRef");
while(<$FHb>){
	chomp;
	my @a = split "\t",$_;
	if(scalar @a != 9 || $a[2] ne $gff_filter){next}

	my $chr = $a[0];

	make_stat($chr,"gffRef total '$gff_filter'");
	my $id = "";
	if($_=~/$gff_id=([^;]+)/){$id=$1}
	else{
		make_stat($chr,"ERROR: missing -gff_id=$id entry in A");
		next;
	}

	if(exists $gffRef_data{$id}){die "exists already $id";}

	if($a[3] eq "?" || $a[4] eq "?"){
		make_stat($chr,"ERROR: ? start/end entry in A");
		next;
	}

	push(@{$gffRef_data{$chr}},\@a);
}close($FHb);

print STDERR "[$me] loaded ".(scalar keys %gffRef_data)." gffRef entries\n";

my $asd=0;

open(our $FHmums, "<$mums");
open(our $FHout_pos, ">$out.pos");
open(our $FHout_aln, ">$out.aln");
our $empty="";
our $chr="";
our $pos_new = 1;
our $pos_ref = 1;
our $len_match = 1;
our $last_chr="";
our $last_pos_new = 1;
our $last_pos_ref = 1;
our $last_len_match = 0;

sub mainFun{

	my $seqNew = substr($fastaNew_data{$chr},$last_pos_new+$last_len_match-1,$pos_new-($last_pos_new+$last_len_match)); # `getgff.pl '$fastaRef' '$gffRef_data{$id}[0]' '$gffRef_data{$id}[3]' '$gffRef_data{$id}[4]' '$gffRef_data{$id}[6]' 2>/dev/null | tr -d '\n'`;
	my $seqRef = substr($fastaRef_data{$chr},$last_pos_ref+$last_len_match-1,$pos_ref-($last_pos_ref+$last_len_match)); # `getgff.pl '$fastaNew' '$a[0]' '$a[3]' '$a[4]' '$a[6]' 2>/dev/null | tr -d '\n'`;

	print STDERR ">fastaNew_data{$chr},".($last_pos_new+$last_len_match-1).",".($pos_new-($last_pos_new+$last_len_match))."\n";
	print STDERR "$seqNew\n";
	print STDERR ">fastaRef_data{$chr},".($last_pos_ref+$last_len_match-1).",".($pos_ref-($last_pos_ref+$last_len_match))."\n";
	print STDERR "$seqRef\n";

	if(length($seqNew)>10000 || length($seqRef)>10000){
		make_stat("$chr","uncomparable long regions of differences: ${chr}:".($last_pos_new+$last_len_match)."..".($pos_new-1)." vs ${chr}:".($last_pos_ref+$last_len_match)."..".($pos_ref-1));
		return;
	}

	my $cmd = "easyalignment.py '$seqRef' '$seqNew' | head -n3";

	my @aln;
	if(length($seqNew) == 0 && length($seqRef) > 0){
		@aln = (">score: -".((length($seqRef)-1)*6.6+15)."; match: 0; mismatch: 0; indel: ".length($seqRef)."",$seqRef,"-" x length($seqRef));
	}elsif(length($seqRef) == 0 && length($seqNew) > 0){
		@aln = (">score: -".((length($seqNew)-1)*6.6+15)."; match: 0; mismatch: 0; indel: ".length($seqNew)."","-" x length($seqNew),$seqNew);
	}elsif(length($seqNew) > 0 && length($seqRef) > 0){
		@aln = split("\n",`$cmd`);
	}else{
		return # nothing to compare bot sequences are empty ...
	}
	my $cur_new_pos = $last_pos_new+$last_len_match;
	my $cur_ref_pos = $last_pos_ref+$last_len_match;

	my $contains_mm=0;
	my $contains_in=0;
	my $contains_del=0;

	print $FHout_aln ">${chr}:".($last_pos_new+$last_len_match)."..".($pos_new-1)." (".basename($fastaNew).") stat=$aln[0];\n$aln[1]\n>${chr}:".($last_pos_ref+$last_len_match)."..".($pos_ref-1)." (".basename($fastaRef).")\n$aln[2]\n";
	
	my %genes_mod;
	
	for (my $i = 0; $i < length($aln[1]); $i++) {
		my $a = substr($aln[1],$i,1);
		my $b = substr($aln[2],$i,1);

		if($a eq $b){ $cur_new_pos++; $cur_ref_pos++; next}

		if($a eq "-"){
			print $FHout_pos "$chr\tindel\t$cur_ref_pos\t$cur_new_pos\t$a\t$b";
			$contains_in=1;
		}elsif($b eq "-"){
			print $FHout_pos "$chr\tindel\t$cur_ref_pos\t$cur_new_pos\t$a\t$b";
			$contains_del=1;
		}elsif($a ne $b){
			print $FHout_pos "$chr\tmismatch\t$cur_ref_pos\t$cur_new_pos\t$a\t$b";
			$contains_mm=1;
		}

		my $id = "intergenic";
		my $gene_start=0;
		my $gene_end=0;
		my $gene_strand=0;
		my $gene_attr="-";
		foreach my $arr (@{$gffRef_data{$chr}}) {
			if($cur_ref_pos >= $arr->[3] && $cur_ref_pos <= $arr->[4]){
				if($arr->[8]=~/$gff_id=([^;]+)/){$id=$1}
				$gene_start=$arr->[3];
				$gene_end=$arr->[4];
				$gene_strand=$arr->[6];
				$gene_attr=$arr->[8];
				last;
			}
		}
		print $FHout_pos "\t$id\t";

		if($id ne "intergenic"){

			#my $seqRef_gene = substr($fastaRef_data{$chr},$arr->[3]-1,$arr->[4]-$arr->[3]); # `getgff.pl '$fastaNew' '$a[0]' '$a[3]' '$a[4]' '$a[6]' 2>/dev/null | tr -d '\n'`;
			my $seqRef_gene = substr($fastaRef_data{$chr},$gene_start-1,$gene_end-$gene_start); # `getgff.pl '$fastaNew' '$a[0]' '$a[3]' '$a[4]' '$a[6]' 2>/dev/null | tr -d '\n'`;

			my $start_ref_offset=-1;
			if($gene_strand eq "+"){
				if($seqRef_gene=~/(ATG|CTG|TTG)/i){$start_ref_offset=$-[0]}
			}else{
				# search for the rc of the start codon
				while($seqRef_gene=~/(CAT|CAG|CAA)/gi){$start_ref_offset=$-[0]} # take last occurence ! since it is on the - strand 
			}

			$start_ref_offset = $start_ref_offset % 3; # now start_ref_offset contains the offset of the start codons first position

			if( $start_ref_offset > -1){

				my $cur_ref_pos_in_gene = $cur_ref_pos - $gene_start; # relative position inside the gene
				my $cur_ref_pos_codon_offset = ($cur_ref_pos_in_gene-$start_ref_offset) % 3; # this will transform any position in the gene to the first position of the codon that it is part of
				# --> cur_ref_pos_in_gene - cur_ref_pos_codon_offset = is now the start positon of the codon of the current position inside the gene relative to the gene start codon
				
				my $codon_ref = substr($seqRef_gene, $cur_ref_pos_in_gene - $cur_ref_pos_codon_offset, 3);
				my $codon_new = substr($fastaNew_data{$chr}, $cur_new_pos-1 - $cur_ref_pos_codon_offset, 3); # for a do just the same assuming that the sequences are similar, mhhh

				if( $gene_strand eq "-" ){ 
					$codon_ref =~ tr/ACGTacgt/TGCAtgca/; 
					$codon_ref = reverse $codon_ref;
					$codon_new =~ tr/ACGTacgt/TGCAtgca/; 
					$codon_new = reverse $codon_new;
				}

				my $tra_ref = (exists $transl_table{"1"}{$codon_ref} ? $transl_table{"1"}{$codon_ref}{"aa"} : "?");
				my $tra_new = (exists $transl_table{"1"}{$codon_new} ? $transl_table{"1"}{$codon_new}{"aa"} : "?");
				
				my $blo = (exists $BLOSUM62{$tra_new}{$tra_ref} ? $BLOSUM62{$tra_new}{$tra_ref} : "?");

				print $FHout_pos "${gene_start}..${gene_end}:${gene_strand}\t$codon_new->$codon_ref\t$tra_ref->$tra_new\t$blo\t";

				if($tra_new eq $tra_ref){ 
					if(!exists $genes_mod{"silent substitution"}{$id}){
						make_stat("$chr","gene with silent substitution");
					}
					$genes_mod{"silent substitution"}{$id}=1;
					print $FHout_pos "silent substitution";
				}elsif(($tra_new ne "*" && $tra_ref eq "*") || ($tra_new eq "*" && $tra_ref ne "*")){ 
					if(!exists $genes_mod{"nonsense substitution"}{$id}){
						make_stat("$chr","gene with nonsense substitution");
					}
					$genes_mod{"nonsense substitution"}{$id}=1;
					print $FHout_pos "nonsense substitution";
				}elsif($blo eq "?"){ 
					if(!exists $genes_mod{"unknown substitution"}{$id}){
						make_stat("$chr","gene with unknown substitution");
					}
					$genes_mod{"unknown substitution"}{$id}=1;
					print $FHout_pos "unknown substitution";
				}elsif($blo>=0){ 
					if(!exists $genes_mod{"positive missensense substitution"}{$id}){
						make_stat("$chr","gene with positive missensense substitution");
					}
					$genes_mod{"positive missensense substitution"}{$id}=1;
					print $FHout_pos "positive missensense substitution";
				}elsif($blo<0){ 
					if(!exists $genes_mod{"negative missensense substitution"}{$id}){
						make_stat("$chr","gene with negative missensense substitution");
					}
					$genes_mod{"negative missensense substitution"}{$id}=1;
					print $FHout_pos "negative missensense substitution";
				}
				print $FHout_pos "\t$gene_attr\n";
			}else{
				print $FHout_pos "-\t-\t-\t-\t-\t-\n";
			}
		}else{
			print $FHout_pos "-\t-\t-\t-\t-\t-\n";
			if($contains_mm){ make_stat("$chr","intergenic with mismatch" ) }
			if($contains_del || $contains_in){ make_stat("$chr","intergenic with indel") }
		}
		if($a ne "-"){ $cur_new_pos++ }
		if($b ne "-"){ $cur_ref_pos++ }
	}
	if($contains_mm){ make_stat($chr,"region with mismatch" ) }
	if($contains_del || $contains_in){ make_stat($chr,"region with indel") }
}

print $FHout_pos "# Ref=".basename($fastaRef).", New=".basename($fastaNew)."\n# seqname\ttype\tposition Ref\tposition New\tnt Ref\tnt New\tgene\tgene coordinates\tcodon change\tAA change\tBLOSUM62\ttype AA change\tgff\n";

while(<$FHmums>){
	chomp;

	my @a = split /[ \t]+/,$_;
	if(scalar @a != 5){next}
	($empty,$chr,$pos_new,$pos_ref,$len_match) = @a;

	if( abs($pos_new - $pos_ref) > 1000 ){ print STDERR "mum too far away from identity line\n"; next }

	make_stat($chr,"mum region");
	
	RESTART:

	($empty,$chr,$pos_new,$pos_ref,$len_match) = @a;
	print STDERR "new MUM chr=$chr pos_new=$pos_new pos_ref=$pos_ref ! inter-mum region: last_end_a=".($last_pos_new+$last_len_match)."..$pos_new, last_end_b=".($last_pos_ref+$last_len_match)."..$pos_ref\n";

	if($chr ne $last_chr && $last_chr ne ""){
		print STDERR "END chr=$chr <- last_chr=$last_chr pos_new=".length($fastaNew_data{$last_chr})." pos_ref=".length($fastaRef_data{$last_chr})." !\n";
		$chr = $last_chr;
		$pos_new = length($fastaNew_data{$last_chr});
		$pos_ref = length($fastaRef_data{$last_chr});
		$len_match = 0;
		# this will now scan the last bit of a chr/plasmid
		# after the main if -> this will be reset
	}

	if( ($last_chr eq "" || $last_chr eq $chr) && $pos_new-($last_pos_new+$last_len_match) >= 0 && $pos_ref-($last_pos_ref+$last_len_match) >= 0 ){
		print STDERR "doing analysis\n";
		mainFun();
	}

	($empty,$chr,$pos_new,$pos_ref,$len_match) = @a;
	if($chr ne $last_chr && $last_chr ne ""){ # reset
		$pos_new = 1;
		$pos_ref = 1;
		$len_match = 0;
		# to scan the start area of the next plasmid 
		($last_chr,$last_pos_new,$last_pos_ref,$last_len_match) = ($chr,$pos_new,$pos_ref,$len_match);

		goto RESTART;
	}

	($last_chr,$last_pos_new,$last_pos_ref,$last_len_match) = ($chr,$pos_new,$pos_ref,$len_match);
}

# now the very last area:
$chr = $last_chr;
$pos_new = length($fastaNew_data{$last_chr});
$pos_ref = length($fastaRef_data{$last_chr});
$len_match = 0;
if( ($last_chr eq "" || $last_chr eq $chr) && $pos_new-($last_pos_new+$last_len_match) > 0 && $pos_ref-($last_pos_ref+$last_len_match) > 0 ){
	mainFun();
}

close($FHmums);
close($FHout_pos);
close($FHout_aln);

open(my $FHout3, ">$out.stat");
foreach my $chr (sort keys %stat) {
	foreach my $key (sort keys %{$stat{$chr}}){
		print $FHout3 "$chr\t$key\t$stat{$chr}{$key}\n";
	}
}
close($FHout3);


# just copy paste the whole https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes below __TRANSTABLESTRING__
# last time modified 3.2.2020

__DATA__

NCBI Front Page 	Taxonomy Logo
PubMed 	Entrez 	BLAST 	Genome 	Taxonomy 	Structure
	Search for As
lock    
	
Just empty space

Taxonomy browser

Taxonomy common tree

Taxonomy information

Taxonomic advisors

Genetic codes
Translation tables
1; 2; 3; 4; 5; 6; 9; 10; 11; 12; 13; 14; 16; 21; 22; 23; 24; 25; 26; 27; 28; 29; 30; 31; 33;


Taxonomy Statistics

Taxonomy Name/Id Status Report

Taxonomy FTP site

FAQs

How to reference the NCBI taxonomy database

How to create links to the NCBI taxonomy

How to create LinkOut links from the NCBI taxonomy

Extinct organisms

Recent changes to classification

 
	Just empty space 	

 
The Genetic Codes

Compiled by Andrzej (Anjay) Elzanowski and Jim Ostell at National Center for Biotechnology Information (NCBI), Bethesda, Maryland, U.S.A.
Last update of the Genetic Codes: Jan. 7, 2019

NCBI takes great care to ensure that the translation for each coding sequence (CDS) present in GenBank records is correct. Central to this effort is careful checking on the taxonomy of each record and assignment of the correct genetic code (shown as a /transl_table qualifier on the CDS in the flat files) for each organism and record. This page summarizes and references this work.

The synopsis presented below is based primarily on the reviews by Osawa et al. (1992) and Jukes and Osawa (1993). Listed in square brackets [] (under Systematic Range) are tentative assignments of a particular code based on sequence homology and/or phylogenetic relationships.

The print-form ASN.1 version of this document, which includes all the genetic codes outlined below, is also available here. Detailed information on codon usage can be found at the Codon Usage Database.

GenBank format by historical convention displays mRNA sequences using the DNA alphabet. Thus, for the convenience of people reading GenBank records, the genetic code tables shown here use T instead of U.

The following genetic codes are described here:

    1. The Standard Code
    2. The Vertebrate Mitochondrial Code
    3. The Yeast Mitochondrial Code
    4. The Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code
    5. The Invertebrate Mitochondrial Code
    6. The Ciliate, Dasycladacean and Hexamita Nuclear Code
    9. The Echinoderm and Flatworm Mitochondrial Code
    10. The Euplotid Nuclear Code
    11. The Bacterial, Archaeal and Plant Plastid Code
    12. The Alternative Yeast Nuclear Code
    13. The Ascidian Mitochondrial Code
    14. The Alternative Flatworm Mitochondrial Code
    16. Chlorophycean Mitochondrial Code
    21. Trematode Mitochondrial Code
    22. Scenedesmus obliquus Mitochondrial Code
    23. Thraustochytrium Mitochondrial Code
    24. Pterobranchia Mitochondrial Code
    25. Candidate Division SR1 and Gracilibacteria Code
    26. Pachysolen tannophilus Nuclear Code
    27. Karyorelict Nuclear Code
    28. Condylostoma Nuclear Code
    29. Mesodinium Nuclear Code
    30. Peritrich Nuclear Code
    31. Blastocrithidia Nuclear Code
    33. Cephalodiscidae Mitochondrial UAA-Tyr Code

1. The Standard Code (transl_table=1)

By default all transl_table in GenBank flatfiles are equal to id 1, and this is not shown. When transl_table is not equal to id 1, it is shown as a qualifier on the CDS feature.

    AAs  = FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ---M------**--*----M---------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Initiation Codon:

AUG

Alternative Initiation Codons:

In rare cases, translation in eukaryotes can be initiated from codons other than AUG. A well documented case (including direct protein sequencing) is the GUG start of a ribosomal P protein of the fungus

Candida albicans (Abramczyk et al.) and the GUG initiation in mammalian NAT1 (Takahashi et al. 2005).
Other examples can be found in the following references: Peabody 1989; Prats et al. 1989; Hann et al. 1992; Sugihara et al. 1990. The standard code currently allows initiation from UUG and CUG in addition to AUG.
Back to top
2. The Vertebrate Mitochondrial Code (transl_table=2)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIMMTTTTNNKKSS**VVVVAAAADDEEGGGG
  Starts = ----------**--------------------MMMM----------**---M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

        Code 2          Standard

 AGA    Ter  *          Arg  R
 AGG    Ter  *          Arg  R
 AUA    Met  M          Ile  I
 UGA    Trp  W          Ter  *

Alternative Initiation Codons:

Bos: AUA

Homo: AUA, AUU

Mus: AUA, AUU, AUC

Coturnix, Gallus: also GUG (Desjardins and Morais, 1991)

Systematic Range:

Vertebrata

Comments:

AGA and AGG were thought to have become mitochondrial stop codons early in vertebrate evolution (Osawa, Ohama, Jukes & Watanabe 1989). However, at least in humans it has now been shown that AGA and AGG sequences are not recognized as termination codons. A -1 mitoribosome frameshift occurs at the AGA and AGG codons predicted to terminate the CO1 and ND6 ORFs, and consequently both ORFs terminate in the standard UAG codon (Temperley et al. 2010).
Mitochondrial genes in some vertebrate (including humans) have incomplete stop codons ending in U or UA, which become complete termination codons (UAA) upon subsequent polyadenylation (Hou et al. 2006; Oh et al. 2007; Ki et al. 2010; Temperley R J et al 2010).
Back to top
3. The Yeast Mitochondrial Code (transl_table=3)

    AAs  = FFLLSSSSYY**CCWWTTTTPPPPHHQQRRRRIIMMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**----------------------MM---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

        Code 3          Standard

 AUA    Met  M          Ile  I
 CUU    Thr  T          Leu  L
 CUC    Thr  T          Leu  L
 CUA    Thr  T          Leu  L
 CUG    Thr  T          Leu  L
 UGA    Trp  W          Ter  *

Systematic Range:

Saccharomyces cerevisiae, Candida glabrata, Hansenula saturnus, and Kluyveromyces thermotolerans (Clark-Walker and Weiller, 1994)

Comments:

GUG (GTG) is used as a start codon for a few proteins in some Saccharomyces species (Sulo et al. 2017). The remaining CGN codons are rare in Saccharomyces cerevisiae and absent in Candida glabrata (= Torulopsis glabrata).

The AUA codon is common in the gene var1 coding for the single mitochondrial ribosomal protein, but rare in genes encoding the enzymes.

The coding assignments of the AUA (Met or Ile) and CUU (possibly Leu, not Thr) are uncertain in Hansenula saturnus.

The coding assignment of Thr to CUN is uncertain in Kluyveromyces thermotolerans (Clark-Walker and Weiller, 1994).
Back to top
4. The Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code (transl_table=4)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --MM------**-------M------------MMMM---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

        Code 4         Standard

 UGA    Trp  W          Ter  *

Alternative Initiation Codons:

Trypanosoma: UUA, UUG, CUG

Leishmania: AUU, AUA

Tertrahymena: AUU, AUA, AUG

Paramecium: AUU, AUA, AUG, AUC, GUG, GUA(?)

(Pritchard et al., 1990)

Systematic Range:

Bacteria: The code is used in Entomoplasmatales and Mycoplasmatales (Bove et al. 1989). The situation in the Acholeplasmatales is unclear. Based on a study of ribosomal protein genes, it had been concluded that UGA does not code for tryptophan in plant-pathogenic mycoplasma-like organisms (MLO) and the Acholeplasmataceae (Lim and Sears, 1992) and there seems to be only a single tRNA-CCA for tryptophan in Acholeplasma laidlawii (Tanaka et al. 1989). In contrast, in a study of codon usage in Phytoplasmas, it was found that 30 out of 78 ORFs analyzed translated better with code 4 (UGA for tryptophan) than with code 11 while the remainder showed no differences between the two codes (Melamed et al. 2003). In addition, the coding reassignment of UGA Stop --> Trp can be found in an alpha-proteobacterial symbiont of cicadas: Candidatus Hodgkinia cicadicola (McCutcheon et al. 2009).

Fungi: Emericella nidulans, Neurospora crassa, Podospora anserina, Acremonium (Fox, 1987), Candida parapsilosis (Guelin et al., 1991), Trichophyton rubrum (de Bievre and Dujon, 1992), Dekkera/Brettanomyces, Eeniella (Hoeben et al., 1993), and probably Ascobolus immersus, Aspergillus amstelodami, Claviceps purpurea, and Cochliobolus heterostrophus.

Other Eukaryotes: Gigartinales among the red algae (Boyen et al. 1994), and the protozoa Trypanosoma brucei, Leishmania tarentolae, Paramecium tetraurelia, Tetrahymena pyriformis and probably Plasmodium gallinaceum (Aldritt et al., 1989).

Metazoa: Coelenterata (Ctenophora and Cnidaria)

Comments:

This code is also used for the kinetoplast DNA (maxicircles, minicircles). Kinetoplasts are modified mitochondria (or their parts).
Back to top
5. The Invertebrate Mitochondrial Code (transl_table=5)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIMMTTTTNNKKSSSSVVVVAAAADDEEGGGG
  Starts = ---M------**--------------------MMMM---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Comment:

The codon AGG is absent in Drosophila.

Differences from the Standard Code:


        Code 5          Standard

 AGA    Ser  S          Arg  R
 AGG    Ser  S          Arg  R
 AUA    Met  M          Ile  I
 UGA    Trp  W          Ter  *

Alternative Initiation Codons:

AUA, AUU

AUC: Apis (Crozier and Crozier, 1993)

GUG: Polyplacophora (Boore and Brown, 1994 GenBank Accession Number:

U09810)

UUG: Ascaris, Caenorhabditis

Systematic Range:

Nematoda: Ascaris, Caenorhabditis;

Mollusca: Bivalvia (Hoffmann et al., 1992); Polyplacophora (Boore and Brown, 1994)

Arthropoda/Crustacea: Artemia (Batuecas et al., 1988);

Arthropoda/Insecta: Drosophila [Locusta migratoria (migratory locust), Apis mellifera (honeybee)]

Comments:

Several arthropods translate the codon AGG as lysine instead of serine (as in the invertebrate mitochondrial genetic code) or arginine (as in the standard genetic code) (Abascal et al., 2006).

GUG may possibly function as an initiator in Drosophila (Clary and Wolstenholme, 1985; Gadaleta et al., 1988). AUU is not used as an initiator in Mytilus (Hoffmann et al., 1992).

"An exceptional mechanism must operate for initiation of translation of the cytochrome oxidase subunit I mRNA in both D. melanogaster (de Bruijn, 1983) and D. yakuba (Clary and Wolstenholme 1983), since its only plausible initiation codon, AUA, is out of frame with the rest of the gene. Initiation appears to require the "reading" of of an AUAA quadruplet, which would be equivalent to initiation at AUA followed immediately by a specific ribosomal frameshift. Another possible mechanism ... is that the mRNA is "edited" to bring the AUA initiation into frame." (Fox, 1987)
Back to top
6. The Ciliate, Dasycladacean and Hexamita Nuclear Code (transl_table=6)

    AAs  = FFLLSSSSYYQQCC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --------------*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 6       Standard

 UAA      Gln  Q        Ter  *
 UAG      Gln  Q        Ter  *

Systematic Range:

Ciliata: Oxytricha and Stylonychia (Hoffman et al. 1995), Paramecium, Tetrahymena, Oxytrichidae and probably Glaucoma chattoni.

Dasycladaceae: Acetabularia (Schneider et al., 1989) and

Batophora (Schneider and de Groot, 1991).

Diplomonadida:

Scope: Hexamita inflata, Diplomonadida ATCC50330, and ATCC50380.

Ref.: Keeling, P.J. and Doolittle, W.F. 1996.. A non-canonical genetic code in an early diverging eukaryotic lineage. The EMBO Journal 15, 2285-2290.

Comment:

The ciliate macronuclear code has not been determined completely. The codon UAA is known to code for Gln only in the Oxytrichidae.
Back to top
9. The Echinoderm and Flatworm Mitochondrial Code (transl_table=9)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIIMTTTTNNNKSSSSVVVVAAAADDEEGGGG
  Starts = ----------**-----------------------M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:


          Code 9        Standard

 AAA      Asn  N        Lys K
 AGA      Ser  S        Arg R
 AGG      Ser  S        Arg R
 UGA      Trp  W        Ter *

Systematic Range:

Asterozoa (starfishes) (Himeno et al., 1987)

Echinozoa (sea urchins) (Jacobs et al., 1988; Cantatore et al., 1989)
Rhabditophora among the Platyhelminthes (Telford et al. 2000)
Back to top
10. The Euplotid Nuclear Code (transl_table=10)

    AAs  = FFLLSSSSYY**CCCWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**-----------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 10     Standard

 UGA      Cys  C        Ter  *

Systematic Range:

Ciliata: Euplotidae (Hoffman et al. 1995).
Back to top
11. The Bacterial, Archaeal and Plant Plastid Code (transl_table=11)

    AAs  = FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ---M------**--*----M------------MMMM---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Systematic Range and Comments:

Table 11 is used for Bacteria, Archaea, prokaryotic viruses and chloroplast proteins. As in the standard code, initiation is most efficient at AUG. In addition, GUG and UUG starts are documented in Archaea and Bacteria (Kozak 1983, Fotheringham et al. 1986, Golderer et al. 1995, Nolling et al. 1995, Sazuka & Ohara 1996, Genser et al. 1998, Wang et al. 2003). In E. coli, UUG is estimated to serve as initiator for about 3% of the bacterium's proteins (Blattner et al. 1997). CUG is known to function as an initiator for one plasmid-encoded protein (RepA) in Escherichia coli (Spiers and Bergquist, 1992). In addition to the NUG initiations, in rare cases Bacteria can initiate translation from an AUU codon as e.g. in the case of poly(A) polymerase PcnB and the InfC gene that codes for translation initiation factor IF3 (Polard et al. 1991, Liveris et al. 1993, Sazuka & Ohara 1996, Binns & Masters 2002). The internal assignments are the same as in the standard code though UGA codes at low efficiency for Trp in Bacillus subtilis and, presumably, in Escherichia coli (Hatfiled and Diamond, 1993).
Back to top
12. The Alternative Yeast Nuclear Code (transl_table=12)

    AAs  = FFLLSSSSYY**CC*WLLLSPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**--*----M---------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

           Code 12      Standard

 CUG       Ser          Leu
       

Alternative Initiation Codons:

CAG may be used in Candida albicans (Santos et al., 1993).

Systematic Range:

Endomycetales (yeasts): Candida albicans, Candida cylindracea, Candida melibiosica, Candida parapsilosis, and Candida rugosa (Ohama et al., 1993).

Comment:

However, other yeast, including Saccharomyces cerevisiae, Candida azyma, Candida diversa, Candida magnoliae, Candida rugopelliculosa, Yarrowia lipolytica, and Zygoascus hellenicus, definitely use the standard (nuclear) code (Ohama et al., 1993).
Back to top
13. The Ascidian Mitochondrial Code (transl_table=13)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIMMTTTTNNKKSSGGVVVVAAAADDEEGGGG
  Starts = ---M------**----------------------MM---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 13     Standard

 AGA      Gly  G        Arg  R
 AGG      Gly  G        Arg  R
 AUA      Met  M        Ile  I
 UGA      Trp  W        Ter  *

Systematic range and Comments:

There is evidence from a phylogenetically diverse sample of tunicates (Urochordata) that AGA and AGG code for glycine. In other organisms, AGA/AGG code for either arginine or serine and in vertebrate mitochondria they code a STOP. Evidence for glycine translation of AGA/AGG has been found in Pyura stolonifera (Durrheim et al. 1993), Halocynthia roretzi (Kondow et al. 1999,Yokobori et al., 1993, Yokobori et al. 1999) and Ciona savignyi (Yokobori et al. 2003).

In addition, the Halocynthia roretzi mitochondrial genome encodes an additional tRNA gene with the anticodon U*CU that is thought to enable the use of AGA or AGG codons for glycine and the gene has been shown to be transcribed in vivo (Kondow et al. 1999, Yokobori et al. 1999).

Alternative initiation codons:

ATA, GTG and TTG (Yokobori et al. 1999). ATT is the start codon for the CytB gene in Halocynthia roretzi (Gissi and Pesole, 2003).
Back to top
14. The Alternative Flatworm Mitochondrial Code (transl_table=14)

    AAs  = FFLLSSSSYYY*CCWWLLLLPPPPHHQQRRRRIIIMTTTTNNNKSSSSVVVVAAAADDEEGGGG
  Starts = -----------*-----------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 14      Standard

 AAA      Asn  N       Lys  K
 AGA      Ser  S       Arg  R
 AGG      Ser  S       Arg  R
 UAA      Tyr  Y       Ter  *
 UGA      Trp  W       Ter  *

Systematic Range:

Platyhelminthes (flatworms) and Nematoda (roundworms)

Comments:

Code 14 differs from code 9 only by translating UAA to Tyr rather than STOP. A recent study (Telford et al. 2000) has found no evidence that the codon UAA codes for Tyr in the flatworms but other opinions exist. There are very few GenBank records that are translated with code 14 but a test translation shows that retranslating these records with code 9 can cause premature terminations. More recently, UAA has been found to code for tyrosine in the nematodes Radopholus similis and Radopholus arabocoffeae (Jacob et al. 2009).
Back to top
16. Chlorophycean Mitochondrial Code (transl_table=16)

    AAs  = FFLLSSSSYY*LCC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------*---*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 16       Standard

TAG       Leu  L        STOP

Systematic Range:

Chlorophyceae (Hayashi-Ishimaru et al. 1996. UAG is a sense codon in several chlorophycean mitochondria) and the chytridiomycete fungus Spizellomyces punctatus (Laforest et al. 1997. Mitochondrial tRNAs in the lower fungus Spizellomyces punctatus: tRNA editing and UAG 'stop' codons recognized as leucine).
Back to top
21. Trematode Mitochondrial Code (transl_table=21)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIMMTTTTNNNKSSSSVVVVAAAADDEEGGGG
  Starts = ----------**-----------------------M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 21       Standard

TGA       Trp  W        STOP
ATA       Met  M        Ile
AGA       Ser  S        Arg
AGG       Ser  S        Arg
AAA       Asn  N        Lys 

Systematic Range:

Trematoda: Ohama, T, S. Osawa, K. Watanabe, T.H. Jukes, 1990. J. Molec Evol. 30
Garey, J.R. and D.R. Wolstenholme, 1989. J. Molec. Evol. 28: 374-387 329-332.
Back to top
22. Scenedesmus obliquus Mitochondrial Code (transl_table=22)

    AAs  = FFLLSS*SYY*LCC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ------*---*---*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 22       Standard

TCA       STOP *        Ser
TAG       Leu  L        STOP

Systematic Range:

Scenedesmus obliquus: Nedelcu A, Lee RW, Lemieux C, Gray MW and Burger G. "The complete mitochondrial DNA sequence of Scenedesmus obliquus reflects an intermediate stage in the evolution of the green algal mitochondrial genome." Genome Res. 2000 Jun;10(6):819-31.
Back to top
23. Thraustochytrium Mitochondrial Code (transl_table=23)

    AAs  = FF*LSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --*-------**--*-----------------M--M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

This code has been created for the mitochondrial genome of the labyrinthulid Thraustochytrium aureum sequenced by The Organelle Genome Megasequencing Program (OGMP).

It is the similar to the bacterial code (transl_table 11) but it contains an additional stop codon (TTA) and also has a different set of start codons.
Back to top
24. Pterobranchia Mitochondrial Code (transl_table=24)

    AAs  = FFLLSSSSYY**CCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSSKVVVVAAAADDEEGGGG
  Starts = ---M------**-------M---------------M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 24        Standard

AGA       Ser S          Arg  R
AGG       Lys K          Arg  R
UGA       Trp W          STOP *

Code 24 has been created for the mitochondrial genome of Rhabdopleura compacta (Pterobranchia). The Pterobranchia are one of the two groups in the Hemichordata which together with the Echinodermata and Chordata form the three major lineages of deuterostomes. AUA translates to isoleucine in Rhabdopleura as it does in the Echinodermata and Enteropneusta while AUA encodes methionine in the Chordata. The assignment of AGG to Lys is not found elsewhere in deuterostome mitochondria but it occurs in some taxa of Arthropoda (Perseke et al. 2011). Code 24 shares with many other mitochondrial codes the reassignment of the UGA STOP to Trp, and AGG and AGA to an amino acid other than Arg. The initiation codons in Rhabdopleura compacta are ATG and GTG (Perseke et al. 2011).
Back to top
25. Candidate Division SR1 and Gracilibacteria Code (transl_table=25)

    AAs  = FFLLSSSSYY**CCGWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ---M------**-----------------------M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 25        Standard

UGA       Gly            STOP

Initiation Codons:

AUG, GUG, UUG

Systematic Range:

Candidate Division SR1, Gracilibacteria

Comments:

Code 25 is used in two groups of (so far) uncultivated Bacteria found in marine and fresh-water environment and in the intestines and oral cavities of mammals among others. The difference to the standard and the bacterial code is that UGA represents an additional glycine codon and does not code for termination (Campbell et al. 2013).
Back to top
26. Pachysolen tannophilus Nuclear Code (transl_table=26)

    AAs  = FFLLSSSSYY**CC*WLLLAPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**--*----M---------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 26        Standard

CUG       Ala            Leu

Initiation Codons:

AUG, GUG, UUG

Systematic Range:

Pachysolen tannophilus

Comments:
Code 26 is used so far only for the ascomycete fungus Pachysolen tannophilus. The only difference to the standard code is the translation of CUG as alanine (as opposed to leucine). (Muhlhausen et al. 2016).
Back to top
27. Karyorelict Nuclear Code (transl_table=27)

    AAs  = FFLLSSSSYYQQCCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --------------*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 27        Standard

UAG       Gln            STOP
UAA       Gln            STOP
UGA       STOP or Trp    STOP

Initiation Codons:

AUG

Systematic Range:

the karyorelictid ciliate Parduczia

Comments:
Code 27 reassigns the UAG and UAA stops to glutamine while UGA can function as either STOP or tryptophan. Code 27 is used for the karyorelictid ciliate Parduczia sp. (Swart et al. 2016).
Back to top
28. Condylostoma Nuclear Code (transl_table=28)

    AAs  = FFLLSSSSYYQQCCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**--*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 28          Standard

UAA       Gln or STOP      STOP
UAG       Gln or STOP      STOP
UGA       Trp or STOP      STOP

Initiation Codons: AUG

Systematic Range:

Condylostoma magnum

Comments:
Code 28 is used in Condylostoma magnum. The difference to the standard code is that the three stop codons can also be translated as glutamine (UAA, UAG) or tryptophan (UGA), respectively(Swart et al. 2016, Heaphy et al. 2016).
Back to top
29. Mesodinium Nuclear Code (transl_table=29)

    AAs  = FFLLSSSSYYYYCC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --------------*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 29        Standard

UAA       Tyr            STOP
UAG       Tyr            STOP

Initiation Codons:

AUG

Systematic Range:

the mesodiniid ciliates Mesodinium and Myrionecta

Comments:
Code 29 is used for the haptorid ciliates Mesodinium and Myrionecta. It differs from the standard code in reassigning the stop codons UAA and UAG to Tyrosine. (Heaphy et al. 2016).
Back to top
30. Peritrich Nuclear Code (transl_table=30)

    AAs  = FFLLSSSSYYEECC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = --------------*--------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 30        Standard

UAA       Glu            STOP
UAG       Glu            STOP

Initiation Codons:

AUG

Systematic Range:

the peritrich ciliate Carchesium

Comments:
Code 30 is used in the peritrich ciliate Carchesium. The stop codons UAA and UAG are reassigned to Glutamine. (Sanchez-Silva et al. 2003).
Back to top
31. Blastocrithidia Nuclear Code (transl_table=31)

    AAs  = FFLLSSSSYYEECCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
  Starts = ----------**-----------------------M----------------------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

          Code 31          Standard

UGA       Trp              STOP
UAG       Glu or STOP      STOP
UAA       Glu or STOP      STOP

Initiation Codons:

AUG

Systematic Range:

Blastocrithidia sp.

Comments:
Code 31 is used for the trypanosome Blastocrithidia sp. UGA encodes trytophan and UAG and UAA encode glutamate and also serve as termination codons. (Zahonova et al. 2016).
Back to top
33. Cephalodiscidae Mitochondrial UAA-Tyr Code (transl_table=33)

    AAs  = FFLLSSSSYYY*CCWWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSSKVVVVAAAADDEEGGGG
  Starts = ---M-------*-------M---------------M---------------M------------
  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

Click here to change format

Differences from the Standard Code:

     Code 33     Standard
UAA  Tyr         STOP
UGA  Trp         STOP
AGA  Ser         Arg
AGG  Lys         Arg

Systematic Range:

Cephalodiscidae (Hemichordata)

Comments:

Code 33 is very similar to the mitochondrial code 24 for the Pterobranchia, which also belong to the Hemichordata, except that it uses UAA for tyrosine rather than as a stop codon (Li Y, Kocot KM, Tassia MG, Cannon JT, Bernt M, Halanych KM. Mitogenomics Reveals a Novel Genetic Code in Hemichordata. Genome Biol Evol. 2019 Jan 1;11(1):29-40.)

Back to top

 
Comments and questions to info@ncbi.nlm.nih.gov
Credits: Andrzej (Anjay) Elzanowski, Jim Ostell, Detlef Leipe, Vladimir Soussov.
	[Search] 	  	  	[NLM NIH] 	[Disclaimer]
