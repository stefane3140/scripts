#!/usr/bin/perl
#pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use Net::FTP;
use File::Basename;

my $modus = "";
my @arg;
my $help;
my $version="0.1.19"; # last updated on 2021-10-05 11:53:44

my $assembly_level = "Complete Genome";
my $release_type = "Major";
my $genome_rep = "Full";
my $version_status = "latest";
my $exclude = "";
my $timeout = 1;
my $first = 0;
my $debug = 0;
my $dry;
my $drier;
my $fallback;
my $relaxed;
my $reference;

my $usage = <<ENDUSAGE;
easyNCBI.pl        simple interface to NCBI refseq/genbank ftp server
 
SYNOPSIS

easyNCBI.pl options (genbank|refseq|ensembl) MODUS species

	Allready downloaded files are not overwritten (save to rerun) only if there are new files on the NCBI server.

	(genbank|refseq|ensembl)
		either the genbank, refseq or ensembl database (ftp)

	MODUS  the main modus
		get -> download all available data for the given species
		get genome        : *genomic.fna
		get gff           : *genomic.gff
		get genomic       : *genomic.fna and *genomic.gff (both)
		get proteome      : *protein.faa
		get transcriptome : *rna.fna
		get cds           : *cds.fna
		get cdna          : *cdna.fna
		get from          : *_from_*
		get ... reference : only download entries marked as "reference genome" 
		
	species
		A species name / NCBI id (GCF* or ASM* or PRJNA*) or a file with multiple species names / ids (use - for STDIN)

	prefilter options: (set to "" to disable)
		-assembly_level (default:"$assembly_level")
			if you dont find anything usually set this to an empty string.
		-release_type (default:"$release_type")
		-genome_rep (default:"$genome_rep")
		-version_status (default:"$version_status")

	with -relaxed all prefilters are disabled (except -version_status "latest")

	other options:
		-exclude term : exclude any entry with the given search term (e.g. you could use 'virus')
		-dry : do not download anything just search the database (FTP connection is required to check files)
		-drier : only search the summary files (very fast)
		-timeout : timeout between ftp->get queries in seconds (default:$timeout)
		-fallback : if nothing found try again without the defined prefilters. If reference is set and nothing was found
					then first a search is conducted without the reference option and then without the filters.
		-first : download only the first entry if there are multiple hits

	Enviroment variable:
		EASYNCBISUMMARY="\$HOME/"
			the path to the summary file. If set then the summary file is not dowloaded in every directory easyNCBI.pl is executed.
			In case nothing is defined or the path is invalid, the home directory is used (\$HOME).
	! usually use -exclude virus to exclude any virus !

EXAMPLES
	\$ easyNCBI.pl refseq get genomic reference Bacillus subtilis -exclude virus
	would download the 2 files:
		Bacillus_subtilis_subsp._subtilis_str._168_2009_03_17_PRJNA57675.GCF_000009045.1_ASM904v1_genomic.fna.gz
		Bacillus_subtilis_subsp._subtilis_str._168_2009_03_17_PRJNA57675.GCF_000009045.1_ASM904v1_genomic.gff.gz 

	\$ easyNCBI.pl refseq get genome Bacillus subtilis 3610
	would download:
		Bacillus_subtilis_subsp._subtilis_NCIB_3610_=_ATCC_6051_=_DSM_10_2019_06_06_PRJNA224116.GCF_006088795.1_ASM608879v1_genomic.fna.gz

	\$ easyNCBI.pl refseq get gff ^Bacillus
	would download the genomic.gff files of all ~780 species of the bacillus genus

	\$ easyNCBI.pl ensembl get cdna Bacillus subtilis 168 -first
	would download the latest cdna.fa from ensembl 

	\$ easyNCBI.pl -drier refseq get gff 000027085
	search for GCF identifier

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long, Net::FTP
ENDUSAGE

GetOptions(
	'assembly_level=s' => \$assembly_level,
	'release_type=s' => \$release_type,
	'genome_rep=s' => \$genome_rep,
	'version_status=s' => \$version_status,
	'exclude=s' => \$exclude,
	'timeout=i' => \$timeout,
	'dry!' => \$dry,
	'drier!' => \$drier,
	'fallback!' => \$fallback,
	'first!' => \$first,
	'relaxed!' => \$relaxed,
	'reference!' => \$reference,
	'representative!' => \$reference,
	'x!' => \$debug,
	'help!'=>\$help,'h!'=>\$help,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	'v!'=>sub { print "$0\tv$version\n";exit 0; });

if($relaxed){
	$assembly_level = "";
	$release_type = "";
	$genome_rep = "";
	$version_status = "latest";
}

if($exclude eq ""){print STDERR "# usually a good option is: -exclude virus\n"}

my $summary_dir = $ENV{'EASYNCBISUMMARY'};
if(!defined $summary_dir || !-d $summary_dir){
	$summary_dir = $ENV{'HOME'}."/";
}
if(!-d $summary_dir){
	print STDERR "[ERROR] The enviroment variable \$EASYNCBISUMMARY='$summary_dir' is invalid\n";
	die;
}

my @species;
my %species2stat;
if(scalar @ARGV > 0){

	my $join_ARGV = join " ",@ARGV;

	if($join_ARGV=~/((refseq|genbank|ensembl) (get|summary)(reference|representative)? ?(genome?|transcriptome?|cdna|cds|gff|proteome?|from|genomic|) (reference|representative)?)/){
		$modus = $1;
		if($modus =~ /reference|representative/){$reference=1}

		$join_ARGV =~ s/$modus ?//g;
		@species = split(" ",$join_ARGV);
	}else{
		print STDERR "[ERROR] invalid options\n";exit 1;
	}
}

if ( $help || $modus eq "" ){
    print $usage;
    exit(0);
}

if(scalar @species > 0){
	if($species[0] eq "-"){
		shift @species;
		while(<STDIN>){
			chomp; push(@species,$_);
		}
	}elsif(-e $species[0]){
		open(FH,"<",$species[0]);
		shift @species;
		while(<FH>){
			chomp; push(@species,$_);
		}
		close(FH);
	}else{
		my $t = join " ", @species;
		@species = ();
		$species[0] = $t;
	}
}

$modus = lc $modus;

my $datestring = localtime();
$datestring =~ s/\d\d:\d\d:\d\d //g;
$datestring =~ s/ /_/g;

if($modus =~ /^(refseq|genbank|ensembl) (get|summary)/){

	my $db = $1;

	my $ftp = Net::FTP->new(
	    $db eq "ensembl" ? 'ftp.ensemblgenomes.org' : 'ftp.ncbi.nlm.nih.gov', 
	    Passive => 1, Timeout => 1, Debug=>$debug
	) or die "Net::FTP failed with: ".$@;
	
	$ftp->login('anonymous');
	$ftp->binary;

	print STDERR "searching for the summary $db file in '$summary_dir' (= enviroment variable EASYNCBISUMMARY)\n";

	if($db eq "ensembl"){
		$ftp->cwd("/pub/current/");

		$assembly_level = "";
		$release_type = "";
		$genome_rep = "";
		$version_status = "";

	}else{
		$ftp->cwd("/genomes/ASSEMBLY_REPORTS/");
	}

	my $unix_timestamp = 0;

	if($db eq "ensembl"){
		$unix_timestamp = 0+$ftp->mdtm("species.txt");
	}else{
		$unix_timestamp = 0+$ftp->mdtm("assembly_summary_$db.txt");
	}
	my $datetime = scalar localtime($unix_timestamp);
	if($datetime =~ /^[^ ]+ ([^ ]+) (\d+) [0-9:]+ (\d+)/){ $datetime="${1}_${2}_${3}" }

	if( !-e "$summary_dir/assembly_summary_${db}_$datetime.txt" ){
		my @summaryFiles = glob "'${summary_dir}/assembly_summary_${db}_*.txt'";
		if( scalar @summaryFiles != 0){
			print STDERR "there is allready a $db summary file present but the file is outdated, so I load a new version...\n";
			foreach my $sumfn (@summaryFiles){ unlink $sumfn }
		}
		print STDERR "downloading '$summary_dir/assembly_summary_${db}_$datetime.txt'\n";
		if($db eq "ensembl"){
			$ftp->get("species.txt","$summary_dir/assembly_summary_${db}_$datetime.tmp2");
			system('perl -lne \'$_=~s/ ?\(GC._[0-9]+\)//g; print $_\''." $summary_dir/assembly_summary_${db}_$datetime.tmp2 | sort -t '\t' -k1,1 -k7,7r > $summary_dir/assembly_summary_${db}_$datetime.tmp; rm $summary_dir/assembly_summary_${db}_$datetime.tmp2");
		}else{
			$ftp->get("assembly_summary_$db.txt","$summary_dir/assembly_summary_${db}_$datetime.tmp");
			system("sort -t '\t' -k8,8 -k10,13 -k15,15r $summary_dir/assembly_summary_${db}_$datetime.tmp -o $summary_dir/assembly_summary_${db}_$datetime.tmp");
		}
		rename "$summary_dir/assembly_summary_${db}_$datetime.tmp", "$summary_dir/assembly_summary_${db}_$datetime.txt";

	}else{
		print STDERR "there is allready a $db summary file '$summary_dir/assembly_summary_${db}_$datetime.txt' that is up to date!\n";
	}

	my %assembly_summary;
	my %assembly_summary_fallback;
	my @assembly_summary_keys;
	my @assembly_summary_fallback_keys;
	if($modus !~ / summary/){
		open(FH,"<$summary_dir/assembly_summary_${db}_$datetime.txt") or die "[ERROR]: $?";
		while(<FH>){ chomp;
			if(/^#/){next}
			if( $exclude ne "" && $_ =~ $exclude ){next}
			
			my @spl;

			if($db eq "ensembl"){

				@spl = ("") x 23;
				my @spl_ensembl = split("\t",$_);

				$spl[1]=$spl_ensembl[4]; # bioproject
				$spl[7]=$spl_ensembl[0]; # organism_name
				$spl[7]=~s/[\[\]]//g;
				$spl[7]=~s/_/ /g;
				$spl[14]=$spl_ensembl[6]; # seq_rel_date

				$spl[10]="";
				$spl[11]="";
				$spl[12]="";
				$spl[13]="";
				$spl[15]="";

				$spl_ensembl[2] =~s/Ensembl//g;
				$spl_ensembl[2] = lc $spl_ensembl[2];
				$spl_ensembl[13] =~ s/_core.*//g;
				$spl[19]="ftp://ftp.ensemblgenomes.org/pub/current/".$spl_ensembl[2]."/fasta/".($spl_ensembl[2] eq "bacteria" ? $spl_ensembl[13]."/" : "" ).$spl_ensembl[1];

				#print join("\t",@spl)."\n";

			}else{

				@spl = split("\t",$_);
			}

			my $id = $spl[7]."_".$spl[14]."_".$spl[1]."_".$spl[0]."_".$spl[15];
			$id =~ s/[ \/]/_/g;

			if( ($assembly_level ne "" && lc $spl[11] ne lc $assembly_level) || 
				($release_type ne "" && lc $spl[12] ne lc $release_type) || 
				($genome_rep ne "" && lc $spl[13] ne lc $genome_rep) || 
				($version_status ne "" && lc $spl[10] ne lc $version_status) ){

				$assembly_summary_fallback{$id} = join "#", @spl;
				push @assembly_summary_fallback_keys,$id;
			}else{
				$assembly_summary{$id} = join "#", @spl;
				push @assembly_summary_keys,$id;
			}

		}
		close(FH);
		if(scalar keys %assembly_summary == 0){
			print STDERR "[ERROR] no entries are matching the $db summary file from the defined filter (e.g. try to search contigs,... with -assembly_level='')\n";
			exit 1;
		}
		print STDERR "loaded ".(scalar keys %assembly_summary)." entries from the $db summary file 'assembly_summary_${db}_$datetime.txt' using the defined filter ".($reference ? "(only 'reference genomes' are used)":"")."\n";
		if($fallback){print STDERR "and loaded ".(scalar keys %assembly_summary_fallback)." entries from the $db summary file that do not match the filter\n";}
	}

	foreach my $cur_species (@species) {

		print STDERR ">$cur_species\n";
		$species2stat{$cur_species}=0;

		my $search = lc $cur_species; $search =~ s/ /.*/g; 
		my @entries = grep { (lc $_) =~ $search } @assembly_summary_keys;
		
		print STDERR " found ".(scalar @entries)." intial entries for '$cur_species'\n";

		if($reference){
			@entries = grep { $assembly_summary{$_} =~ /reference genome/i } @entries;

			print STDERR "  of which ".(scalar @entries)." are marked as 'reference genome'\n";

			if(scalar @entries == 0 && $fallback){ 
				@entries = grep { (lc $_) =~ $search } @assembly_summary_keys;
				if(scalar @entries > 0){
					print STDERR " but I found ".(scalar @entries)." without searching for reference genome (fallback)\n"; 
				}else{
					print STDERR "  still nothing found for '$cur_species' in the given database (even without searching for reference genome)\n";
				}
			}
		}

		if(scalar @entries == 0){

			if($fallback){ 
				@entries = grep { (lc $_) =~ $search } @assembly_summary_fallback_keys;

				if(scalar @entries > 0){
					print STDERR " but I found ".(scalar @entries)." without the prefilters (fallback)\n";
				}else{
					print STDERR "  still nothing found for '$cur_species' in the given database (even without the prefilters)\n";
				}
			}else{
				print STDERR " [ERROR] nothing found for '$cur_species' in the given database using given filters\n";
			}
		}else{
			print STDERR " processing '$cur_species' (found ".(scalar @entries)." total entries of the summary file)\n";
		}

		if($drier){print STDERR "".join("\n",@entries)."\n";next}

		foreach my $id (@entries) {

			if( ( exists $assembly_summary{ $id } && $assembly_summary{ $id } =~ /(ftp:\/\/ftp\.ncbi\.nlm\.nih\.gov|ftp:\/\/ftp\.ensemblgenomes\.org)([^ \t#]+)/) || 
			    ( exists $assembly_summary_fallback{ $id } && $assembly_summary_fallback{ $id } =~ /(ftp:\/\/ftp\.ncbi\.nlm\.nih\.gov|ftp:\/\/ftp\.ensemblgenomes\.org)([^ \t#]+)/) ){ 

				my @detail = split "#", (exists $assembly_summary{ $id } ? $assembly_summary{ $id } : $assembly_summary_fallback{ $id });

				my $ftp_path=$2;
				$ftp->cwd($ftp_path);

				my @files;
				if($db eq "ensembl"){
					@files = $ftp->ls("cdna/*gz");
					push @files, $ftp->ls("dna/*gz");
					push @files, $ftp->ls("ncrna/*gz");
					push @files, $ftp->ls("cds/*gz");
					push @files, $ftp->ls("pep/*gz");
				}else{
					@files = $ftp->ls();
				}

				my @download_files;
				if($db eq "ensembl"){
					if($modus =~ /transcriptome?/){
						@download_files = grep { $_ =~ /\bcdna\b.*\.fa/i } @files;
					}elsif($modus =~ /proteome?/){
						@download_files = grep { $_ =~ /\bpep\b.*\.fa/i } @files;
					}elsif($modus =~ /cdna/){
						@download_files = grep { $_ =~ /\bcdna\b.*\.fa/i } @files;
					}elsif($modus =~ /cds/){
						@download_files = grep { $_ =~ /\bcds\b.*\.fa/i } @files;
					}elsif($modus =~ /genomic/){
						die "ensembl + genomic is not supported"
					}elsif($modus =~ /from/){
						die "ensembl + from is not supported"
					}elsif($modus =~ /genome?/){
						@download_files = grep { $_ =~ /\bdna\b.*\.fa/i } @files;
					}elsif($modus =~ /gff/){
						die "ensembl + gff is not supported"
					}else{
						@download_files = @files;
					}
				}else{
					if($modus =~ /transcriptome?/){
						@download_files = grep { $_ =~ /_rna\.fna/ } @files;
					}elsif($modus =~ /proteome?/){
						@download_files = grep { $_ =~ /protein\.faa/ } @files;
					}elsif($modus =~ /cdna/){
						@download_files = grep { $_ =~ /cdna.*\.fna/i } @files;
					}elsif($modus =~ /cds/){
						@download_files = grep { $_ =~ /cds.*\.fna/i } @files;
					}elsif($modus =~ /genomic/){
						@download_files = grep { $_ =~ /(?<!from_)genomic\.(fna|gff)/ } @files;
					}elsif($modus =~ /from/){
						@download_files = grep { $_ =~ /_from_/ } @files;
					}elsif($modus =~ /genome?/){
						@download_files = grep { $_ =~ /(?<!from_)genomic\.fna/ } @files;
					}elsif($modus =~ /gff/){
						@download_files = grep { $_ =~ /genomic\.gff/ } @files;
					}else{
						@download_files = @files;
					}
				}
				
				# if(scalar @download_files == 0){ print STDERR "   nothing found for '$modus' for '$id' in '$ftp_path' ...\n" }

				my $downloaded_something=0;
				foreach my $download_file (@download_files) {

					my $bn_download_file = basename($download_file);

					my $GCF="";
					if($id =~ /(GCF.*)/){$GCF = $1}
					if($bn_download_file =~ $GCF){$id =~ s/_$GCF//g;}

					my $download_file_extracted = "$id.$bn_download_file";
					$download_file_extracted=~s/\.gz//g;

					if( -e "$id.$bn_download_file" || -e $download_file_extracted ){
						print STDERR "  file '$id.$bn_download_file' is allready present, skipping ...\n";
					}else{
						
						$species2stat{$cur_species}++;
						$downloaded_something=1;

						if($dry){
							print STDERR "  would download '$id.$bn_download_file' (assembly_level=$detail[11],release_type=$detail[12],genome_rep=$detail[13],version_status=$detail[10])...\n";
						}else{
							my $try_counter = 0;
							while(! -e "$id.$bn_download_file"){
								print STDERR "  downloading '$id.$bn_download_file' ...\n";
								$ftp->get($download_file,"$id.$bn_download_file.tmp");
								rename "$id.$bn_download_file.tmp", "$id.$bn_download_file";
								sleep( $timeout );
								if(++$try_counter > 3){
									print STDERR "[ERROR] I tried to download '$download_file' multiple times but something went wrong...\n";
									die;
								}
							}
						}

						if($first){last}
					}
				}
				if($first && $downloaded_something){last}
				if(!$dry && $downloaded_something){sleep( 5*$timeout )}
			}else{
				print STDERR " [ERROR] no ftp path found in the assembly_summary for '$id', seems like the file got corrupted ! Please delete the assembly_summary and try again\n";
				die;
			}
		}
	}

	$ftp->quit;

}else{
	print STDERR "[ERROR] invalid modus\n";
	exit 1;
}

my $total=0;
print STDERR "# query\tdownload statistic\n";
foreach my $cur_species (@species) {
	print STDERR "$cur_species: $species2stat{$cur_species}\n";
	$total+=$species2stat{$cur_species};
}

if(!$first && $total > scalar @species){ print STDERR "# try -first to only download the first match or -reference to filter results\n" }
if(!$relaxed && !$fallback && $total < scalar @species){ print STDERR "# try -relaxed (almost no filter) or -fallback (first with filter then retry without) to search for non-optimal assemblies\n" }
