#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
my $version="0.0.4"; # last updated on 2021-10-02 23:08:46

my $usage = <<ENDUSAGE;
splitgff.pl (options) <input.gff>        splits the input.gff into chromosom and strand specific gff files (the files are created in the cwd)

OPTIONS

	-nostrand : disables the splitting by strand
	-nochromosome : disables the splitting by chromosome

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long, perl : File::Basename
ENDUSAGE

my $nostrand=0;
my $nochromosome=0;
my $help;

GetOptions('help!'=>\$help,
	'h!'=>\$help, 
	"nochromosome!"=>\$nochromosome,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	"nostrand!"=>\$nostrand);

if ($help || scalar(@ARGV) != 1 ){
    print $usage;
    exit(1);
}


open(my $GFF_FH,$ARGV[0]) || die($!);
while (<$GFF_FH>) {
	$_=~s/[\n\r]+$//g; # windows prove chomp
	if(length $_ < 1 || substr($_,0,1) eq "#"){next;}
	my ($sequence, $source, $feature, $start, $end, $score, $strand, $phase, $attributes) = split;
	# sequence : sequence name (chromosome)
	# source : program name
	# feature : gene/exon/CDS/...
	# start, end: 1based 
	# score : .=NA
	# strand : +,-
	# phase : for CDS can be 0,1,2 for the reading frame offset 
	# attributes : ID,Parent : Parent=geneXYZ ID=CDSXYZ1 

	if($strand eq "+"){$strand="plus"}
	if($strand eq "-"){$strand="minus"}
	if($strand eq "."){$strand="NA"}

	my $ret="echo '$_' >>".(basename $ARGV[0]).".";
	if(!$nochromosome){$ret.="chr_$sequence."}
	if(!$nostrand){$ret.="strand_$strand."}
	$ret.=".gff";
	
	system($ret);
}
close($GFF_FH);
