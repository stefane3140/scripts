#!/usr/bin/env perl
# pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line

my $version="0.0.6"; # last updated on 2021-10-02 23:08:45
my $usage = <<ENDUSAGE;
grep_k_neighbourhood.pl        finds k upper and lower proteins
 
SYNOPSIS
 
grep_k_neighbourhood.pl (OPTIONS) INFILE

OPTIONS

	INFILE : a gff file or - for STDIN

	mandatory:
		-k=	 the number of matches that are reported (up and down)
		-q=	 the query string

	optional:
		-prefix= 	
			the regularexpression matching the query string 
			(e.g. for gff-files: gene=([^:]+))
		-preselection=
			regular expression for for identifing rows
			(e.g. for genes in a gff-files: \tgene\t)
		-full         
			if set then the full lines are printed instead of the raw IDs

DESCRIPTION
 
	The gff is assumed to be sorted.
       
	Example:
 
	perl grep_k_neighbourhood.pl -k=3 -q='POP1' gffs/saccharomyces_cerevisiae.gff3

	STDOUT:
		SQS1
		ATG4
		SSU72
		POP1 <--------
		ADE12
		ALG9
		MGS1
 
	If the gff is not sorted, use sort first:

	LC_ALL=C sort -k1,1 -k4,4n -t\$'\\t' Lactobacillus_sen.gff | grep_k_neighbourhood.pl -preselection=".*" -prefix="ID=([^;]+)" -q=6SRNA -k=1 -

	STDOUT:
		cds549
		6SRNA <--------
		cds444

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long
ENDUSAGE

our $printFull;
our $help;
our $k=5;
our $identifier_prefix="gene=([^;]+)";
our $query="";
our $rowpreselector="\tgene\t";

GetOptions('help!'=>\$help,
	'k=i'=>\$k,
	'q=s'=>\$query,
	'full'=>\$printFull,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	'prefix=s'=>\$identifier_prefix,
	'preselection=s'=>\$rowpreselector
);
 
if ($help || scalar(@ARGV) == 0){
    print $usage;
    exit(1);
}

use File::Basename;

my $restart_bit=0; # set bits if this restart is allready conducted
RESTART:

#2. the gff file (genome)

our $foundit=0;
our @qdata;
our $numOfPreselector=0;
our $numOfPrefix=0;

sub doWork{
	if($_ =~ $rowpreselector){
		$numOfPreselector++;
		if($_ =~ $identifier_prefix){
			$numOfPrefix++;
			my $cur_gene_name = $1;
			push(@qdata, $printFull ? $_ : $cur_gene_name);
			if($foundit==0 && scalar @qdata > $k+1){
				shift @qdata;
			}elsif($foundit==1 && scalar @qdata > 2*$k){
				return(1);
			}
			if($cur_gene_name eq $query){
				$foundit=1;
			}
		}
	}
	return(0)
}

if(scalar @ARGV > 0 && $ARGV[0] ne "-"){
	open(FH,'<',$ARGV[0]) || die($!);
		while(<FH>){
			chomp;
			if(doWork($_)){last};
		}
	close(FH);
}else{
	#print STDERR "reading from STDIN\n";
	$restart_bit = ($restart_bit | 0x1 | 0x2);
	while(<>){
		chomp;
		if(doWork($_)){last};
	}
}

if(scalar @qdata == 0 || $foundit == 0){
	if($numOfPreselector == 0){
		print STDERR "WARNING : no row found using the given preselector ($rowpreselector), use the -preselect option to adjust this behaviour.\n";
		
		if( ($restart_bit & 0x1) == 0 ){ # test if this restart was done before
			$restart_bit = ($restart_bit | 0x1);

			print STDERR "WARNING : restarting with relaxed condtions (-preselect='')\n";
			$rowpreselector="";
			goto RESTART;
		}
	}elsif($numOfPrefix == 0){	
		print STDERR "WARNING : found rows matching the preselect option ($numOfPreselector) but no row match the prefix ($identifier_prefix), use the -prefix option to adjust this behaviour.\n";
		
		if( ($restart_bit & 0x2) == 0 ){ # test if this restart was done before
			$restart_bit = ($restart_bit | 0x2); 

			print STDERR "WARNING : restarting with relaxed condtions (-prefix='')\n";
			$identifier_prefix="";
			goto RESTART;
		}
	}else{	
		print STDERR "WARNING : found rows matching the preselect option ($numOfPreselector) and also the prefix but no row match the query ($query), use the -q option to adjust this behaviour.\n"
	}
}

print join("\n",@qdata)."\n";
