#!/usr/bin/env perl
#pk 

use strict;
use warnings "all";

my $version="0.0.3"; # last updated on 2021-10-01 14:11:18

if(scalar(@ARGV)!=1 || $ARGV[0] eq "-h" || $ARGV[0] eq "-help" || $ARGV[0] eq "--help"){
	print "USAGE: splitfasta.pl <FASTAFILE>
Returns for each '>' entry of the fasta a single file inside a created *_fastaFiles/ directory (* = input fasta name)

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES -
";
	exit 0;
}

if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }

my $gen="";
my $genname="";
my $tic=0;
my $genname_original;

my $basefilename=$ARGV[0];
$basefilename=~s/^.*\/([^\/]+)$/$1/g;
$basefilename=~s/[^a-zA-Z0-9_.]/_/g;
system("mkdir $basefilename"."_fastaFiles 2>&1 >/dev/null");
while(<>){
	#chomp;
	if(substr($_,0,1)eq">"){
		if($tic==1){
			$genname_original=$genname;
			$genname=~s/[>]//g;
			$genname=~s/[^a-zA-Z0-9_.]/_/g;
			$gen=~s/[^A-Za-z().\n]//g;
			open(FH,">","$basefilename"."_fastaFiles/".$genname.".fasta");
			print FH $genname_original.($gen);
			close FH;
			$tic=0;
		}
		$tic=1;
		$genname=$_;
		$gen="";
	}elsif($tic==1){
		$gen.=$_;
	}
}
if($tic==1){
	$genname_original=$genname;
	$genname=~s/[>]//g;
	$genname=~s/[^a-zA-Z0-9_.]/_/g;
	$gen=~s/[^A-Za-z().\n]//g;
	open(FH,">","$basefilename"."_fastaFiles/".$genname.".fasta");
	print FH $genname_original.($gen);
	close FH;
	$tic=0;
	$gen="";
}

