#!/usr/bin/env perl
# pk

use Bio::TreeIO;
use Getopt::Long; # for parameter specification on the command line
my $version="0.0.3"; # last updated on 2021-10-01 14:11:18

my $usage = <<ENDUSAGE;
newickToAverageDistance.pl        calculates the average distance of a phylogenetic tree in newick format 
 
SYNOPSIS
 
newickToAverageDistance.pl (--printAllDistances) FILES
 
    FILES      files in newick format (distances are needed) e.g. ((A:1,B:1)99:0.5,C:0.5)100:100;
    --printAllDistances optional flag: prints all pairwise non-redundant distances.
 
DESCRIPTION
 
    This script calculates the average distance and a list of all pairwise distances of a phylogenetic tree in newick format and prints
    the results to STDOUT.
       
  Example:
 
    newickToAverageDistance.pl test/*.nwk

    STDOUT:
	    test/avg1  1
	    test/avg2  2
	    test/avg42 42

    newickToAverageDistance.pl --printAllDistances test/*.nwk

    STDOUT:
	    test/avg1  1       1
	    test/avg2  2       2;2;2
	    test/avg42 42      30;36;60

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Bio::TreeIO
	perl : Getopt::Long

ENDUSAGE

my $help,$printAllDistances=0;
GetOptions('help!'=>\$help,
			'h!'=>\$help,
			'version!'=>sub { print "$0\tv$version\n";exit 0; },
			'v!'=>sub { print "$0\tv$version\n";exit 0; },
		   'printAllDistances'=>\$printAllDistances);
 
if ($help || scalar(@ARGV) == 0){
    print $usage;
    exit(1);
}


for($i = 0 ; $i < scalar(@ARGV) ; $i++){

	my $treeio = Bio::TreeIO->new(-format => 'newick', -file => $ARGV[$i]);
	my $tree = $treeio->next_tree;
	my @nodes = $tree->get_leaf_nodes;
	my @distances;

	my $avg=0;
	for($v_i = 0 ; $v_i < scalar(@nodes) ; $v_i++){
		for($v_j = $v_i+1 ; $v_j < scalar(@nodes) ; $v_j++){
			my $d = $tree->distance(-nodes => [$nodes[$v_i], $nodes[$v_j]]);
			$avg += $d;
			push(@distances,$d);
		}
	}
	$avg /= (scalar(@nodes)*(scalar(@nodes)-1))/2;
	print($ARGV[$i]."\t".$avg."\t");
	if($printAllDistances){
		print(join(";",@distances));
	}
	print("\n");

}
