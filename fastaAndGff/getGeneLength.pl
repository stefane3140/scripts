#!/usr/bin/env perl
#pk 

use strict;
use warnings "all";

my $version="0.0.11"; # last updated on 2021-10-01 14:11:18

if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }
if(scalar(@ARGV)==0 || $ARGV[0] eq "-h" || $ARGV[0] eq "-help" || $ARGV[0] eq "--help"){
	print "USAGE: getGeneLength.pl <FASTAFILE> Returns for each '>' entry of the fasta file: 'name\\tlength(gene)'

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES -
";
	exit 1;
}

my $delim="\t";
if(scalar @ARGV == 2){$delim=$ARGV[1]}

my $gen_len=0;
my $genname="";
my $tic=0;
while(<>){
	$_=~s/[\n\r]+$//g;
	if(substr($_,0,1) eq ">"){
		if($tic==1){
			print $genname.$delim.$gen_len."\n";
			$tic=0;
		}
		$tic=1;
		$genname=$_;
		$gen_len=0;
	}elsif($tic==1){
		$gen_len+=length($_);
	}
}
if($tic==1){
	print $genname.$delim.$gen_len."\n";
}
