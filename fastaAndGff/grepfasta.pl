#!/usr/bin/env perl
# pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()

my $version="0.0.39"; # last updated on 2021-10-02 23:08:45

my $usage = <<ENDUSAGE;
grepfasta.pl        greps all genes/proteins of a given fasta file
 
SYNOPSIS
 
grepfasta.pl (options) QUERY INFILE1 (INFILE2 ...)

	QUERY	identifier FILE or search query STRING:
		a)	string of one identifier e.g. 'tr|asd3|asd' OR multiple identifier separated by ',' OR '-' for STDIN
		b)	(-file QUERY) file with the ids. The file contains either just 
			identifier or tabseparated a mapping:
			e.g. POP1 to the 3 identifier 
			tr|asd|asd, tr|asd2|asd and tr|asd3|asd.
	INFILE	file containing the query ids (database)

	optional:
		-tofiles, -t 	print everything to files instead of files 
		-prefix=s 	prefix of each identifier (is regex interpreted)
				[default: -prefix=">"]
		-E    	enables regex matching otherwise the string is escaped (e.g. | -> \|)
		-source, -s    	adds the filename to the found gene-name
		-F=s 	char delimiter for multiple identifier (default: ',')
		-tag=s 	search by fasta tag, e.g. -tag='GN' will search for ... GN=PRPSAP2 ...
		-seq    search in the sequence space instead of in the header

DESCRIPTION
 
	This script finds all ids of a list of fasta files with identifier
	provided in a different file and saves the output to seperate files named
	ID_FASTA where ID is the found identifier and FASTA the 
	name of the fasta file that contains this identifier.
	STDERR contains ids and the number of times they are found.
       
EXAMPLES
 
 	# 1. most simple call:

	perl grepfasta.pl 'tr|asd|asd' *.faa

		STDOUT:
			>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
			MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG

 	# 2. regex search:

	perl grepfasta.pl -E '.*a?sd[0-3].*' *.faa

 	# 3. multiple ids:

	perl grepfasta.pl 'tr|asd|asd,tr|asd2|asd' *.faa

 	# 4. id file and write output to files:

	cat test.idlist | grepfasta.pl - test.faa

		-----test.idlist:---------- (does not need the first column)
		POP1	tr|asd|asd
		POP1	tr|asd1|asd
		POP1	tr|asd2|asd
		POP2	tr|asd3|asd
		
 	# 5. id file and write output to files:

	grepfasta.pl -tofiles test.idlist test.faa

		-----test.idlist:---------- (does not need the first column)
		POP1	tr|asd|asd
		POP1	tr|asd1|asd
		POP1	tr|asd2|asd
		POP2	tr|asd3|asd
		
		------test.faa:------------
		>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
		MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG
		>tr|asd3|asd Histone H4 OS=Acremonium chrysogenum (strain ATCC 11550 (...)
		MTGRGKGGKGLGKGGAKRHRKILKDNIQGITKPAIRRLARRGGVKRISAMIYEETRGVLK
		>tr|A0A086SUI7|A0A086SUI7_ACRC1 Uncharacterized protein OS=Acremonium(...)
		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN
		>tr|asd2|asd 1-phosphatidylinositol 3-phosphate 5-(...)
		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN

		OUTPUT (2 files)

		------POP1_test:------------
		>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
		MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG
		>tr|asd2|asd 1-phosphatidylinositol 3-phosphate 5-(...)
		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN
		
		------POP2_test:------------
		>tr|asd3|asd Histone H4 OS=Acremonium chrysogenum (strain ATCC 11550 (...)
		MTGRGKGGKGLGKGGAKRHRKILKDNIQGITKPAIRRLARRGGVKRISAMIYEETRGVLK

		STDERR: 	
		tr|asd|asd	1
		tr|asd1|asd	1
		tr|asd2|asd	0
		tr|asd3|asd	1

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long, perl : File::Basename
ENDUSAGE

my $query;
my $help;
my $tofiles=0;
my $justid;
my $prefix=">";
my $tag="";
my $doregex=0;
my $source=0;
my $del=',';
my $stat=0;
my $seq=0;
GetOptions('help!'=>\$help,'h!'=>\$help,
	'tofiles!'=>\$tofiles,
	't!'=>\$tofiles,
	'prefix=s'=>\$prefix,
	'source'=>\$source,
	'tag=s'=>\$tag,
	'stat'=>\$stat,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	's'=>\$source,
	'seq'=>\$seq,
	'F=s'=>\$del,
	'E'=>\$doregex);

$query = $ARGV[0];
shift(@ARGV);

if($tag ne ""){$tag="$tag=([^ ]+)"}

if ($help || scalar(@ARGV) == 0 || !$query ){
    print $usage;
    exit(1);
}

my %qdata;
my %qdata_count;

unless(open(FH,'<',$query)) {
	if($query eq "-"){
		foreach my $line (<STDIN>) 
		{
			chomp($line);
			if($line eq ""){next}
			$qdata{$line}=$line;
			$qdata_count{$line}=0;
		}
	}else{
		my @sp = split($del,$query);
		for(my $v = 0 ; $v < scalar @sp ; $v++){
			chomp($sp[$v]);
			if($sp[$v] eq ""){next}
			$qdata{$sp[$v]}=$sp[$v];
			$qdata_count{$sp[$v]}=0;
		}
	}
}else{
	while(<FH>){
		chomp($_);
		my @sp = split("\t",$_);
		if(scalar(@sp)==2){
			$qdata{$sp[1]}=$sp[0];
			$qdata_count{$sp[1]}=0;
		}elsif(scalar(@sp)==1 && $_ ne ""){
			$qdata{$sp[0]}=basename($query);
			$qdata_count{$sp[0]}=0;
		}
	}
	close(FH);
}

for(my $v = 0 ; $v < scalar @ARGV ; $v++){
	open(FH,'<',$ARGV[$v]) || die "Can't open file: $!";
	my $geneprintswitch = "";

	my $cur_header="";
	my $cur_seq="";

	while(<FH>){
		
		chomp($_);
		if($_ eq ""){ next }

		if(substr($_,0,1) eq $prefix ){
			$cur_header = $_;
			$cur_seq = "";

			$geneprintswitch = "";
			$cur_header=substr($cur_header,1); # remove >

			if($seq){next}

			foreach my $key (keys %qdata) { 
				my $regexv = $key;
				my $cleankey = $key; $cleankey =~s/^["'>]+//g; $cleankey =~s/["']$//g; 
				my $nodescr = $cur_header; $nodescr =~s/[ \t]+.*$//g;

				my $found_tag="";
				if($tag ne "" && $cur_header =~ $tag){ $found_tag = $1 }

				if( ($tag ne "" && $found_tag ne "" && 
						(( $doregex && $found_tag =~ $regexv) || $found_tag eq $key )
					) ||
					( $tag eq "" && 
						(
							($doregex && $cur_header =~ $regexv) || 
							$cur_header eq $key || 
				 			(!$doregex && $cur_header eq $cleankey) || 
				 			(!$doregex && $nodescr eq $key)|| 
				 			(!$doregex && $nodescr eq $cleankey)
				 		)
					)
				){

					$geneprintswitch = $qdata{$key};
					$qdata_count{$key}=$qdata_count{$key}+1;
					if($tofiles){
						if($qdata_count{$key} == 1){
							system("echo '$cur_header' >".$geneprintswitch."_".basename($ARGV[$v]).".fasta");
						}else{
							system("echo '$cur_header' >>".$geneprintswitch."_".basename($ARGV[$v]).".fasta");
						}
					}else{
						if($source){
							print ">$cur_header ".basename($ARGV[$v])."\n";
						}else{
							print ">$cur_header\n";
						}
					}
				}
			}
		}else{
			
			if($geneprintswitch ne ""){
				if($tofiles){
					system("echo '$_' >>".$geneprintswitch."_".basename($ARGV[$v]).".fasta");
				}else{
					print "$_\n";	
				}
			}else{
				if($geneprintswitch eq ""){
					$cur_seq.=$_;
				}
			}
			if($seq){
				foreach my $key (keys %qdata) { 
					if($cur_seq =~ $key){
						$geneprintswitch = $qdata{$key};
						$qdata_count{$key}=$qdata_count{$key}+1;
						if($tofiles){
							if($qdata_count{$key} == 1){
								system("echo '$cur_header\n$cur_seq' >".$geneprintswitch."_".basename($ARGV[$v]).".fasta");
							}else{
								system("echo '$cur_header\n$cur_seq' >>".$geneprintswitch."_".basename($ARGV[$v]).".fasta");
							}
						}else{
							print ">$cur_header\n$cur_seq\n";
						}
					}
				}
			}
		}
	}
	close(FH);
}

if($stat == 0 && scalar keys %qdata_count > 3 ){
	my $foundNum = 0;
	my $foundMultiNum = 0;
	foreach my $key (keys %qdata_count) { 
		if($qdata_count{$key} == 1){$foundNum++}
		elsif($qdata_count{$key} > 1){$foundMultiNum++}
	}
	print STDERR "I found ".$foundNum." (".( int(10000*($foundNum/scalar keys %qdata_count))/100 )."%) uniquely and ".$foundMultiNum." (".( int(10000*($foundMultiNum/scalar keys %qdata_count))/100 )."%) multiple times out of the given ".(scalar keys %qdata_count)." input queries (use --stats to recive a full table of all entries)\n";
}else{
	print STDERR "# key\toccurences\n";
	foreach my $key (keys %qdata_count) { 
		print STDERR $key."\t".$qdata_count{$key}."\n";
	}
}
