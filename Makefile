SRC_FILES_PL := $(wildcard *.pl) $(wildcard */*.pl)
SRC_FILES_SH := $(wildcard *.sh) $(wildcard */*.sh)
SRC_FILES_R := $(wildcard *.R) $(wildcard */*.R)
SRC_FILES_PY := $(wildcard *.py) $(wildcard */*.py)
SRC_CPP := $(wildcard *.cpp) $(wildcard */*.cpp)
OBJ_FILES := $(patsubst %.cpp,%,$(SRC_CPP))

# ALIAS for PREFIX
INSTALLDIR=/usr/local/bin

ifdef PREFIX
INSTALLDIR=$(PREFIX)
endif

$(OBJ_FILES): $(SRC_CPP)
	g++ -fopenmp -o $(INSTALLDIR)/$$(basename $@) $<

all: $(SRC_FILES_PL) $(SRC_FILES_SH) $(SRC_FILES_R) $(SRC_FILES_PY)
	@echo "INSTALLING everything to $(INSTALLDIR) (please do this on jupiter)"
	install -v $^ $(INSTALLDIR);

install: $(SRC_FILES_PL) $(SRC_FILES_SH) $(SRC_FILES_R) $(SRC_FILES_PY)
	@echo "INSTALLING everything to $(INSTALLDIR) (please do this on jupiter)"
	install -v $^ $(INSTALLDIR);

uninstall: $(SRC_FILES_PL) $(SRC_FILES_SH) $(SRC_FILES_R) $(SRC_FILES_PY)
	@for f in $^; do echo "rm "$(INSTALLDIR)"/"$$(basename $${f})"; "; done
	@echo "# please run 'make uninstall | sh' to execute the commands of above!";
