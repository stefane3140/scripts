#!/usr/bin/env perl
# pk

use strict;
use warnings "all";

# RUN : perl pairwiseAverageNW.pl data/*/*_clean >>results.txt

# computes all pairwise averaged needlemanwunsch global alignment score of all genes versus all genes
# e.g. 2 files (A,B) with 3 genes each (A1,A2,A3,B1,...) 
# -> score for A vs B = mean(needlemanwunsch(A1,B1),needlemanwunsch(A1,B2),...,needlemanwunsch(A3,B3)) 
# scores are length normed (/max length of the two sequences)

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
my $version="0.0.18"; # last updated on 2021-10-20 11:36:19


my $usage = <<ENDUSAGE;
pairwiseAverageNW.pl        performs an all versus all pairwise averaged length-normalized needlemanwunsch
 
SYNOPSIS
 
pairwiseAverageNW.pl (-help|-h) (-noselfhit) (-notlengthnormed) (-verbose) FASTA1 (FASTA2 ...)

	FASTA	Fasta file containing genes/proteins
		if one file is given, all sequences are compared
		if multiple files are given, the average score is calculated between the files

	optional:
		-noself	do not compare the same file/sequence
		-nonorm	do not normalizing the scores to the maximal length
		-verbose	displaying the progress (STDERR)
		-all	print the list of all scores instead of the mean (STDERR)
		-wide   outputs a matrix format instead of a long list of pairs
		-help	displays this.

DESCRIPTION 

	This script computes all pairwise averaged (normalized) needlemanwunsch 
	global alignment score of all genes/proteins versus all genes/proteins of
	a set of fasta files. 
    e.g. 2 files (A,B) with 2 genes each (A1,A2,B1,B2) 
	-> score for A vs B = mean(needlemanwunsch(A1,B1),needlemanwunsch(A1,B2),needlemanwunsch(A1,B2),needlemanwunsch(A2,B1),needlemanwunsch(A2,B2)) 
 	scores are length normed (/max length of the two sequences)

 	Careful: This will fail if there are many input sequences (curse of dimensionality), many sequences are on average always similar

EXAMPLES
 
	perl pairwiseAverageNW.pl Test1.faa Test2.faa
	
	with
		------Test1.faa:------------
		>G1_1
		ABC
		>G1_2
		AB

		------Test2.faa:------------
		>G2_1
		AB
		>G2_2
		AA

	STDOUT:
		Test1.faa       Test1.faa       0.7778
		Test1.faa       Test2.faa       0.25
		Test2.faa       Test2.faa       0.6667

	perl pairwiseAverageNW.pl Test1.faa --> comparing the sequences of Test1.faa against each other

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES Getopt::Long, File::Basename, easyalignment.py (https://gitlab.com/paulklemm_PHD/scripts)
ENDUSAGE

my $help;
my $noselfhit;
my $notlengthnormed;
my $verbose;
my $wide=0;
my $printAllScores;

GetOptions('help!'=>\$help,
	'noself!'=>\$noselfhit,
	'noselfhit!'=>\$noselfhit,
	'nonorm!'=>\$notlengthnormed,
	'notlengthnormed!'=>\$notlengthnormed,
	'all!'=>\$printAllScores,
	'printAllScores!'=>\$printAllScores,
	'wide!'=>\$wide,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	'verbose!'=>\$verbose);
 
if ($help || scalar(@ARGV) < 1){
    print $usage;
    exit(1);
}

my $seed = rand();

my @files;
my @headers;

for(my $v = 0 ; $v < scalar @ARGV ; $v++){

	open(FH,'<',$ARGV[$v]);

	my $cur_file="";

	while(<FH>){
		chomp;
		if(length $_ < 1 or $_ eq ""){next;}
		if(substr($_,0,1) ne ">"){
			$cur_file.=$_;
		}else{
			$_=~s/^>//g;
			push(@headers,$_);
			if($cur_file ne ""){
				$cur_file.="\n";
			}
		}
	}

	# $cur_file.="\n";
	push(@files,$cur_file);

	close(FH);
}

if(scalar @files == 1){
	@files = split(/\n/,$files[0]);
}else{
	@headers = @ARGV;
}

# each entry in @files corresponds to all genes of one file. 
# In particular one entry contains linewise all genes without the >header or a 80 characters line restriction.
# next for each pair of files, an all-versus-all pairwise needlemanwunsch is performed -> averaging the scores

my @wide_mat;
if($wide){
	@wide_mat=( () x scalar @files );
	for (my $i = 0; $i < scalar @files; $i++) {
		my @tmp = (0) x scalar @files;
		$wide_mat[$i] = \@tmp;
	}
}

for(my $file_i = 0 ; $file_i < scalar @files ; $file_i++){
	for(my $file_j = $file_i ; $file_j < scalar @files ; $file_j++){

		if($noselfhit and $file_i == $file_j){next;}

		my @lines_i = split(/\n/,$files[$file_i]); # each gene is now one entry in @lines_i/j
		my @lines_j = split(/\n/,$files[$file_j]);

		my $sum=0; # sum of the needlemanwunsch scores
		my $n=0;
		for(my $i = 0 ; $i < (scalar @lines_i) ; $i++){
			if(length $lines_i[$i] < 1){next;}
			for(my $j = 0 ; $j < (scalar @lines_j) ; $j++){
				if(length $lines_j[$j] < 1){next;}
				if($file_i == $file_j && $j <= $i){next;} # if selfhit : file_i = (A,B,C), i=j, then compare only A-B,A-C,B-C (not e.g. A-A,B-A,..)

				if($verbose){ print STDERR $lines_i[$i]." vs ".$lines_j[$j]." \n"; }

				my $max=0;
				if(length($lines_i[$i])>length($lines_j[$j])){
					$max=length($lines_i[$i]);
				}else{
					$max=length($lines_j[$j]);
				}
				my $out=`easyalignment.py -e 0 '$lines_i[$i]' '$lines_j[$j]' 2>/dev/null`; # performing the needlemanwunsch with the tmp file
				my $score=0;
				if($out=~/score: ?([+\-0-9]+)/ig){$score=int($1)}
				
				if($verbose){ print STDERR $out; }
				if($verbose){ print STDERR " = ".$score." (".($score/$max).") \n"; }
				
				if(!$notlengthnormed){$score=$score/$max;}

				if($printAllScores){
					print $headers[$file_i]."\t".$headers[$file_j]."\t".(int(10000*$score)/10000)."\n"; # print the average result for the two files
				}
				$sum+=$score;
				$n++;
			}
		}
		if($n==0){next}
		if($wide){
			#print STDERR ""
			$wide_mat[$file_i][$file_j]=int(10000*$sum/$n)/10000;
			$wide_mat[$file_j][$file_i]=int(10000*$sum/$n)/10000;
		}else{	
			if(!$printAllScores){
				print $headers[$file_i]."\t".$headers[$file_j]."\t".(int(10000*$sum/$n)/10000)."\n"; # print the average result for the two files
			}
		}
	}
}
if($wide){
	for (my $i = 0; $i < scalar @files; $i++) {
		if ($i==0) {
			print join("\t",@headers)."\n";
		}
		print "$headers[$i]\t".join("\t",@{$wide_mat[$i]})."\n";
	}
}
#system('rm tmp_nw'.$seed);
