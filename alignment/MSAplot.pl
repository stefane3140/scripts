#!/usr/bin/env perl
# pk

use strict;
use warnings "all";
# RUN : perl MSAplot.pl (options) somealn.aln | aha

# given an alignment in aln format, this script plots the alignment !

our $RED="\033[0;41m";
our $PURPLE="\033[0;45m";
our $YELLOW="\033[0;43m";
our $ORANGE="\033[1;33m";
our $BOLD="\033[1m";
our @COLORPAL = ("\033[7;32m","\033[7;31m","\033[7;33m","\033[2;1m") ;
our $NC="\033[0m"; # No Color
my $version="0.1.21"; # last updated on 2021-10-27 12:50:05

my $usage = "MSAplot.pl        produces a colored representation of a given MSA
 
SYNOPSIS
 
MSAplot.pl (options) ALIGNMENTFILE 

	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

		e.g. CLUSTAL W formated input:
		...
		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
		...

OUTPUT
	Sequence identity color code: ".$COLORPAL[0]."100%".$NC.", ".$COLORPAL[1].">90%".$NC.", ".$COLORPAL[2].">80%".$NC.", ".$COLORPAL[3].">50%".$NC.".
	You can use 'aha' to convert the output to html (conda install aha)

VERSION v$version
AUTHOR Paul Klemm
";

if(scalar @ARGV == 0){print $usage; exit 0;}
if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }

if($ARGV[0] eq "-help" || $ARGV[0] eq "-h" || $ARGV[0] eq "--help" ){print $usage; exit(0);}

my $maxnumberofgaps=0.5;
my $minDominantChar=0;
my $minrowid=0;
my $group="";

# hidden options :
my $alignNrdbReadsPlot=0;
if(exists $ENV{'alignNrdbReadsPlot'} && $ENV{'alignNrdbReadsPlot'} eq "1"){$alignNrdbReadsPlot=1}
my $alignNrdbReadsPlotName="";
if(exists $ENV{'alignNrdbReadsPlotName'}){$alignNrdbReadsPlotName=$ENV{'alignNrdbReadsPlotName'}}
my $alignNrdbReadsPlotGroup="";
if(exists $ENV{'alignNrdbReadsPlotGroup'}){$alignNrdbReadsPlotGroup=$ENV{'alignNrdbReadsPlotGroup'}}

my $printINFO=0;

for(my $argv_i = 0 ; $argv_i < scalar @ARGV ; $argv_i++){
	if(-e $ARGV[$argv_i] || $ARGV[$argv_i] eq "-"){next;}
	print $usage."\n";
	print STDERR "[STDERR] ERROR : I dont understand the argument '".$ARGV[$argv_i]."' ! Aborting...\n";exit(1);
}

sub padRight{
	my $s = shift;
	my $l = shift;
	return $s . (" " x ($l-length($s)));
}

if($alignNrdbReadsPlotGroup ne ""){print "<a href='#' id='expAll' class='exp'>[expand/collapse all]</a> "}

our %data;
our $cur_geneName="";
our @rownames;
our $cur_header="";

sub processLine{
	$_ = shift;
	chomp($_);
	my $entry=$_;
	$entry=~s/^[ \t]+//g;
	$entry=~s/[ \t]+\d+$//g;
	my @arr=split(/[ \t][ \t]+/,$entry);
	if( scalar(@arr) == 2 ){ # input is clustal format
		$arr[0]=~s/\t/ /g;
		if(!exists($data{$arr[0]})){$data{$arr[0]}="";push(@rownames,$arr[0]);}
		$data{$arr[0]}.=$arr[1];
	}else{ # input is fasta format
		if($entry=~m/>([^ ]+)/){
			$cur_header=$1;
			$cur_header=~s/\t/ /g;
			$cur_header=~s/ .*//g;
		}elsif($cur_header ne "" && $entry=~m/^[^ ]+$/){
			if(!exists($data{$cur_header})){$data{$cur_header}="";push(@rownames,$cur_header);}
			$data{$cur_header}.=$entry;
		}
	}
}


for(my $argv_i = 0 ; $argv_i < scalar @ARGV ; $argv_i++){

	if(($ARGV[$argv_i] ne "-" && !-e $ARGV[$argv_i]) || $ARGV[$argv_i] =~ /^-[^-]+/){next}

	my $argv_name=$ARGV[$argv_i];

	%data  = ();
	$cur_geneName="";
	$cur_header="";
	

	if($argv_name eq "-"){
		while(<>){ processLine($_) }
	}else{
			
		open (FH,'<',$argv_name);
		while(<FH>){ processLine($_) }
		close(FH);
	}

	#2. compute the score

	if($minDominantChar==-1){$minDominantChar=scalar @rownames;}
	if($maxnumberofgaps==-1){$maxnumberofgaps=scalar @rownames;}

	my @colinfo;
	my @rowinfo;
	my $longestRowname=0;
	my @colisvalid;

	for (my $i=0 ; $i<scalar @rownames ; $i++){

		if(length $rownames[$i] > $longestRowname){$longestRowname=length $rownames[$i]}

		if(length $rownames[$i] < 1 or length $data{$rownames[$i]} < 1 ){next;}
		for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){

			if(!exists($colinfo[$k])){$colinfo[$k] = {};}
			if(!exists($colinfo[$k]{substr($data{$rownames[$i]},$k,1)})){$colinfo[$k]{substr($data{$rownames[$i]},$k,1)} = 0;}

			$colinfo[$k]{substr($data{$rownames[$i]},$k,1)}++;
		}

		if(!exists($rowinfo[$i])){$rowinfo[$i]=1;}
		if($minrowid == 0){next;}
		
		my $cur_row_id=0;
		my $unalignedA = () = $data{$rownames[$i]} =~ /[^-]/g;

		for (my $j=0 ; $j<scalar @rownames ; $j++){
			if($i==$j or length $rownames[$j] < 1 or length $data{$rownames[$j]} < 1 ){next;}
			
			my $cur_score_api = 0; 
			for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){
				if(substr($data{$rownames[$i]},$k,1) eq substr($data{$rownames[$j]},$k,1) && substr($data{$rownames[$i]},$k,1) ne "-" && substr($data{$rownames[$j]},$k,1) ne "-" ){
					$cur_score_api++;
				}
			}

			my $minLengthWithoutGaps=0;
			my $unalignedB= () = $data{$rownames[$j]} =~ /[^-]/g;
			# print $unalignedA;
			if( $unalignedA > $unalignedB  ){
				$minLengthWithoutGaps = $unalignedB;
			}else{
				$minLengthWithoutGaps = $unalignedA;
			}

			$cur_row_id += ($cur_score_api/($minLengthWithoutGaps)); #relative identity 
		}


		if ( $minrowid<=1 && $minrowid > ( $cur_row_id / ((scalar @rownames) -1)) ) {
			$rowinfo[$i]=0;
		}
	}

	my @colpercentid;

	for(my $k=0 ; $k < scalar @colinfo ; $k++){

		if(!exists($colisvalid[$k])){$colisvalid[$k] = 1;}

		if(exists($colinfo[$k]{"-"})){
			if( ($maxnumberofgaps>=1 && $colinfo[$k]{"-"} > $maxnumberofgaps) || 
				($maxnumberofgaps<1 && $maxnumberofgaps>=0 && $colinfo[$k]{"-"} > $maxnumberofgaps*(scalar @rownames)) ){
				$colisvalid[$k]=0;
			}
		}

		my $dominantNumberOfOccurencesInThisColumn=0;
		my $numOfGapsInThisCol=0;
		foreach my $key (keys %{$colinfo[$k]}){
			if($key ne "-"){
				if($dominantNumberOfOccurencesInThisColumn<$colinfo[$k]{$key}){ $dominantNumberOfOccurencesInThisColumn=$colinfo[$k]{$key}; }
			}else{
				$numOfGapsInThisCol=$colinfo[$k]{$key};
			}
		}

		$colpercentid[$k] = $dominantNumberOfOccurencesInThisColumn / scalar @rownames;
	}

	if($alignNrdbReadsPlotName ne ""){print "$alignNrdbReadsPlotName\n\n"}
	else{
		print "Multiple sequence alignment of ".($argv_name eq "-" ? "STDIN": $argv_name)."\n\n";
		print "sequence identity color code: ".$COLORPAL[0]."100%".$NC.", ".$COLORPAL[1].">90%".$NC.", ".$COLORPAL[2].">80%".$NC.", ".$COLORPAL[3].">50%".$NC."\n\n";
	}

	my %rownames_cnt_grouped;
	my %rownames_cnt_grouped_rows;
	for (my $i=0 ; $i<scalar @rownames ; $i++){
		if( $rownames[$i]=~/([^#]+)#x#([0-9-]+)/ ){
			if( $group ne $1 ){ $group = $1 }	
			if($group eq "-1"){$group = "REF"}
			$rownames_cnt_grouped{$group}+=$2;
			$rownames_cnt_grouped_rows{$group}++;
		}
	}
	$group="";
	my %count = (0=>0,1=>0,2=>0,3=>0,4=>0);

	for (my $i=0 ; $i<scalar @rownames ; $i++){
		
		if( $rownames[$i]=~/([^#]+)#x#/ ){
			if($group ne $1){
				if( $group ne ""){print "</details>"}
				$group = $1;
				if($group eq "-1"){$group = "REF"}
				if($alignNrdbReadsPlotGroup ne ""){print "<details><summary>"}; #}else{print "▾ "}
				print "$BOLD$group$NC (entries=".$rownames_cnt_grouped{$group}.($alignNrdbReadsPlotGroup ne "" ? ", groups=".$rownames_cnt_grouped_rows{$group} : "" ).")";
				if($alignNrdbReadsPlotGroup ne ""){print "</summary>"};
			}
		}

		if($i==0 && !$alignNrdbReadsPlot){
			print padRight("",$longestRowname)."      ";
			for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){
				
				if( $colpercentid[$k] == 1){
					print $COLORPAL[0]."v".$NC;
				}elsif( $colpercentid[$k] >= 0.9){
					print $COLORPAL[1].".".$NC;
				}elsif( $colpercentid[$k] >= 0.8){
					print $COLORPAL[2].".".$NC;
				}elsif( $colpercentid[$k] >= 0.5){
					print $COLORPAL[3]." ".$NC;
				}else{
					print " ";
				}
			}
			print "\n";
		}

		print padRight($rownames[$i],$longestRowname)."      ";
		
		for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){
			
			if($alignNrdbReadsPlot || 
				substr($data{$rownames[$i]},$k,1) eq "." || 
				substr($data{$rownames[$i]},$k,1) eq ":" || 
				substr($data{$rownames[$i]},$k,1) eq "-"){
				print substr($data{$rownames[$i]},$k,1);
			}elsif( $colpercentid[$k] == 1){
				print $COLORPAL[0].substr($data{$rownames[$i]},$k,1).$NC;
			}elsif( $colpercentid[$k] >= 0.9){
				print $COLORPAL[1].substr($data{$rownames[$i]},$k,1).$NC;
			}elsif( $colpercentid[$k] >= 0.8){
				print $COLORPAL[2].substr($data{$rownames[$i]},$k,1).$NC;
			}elsif( $colpercentid[$k] >= 0.5){
				print $COLORPAL[3].substr($data{$rownames[$i]},$k,1).$NC;
			}else{
				print substr($data{$rownames[$i]},$k,1);
			}
		}
		
		print "\n";

		if($i+1 == scalar @rownames && !$alignNrdbReadsPlot){
			print padRight("",$longestRowname)."      ";
			for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){
				
				if( $colpercentid[$k] == 1){
					$count{0}++;
					print $COLORPAL[0]."^".$NC;
				}elsif( $colpercentid[$k] >= 0.9){
					$count{1}++;
					print $COLORPAL[1].".".$NC;
				}elsif( $colpercentid[$k] >= 0.8){
					$count{2}++;
					print $COLORPAL[2].".".$NC;
				}elsif( $colpercentid[$k] >= 0.5){
					$count{3}++;
					print $COLORPAL[3]." ".$NC;
				}else{
					$count{4}++;
					print " ";
				}
			}
			print "\n";
		}
	}
	print "\n";

	print "100\% (".$COLORPAL[0]."^".$NC.") : ".$count{0}."/".(length $data{$rownames[0]})." = ".(int(($count{0}/(length $data{$rownames[0]}))*10000)/100)."\%\n";
	print ">90\% (".$COLORPAL[1].".".$NC.") : ".$count{1}."/".(length $data{$rownames[0]})." = ".(int(($count{1}/(length $data{$rownames[0]}))*10000)/100)."\%\n";
	print ">80\% (".$COLORPAL[2].".".$NC.") : ".$count{2}."/".(length $data{$rownames[0]})." = ".(int(($count{2}/(length $data{$rownames[0]}))*10000)/100)."\%\n";
	print ">50\% (".$COLORPAL[3]." ".$NC.") : ".$count{3}."/".(length $data{$rownames[0]})." = ".(int(($count{3}/(length $data{$rownames[0]}))*10000)/100)."\%\n";
	print "<50\% ("." ".") : ".$count{4}."/".(length $data{$rownames[0]})." = ".(int(($count{4}/(length $data{$rownames[0]}))*10000)/100)."\%\n";
}
if($alignNrdbReadsPlotGroup ne ""){ print "<script>var xa = document.getElementById('expAll');xa.addEventListener('click', function(e) { e.target.classList.toggle('exp'); e.target.classList.toggle('col');var details = document.querySelectorAll('details');Array.from(details).forEach(function(obj, idx) { if (e.target.classList.contains('exp')) { obj.open = true; } else { obj.open = false; } });}, false);</script>"; }
