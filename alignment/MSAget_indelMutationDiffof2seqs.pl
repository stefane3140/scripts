#!/usr/bin/env perl
#pk

use strict;
use warnings "all";
my $version="0.0.3"; # last updated on 2021-10-02 23:08:45

if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }

if(scalar @ARGV ==0 || $ARGV[0] =~m/^--?he?l?p?$/){
	print "MSAget_indelMutationDiffof2seqs.pl	calculates the indel/mutation difference between 2 given fasta files

SYNOPSIS	

	MSAget_indelMutationDiffof2seqs.pl ALNFILE

	ALNFILE : alignment file of 2 sequences in FASTA format. 
	STDOUT : a list of areas with indes/mutations (and the genomic position of the first sequences)

VERSION v$version
AUTHOR Paul Klemm
";
	exit 0;
}

my $f_modus=0; # the current number
our $fA="";
our $fAname="";
our $fB="";
our $fBname="";
while(<>){
	chomp;
	if($_=~m/^>/){
		$f_modus++;
		$_=~s/^>([^ ]+) .*$/$1/g;
		if($f_modus == 1){
			$fAname=$_;
		}elsif($f_modus == 2){
			$fBname=$_;
		}
	}else{
		if($f_modus == 1){
			$fA.=$_;
		}elsif($f_modus == 2){
			$fB.=$_;
		}
	}
}

our $cur_genome_pos_A = 1 ; # 1 based index

sub printHit{
	my $modus = shift;
	my $to = shift;
	my $length = shift;

	$to++; # make a 1 based index

	if($modus eq "deletion"){
		print "$fAname has a deletion of $length nt at alignment position ".($to-$length+1)." to ".($to)." | genome position ($fAname) of ".($cur_genome_pos_A-$length)." to ".($cur_genome_pos_A-1)." (".substr($fA,($to-$length),$length).") \n";
	}elsif($modus eq "insertion"){
		print "$fAname has an insertion of $length nt at alignment position ".($to-$length+1)." to ".($to)." | genome position ($fAname) of ".($cur_genome_pos_A)." (".substr($fB,($to-$length),$length).")\n";
	}elsif($modus eq "mutation"){
		print "$fAname has a mutation of $length nt at alignment position ".($to-$length+1)." to ".($to)." | genome position ($fAname) of ".($cur_genome_pos_A-$length)." to ".($cur_genome_pos_A-1)." (".substr($fA,($to-$length),$length)." -> ".substr($fB,($to-$length),$length).")\n";
	}
}

my $length_total=0;

my $last_modus="";
my $last_length=0;
my $i = 0 ;

for($i ; $i < length $fA; $i++){

	my $cur_modus="";

	if(substr($fA,$i,1) ne substr($fB,$i,1) && !(substr($fA,$i,1) eq "-" && substr($fB,$i,1) eq "-")){

		if(substr($fA,$i,1) eq "-"){

			$cur_modus="insertion";

		}elsif(substr($fB,$i,1) eq "-"){

			$cur_modus="deletion";

		}else{

			$cur_modus="mutation";
		}
	}

	if($cur_modus eq $last_modus && $last_modus ne ""){
		$last_length++;
	}else{
		if($last_modus ne ""){
			$length_total+=$last_length;
			printHit($last_modus,$i-1,$last_length);
		}
		if($cur_modus ne ""){
			$last_length=1;$last_modus=$cur_modus;
		}else{
			$last_length=0;$last_modus="";
		}
	}
	if(substr($fA,$i,1) ne "-"){
		$cur_genome_pos_A++;
	}
}

if($last_modus ne "" && $last_length > 0){
	$length_total+=$last_length;
	printHit($last_modus,$i-1,$last_length);
}

print "Total number of indel/mutations : $length_total nt\n";

# for(my $i = 0 ; $i < length $fA; $i++){
# 	if(substr($fA,$i,1) ne substr($fB,$i,1) && !(substr($fA,$i,1) eq "-" && substr($fB,$i,1) eq "-")){
# 		if(substr($fA,$i,1) eq "-"){
# 			if($last_modus eq "deletion"){
# 				$last_length++;
# 			}elsif($last_modus ne ""){
# 				print "$fAname $last_modus at ".($i-$last_length)." to ".$i." (".substr($fB,($i-$last_length),$last_length).")\n";
# 			}
# 			print "$fAname deletion at $i (-".substr($fB,$i,1).")\n";
# 		}elsif(substr($fB,$i,1) eq "-"){
# 			print "$fAname insertion at $i (+".substr($fA,$i,1).")\n";
# 		}else{
# 			print "$fAname mutation at $i (".substr($fA,$i,1)." -> ".substr($fB,$i,1).")\n";
# 		}
# 	}
# }
