#!/usr/bin/env perl
# pk

use strict;
use warnings "all";
# RUN : perl MSAscorer.pl somealn.aln

# given an alignment in aln format, this script produces the average relative pairwise identity
# for 2 lines in the alignment, the relative pairwise identity is computed and then averaged

our $RED="\033[0;41m";
our $PURPLE="\033[0;45m";
our $YELLOW="\033[0;43m";
our $COLOR="\033[36m";
our $NC="\033[0m"; # No Color
my $version="0.0.13"; # last updated on 2021-10-01 14:11:18

my $usage = <<ENDUSAGE;
MSAscorer.pl        scores a given alignment (average linewise identity, number of gaps, ...)
 
SYNOPSIS
 
MSAscorer.pl ALIGNMENTFILE 

	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

		e.g. CLUSTAL W formated input:

		...
		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
		...

	OUTPUT : clustalw formatted alignment

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES -
ENDUSAGE

#my $noInput=1;
#for($v = 0 ; $v < scalar @ARGV ; $v++){
#	if($ARGV[$v] ne "-" && $ARGV[$v] =~ m/^-/){ next;}
#	$noInput=0;
#}
#if($noInput){push(@ARGV,"-")}

if(scalar @ARGV == 0){print $usage; die;}
if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }
if( $ARGV[0] eq "-help" ||  $ARGV[0] eq "-h" ||  $ARGV[0] eq "--help" ){print $usage; die;}

print "# filename\tcategory\tscore\tabsolute value/omega\trelative value\n";

our %data;
our $cur_geneName="";
our $cur_header="";

sub processLine{
	$_=shift;
	chomp($_);
	my $entry=$_;
	$entry=~s/^[ \t]+//g;
	my @arr=split(/[ \t][ \t]+/,$entry);
	if( scalar(@arr) == 2){
		if(!exists($data{$arr[0]})){$data{$arr[0]}="";}
		$data{$arr[0]}.=$arr[1];
	}else{
		if($entry=~m/>([^ ]+)/){
			$cur_header=$1;
		}elsif($cur_header ne "" && $entry=~m/^[^ ]+$/){
			if(!exists($data{$cur_header})){$data{$cur_header}="";}
			$data{$cur_header}.=$entry;
		}
	}
}

for(my $v = 0 ; $v < scalar @ARGV ; $v++){

	%data = ();
	$cur_geneName="";
	$cur_header="";

	#1. load in the alignment
	if($ARGV[$v] eq "-"){
		while(<>){ processLine($_) }
	}else{
		open (FH,'<',$ARGV[$v]);
		while(<FH>){ processLine($_) }
		close(FH);
	}


	#2. compute the score
	my @genes = keys %data;

	my $api=0;
	my $numberOfComparisons=0;
	my $num_gaps_total=0;
	my $num_pos_total=0;
	my $api_without_gaps=0;

	my @gapcols;

	my %sortedRows;

	for (my $i=0 ; $i<scalar @genes ; $i++){
		if(length $genes[$i] < 1 or length $data{$genes[$i]} < 1 ){next;}
		for(my $k=0 ; $k < length $data{$genes[$i]} ; $k++){

			if(!exists($gapcols[$k])){$gapcols[$k] = 0;}

			if( substr($data{$genes[$i]},$k,1) eq "-"){
				$num_gaps_total++;
				$gapcols[$k]++;
			}
			$num_pos_total++;
		}

		my $cur_value=0;
		my $unalignedA= () = $data{$genes[$i]} =~ /[^-]/g;

		for (my $j=0 ; $j<scalar @genes ; $j++ ){
			if($j==$i or length $genes[$j] < 1 or length $data{$genes[$j]} < 1 ){next;}

			my $cur_score_api = 0; #the identity counter for the line i and j
			my $cur_score_api_without_gapmathing = 0; #the identity counter for the line i and j

			for(my $k=0 ; $k < length $data{$genes[$i]} ; $k++){
				if(length($data{$genes[$j]}) > $k && 
					substr($data{$genes[$i]},$k,1) eq 
					substr($data{$genes[$j]},$k,1)){
					$cur_score_api++;
				}
				if(length($data{$genes[$j]}) > $k && 
					substr($data{$genes[$i]},$k,1) eq 
					substr($data{$genes[$j]},$k,1) && 
					substr($data{$genes[$i]},$k,1) ne "-" && 
					substr($data{$genes[$j]},$k,1) ne "-"){
					$cur_score_api_without_gapmathing++;
				}
			}

			$api += ($cur_score_api/(length $data{$genes[$i]})); #relative identity with gaps

			my $minLengthWithoutGaps=0;
			my $unalignedB= () = $data{$genes[$j]} =~ /[^-]/g;
			# print $unalignedA;
			if( $unalignedA > $unalignedB  ){
				$minLengthWithoutGaps = $unalignedB;
			}else{
				$minLengthWithoutGaps = $unalignedA;
			}

			$cur_value += ($cur_score_api_without_gapmathing/($minLengthWithoutGaps)); #relative identity without gaps = normal pair identity

			$numberOfComparisons++;
		}
		$sortedRows{$genes[$i]}=$cur_value/((scalar @genes )-1);
		$api_without_gaps+=$cur_value;
	}

	my $gapcols_i0=0;
	for(my $k=0 ; $k < scalar @gapcols ; $k++){
		if($gapcols[$k]==0){$gapcols_i0++;}
	}
	my $gapcols_i5=0;
	for(my $k=0 ; $k < scalar @gapcols ; $k++){
		if($gapcols[$k]<(0.05*scalar @genes)){$gapcols_i5++;}
	}
	my $gapcols_i15=0;
	for(my $k=0 ; $k < scalar @gapcols ; $k++){
		if($gapcols[$k]<(0.15*scalar @genes)){$gapcols_i15++;}
	}
	my $gapcols_i50=0;
	for(my $k=0 ; $k < scalar @gapcols ; $k++){
		if($gapcols[$k]<(0.5*scalar @genes)){$gapcols_i50++;}
	}
	my $gapcols_i85=0;
	for(my $k=0 ; $k < scalar @gapcols ; $k++){
		if($gapcols[$k]>(0.85*scalar @genes)){$gapcols_i85++;}
	}
	my $gapcols_i95=0;
	for(my $k=0 ; $k < scalar @gapcols ; $k++){
		if($gapcols[$k]>(0.95*scalar @genes)){$gapcols_i95++;}
	}
	my $gapcols_i100=0;
	for(my $k=0 ; $k < scalar @gapcols ; $k++){
		if($gapcols[$k]==(scalar @genes)){$gapcols_i100++;}
	}


	print STDERR "${COLOR}[STDERR]$NC **** The individual values: ****$NC\n";
	print STDERR "${COLOR}[STDERR]$NC : vvvvv is the most dissimilar to all other lines. The score used is the average pairwise identity$NC\n";
	foreach my $name (sort { $sortedRows{$a} <=> $sortedRows{$b} } keys %sortedRows) {
		if(!defined $ARGV[$v]){$ARGV[$v]="STDIN"}
	    print $ARGV[$v] ."\tsorted AvgPairId\t$name\t".($sortedRows{$name}*((scalar @genes ) -1))."/".((scalar @genes ) -1)."\t".$sortedRows{$name}."\n";
	}
	print STDERR "${COLOR}[STDERR]$NC : ^^^^ is the most similar to all other lines. The score used is the average pairwise identity$NC\n";
	print STDERR "${COLOR}[STDERR]$NC : For a given line normalize the pairwise identity agains all other lines = ".((scalar @genes ) -1)." = the number of lines -1. The nominator = sum of the PairId.$NC\n";
	print STDERR "${COLOR}[STDERR]$NC **** The average values: ****$NC\n";

	print $ARGV[$v] . "\tHorizontal/ Rows\tAvgPairId with gaps\t" . $api . "/" . $numberOfComparisons . "\t" . (int(($api/ $numberOfComparisons)*10000)/10000) . "\n";
	print STDERR "${COLOR}[STDERR]$NC : ^^^ PairId with gaps = (idents / len1) with idents = number of identical characters (also gaps) and len1 is the aligned lenght of the first sequence (same as the second). Averaged over $numberOfComparisons = the number of pairs (A,B), A!=B, that can be formed using the ".(scalar @gapcols)." lines.$NC\n";
	print $ARGV[$v] . "\tHorizontal/ Rows\tAvgPairId\t" . $api_without_gaps . "/" . $numberOfComparisons . "\t" . (int(($api_without_gaps/ $numberOfComparisons)*10000)/10000) . "\n";
	print STDERR "${COLOR}[STDERR]$NC : ^^^ PairId = (idents / MIN(len1, len2)) with idents = number of identical nongap characters and len1,len2 are the lengths of the unaligned sequences.$NC\n";
	print $ARGV[$v] . "\t-\ttotal gaps\t" . $num_gaps_total . "/" . $num_pos_total . "\t" . (int(($num_gaps_total/ $num_pos_total)*10000)/10000) . "\n";
	print $ARGV[$v] . "\tVertical/Columns\tno gap columns\t" . $gapcols_i0 . "/" . scalar @gapcols . "\t" . (int(($gapcols_i0/ scalar @gapcols)*10000)/10000) . "\n";
	print $ARGV[$v] . "\tVertical/Columns\t<5% (=".(0.05*scalar @genes)."/".scalar @genes.") gap columns\t" . $gapcols_i5 . "/" . scalar @gapcols . "\t" . (int(($gapcols_i5/ scalar @gapcols)*10000)/10000) . "\n";
	print STDERR "${COLOR}[STDERR]$NC <5% => only up to 5% of the characters in this column are a gap.$NC\n";
	print $ARGV[$v] . "\tVertical/Columns\t<15% (=".(0.15*scalar @genes)."/".scalar @genes.") gap columns\t" . $gapcols_i15 . "/" . scalar @gapcols . "\t" . (int(($gapcols_i15/ scalar @gapcols)*10000)/10000) . "\n";
	print $ARGV[$v] . "\tVertical/Columns\t<50% (=".(0.5*scalar @genes)."/".scalar @genes.") gap columns\t" . $gapcols_i50 . "/" . scalar @gapcols . "\t" . (int(($gapcols_i50/ scalar @gapcols)*10000)/10000) . "\n";
	print $ARGV[$v] . "\tVertical/Columns\t>85% (=".(0.85*scalar @genes)."/".scalar @genes.") gap columns\t" . $gapcols_i85 . "/" . scalar @gapcols . "\t" . (int(($gapcols_i85/ scalar @gapcols)*10000)/10000) . "\n";
	print $ARGV[$v] . "\tVertical/Columns\t>95% (=".(0.95*scalar @genes)."/".scalar @genes.") gap columns\t" . $gapcols_i95 . "/" . scalar @gapcols . "\t" . (int(($gapcols_i95/ scalar @gapcols)*10000)/10000) . "\n";
	print $ARGV[$v] . "\tVertical/Columns\tfull gap columns\t" . $gapcols_i100 . "/" . scalar @gapcols . "\t" . (int(($gapcols_i100/ scalar @gapcols)*10000)/10000) . "\n";
	print STDERR "${COLOR}[STDERR]$NC : The full gap score is just for sanity check.$NC\n";
	# print $ARGV[$v] . "\tVertical/Columns\tall identical non-gap\t" . $identicalCols . "/" . scalar @gapcols . "\t" . (int(($identicalCols/ scalar @gapcols)*10000)/10000) . "\n";
}
