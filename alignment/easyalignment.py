#!/usr/bin/env python
#pk
from __future__ import print_function
from copy import deepcopy
from optparse import OptionParser
from os.path import isfile
import sys

extra_packages = {"progressbar":None, "psutil":None, "sys":None}
progressbar_widgets = []

for cur_pks in list(extra_packages.keys()):
	try:
		extra_packages[cur_pks] = __import__(cur_pks)
	except ImportError:
		display_pks = cur_pks if cur_pks != "progressbar" else "progressbar2" # progressbar2 is the install name of the module progressbar...
		print(f"WARNING: Did not found '{display_pks}', this package is not essential but enhances the program. I will skip it but consider installing it...")
		extra_packages = {p:extra_packages[p] for p in list(extra_packages.keys()) if p != cur_pks}

if "progressbar" in list(extra_packages.keys()):
	progressbar_widgets = [
	    extra_packages["progressbar"].Percentage(),
	    extra_packages["progressbar"].Bar(),
	    ' ', extra_packages["progressbar"].SimpleProgress(),
	    ' ', extra_packages["progressbar"].AdaptiveETA(),
	    ' ', extra_packages["progressbar"].AdaptiveTransferSpeed(unit='it'),
	]

def eprint(*args, **kwargs):
	print(*args, file=extra_packages["sys"].stderr, **kwargs) if "sys" in list(extra_packages.keys()) else print(*args, **kwargs)

def test_virtual_mem(maxmemp):
	if "psutil" in list(extra_packages.keys()) and extra_packages["psutil"].virtual_memory()[2] >= maxmemp:
		eprint(f"maximal virtual_memory {maxmemp}% exceeded (use --maxmemp and --maxdiag to adjust this behaviour)")
		exit(1)
	return True

version="0.5.35" # last updated on 2021-10-27 12:50:06

class bc:
    HIGHLIGHT = '\033[1;47m\033[30m'
    NC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class alignment:

	BLOSUM62 = {'A': {'A': 4, 'C': 0, 'E': -1, 'D': -2, 'G': 0, 'F': -2, 'I': -1, 'H': -2, 'K': -1, 'M': -1, 'L': -1, 'N': -2, 'Q': -1, 'P': -1, 'S': 1, 'R': -1, 'T': 0, 'W': -3, 'V': 0, 'Y': -2}, 'C': {'A': 0, 'C': 9, 'E': -4, 'D': -3, 'G': -3, 'F': -2, 'I': -1, 'H': -3, 'K': -3, 'M': -1, 'L': -1, 'N': -3, 'Q': -3, 'P': -3, 'S': -1, 'R': -3, 'T': -1, 'W': -2, 'V': -1, 'Y': -2}, 'E': {'A': -1, 'C': -4, 'E': 5, 'D': 2, 'G': -2, 'F': -3, 'I': -3, 'H': 0, 'K': 1, 'M': -2, 'L': -3, 'N': 0, 'Q': 2, 'P': -1, 'S': 0, 'R': 0, 'T': -1, 'W': -3, 'V': -2, 'Y': -2}, 'D': {'A': -2, 'C': -3, 'E': 2, 'D': 6, 'G': -1, 'F': -3, 'I': -3, 'H': -1, 'K': -1, 'M': -3, 'L': -4, 'N': 1, 'Q': 0, 'P': -1, 'S': 0, 'R': -2, 'T': -1, 'W': -4, 'V': -3, 'Y': -3}, 'G': {'A': 0, 'C': -3, 'E': -2, 'D': -1, 'G': 6, 'F': -3, 'I': -4, 'H': -2, 'K': -2, 'M': -3, 'L': -4, 'N': 0, 'Q': -2, 'P': -2, 'S': 0, 'R': -2, 'T': -2, 'W': -2, 'V': -3, 'Y': -3}, 'F': {'A': -2, 'C': -2, 'E': -3, 'D': -3, 'G': -3, 'F': 6, 'I': 0, 'H': -1, 'K': -3, 'M': 0, 'L': 0, 'N': -3, 'Q': -3, 'P': -4, 'S': -2, 'R': -3, 'T': -2, 'W': 1, 'V': -1, 'Y': 3}, 'I': {'A': -1, 'C': -1, 'E': -3, 'D': -3, 'G': -4, 'F': 0, 'I': 4, 'H': -3, 'K': -3, 'M': 1, 'L': 2, 'N': -3, 'Q': -3, 'P': -3, 'S': -2, 'R': -3, 'T': -1, 'W': -3, 'V': 3, 'Y': -1}, 'H': {'A': -2, 'C': -3, 'E': 0, 'D': -1, 'G': -2, 'F': -1, 'I': -3, 'H': 8, 'K': -1, 'M': -2, 'L': -3, 'N': 1, 'Q': 0, 'P': -2, 'S': -1, 'R': 0, 'T': -2, 'W': -2, 'V': -3, 'Y': 2}, 'K': {'A': -1, 'C': -3, 'E': 1, 'D': -1, 'G': -2, 'F': -3, 'I': -3, 'H': -1, 'K': 5, 'M': -1, 'L': -2, 'N': 0, 'Q': 1, 'P': -1, 'S': 0, 'R': 2, 'T': -1, 'W': -3, 'V': -2, 'Y': -2}, 'M': {'A': -1, 'C': -1, 'E': -2, 'D': -3, 'G': -3, 'F': 0, 'I': 1, 'H': -2, 'K': -1, 'M': 5, 'L': 2, 'N': -2, 'Q': 0, 'P': -2, 'S': -1, 'R': -1, 'T': -1, 'W': -1, 'V': 1, 'Y': -1}, 'L': {'A': -1, 'C': -1, 'E': -3, 'D': -4, 'G': -4, 'F': 0, 'I': 2, 'H': -3, 'K': -2, 'M': 2, 'L': 4, 'N': -3, 'Q': -2, 'P': -3, 'S': -2, 'R': -2, 'T': -1, 'W': -2, 'V': 1, 'Y': -1}, 'N': {'A': -2, 'C': -3, 'E': 0, 'D': 1, 'G': 0, 'F': -3, 'I': -3, 'H': 1, 'K': 0, 'M': -2, 'L': -3, 'N': 6, 'Q': 0, 'P': -2, 'S': 1, 'R': 0, 'T': 0, 'W': -4, 'V': -3, 'Y': -2}, 'Q': {'A': -1, 'C': -3, 'E': 2, 'D': 0, 'G': -2, 'F': -3, 'I': -3, 'H': 0, 'K': 1, 'M': 0, 'L': -2, 'N': 0, 'Q': 5, 'P': -1, 'S': 0, 'R': 1, 'T': -1, 'W': -2, 'V': -2, 'Y': -1}, 'P': {'A': -1, 'C': -3, 'E': -1, 'D': -1, 'G': -2, 'F': -4, 'I': -3, 'H': -2, 'K': -1, 'M': -2, 'L': -3, 'N': -2, 'Q': -1, 'P': 7, 'S': -1, 'R': -2, 'T': -1, 'W': -4, 'V': -2, 'Y': -3}, 'S': {'A': 1, 'C': -1, 'E': 0, 'D': 0, 'G': 0, 'F': -2, 'I': -2, 'H': -1, 'K': 0, 'M': -1, 'L': -2, 'N': 1, 'Q': 0, 'P': -1, 'S': 4, 'R': -1, 'T': 1, 'W': -3, 'V': -2, 'Y': -2}, 'R': {'A': -1, 'C': -3, 'E': 0, 'D': -2, 'G': -2, 'F': -3, 'I': -3, 'H': 0, 'K': 2, 'M': -1, 'L': -2, 'N': 0, 'Q': 1, 'P': -2, 'S': -1, 'R': 5, 'T': -1, 'W': -3, 'V': -3, 'Y': -2}, 'T': {'A': 0, 'C': -1, 'E': -1, 'D': -1, 'G': -2, 'F': -2, 'I': -1, 'H': -2, 'K': -1, 'M': -1, 'L': -1, 'N': 0, 'Q': -1, 'P': -1, 'S': 1, 'R': -1, 'T': 5, 'W': -2, 'V': 0, 'Y': -2}, 'W': {'A': -3, 'C': -2, 'E': -3, 'D': -4, 'G': -2, 'F': 1, 'I': -3, 'H': -2, 'K': -3, 'M': -1, 'L': -2, 'N': -4, 'Q': -2, 'P': -4, 'S': -3, 'R': -3, 'T': -2, 'W': 11, 'V': -3, 'Y': 2}, 'V': {'A': 0, 'C': -1, 'E': -2, 'D': -3, 'G': -3, 'F': -1, 'I': 3, 'H': -3, 'K': -2, 'M': 1, 'L': 1, 'N': -3, 'Q': -2, 'P': -2, 'S': -2, 'R': -3, 'T': 0, 'W': -3, 'V': 4, 'Y': -1}, 'Y': {'A': -2, 'C': -2, 'E': -2, 'D': -3, 'G': -3, 'F': 3, 'I': -1, 'H': 2, 'K': -2, 'M': -1, 'L': -1, 'N': -2, 'Q': -1, 'P': -3, 'S': -2, 'R': -2, 'T': -2, 'W': 2, 'V': -1, 'Y': 7}}
	PAM250 = {'A': {'A': 2, 'C': -2, 'E': 0, 'D': 0, 'G': 1, 'F': -3, 'I': -1, 'H': -1, 'K': -1, 'M': -1, 'L': -2, 'N': 0, 'Q': 0, 'P': 1, 'S': 1, 'R': -2, 'T': 1, 'W': -6, 'V': 0, 'Y': -3}, 'C': {'A': -2, 'C': 12, 'E': -5, 'D': -5, 'G': -3, 'F': -4, 'I': -2, 'H': -3, 'K': -5, 'M': -5, 'L': -6, 'N': -4, 'Q': -5, 'P': -3, 'S': 0, 'R': -4, 'T': -2, 'W': -8, 'V': -2, 'Y': 0}, 'E': {'A': 0, 'C': -5, 'E': 4, 'D': 3, 'G': 0, 'F': -5, 'I': -2, 'H': 1, 'K': 0, 'M': -2, 'L': -3, 'N': 1, 'Q': 2, 'P': -1, 'S': 0, 'R': -1, 'T': 0, 'W': -7, 'V': -2, 'Y': -4}, 'D': {'A': 0, 'C': -5, 'E': 3, 'D': 4, 'G': 1, 'F': -6, 'I': -2, 'H': 1, 'K': 0, 'M': -3, 'L': -4, 'N': 2, 'Q': 2, 'P': -1, 'S': 0, 'R': -1, 'T': 0, 'W': -7, 'V': -2, 'Y': -4}, 'G': {'A': 1, 'C': -3, 'E': 0, 'D': 1, 'G': 5, 'F': -5, 'I': -3, 'H': -2, 'K': -2, 'M': -3, 'L': -4, 'N': 0, 'Q': -1, 'P': 0, 'S': 1, 'R': -3, 'T': 0, 'W': -7, 'V': -1, 'Y': -5}, 'F': {'A': -3, 'C': -4, 'E': -5, 'D': -6, 'G': -5, 'F': 9, 'I': 1, 'H': -2, 'K': -5, 'M': 0, 'L': 2, 'N': -3, 'Q': -5, 'P': -5, 'S': -3, 'R': -4, 'T': -3, 'W': 0, 'V': -1, 'Y': 7}, 'I': {'A': -1, 'C': -2, 'E': -2, 'D': -2, 'G': -3, 'F': 1, 'I': 5, 'H': -2, 'K': -2, 'M': 2, 'L': 2, 'N': -2, 'Q': -2, 'P': -2, 'S': -1, 'R': -2, 'T': 0, 'W': -5, 'V': 4, 'Y': -1}, 'H': {'A': -1, 'C': -3, 'E': 1, 'D': 1, 'G': -2, 'F': -2, 'I': -2, 'H': 6, 'K': 0, 'M': -2, 'L': -2, 'N': 2, 'Q': 3, 'P': 0, 'S': -1, 'R': 2, 'T': -1, 'W': -3, 'V': -2, 'Y': 0}, 'K': {'A': -1, 'C': -5, 'E': 0, 'D': 0, 'G': -2, 'F': -5, 'I': -2, 'H': 0, 'K': 5, 'M': 0, 'L': -3, 'N': 1, 'Q': 1, 'P': -1, 'S': 0, 'R': 3, 'T': 0, 'W': -3, 'V': -2, 'Y': -4}, 'M': {'A': -1, 'C': -5, 'E': -2, 'D': -3, 'G': -3, 'F': 0, 'I': 2, 'H': -2, 'K': 0, 'M': 6, 'L': 4, 'N': -2, 'Q': -1, 'P': -2, 'S': -2, 'R': 0, 'T': -1, 'W': -4, 'V': 2, 'Y': -2}, 'L': {'A': -2, 'C': -6, 'E': -3, 'D': -4, 'G': -4, 'F': 2, 'I': 2, 'H': -2, 'K': -3, 'M': 4, 'L': 6, 'N': -3, 'Q': -2, 'P': -3, 'S': -3, 'R': -3, 'T': -2, 'W': -2, 'V': 2, 'Y': -1}, 'N': {'A': 0, 'C': -4, 'E': 1, 'D': 2, 'G': 0, 'F': -3, 'I': -2, 'H': 2, 'K': 1, 'M': -2, 'L': -3, 'N': 2, 'Q': 1, 'P': 0, 'S': 1, 'R': 0, 'T': 0, 'W': -4, 'V': -2, 'Y': -2}, 'Q': {'A': 0, 'C': -5, 'E': 2, 'D': 2, 'G': -1, 'F': -5, 'I': -2, 'H': 3, 'K': 1, 'M': -1, 'L': -2, 'N': 1, 'Q': 4, 'P': 0, 'S': -1, 'R': 1, 'T': -1, 'W': -5, 'V': -2, 'Y': -4}, 'P': {'A': 1, 'C': -3, 'E': -1, 'D': -1, 'G': 0, 'F': -5, 'I': -2, 'H': 0, 'K': -1, 'M': -2, 'L': -3, 'N': 0, 'Q': 0, 'P': 6, 'S': 1, 'R': 0, 'T': 0, 'W': -6, 'V': -1, 'Y': -5}, 'S': {'A': 1, 'C': 0, 'E': 0, 'D': 0, 'G': 1, 'F': -3, 'I': -1, 'H': -1, 'K': 0, 'M': -2, 'L': -3, 'N': 1, 'Q': -1, 'P': 1, 'S': 2, 'R': 0, 'T': 1, 'W': -2, 'V': -1, 'Y': -3}, 'R': {'A': -2, 'C': -4, 'E': -1, 'D': -1, 'G': -3, 'F': -4, 'I': -2, 'H': 2, 'K': 3, 'M': 0, 'L': -3, 'N': 0, 'Q': 1, 'P': 0, 'S': 0, 'R': 6, 'T': -1, 'W': 2, 'V': -2, 'Y': -4}, 'T': {'A': 1, 'C': -2, 'E': 0, 'D': 0, 'G': 0, 'F': -3, 'I': 0, 'H': -1, 'K': 0, 'M': -1, 'L': -2, 'N': 0, 'Q': -1, 'P': 0, 'S': 1, 'R': -1, 'T': 3, 'W': -5, 'V': 0, 'Y': -3}, 'W': {'A': -6, 'C': -8, 'E': -7, 'D': -7, 'G': -7, 'F': 0, 'I': -5, 'H': -3, 'K': -3, 'M': -4, 'L': -2, 'N': -4, 'Q': -5, 'P': -6, 'S': -2, 'R': 2, 'T': -5, 'W': 17, 'V': -6, 'Y': 0}, 'V': {'A': 0, 'C': -2, 'E': -2, 'D': -2, 'G': -1, 'F': -1, 'I': 4, 'H': -2, 'K': -2, 'M': 2, 'L': 2, 'N': -2, 'Q': -2, 'P': -1, 'S': -1, 'R': -2, 'T': 0, 'W': -6, 'V': 4, 'Y': -2}, 'Y': {'A': -3, 'C': 0, 'E': -4, 'D': -4, 'G': -5, 'F': 7, 'I': -1, 'H': 0, 'K': -4, 'M': -2, 'L': -1, 'N': -2, 'Q': -4, 'P': -5, 'S': -3, 'R': -4, 'T': -3, 'W': 0, 'V': -2, 'Y': 10}}
	IUB =  {'A': {'A': 1.9, 'C': 0, 'G': 0, 'T': 0, 'U': 0, 'N': 0, 'X': 0}, 'C': {'A': 0, 'C': 1.9, 'G': 0, 'T': 0, 'U': 0, 'N': 0, 'X': 0}, 'G': {'A': 0, 'C': 0, 'G': 1.9, 'T': 0, 'U': 0, 'N': 0, 'X': 0}, 'T': {'A': 0, 'C': 0, 'G': 0, 'T': 1.9, 'U': 0, 'N': 0, 'X': 0}, 'U': {'A': 0, 'C': 0, 'G': 0, 'T': 0, 'U': 1.9, 'N': 0, 'X': 0}, 'N': {'A': 0, 'C': 0, 'G': 0, 'T': 0, 'U': 0, 'N': 1.9, 'X': 0}, 'X': {'A': 0, 'C': 0, 'G': 0, 'T': 0, 'U': 0, 'N': 0, 'X': 1.9}}

	@staticmethod
	def createSimpleScoringMatrix(match=1, mismatch=1, atype = "prot"):
		if atype =="prot":
			names = 'ACDEFGHIKLMNPQRSTVWY'
		elif atype == "nucl": 
			names = 'ACGTUNX'
		else:
			names = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ.,_-:;#+*?=)(/&%$§"!\''
		return {aa1: {aa2: match if aa1 == aa2 or (atype =="prot" and (aa1 == ["N","X"] or aa2 in ["N","X"])) else -mismatch for aa2 in names} for aa1 in names}

	@staticmethod
	def buildAlignmentMatrix(v:str, w:str, scoring:dict, modus="needlemanwunsch",maxdiag=200, maxmemp=95, progress=False):
		"""
		global alignment of 2 sequences
		modus = ["needlemanwunsch","smithwaterman","fitted","overlapped"]
			needlemanwunsch = global alignment
			smithwaterman = local alignment
			fitted = local alignment of v (longer sequence) and global alignment w (shorter sequence)
			overlapped = maximal overlap of suffix of v and prefix of w sequences
		"""
		if modus not in ["needlemanwunsch","smithwaterman","fitted","overlapped"]:
			raise ValueError("unknown modus '",modus,"' should be either needlemanwunsch (=global) or smithwaterman (=local) or fitted (local v and global w) or overlapped (suffix of v with prefix of w")

		eprint("building alignment matrix...")

		if maxdiag > 0:

			coef = len(w)/len(v)

			max_interaction = min(alignment.ceil(coef*int(len(v)/2))+maxdiag,len(w))+1 - max(alignment.floor(coef*int(len(v)/2))-maxdiag,0)
			eprint(f"the --maxdiag of {maxdiag} results in a maximal gap length of {max_interaction} ({min([100,int(100*100*max_interaction/(max([len(v),len(w)])))/100])}%)")

			s = {i:{j:{"score":0,"dir":[]} for j in range( max(alignment.floor(coef*i)-maxdiag,0),min(alignment.ceil(coef*i)+maxdiag,len(w))+1 ) } for i in range(len(v)+1) if i % 2000 or test_virtual_mem(maxmemp) }
			
			# build the border (top and left)
			fa = list(s.keys())[0]
			if modus == "needlemanwunsch":
				for i in s.keys():
					fi = list(s[i].keys())[0]
					s[i][fi] = {"score":i*scoring["open"], "dir":["↓"]} 
				for j in s[fa].keys():
					s[fa][j] = {"score":j*scoring["open"], "dir":["→"]} 
			elif modus=="fitted" or modus=="overlapped":
				for j in s[fa].keys():
					s[fa][j] = {"score":j*scoring["open"], "dir":["→"]}

		else:

			# 1. build the border (top and left) 
			s = {i:{j:{"score":0,"dir":[]} for j in range(len(w)+1)} for i in range(len(v)+1)}

			for i in range(len(v)+1):
				s[i][0]["dir"] = ["↓"] 
			for j in range(len(w)+1):
				s[0][j]["dir"] = ["→"]
			if modus == "needlemanwunsch":
				for i in range(len(v)+1):
					s[i][0] = {"score":i*scoring["open"], "dir":["↓"]} 
				for j in range(len(w)+1):
					s[0][j] = {"score":j*scoring["open"], "dir":["→"]} 
			elif modus=="fitted" or modus=="overlapped":
				for j in range(len(w)+1):
					s[0][j] = {"score":j*scoring["open"], "dir":["→"]}

		s[0][0] = {"score":0,"dir":[""]}

		eprint("forwardpropagation in alignment matrix...")
		# 2. loop from 1,1 all the way to n,n: at each step find the best solution of the previous results
		pb=extra_packages["progressbar"].progressbar(range(1,len(v)+1),widgets=progressbar_widgets) if "progressbar" in list(extra_packages.keys()) and progress else range(1,len(v)+1)
		for i in pb:
			for j in range(1,len(w)+1):
				if j not in s[i]:
					continue

				is_match = (v[i-1] == w[j-1])
				if "scoringmatrix" in scoring:
					score_match_or_missmatch = scoring["scoringmatrix"][v[i-1]][w[j-1]]
				else:
					score_match_or_missmatch = scoring["match"] if is_match else scoring["missmatch"]
				
				s[i][j]["score"] = max([ 0 if modus == "smithwaterman" else -float('inf'),
										 s[i-1][j]["score"] + scoring["open"] if j in s[i-1] else -float('inf'), 
										 s[i][j-1]["score"] + scoring["open"] if j-1 in s[i] else -float('inf'), 
										 s[i-1][j-1]["score"] + score_match_or_missmatch if j-1 in s[i-1] else -float('inf') ])
				s[i][j]["dir"] = []
				if j-1 in s[i-1] and s[i][j]["score"] == s[i-1][j-1]["score"] + score_match_or_missmatch:
					s[i][j]["dir"].append("⇘" if is_match else "⤰")
				if j in s[i-1] and s[i][j]["score"] == s[i-1][j]["score"] + scoring["open"]:
					s[i][j]["dir"].append("↓")
				if j-1 in s[i] and s[i][j]["score"] == s[i][j-1]["score"] + scoring["open"]:
					s[i][j]["dir"].append("→")
				

		return s

	@staticmethod
	def backtrack(s:list, v:str, w:str, modus="needlemanwunsch", only_one_result=True, progress = False):
		"""
		backtrack the result of buildAlignmentMatrix
		"""
		if modus not in ["needlemanwunsch","smithwaterman","fitted","overlapped"]:
			raise ValueError("unknown modus '",modus,"' should be either needlemanwunsch (=global) or smithwaterman (=local) or fitted (local v and global w) or overlapped")

		eprint("backtracking...")
		# result hold the queue that needs to be worked on (do until the break statement is satifsfied)

		max_score=s[len(v)][len(w)]["score"]

		if modus == "needlemanwunsch":
			# take the value (bottom right) as start values
			result=[{ "v":"", "w":"", "i":len(v), "j":len(w), "path":[], "match":0, "mismatch":0, "indel":0, "done":False }]
		elif modus == "smithwaterman":
			# find the maximal value of all fields
			result=[]
			for i in range(len(v)+1):
				for j in range(len(w)+1):
					if j not in s[i]:
						continue
					if s[i][j]["score"] > max_score:
						max_score=s[i][j]["score"]
						result=[]
					if s[i][j]["score"] == max_score:
						result.append({"v":"", "w":"", "i":i, "j":j, "path":[], "match":0, "mismatch":0, "indel":0, "done":False})
		elif modus == "fitted":
			# find the maximal value of the right (!) side
			result=[]
			for i in range(len(s)):
				fi = list(s[i].keys())[-1]
				if s[i][fi]["score"] > max_score:
					max_score=s[i][fi]["score"]
					result=[]
				if s[i][fi]["score"] == max_score:
					result.append({"v":"", "w":"", "i":i, "j":fi, "path":[], "match":0, "mismatch":0, "indel":0, "done":False})
		elif modus == "overlapped":
			# find the maximal value of the bottom (!) side
			result=[]
			fi = list(s.keys())[-1]
			for j in range(len(w)+1):
				if j not in s[fi]:
					continue
				if s[fi][j]["score"] > max_score:
					max_score=s[fi][j]["score"]
					result=[]
				if s[fi][j]["score"] == max_score:
					result.append({"v":"", "w":"", "i":fi, "j":j, "path":[], "match":0, "mismatch":0, "indel":0, "done":False})

		all_done=False # nothing changes anymore -> return
		bar = extra_packages["progressbar"].ProgressBar(max_value=len(v),widgets=progressbar_widgets) if "progressbar" in list(extra_packages.keys()) and progress else None
		
		while not all_done:
			#if progress:
			#	print(result)
			new_result=[]
			all_done=True
			for r in result:
				i = r["i"]
				j = r["j"]
				if "progressbar" in list(extra_packages.keys()) and progress:
					bar.update(len(v)-r["i"])
				if  (i==0 and j==0) or \
					(modus == "smithwaterman" and s[i][j]["score"]<=0) or \
					(modus == "fitted" and j==0) or \
					(modus == "overlapped" and (j==0 or i==0)):
					if [r["i"],r["j"],"middle"] not in r["path"]:
						r["path"] += [[r["i"],r["j"],"middle"]]
					r["done"]=True
					new_result.append(r)
					continue
				all_done=False
				for direction in s[i][j]["dir"]:
					new_v = r["v"]
					new_w = r["w"]
					new_i = r["i"]
					new_j = r["j"]
					new_path = r["path"]+[[new_i,new_j,"middle"]]
					new_match = r["match"]
					new_mismatch = r["mismatch"]
					new_indel = r["indel"]

					if direction == "↓":
						new_i = i-1
						new_v = v[i-1]+r["v"]
						new_w = "-"+r["w"]
						new_indel+=1
					elif direction == "→":
						new_j = j-1
						new_v = "-"+r["v"]
						new_w = w[j-1]+r["w"]
						new_indel+=1
					elif direction in [ "⇘", "⤰" ]:
						new_v = v[i-1]+r["v"]
						new_w = w[j-1]+r["w"]
						new_i = i-1
						new_j = j-1
						if direction == "⇘":
							new_match+=1
						else:
							new_mismatch+=1
					else:
						raise ValueError("unknown direction '",direction,"'")
					new_result.append({ "v":new_v, 
										"w":new_w, 
										"i":new_i, 
										"j":new_j,
										"path":new_path,
										"match":new_match, 
										"mismatch":new_mismatch, 
										"indel":new_indel })
					if only_one_result:
						break
			if not all_done :
				result = deepcopy(new_result)
		if len(result)==0 or not result[0]["done"]:
			eprint("ERROR: did not found a valid path while backtrack, please increase the --maxdiag")
			exit(1)
		return {"score": max_score, "alignments": [ { 0:r["v"], 1:r["w"], "match":r["match"], "mismatch":r["mismatch"], "indel":r["indel"] } for r in result], "paths":[r["path"] for r in result]}

	@staticmethod
	def floor(x):
		return int(x-.5)

	@staticmethod
	def ceil(x):
		return int(x+.5)

	@staticmethod
	def buildAlignmentMatrix_affineGapPen(v:str, w:str, scoring:dict, modus="needlemanwunsch", only_one_result = True, maxdiag=200, maxmemp=95, progress=False):
		"""
		global alignment of 2 sequences
		modus = ["needlemanwunsch","smithwaterman","fitted"]
			needlemanwunsch = global alignment
			smithwaterman = local alignment
			fitted = local alignment of v (longer sequence) and global alignment w (shorter sequence)
			overlapped = maximal overlap of suffix of v and prefix of w sequences
		"""
		if modus not in ["needlemanwunsch","smithwaterman","fitted","overlapped"]:
			raise ValueError("unknown modus '",modus,"' should be either needlemanwunsch (=global) or smithwaterman (=local) or fitted (local v and global w) or overlapped (suffix of v with prefix of w")

		eprint("building alignment matrix (with affine gap penalty)...")

		# 1. build the border (top and left) 
		s={}
		if maxdiag>0:

			#if maxdiag < abs(len(w)-len(v)) :
			#	print(f"WARNING maxdiag={maxdiag} is too low for input string, correcting to maxdiag={abs(len(w)-len(v))}")
			#	maxdiag = abs(len(w)-len(v))

			coef = len(w)/len(v)
			
			max_interaction =  min(alignment.ceil(coef*int(len(v)/2))+maxdiag,len(w))+1 - max(alignment.floor(coef*int(len(v)/2))-maxdiag,0)
			eprint(f"the --maxdiag of {maxdiag} results in a maximal gap length of {max_interaction} ({min([100,int(100*100*max_interaction/(max([len(v),len(w)])))/100])}%)")

			s["middle"] = {i:{j:{"score":0,"dir":[]} for j in range( max( alignment.floor(coef*i)-maxdiag,0),min( alignment.ceil(coef*i)+maxdiag,len(w))+1 )} for i in range(len(v)+1) if i % 2000 or test_virtual_mem(maxmemp)}
			test_virtual_mem(maxmemp)

			s["lower_down"] = {i:{j:{"score":0,"dir":[]} for j in range( max( alignment.floor(coef*i)-maxdiag,0),min( alignment.ceil(coef*i)+maxdiag,len(w))+1 )} for i in range(len(v)+1) if i % 2000 or test_virtual_mem(maxmemp)}
			test_virtual_mem(maxmemp)

			s["upper_right"] = {i:{j:{"score":0,"dir":[]} for j in range( max( alignment.floor(coef*i)-maxdiag,0),min( alignment.ceil(coef*i)+maxdiag,len(w))+1 )} for i in range(len(v)+1) if i % 2000 or test_virtual_mem(maxmemp)}
			test_virtual_mem(maxmemp)

			fa = list(s["middle"].keys())[0]
			faa = list(s["middle"].keys())[1]

			for i in s["middle"].keys():
				if len(s["middle"][i].keys())>0:
					fi = list(s["middle"][i].keys())[0]
					s["middle"][i][fi]["dir"] = ["↓"]
			for j in s["middle"][fa].keys():
				s["middle"][fa][j]["dir"] = ["→"]

			for i in s["middle"].keys():
				if len(s["middle"][i].keys())>0:
					fi = list(s["middle"][i].keys())[0]
					s["lower_down"][i][fi]["dir"] = ["↓"]
			for j in s["middle"][fa].keys():
				s["upper_right"][fa][j]["dir"] = ["→"]

			for i in s["middle"].keys():
				if len(s["middle"][i].keys())>1:
					fii = list(s["middle"][i].keys())[1]
					s["upper_right"][i][fii]["dir"] = ["*"]
			for j in s["middle"][faa].keys():
				s["lower_down"][faa][j]["dir"] = ["*"]

		else:
			s["middle"] = {i:{j:{"score":0,"dir":[]} for j in range(len(w)+1)} for i in range(len(v)+1)}
			s["lower_down"] = {i:{j:{"score":0,"dir":[]} for j in range(len(w)+1)} for i in range(len(v)+1)}
			s["upper_right"] = {i:{j:{"score":0,"dir":[]} for j in range(len(w)+1)} for i in range(len(v)+1)}

			for i in range(len(v)+1):
				s["middle"][i][0]["dir"] = ["↓"]
			for j in range(len(w)+1):
				s["middle"][0][j]["dir"] = ["→"]

			for i in range(1,len(v)+1):
				s["lower_down"][i][0]["dir"] = ["↓"]
			for j in range(1,len(w)+1):
				s["upper_right"][0][j]["dir"] = ["→"]

			for i in range(len(v)+1):
				s["upper_right"][i][1]["dir"] = ["*"]
			for j in range(len(w)+1):
				s["lower_down"][1][j]["dir"] = ["*"]

		if modus == "needlemanwunsch":
			for i in range(1,len(v)+1):
				if 0 in s["middle"][i]:
					#s["lower_down"][i][0] = {"score": scoring["open"] + (i-1)*scoring["extend"], "dir":["↓"]} 
					s["middle"][i][0] = {"score": scoring["open"] + (i-1)*scoring["extend"], "dir":["↓"]} 
			for j in range(len(w)+1):
				if j in s["middle"][0]:
					#s["upper_right"][0][j] = {"score": scoring["open"] + (j-1)*scoring["extend"], "dir":["→"]} 
					s["middle"][0][j] = {"score": scoring["open"] + (j-1)*scoring["extend"], "dir":["→"]} 
		elif modus=="fitted" or modus=="overlapped":
			for j in range(1,len(w)+1):
				if j in s["middle"][0]:
					s["middle"][0][j] = {"score": scoring["open"] + (j-1)*scoring["extend"], "dir":["→"]}

		s["middle"][0][0] = {"score":0,"dir":[""]} 

		eprint("forwardpropagation in alignment matrix (with affine gap penalty)...")

		# 2. loop from 1,1 all the way to n,n: at each step find the best solution of the previous results 
		pb=extra_packages["progressbar"].progressbar(range(1,len(v)+1),widgets=progressbar_widgets) if "progressbar" in list(extra_packages.keys()) and progress else range(1,len(v)+1)
		
		for i in pb:
			for j in range(1,len(w)+1):
				if j not in s["middle"][i]:
					continue

				#print(i,j,v[i-1],w[j-1])

				is_match = (v[i-1] == w[j-1])
				if "scoringmatrix" in scoring:
					score_match_or_missmatch = scoring["scoringmatrix"][v[i-1]][w[j-1]]
				else:
					score_match_or_missmatch = scoring["match"] if is_match else scoring["missmatch"]

				####### -- lower_down -- #######
				if i>=1: # open and extend
					cur_entry = s["lower_down"][i][j]

					cur_entry["score"] = max([ 0 if modus == "smithwaterman" else -float('inf'),
											   s["lower_down"][i-1][j]["score"] + scoring["extend"]		if i>1 and j in s["middle"][i-1] else -float('inf'), 
											   s["middle"][i-1][j]["score"] + scoring["open"] 			if j in s["middle"][i-1] else -float('inf') ])

					cur_entry["dir"] = []
					if j in s["middle"][i-1] and cur_entry["score"] == s["middle"][i-1][j]["score"] + scoring["open"]:
						if only_one_result:
							cur_entry["dir"] = ["*"]
						else:
							cur_entry["dir"].append("*")
					if i>1 and j in s["middle"][i-1] and cur_entry["score"] == s["lower_down"][i-1][j]["score"] + scoring["extend"]:
						if only_one_result:
							cur_entry["dir"] = ["↓"]
						else:
							cur_entry["dir"].append("↓")

				####### -- upper_right -- #######
				if j>=1: # open and extend
					cur_entry = s["upper_right"][i][j]

					cur_entry["score"] = max([ 0 if modus == "smithwaterman" else -float('inf'),
											   s["upper_right"][i][j-1]["score"] + scoring["extend"] 	if j>1 and j-1 in s["middle"][i] else -float('inf'), 
											   s["middle"][i][j-1]["score"] + scoring["open"] 			if j-1 in s["middle"][i] else -float('inf') ])
					cur_entry["dir"] = []
					if j-1 in s["middle"][i] and cur_entry["score"] == s["middle"][i][j-1]["score"] + scoring["open"]:
						if only_one_result:
							cur_entry["dir"] = ["*"]
						else:
							cur_entry["dir"].append("*")
					if j>1 and j-1 in s["middle"][i] and cur_entry["score"] == s["upper_right"][i][j-1]["score"] + scoring["extend"]:
						if only_one_result:
							cur_entry["dir"] = ["→"]
						else:
							cur_entry["dir"].append("→")

				####### -- middle -- #######

				cur_entry = s["middle"][i][j]

				cur_entry["score"] = max([ 0 if modus == "smithwaterman" else -float('inf'),
										   s["upper_right"][i][j]["score"] 								if j in s["middle"][i] else -float('inf'), 
										   s["lower_down"][i][j]["score"] 								if j in s["middle"][i] else -float('inf'),
										   s["middle"][i-1][j-1]["score"] + score_match_or_missmatch 	if j-1 in s["middle"][i-1] else -float('inf') ])
				cur_entry["dir"] = []
				if j-1 in s["middle"][i-1] and cur_entry["score"] == s["middle"][i-1][j-1]["score"] + score_match_or_missmatch:
					if only_one_result:
						cur_entry["dir"] = ["⇘" if is_match else "⤰"]
					else:
						cur_entry["dir"].append("⇘" if is_match else "⤰")
				if j in s["middle"][i] and cur_entry["score"] == s["upper_right"][i][j]["score"]:
					if only_one_result:
						cur_entry["dir"] = ["→"]
					else:
						cur_entry["dir"].append("→")
				if j in s["middle"][i] and cur_entry["score"] == s["lower_down"][i][j]["score"]:
					if only_one_result:
						cur_entry["dir"] = ["↓"]
					else:
						cur_entry["dir"].append("↓")
				
		return s

	@staticmethod
	def backtrack_affineGapPen(s:list, v:str, w:str, modus="needlemanwunsch", only_one_result=True, progress=False):
		"""
		backtrack the result of buildAlignmentMatrix
		"""
		if modus not in ["needlemanwunsch","smithwaterman","fitted","overlapped"]:
			raise ValueError("unknown modus '",modus,"' should be either needlemanwunsch (=global) or smithwaterman (=local) or fitted (local v and global w) or overlapped")

		eprint("backtracking...")
		# result hold the queue that needs to be worked on (do until the break statement is satifsfied)
		max_score=s["middle"][len(v)][len(w)]["score"]
		if modus == "needlemanwunsch":
			# take the value (bottom right) as start values
			result=[{ "v":"", "w":"", "i":len(v), "j":len(w), "layer":"middle", "path":[], "match":0, "mismatch":0, "indel":0, "done":False }]
		elif modus == "smithwaterman":
			# find the maximal value of all fields
			result=[]
			for i in range(len(v)+1):
				for j in range(len(w)+1):
					if j not in s["middle"][i]:
						continue
					if s["middle"][i][j]["score"] > max_score:
						max_score=s["middle"][i][j]["score"]
						result=[]
					if s["middle"][i][j]["score"] == max_score:
						result.append({"v":"", "w":"", "i":i, "j":j, "layer":"middle", "path":[], "match":0, "mismatch":0, "indel":0, "done":False })
		elif modus == "fitted":
			# find the maximal value of the right (!) side
			result=[]
			for i in range(len(s["middle"])):
				j=len(s["middle"][i])-1
				if s["middle"][i][j]["score"] > max_score:
					max_score=s["middle"][i][j]["score"]
					result=[]
				if s["middle"][i][j]["score"] == max_score:
					result.append({"v":"", "w":"", "i":i, "j":j, "layer":"middle", "path":[], "match":0, "mismatch":0, "indel":0, "done":False })
		elif modus == "overlapped":
			# find the maximal value of the bottom (!) side
			result=[]
			i=len(v)
			for j in list(s["middle"][i].keys()): # range(len(s[i])):
				if j not in s["middle"][i]:
					continue
				if s["middle"][i][j]["score"] > max_score:
					max_score=s["middle"][i][j]["score"]
					result=[]
				if s["middle"][i][j]["score"] == max_score:
					result.append({"v":"", "w":"", "i":i, "j":j, "layer":"middle", "path":[], "match":0, "mismatch":0, "indel":0, "done":False })

		bar = extra_packages["progressbar"].ProgressBar(max_value=len(v),widgets=progressbar_widgets) if "progressbar" in list(extra_packages.keys()) and progress else None
		
		all_done=False # nothing changes anymore -> return
		while not all_done:
			# print(result)
			new_result=[]
			all_done=True
			for r in result:
				#print(r)
				i = r["i"]
				j = r["j"]
				layer = r["layer"]
				if "progressbar" in list(extra_packages.keys()) and progress:
					bar.update(len(v)-r["i"])
				if (i==0 and j==0) or \
						(modus == "smithwaterman" and s["middle"][i][j]["score"]<=0) or \
						(modus == "fitted" and j==0) or \
						(modus == "overlapped" and (j==0 or i==0)):
					#print("done",i,j,layer)
					if [r["i"],r["j"],r["layer"]] not in r["path"]:
						r["path"] += [[r["i"],r["j"],r["layer"]]]
					r["done"]=True
					new_result.append(r)
					continue

				all_done=False

				#if layer=="lower_down" and i==1 or layer=="upper_right" and j==1:
				#	s[layer][i][j]["dir"]=["*"]
				#print("not done",i,j,layer,s[layer][i][j])
				for direction in s[layer][i][j]["dir"]:
					#print("direction",direction,layer,i,j)
					new_v = r["v"]
					new_w = r["w"]
					new_i = r["i"]
					new_j = r["j"]
					new_path = r["path"]+[[new_i,new_j,r["layer"]]]
					new_layer = r["layer"]
					new_match = r["match"]
					new_mismatch = r["mismatch"]
					new_indel = r["indel"]

					if layer == "middle":
						if direction == "↓":
							new_j = j
							new_i = i
							new_layer="lower_down"
							new_indel += 1
						elif direction == "→":
							new_j = j
							new_i = i
							new_layer="upper_right"
							new_indel += 1
						elif direction in [ "⇘", "⤰" ]:
							new_v = v[i-1]+r["v"]
							new_w = w[j-1]+r["w"]
							new_i = i-1
							new_j = j-1
							new_layer="middle"
							if direction == "⇘":
								new_match += 1
							else:
								new_mismatch += 1
						else:
							raise ValueError("unknown direction '",direction,"'")
					elif layer == "upper_right":
						if direction == "→":
							new_j = j-1
							new_v = "-"+r["v"]
							new_w = w[j-1]+r["w"]
							new_layer="upper_right"
							new_indel += 1
						elif direction == "*":
							new_i = i
							new_j = j-1
							new_v = "-"+r["v"]
							new_w = w[j-1]+r["w"]
							new_layer="middle"
							#new_indel += 1
					elif layer == "lower_down":
						if direction == "↓":
							new_i = i-1
							new_v = v[i-1]+r["v"]
							new_w = "-"+r["w"]
							new_layer="lower_down"
							new_indel += 1
						elif direction == "*":
							new_i = i-1
							new_j = j
							new_v = v[i-1]+r["v"]
							new_w = "-"+r["w"]
							new_layer="middle"
							#new_indel += 1
					new_result.append({ "v":new_v,
										"w":new_w,
										"i":new_i,
										"j":new_j,
										"layer":new_layer,
										"path":new_path, 
										"match":new_match, 
										"mismatch":new_mismatch, 
										"indel":new_indel })
					#print("new_result",new_result)
					if only_one_result:
						break
			if not all_done:
				#print("not all_done",new_result)
				result = deepcopy(new_result)

		if len(result)==0 or not result[0]["done"]:
			eprint("ERROR: did not found a valid path while backtrack, please increase the --maxdiag")
			exit(1)
		return {"score": max_score, "alignments": [ { 0:x["v"], 1:x["w"], "match":r["match"], "mismatch":r["mismatch"], "indel":r["indel"] } for x in result ], "paths":[ x["path"] for x in result ] }

	@staticmethod
	def print_alignmentMatrix(s, v, w, bt=None, layer="", roundprecision = 2):

		eprint("# Legend: ⇘: match, ⤰: missmatch, ↓: gap in v (first sequence), →: gap in w, *: return, -: empty character")
		pad=max([len(str(round(s[i][j]["score"],roundprecision))) for i in range(len(v)+1) for j in range(len(w)+1) if j in s[i]])
		eprint("".rjust(pad),end=" ")
		eprint("".rjust(pad),end=" ")
		eprint("-".rjust(pad),end=" ")
		eprint("".rjust(pad),end=" ")

		for i in range(len(w)):
			eprint(w[i].rjust(pad),end=" ")
			eprint("".rjust(pad),end=" ")
		eprint("")

		for i in range(len(v)+1):
			eprint("".rjust(pad),end=" ")
			for j in range(len(w)+1):
				if j not in s[i]:
					eprint("".rjust(pad),end=" ")
					eprint("".rjust(pad),end=" ")
					continue
				if "⤰" in s[i][j]["dir"] or "⇘" in s[i][j]["dir"]:
					a=""
					if "⤰" in s[i][j]["dir"]:
						a=a+"⤰"
					if "⇘" in s[i][j]["dir"]:
						a=a+"⇘"
					eprint(a.rjust(pad),end=" ")
				else:
					eprint("".rjust(pad),end=" ")

				if "↓" in s[i][j]["dir"]:
					eprint("↓".rjust(pad),end=" ")
				elif "*" in s[i][j]["dir"]:
					eprint("*".rjust(pad),end=" ")
				else:
					eprint("".rjust(pad),end=" ")

			eprint("")

			if i == 0:
				eprint("-".rjust(pad),end=" ")
			else:
				eprint(v[i-1].rjust(pad),end=" ")

			for j in range(len(w)+1):
				if j not in s[i]:
					eprint("".rjust(pad),end=" ")
					eprint("".rjust(pad),end=" ")
					continue

				if "→" in s[i][j]["dir"]:
					eprint("→".rjust(pad),end=" ")
				elif "*" in s[i][j]["dir"]:
					eprint("*".rjust(pad),end=" ")
				else:
					eprint("".rjust(pad),end=" ")
				#print([i,j,layer])
				c=False
				if bt is not None and [i,j,layer] in bt["paths"][0]:
					c=True
				eprint( (bc.HIGHLIGHT if c else "") + str(round(s[i][j]["score"],roundprecision)).rjust(pad) + (bc.NC if c else ""),end=" ")
			eprint("")

	@staticmethod
	def HammingDistance(v,w):
		"""
		number of non-identical characters of 2 strings (no alignment is done here !)
		"""
		score = 0
		if len(w) < len(v):
			w = w+("-"*(len(v)-len(w)))
		elif len(v) < len(w):
			v = v+("-"*(len(w)-len(v)))
		for i in range(len(v)):
			if v[i] != w[i]:
				score = score + 1
		return score

	@staticmethod
	def Levenshtein_editDistance(v,w):
		"""
		minimum number of edit operations (insert, delete, change) to go from v to w
		"""
		s = alignment.buildAlignmentMatrix(v=v, w=w, scoring={"match":0, "missmatch":-1, "open":-1}, modus="needlemanwunsch", maxdiag=0)
		bt = alignment.backtrack(s, v=v, w=w, modus="needlemanwunsch")
		# print(bt)
		return alignment.HammingDistance( bt["alignments"][0][0], bt["alignments"][0][1] )

def test():

	#v="PRTEINS"
	#w="PRTWPSEIN"
	#scoring={"scoringmatrix":alignment.IUB, "open":-15, "extend":-.1}
	#s = alignment.buildAlignmentMatrix_affineGapPen(v=v, w=w, scoring=scoring, modus="needlemanwunsch", maxdiag=0)
	#bt = alignment.backtrack_affineGapPen(s, v=v, w=w, modus="needlemanwunsch")

	v="PRTEINS"
	w="PRTWPSEIN"
	#scoring={"scoringmatrix":alignment.PAM250, "open":-5}
	scoring={"match":1,"missmatch":-1,"open":-1}
	for modus in ["needlemanwunsch","smithwaterman","fitted","overlapped"]:
		eprint("----",modus,"----")
		s = alignment.buildAlignmentMatrix(v=v, w=w,scoring=scoring, modus=modus, maxdiag=0)
		alignment.print_alignmentMatrix(s,v,w)
		bt = alignment.backtrack(s, v=v, w=w, modus=modus)
		eprint(bt)

	eprint("---------------")
	v="PRTEINS"
	w="PRTWPSEIN"
	scoring={"scoringmatrix":alignment.BLOSUM62, "open":-11, "extend":-1}
	s = alignment.buildAlignmentMatrix_affineGapPen(v=v, w=w, scoring=scoring, modus="needlemanwunsch", maxdiag=0)
	for layer in s:
		eprint(layer)
		alignment.print_alignmentMatrix(s[layer],v,w)
	bt = alignment.backtrack_affineGapPen(s, v=v, w=w, modus="needlemanwunsch")
	eprint("needlemanwunsch: BLOSUM62 + extend=-1, opening=-11",bt)


	eprint("---------------")

	eprint("global needlemanwunsch alignment + affine gap penalty:")
	v="YAFDLGYTCMFPVLLGGGELHIVQKETYTAPDEIAHYIKEHGITYIKLTPSLFHTIVNTASFAFDANFESLRLIVLGGEKIIPIDVIAFRKMYGHTEFINHYGPTEATIGA"
	w="AFDVSAGDFARALLTGGQLIVCPNEVKMDPASLYAIIKKYDITIFEATPALVIPLMEYIYEQKLDISQLQILIVGSDSCSMEDFKTLVSRFGSTIRIVNSYGVTEACIDS"
	for o in range(5,21,5):
		scoring={"scoringmatrix":alignment.BLOSUM62, "open":-o, "extend":-1}
		s = alignment.buildAlignmentMatrix_affineGapPen(v=v, w=w, scoring=scoring, modus="needlemanwunsch", maxdiag=0)
		bt = alignment.backtrack_affineGapPen(s, v=v, w=w, modus="needlemanwunsch")
		eprint("needlemanwunsch: BLOSUM62 + extend=-1, opening penalty:",-o,"->",bt)

	for e in range(5,21,5):
		scoring={"scoringmatrix":alignment.BLOSUM62, "open":-11, "extend":-e}
		s = alignment.buildAlignmentMatrix_affineGapPen(v=v, w=w, scoring=scoring, modus="needlemanwunsch", maxdiag=0)
		bt = alignment.backtrack_affineGapPen(s, v=v, w=w, modus="needlemanwunsch")
		eprint("needlemanwunsch: BLOSUM62 + opening=-11, extend penalty:",-e,"->",bt)

	eprint("---------------")
	eprint("global needlemanwunsch alignment + affine gap penalty:")
	v="YHFDVPDCWAHRYWVENPQAIAQMEQICFNWFPSMMMKQPHVFKVDHHMSCRWLPIRGKKCSSCCTRMRVRTVWE"
	w="YHEDVAHEDAIAQMVNTFGFVWQICLNQFPSMMMKIYWIAVLSAHVADRKTWSKHMSCRWLPIISATCARMRVRTVWE"
	scoring={"scoringmatrix":alignment.BLOSUM62, "open":-11, "extend":-1}
	s = alignment.buildAlignmentMatrix_affineGapPen(v=v, w=w,scoring=scoring, modus="needlemanwunsch", maxdiag=0)
	bt = alignment.backtrack(s["middle"], v=v, w=w, modus="needlemanwunsch")
	eprint("sanity check needlemanwunsch + affine gap penalty:",bt["score"]==144 or exit("test failed"))

	eprint("---------------")
	eprint("local smithwaterman alignment + affine gap penalty:")
	v="YAFDLGYTCMFPVLLGGGELHIVQKETYTAPDEIAHYIKEHGITYIKLTPSLFHTIVNTASFAFDANFESLRLIVLGGEKIIPIDVIAFRKMYGHTEFINHYGPTEATIGA"
	w="AFDVSAGDFARALLTGGQLIVCPNEVKMDPASLYAIIKKYDITIFEATPALVIPLMEYIYEQKLDISQLQILIVGSDSCSMEDFKTLVSRFGSTIRIVNSYGVTEACIDS"
	scoring={"scoringmatrix":alignment.BLOSUM62, "open":-11, "extend":-1}
	s = alignment.buildAlignmentMatrix_affineGapPen(v=v, w=w,scoring=scoring, modus="smithwaterman", maxdiag=0)
	bt = alignment.backtrack(s["middle"], v=v, w=w, modus="smithwaterman")
	eprint("sanity check smithwaterman + affine gap penalty:",bt["score"]==100 or exit("test failed"))

	eprint("---------------")
	eprint("global needlemanwunsch alignment:")
	v="ILYPRQSMICMSFCFWDMWKKDVPVVLMMFLERRQMQSVFSWLVTVKTDCGKGIYNHRKYLGLPTMTAGDWHWIKKQNDPHEWFQGRLETAWLHSTFLYWKYFECDAVKVCMDTFGLFGHCDWDQQIHTCTHENEPAIAFLDLYCRHSPMCDKLYPVWDMACQTCHFHHSWFCRNQEMWMKGDVDDWQWGYHYHTINSAQCNQWFKEICKDMGWDSVFPPRHNCQRHKKCMPALYAGIWMATDHACTFMVRLIYTENIAEWHQVYCYRSMNMFTCGNVCLRCKSWIFVKNYMMAPVVNDPMIEAFYKRCCILGKAWYDMWGICPVERKSHWEIYAKDLLSFESCCSQKKQNCYTDNWGLEYRLFFQSIQMNTDPHYCQTHVCWISAMFPIYSPFYTSGPKEFYMWLQARIDQNMHGHANHYVTSGNWDSVYTPEKRAGVFPVVVPVWYPPQMCNDYIKLTYECERFHVEGTFGCNRWDLGCRRYIIFQCPYCDTMKICYVDQWRSIKEGQFRMSGYPNHGYWFVHDDHTNEWCNQPVLAKFVRSKIVAICKKSQTVFHYAYTPGYNATWPQTNVCERMYGPHDNLLNNQQNVTFWWKMVPNCGMQILISCHNKMKWPTSHYVFMRLKCMHVLMQMEYLDHFTGPGEGDFCRNMQPYMHQDLHWEGSMRAILEYQAEHHRRAFRAELCAQYDQEIILWSGGWGVQDCGFHANYDGSLQVVSGEPCSMWCTTVMQYYADCWEKCMFA"
	w="ILIPRQQMGCFPFPWHFDFCFWSAHHSLVVPLNPQMQTVFQNRGLDRVTVKTDCHDHRWKWIYNLGLPTMTAGDWHFIKKHVVRANNPHQWFQGRLTTAWLHSTFLYKKTEYCLVRHSNCCHCDWDQIIHTCAFIAFLDLYQRHWPMCDKLYCHFHHSWFCRNQEMSMDWNQWFPWDSVPRANCLEEGALIALYAGIWANSMKRDMKTDHACTVRLIYVCELHAWLKYCYTSINMLCGNVCLRCKSWIFVKLFYMYAPVVNTIEANSPHYYKRCCILGQGICPVERKSHCEIYAKDLLSFESCCSQKQNCYTDNWGLEYRLFFQHIQMECTDPHANRGWTSCQTAKYWHFNLDDRPPKEFYMWLQATPTDLCMYQHCLMFKIVKQNFRKQHGHANPAASTSGNWDSVYTPEKMAYKDWYVSHPPVDMRRNGSKMVPVWYPPGIWHWKQSYKLTYECFFTVPGRFHVEGTFGCNRWDHQPGTRRDRQANHQFQCPYSDTMAIWEHAYTYVDQWRSIKEGQMPMSGYPNHGQWNVHDDHTNEQERSPICNQPVLAKFVRSKNVSNHEICKKSQTVFHWACEAQTNVCERMLNNQHVAVKRNVTFWWQMVPNCLWSCHNKMTWPTRPEQHRLFFVKMRLKCMHEYLDVAPSDFCRNMQAYMHSMRAILEYQADFDLKRRLRAIAPMDLCAQYDQEIILWSGGYIYDQSLQVVSCEGCSYYADCYVKCINVKEKCMFA"
	scoring={"scoringmatrix":alignment.BLOSUM62, "open":-5}
	s = alignment.buildAlignmentMatrix(v=v, w=w,scoring=scoring, modus="needlemanwunsch", maxdiag=0)
	#alignment.print_alignmentMatrix(s,v,w)
	bt = alignment.backtrack(s, v=v, w=w, modus="needlemanwunsch")
	eprint("sanity check needlemanwunsch:",bt["score"]==1555 or exit("test failed"))

	eprint("---------------")
	eprint("local smithwaterman alignment:")
	v="AMTAFRYRQGNPRYVKHFAYEIRLSHIWLLTQMPWEFVMGIKMPEDVFQHWRVYSVCTAEPMRSDETYEQKPKPMAKWSGMTIMYQAGIIRQPPRGDRGVSDRNYSQCGKQNQAQLDNNPTWTKYEIEWRVQILPPGAGVFEGDNGQNQCLCPNWAWEQPCQWGALHSNEQYPNRIHLWAPMSKLHIKIEKSSYNRNAQFPNRCMYECEFPSYREQVDSCHYENVQIAFTIFSGAEQKRKFCSCHFWSNFIDQAVFSTGLIPWCYRRDDHSAFFMPNWNKQYKHPQLQFRVAGEGTQCRPFYTREMFTKVSAWRIAGRFAGPYERHHDAHLELWYQHHKVRTGQQLGIIWNNRDKTRNPCPFSAYYNKLPWWKINQNAFYNCLQNIAHSTHDETHEFNPVKCIDWLQGTMVPTECKKGFVHEKCECYRNPGPPLHDMYHQMEDIFGVRFDCLTGWKHLSDYNPCQERRNINDFYIFAYEIAPAVKNLVLSPQPLADATKKCAFNYTPLDQSPVVIACKWYIHQPICMLLIVLICAMDKYNAHMIVIRTTEGQQPMHACRMTEGPGMCMKEPLVTFTLPAQWQWPNHEFKYVYMYVLNYHLSQYTYTDEGHAGGQHYSFNVAVDVGMAWGHNRCYCQPACYSQQETQTRTIDYEKWQYMKHQAFKWGLWFCEQERHAWFKGQNRCEMFTAKMTRMGADSNLDQYKLMLAQNYEEQWEQPIMECGMSEIIEIDPPYRSELIFTFWPFCTYSPWQNLIKCRCNNVIEEMDQCVPLTFIGFGVKQAGGIQAWAFYKEEWTSTYYLMCQCMKSDKAQYPYEIILFWMQPMDTGEQEPPQQNMWIFLPHSWFFDWCCNAPWSEICSSRHDHGQCQDAFYPCELFTVFDDIFTAEPVVCSCFYDDPM"
	w="WQEKAVDGTVPSRHQYREKEDRQGNEIGKEFRRGPQVCEYSCNSHSCGWMPIFCIVCMSYVAFYCGLEYPMSRKTAKSQFIEWCDWFCFNHWTNWAPLSIVRTSVAFAVWGHCWYPCGGVCKTNRCKDDFCGRWRKALFAEGPRDWKCCKNDLQNWNPQYSQGTRNTKRMVATTNQTMIEWKQSHIFETWLFCHVIIEYNWSAFWMWMNRNEAFNSIIKSGYPKLLLTQYPLSQGSTPIVKPLIRRDQGKFWAWAQMWWFREPTNIPTADYCHSWWQSRADLQNDRDMGPEADASFYVEFWYWVRCAARTYGQQLGIIWNNRLKTRNPCPYSADGIQNKENYVFWWKNMCTKSHIAFYYCLQNVAHYTHDVTAEFNPVKCIDWLQGHMVLSSWFKYNTECKKLFVHEKCECYRMFCGVVEDIFGVRFHTGWKHLSTAKPVPHVCVYNPSVQERRNINDFYIFYEIAPAVKNLVLSAQPLHDYTKKCAFNYTPITITRIISTRNQIIWAHVVIACQFYSPHQMLLIELAMDKYCADMNVRRSTEGHQPMHACRSTFGPGMAAKEPLVTFTLVAFWQWPNHEFQYVYMYTEDKIIQIGPHLSNGCEMVEYCVDCYAKRPCYRAYSAEAQYWRMITEAEDYSYKTRNAIAATATVRGQYCHPFRWLGIVWMAHHDCFFANECGTICIPQMAEMRPPETTPYEIDIIFMMFWKEHMSTTILDVVGMYRPATFSHWHDAHHQCEPYLTPLMCQSKLVFDAAFTQVGVKGVWYHTEKLELMAGFNHMKFKKEEAQQSCFYWFQDCPDYDPPDAVRKTDEKHIRAHGEIWWLMRYYCMYHILHIASRHEWMHLRWDQACTNPGYELFEFIPWVLRRYVVYDKIRYNYSYRNSASMEFV"
	scoring={"scoringmatrix":alignment.PAM250, "open":-5}
	s = alignment.buildAlignmentMatrix(v=v, w=w,scoring=scoring, modus="smithwaterman", maxdiag=0)
	bt = alignment.backtrack(s, v=v, w=w, modus="smithwaterman")
	eprint("sanity check smithwaterman:",bt["score"]==1062 or exit("test failed"))

	eprint("---------------")
	eprint("edit distance:")
	v="GGACRNQMSEVNMWGCWWASVWVSWCEYIMPSGWRRMKDRHMWHWSVHQQSSPCAKSICFHETKNQWNQDACGPKVTQHECMRRRLVIAVKEEKSRETKMLDLRHRMSGRMNEHNVTLRKSPCVKRIMERTTYHRFMCLFEVVPAKRQAYNSCDTYTMMACVAFAFVNEADWWKCNCAFATVPYYFDDSCRMVCGARQCYRLWQWEVNTENYVSIEHAEENPFSKLKQQWCYIPMYANFAWSANHMFWAYIANELQLDWQHPNAHPIKWLQNFLMRPYHPNCGLQHKERITPLHKSFYGMFTQHHLFCKELDWRIMAHANRYYCIQHGWHTNNPMDPIDTRHCCMIQGIPKRDHHCAWSTCDVAPLQGNWMLMHHCHHWNRVESMIQNQHEVAAGIKYWRLNRNGKLPVHTADNYGVLFQRWWFLGWYNFMMWHYSLHFFAVNFYFPELNAGQMPRFQDDQNRDDVYDTCIWYFAWSNTEFMEVFGNMMMYSRPMTKMGFHGMMLPYIAINGLRSISHVNKGIGPISGENCNLSTGLHHYGQLRMVMCGYCTPYRTEVKNQREMISAVHCHQHIDWRWIWCSGHWFGSNKCDLRIEDLQNYEPAKNKSNWPYMKECRKTEPYQDNIETMFFHQHDLARDSGYIANGWHENCRQHQDFSNTFAGGHKGTPKGEHMRRSLYVWDTDCVEKCQWVPELFALCWWTPLPDGVPVMLGTYRQYMFGLVVLYWFEVKYSCHNSWDYYNFHEGTMKDSDPENWCFWGMQIIQFHDHGKPEFFQDPMKQIIKTECTAYNSFMMGHIGKTTIVYLVSYIGRLWMKSCCLTWPPYATAPIKWAEETLLDFGQGPHPKYACHFTHQNMIRLAKLPMYWLWKLMFHE"
	w="GMWGFVQVSTQSRFRHMWHWSVHQQSSECAKSICHHEWKNQWNQDACGPKVTQHECMANMPMHKCNNWFWRLVIAVKEEKVRETKMLDLIHRHWLVLNQGRMNEHNVTLRKSPCVKRIMHKWKSRTTFHRFMCLMASEVVPAKRGAQCWRQLGTYATYTVYTMMACVAFAFEYQQDNDNEADWWKCNCAFVPVYFDDSCRPVVGAFQCYRLGLPFGTGWNYAEENPFSKLKQQMHRKTMGECKNMMIWAYCANELQLPIKWGSMYHEHDFQLPPYHPNRFHKIRITILHKSFYGMFTQHHLFCKELDWRIMAWANRYYCIQHGWHTNNPDDPITRHKCMIQGGQNSRNADIRHMPVQCGNWGHAIGLEMPMPMHHCHHANRVESMIQTQHYWGPKLNRNADWWFLGWQNFEIFRMPILRWMGAYEWHYSLHFFAVNFYFPELNAGQMPRFQDDQNNNACYDVWAWSNTEFMEVNGIKKLRFGNMMMYSRPMTKMGFHGMMKSRSISHVNKGIGPISGENCSTGLHHYGQLTEVKNQREMISAVHCHQHIWCKCDLRIEPAKNKGYWPYQKEFCWRKQINSRKTEPYQVAPVINIETMFFDFWYIANGMHENCRRTGHKPNPDCVEKCQWVPELFALCWWRAMPDGVPVMLGTMFGLVVYWFEVKYSCHNSLYRRVTDYYNFHEGTMKDHEVPWNWDNEHCHDHGKAEFFFQMLKIPICDPMKAIIPSTEMVNTPWHPFSFMMGHDGKTTIVYSGSYIGRLWVPSRWKPYAPANWKMPIKWAEETLLMVPHPHFTHQQLWGTTLRLAKLPMYWLWKLMFHHLFGVK"
	eprint("sanity check Levenshtein_editDistance:",alignment.Levenshtein_editDistance(v,w)==400 or exit("test failed"))

	eprint("---------------")
	eprint("fitted alignment:")
	v="CAATCACCCCAATCCCTCAATCCTGGCCCCACGCATAGGCTAATGCCAATCGCGGCCAGGGTATAACCGCCATAACTGTGGGTCAGAAGGGATAAGTTCCACAATCCTATTTTCCTCGAGGCGCTTCGATGCGTTAACGCGTACACTCTGTCGGCCAACCGTGTGGGAGCCGAATTGGCTGGGCTGTTGAACATTCTATCAGTAGATAAACGAAGGTACATCCGAGGTTGTCGATCGACCGCGGGGTCGTAGCGCGTGCATGTTCCTTTCAGGCCCACATACTCCGGAACGGTTCATATCACGACTATTCTTGCACAATCGGACAACGGTGTACCATGGTGGACACCGTAGGAGACCAATACTGCGTAAATCATAAGCATTGGAGAGTGGACTGCTAGCGAGGCTCACCATGGAGTCTCGGTCGGCATCTCCTGACTGCTGTTCCATCGCGTTTTTCTTTTACTCACGCAATAAATCAATACCCCCTAACACAGGCCTGCTCCAGCCTTATTAAGGCCATAGTAGCTCTACATGTAGACCGAACGGAAGCACAGTTTGGTAGAAATTCTTAATCGACTATGGTCCGTGCAGGCCAAAAAAGGAATAATCTTCGAATTCTCACGCCTTCATTAGGGCGCACATGGTGGGGTAAATCACTGCACTCTGTTCGCAGTTAAGCGTTGCAATCAATATCGGCAGAACTCGGAGTCCGTATAAAGCCGCCTCAGCGTGCACACGCCCGTGCGGCACGTCATTAGACGAGGATTCCGGGGGACTGGCCTGTTCGTAATCCACTAAAACAATGGTCCTACCATCTAAAACGCACCGTGTTCCCCTCTACGGGAACCCCCTAGAT"
	w="AGAGCGCAGAGAAGTCATTAGAACATGTAGCACATCGCTTATTAAGGGTCAATACCTAAAGGGCCTAACTATACGCCACACGGAACAGCTC"
	scoring={"match":1,"missmatch":-1,"open":-1}
	s = alignment.buildAlignmentMatrix(v=v, w=w,scoring=scoring, modus="fitted", maxdiag=0)
	#alignment.print_alignmentMatrix(s,v,w)
	bt = alignment.backtrack(s, v=v, w=w, modus="fitted")
	eprint("sanity check fitted:",bt["score"] == 22 or exit("test failed"))

	eprint("---------------")
	eprint("overlapped alignment:")
	v="GCTATAAGAATAAACCACTAGATCACCTCCGGCTCGCTCACTCCTGATCATGGTTCGTGCTAACATCGCGCCGCGCTGACGCCGAATCGTTCGTAGGAGACAAGTCGACGACCTCATCTACAGGCAAAAGTTAAATTAGCTCTCGGCTAGATGTGACAATCGGAACCCTGCACCCTGCGTAATAGGGTAAATAGTCGGGAGTTGATGCACACACCTAGATATTGGCTGAATGACAGACTGCCATTCCTGCACTGGAAAGTAGAGTGCATATGTTTCGTGAGATTATGCAGGCTCTACGGTTATACTGGGCTCCACGGATTCGACCGGTACTGTTGATTGAAGACTCTTCTATAGAGGCTCTAACCGCGGAGGCCGCAACCAATCGACAATGAAGCACCCGTCGTCGGTATCGTTGGGAAGGACGACACCGTAAGGGCAGACTTTATCGTGACCCGTCTGCTTGCTAGAAAAGCCCTGGCGTTTGTACAACGTCCGTGCAGAATTAGCGTTTTTCTCAGGAAAGATGAGGGGGTTGATCATCATCTCGTTTCGCACGGGTCAAGCGCATTTTCCTACTGTTTTGGACACAGTACGTCTTCCACTGATCTCATACGGACATTACCAGCACCCTTTTGTACCTGTCGTAACTTGTGCCATTCTAGGCCCGTTTTCACTTGCGCTTATGATCATGGTTCCGCTGATCTATATGGGCCGGGTAGGGCACTCCCAGATGAAGGGGAGTAATGGTAGCCGGATCCAAGTGACGCGCCCTAGCGGCTCCGGAGTTTGATAGACGTCGTGCTATGGAGCGTTGGAGCGACAACGCGCTCGTGCTCTGGAAGGTCGCTGCTGATCCGTAA"
	w="TACTGGTCCTGACCCACCTCACTTTGATGTCCCCTTTTCTCGTTTGCGCATCAAGATCTGGCCCGCAACTATTGGCCGTGAAAGGCACTCATCAATAAAGACAGTACTCACGCGGTCGGATCCAAATGCGCGCACCGAGCGGCCCAGGAGTTGATAGCGTCGAGTAACCTATTAGGACTCGAGGCAACTCGCGCTCTCTCAGGAGGCTCGCCTGCTAGTCCGTGAACGACGGATCTTTGGTGCTGCCTTCCTATCATGACATTGCCTAATAACGAGCGGCACCTACTCCCAGGTCTTTGAAGGGATGGCTTGTTTACCCCGATTCCGAGAAATAGAGATGACTCCTAAGGAAGTAATGAAGGAAGTTCAGTGGTATGGGTATCGTTTAGTTTGCCAGGGAGATTGCCCATAACCTAAGTCCCTAATACAGCAGTAGATCTCACCATAGATGTAGGAAAGCACAGTGATTTAGACGCTTAGCCAAATACAAAGGAATGTACCCCCTCCTAACACTGAGCACCGCTTATTTACTAGTATACTCAGAGTGTGGAGCGCTGAACGTTGTGTCAACAAGAACATAAGCCGCCGTGAATGAATTTGTGAAGGGGAGTGATCATGGTTTTACTCGTGGTAGATTTGGGCAGAACCTGATTCCTCACGTGTGAATGTAATTGAAGCTGACTCCCACACATACAGGCACGATTCTTTTAGATGATGTTTTAGGAAGCGCATTTCGTATTAACACTGCCTTGCATTTGATAACCATCACTTGTTCATTACATGATCCCATAGGGCCGTGTTGTTACTTTCGTGTTAGTCGAGCAGTATGACCACCTTTTCGGCGCTTGATATGCCTCAAGACGTGCGATTCAAGGAATCAAACAAATGAACGCCGCACTGGATGACTGGG"
	scoring={"match":1,"missmatch":-2,"open":-2}
	s = alignment.buildAlignmentMatrix(v=v, w=w,scoring=scoring, modus="overlapped", maxdiag=0)
	#alignment.print_alignmentMatrix(s,v,w)
	bt = alignment.backtrack(s, v=v, w=w, modus="overlapped")
	eprint("sanity check overlapped:",bt["score"] == 13 or exit("test failed"))

	exit(0)

def main():

	parser = OptionParser(usage="usage: %prog [options] 'SEQUENCE_A' 'SEQUENCE_B'\nor: %prog [options] 'FASTAFILE'\nor: %prog [options] 'SEQUENCE_A' 'FASTAFILE'", version=f"%prog {version}")
	parser.add_option("-t", "--type", default="auto",
	                  help="comparison type: auto, nucl or prot [default: %default]")
	parser.add_option("-m", "--modus", default="needlemanwunsch",
	                  help="comparison modus: needlemanwunsch/nw (global), smithwaterman/sw (local), fitted (first longer sequence locally and second shorter sequence globally), overlapped (local suffix with local prefix), levenshtein (edit distance) [default: %default]")
	parser.add_option("-o", "--gapopen", default=-1, type="float",
	                  help="gap open penalty [default 10 for prot and 15 for nucl]")
	parser.add_option("-e", "--gapextend", default=-1, type="float",
	                  help="gap extend penalty, if 0 then affine gap penalty are disabled [default 0.1 for prot and 6.66 for nucl]")
	parser.add_option("-d", "--maxdiag", default=200, type="int",
	                  help="restricts the similarity matrix by this value up and down from the diagonal, corresponds to the maximal possible gap length [default: %default]")
	parser.add_option("-r", "--getAllEquivalentResult", action="store_true", default=False, help="if set, then all possible alignments with same score are printed")
	parser.add_option("-g", "--progress", help="print further informations progressbar, ...",
                      action="store_true")
	parser.add_option("-l", "--plot", help="print the similarity matrix build by the algorithm (only works nicely for smaller examples). You can send the output to aha to produce a html version.",
                      action="store_true")
	parser.add_option("-p", "--path", help="print matrix path (combine this with -v).",
                      action="store_true")
	parser.add_option("-x", "--test", default=False,
                      action="store_true")
	parser.add_option("--maxmemp", default=80, type="int", help="maximal virtual_memory percent (if exceeded the job is killed).")
	parser.add_option("-s", "--scoring", metavar="FILE",
	                  help="comparison scoring file or one of the predefined matrices: BLOSUM62, PAM250, IUB (match=1.9, missmatch=0), ONE (match=1, missmatch=-1). By default IUB is used for nucl and PAM250 for prot comparisons")
	(options, args) = parser.parse_args()

	if options.test:
		test()
		exit(0)

	if len(args) > 2 or len(args) == 0:
		parser.error("you need to provide 2 sequences or at least 1 fasta file")

	fastaheader=[]

	if len(args) == 1:
		eprint("reading input '",args[0],"', extracting the first 2 sequences")
		if args[0]=="-":
			num_header=0
			args=["",""]
			for line in sys.stdin:
				line = line.strip()
				if num_header >= 3:
					break
				if line[0] == ">":
					num_header+=1
					fastaheader = fastaheader + [line]
				else:
					args[num_header-1] += line.upper()
			eprint(f"loaded sequences of lengths {len(args[0])} and {len(args[1])}")
		elif isfile(args[0]):
			file1 = open(args[0], 'r')
			num_header=0
			args=["",""]
			for line in file1.readlines():
				line = line.strip()
				if num_header >= 3:
					break
				if line[0] == ">":
					num_header+=1
					fastaheader = fastaheader + [line]
				else:
					args[num_header-1] += line.upper()
			eprint(f"loaded sequences of lengths {len(args[0])} and {len(args[1])}")
		else:
			parser.error("if you provide only one input, it is expected to be a fasta file with 2 sequences")
	else:
		for argi in range(len(args)):
			if isfile(args[argi]):
				eprint("reading input '",args[argi],"', extracting the first sequence")
				file1 = open(args[argi], 'r')
				num_header=0
				args[argi]=""
				for line in file1.readlines():
					line = line.strip()
					if num_header >= 2:
						break
					if line[0] == ">":
						num_header+=1
						fastaheader = fastaheader + [line]
					else:
						args[argi]+=line.upper()
				eprint(f"loaded a sequence of length {len(args[argi])}")
			else:
				fastaheader = fastaheader + [">STDIN"]

	if fastaheader[0] == ">STDIN" and fastaheader[1] == ">STDIN":
		fastaheader = []

	test_virtual_mem(options.maxmemp)

	if len(args[0]) == 0:
		parser.error("first sequence seems to be empty")
	if len(args[1]) == 0:
		parser.error("second sequence seems to be empty")

	scoringmat = {}

	if options.type not in ["prot","nucl","auto"]:
		parser.error("invalid --type (valid are: prot, nucl)")

	if options.type == "auto":
		all_nt = True
		all_aa = True
		for a in args[0]+args[1]:
			if a.upper() not in ["A","C","G","T","N"]:
				all_nt = False
			if a.upper() not in ["G","P","A","V","L","I","M","C","F","Y","W","H","K","R","Q","N","E","D","S","T"]:
				all_aa = False

		if all_nt:
			options.type = "nucl"
		elif all_aa:
			options.type = "prot"
		else:
			options.type = "other"
			options.scoring = "ONE"

		eprint("detected --type=",options.type)

	if options.gapopen == -1:
		options.gapopen = 10 if options.type == "prot" else 15
		if options.type == "other":
			options.gapopen = 1
			
	if options.gapextend == -1:
		options.gapextend = 0.1 if options.type == "prot" else 6.66
		if options.type == "other":
			options.gapextend = 0

	options.modus = options.modus.lower()
	if options.modus == "nw":
		options.modus = "needlemanwunsch"
	if options.modus == "sw":
		options.modus = "smithwaterman"

	if options.modus not in ["needlemanwunsch", "smithwaterman", "fitted", "overlapped","levenshtein"]:
		parser.error("invalid --modus (valid are: needlemanwunsch, smithwaterman, fitted, overlapped, levenshtein)")

	if options.scoring:
		if options.scoring in "BLOSUM62":
			scoringmat = alignment.BLOSUM62
		elif options.scoring in "PAM250":
			scoringmat = alignment.PAM250
		elif options.scoring in "IUB":
			scoringmat = alignment.IUB
		elif options.scoring in "ONE":
			scoringmat = alignment.createSimpleScoringMatrix(match=1, mismatch=1, atype = options.type)
		else: # read scoring file
			scoringmat = {}
			f = open(options.scoring, "r")
			i = 0
			header=[]
			for x in f:
				spl = x.split("\t| ")
				if i==0:
					header = [ k for k in spl if k != "" ]
				else:
					what = spl[1]
					if what not in scoringmat:
						scoringmat[what]={}
					scoringmat[what]={ header[k-1]: spl[k] for k in range(2,len(spl)) }
				i=1
	else:
		options.scoring = "IUB" if options.type == "nucl" else "PAM250"
		scoringmat = alignment.IUB if options.type == "nucl" else alignment.PAM250

	scoring={"scoringmatrix":scoringmat, "open":-options.gapopen, "extend":-options.gapextend}

	if options.progress:
		eprint("scoring:"+str(scoring))

	v = args[0].upper()
	w = args[1].upper()

	for a in v:
		if a not in scoringmat:
			parser.error(f"In the first sequence the element '{a}' is not known by the scoring {options.scoring}")
			exit(1)
	for a in w:
		if a not in scoringmat:
			parser.error(f"In the second sequence the element '{a}' is not known by the scoring {options.scoring}")
			exit(1)

	if options.modus == "levenshtein":
		print(f"score: {alignment.Levenshtein_editDistance(v,w)}")
	else:
		if options.gapextend > 0:
			s = alignment.buildAlignmentMatrix_affineGapPen( v=v, w=w, scoring=scoring, modus=options.modus, only_one_result = not options.getAllEquivalentResult , maxdiag = options.maxdiag, maxmemp = options.maxmemp, progress=options.progress )
		else:
			s = alignment.buildAlignmentMatrix( v=v, w=w, scoring=scoring, modus=options.modus, maxdiag = options.maxdiag, maxmemp = options.maxmemp, progress=options.progress )

		if options.gapextend > 0:
			bt = alignment.backtrack_affineGapPen(s, v=v, w=w, modus=options.modus, only_one_result = not options.getAllEquivalentResult, progress=options.progress)
		else:
			bt = alignment.backtrack(s, v=v, w=w, modus=options.modus, only_one_result = not options.getAllEquivalentResult, progress=options.progress)

		if options.plot:
			if options.gapextend > 0:
				for layer in s:
					eprint(">layer"+str(layer))
					alignment.print_alignmentMatrix(s[layer],v,w,bt,layer)
			else:
				alignment.print_alignmentMatrix(s,v,w,bt,"middle")

		if len(fastaheader)>0:
			print(f"{fastaheader[0]} score: {round(bt['score'],4)}; match: {bt['alignments'][0]['match']}; mismatch: {bt['alignments'][0]['mismatch']}; indel: {bt['alignments'][0]['indel']}; options:",options)
		else:
			print(f">score: {round(bt['score'],4)}; match: {bt['alignments'][0]['match']}; mismatch: {bt['alignments'][0]['mismatch']}; indel: {bt['alignments'][0]['indel']}; options:",options)

		for i in range(len(bt["alignments"])):
			print( ( (fastaheader[0]+"\n") if 0<len(fastaheader) and i>0 else "")+
					bt["alignments"][i][0]+"\n"+ 
					( (fastaheader[1]+"\n") if 1<len(fastaheader) else "")+
					bt["alignments"][i][1]+ 
					( ("\n"+" ".join( [ ",".join([ str(y) for y in x ]) for x in bt["paths"][i] ] )) if options.path else "" ) )
			if options.getAllEquivalentResult:
				print("")
			else:
				break

if __name__ == '__main__':
	main()
	exit(0)
